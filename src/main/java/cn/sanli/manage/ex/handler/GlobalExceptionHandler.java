package cn.sanli.manage.ex.handler;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.web.JsonResult;
import cn.sanli.manage.web.ServiceCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.text.ParseException;
import java.util.Objects;

/**
 * 全局异常处理器
 */
@Slf4j
@RestControllerAdvice
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ServiceException.class)
    public JsonResult<Void> handleServiceException(ServiceException e) {
        log.warn("程序运行过程中出现ServiceException自定义异常,异常信息：{}", e.getMessage());
        String message=e.getMessage();
        ServiceCode serviceCode = e.getServiceCode();
        return JsonResult.fail(serviceCode,message);
    }

    @ExceptionHandler({
            InternalAuthenticationServiceException.class,
            BadCredentialsException.class})
    public JsonResult<Void> handleBindException(AuthenticationException e) {
        log.warn("程序运行过程中出现BindException，,异常信息：{}", e.getMessage());
        String message = "用户名或密码输入错误,请重新操作";
        return JsonResult.fail(ServiceCode.ERROR_UNAUTHORIZED, message);
    }

//    @ExceptionHandler
//    public JsonResult<Void> handleBindException(NullPointerException e) {
//        log.warn("异常信息：{}", e.getMessage());
//        String message = "程序运行中出现空指针异常，请重新操作";
//        return JsonResult.fail(ServiceCode.ERROR_BAD_REQUEST, message);
//    }

    @ExceptionHandler
    public JsonResult<Void> ParseException(ParseException e) {
        log.warn("异常信息：{}", e.getMessage());
        String message = "时间格式输入异常,请重新操作";
        return JsonResult.fail(ServiceCode.ERROR_BAD_REQUEST, message);
    }

    @ExceptionHandler
    public JsonResult<Void> DisabledException(DisabledException e) {
        log.warn("异常信息：{}", e.getMessage());
        String message = "用户已失效，请重新操作";
        return JsonResult.fail(ServiceCode.ERROR_UNAUTHORIZED, message);
    }

    @ExceptionHandler
    public JsonResult<Void> LockedException(LockedException e) {
        log.warn("异常信息：{}", e.getMessage());
        String message = "该账号已被锁定，请重新操作";
        return JsonResult.fail(ServiceCode.ERROR_UNAUTHORIZED_DISABLED, message);
    }

    @ExceptionHandler
    public JsonResult<Void> handleAccessDeniedException(AccessDeniedException e) {
        log.warn("程序运行过程中出现AccessDeniedException，将统一处理！");
        log.warn("异常信息：{}", e.getMessage());
        String message = "拒绝访问，您当前登录的账号无此操作权限！";
        return JsonResult.fail(ServiceCode.ERROR_FORBIDDEN, message);
    }

    @ExceptionHandler
    public JsonResult<Void> handleThrowable(Throwable e) {
        log.warn("程序运行过程中出现Throwable，将统一处理！");
        log.warn("异常类型：{}", e.getClass());
        log.warn("异常信息：{}", e.getMessage());
        String message = "服务器忙，请稍后再次尝试！（开发过程中，如果看到此提示，请检查控制台的信息，并补充处理异常的方法）";
        e.printStackTrace(); // 打印异常的跟踪信息，主要是为了在开发阶段更好的检查出现异常的原因
        return JsonResult.fail(ServiceCode.ERROR_UNKNOWN, message);
    }

//    @ExceptionHandler
//    public JsonResult<Void> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
//        log.warn("程序运行过程中出现Throwable，将统一处理！");
//        log.warn("异常类型：{}", e.getClass());
//        log.warn("异常信息：{}", e.getMessage());
//        String message = e.getMessage();
//        e.printStackTrace(); // 打印异常的跟踪信息，主要是为了在开发阶段更好的检查出现异常的原因
//        return JsonResult.fail(ServiceCode.ERROR_UNKNOWN, message);
//    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public JsonResult<Void> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        BindingResult bindingResult = e.getBindingResult();
        // 日志将所有错误信息完全打印出来
        String msg = Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage();
        log.warn("程序运行过程中出现Throwable，将统一处理！");
        log.warn("异常类型：{}", e.getClass());
        log.warn("异常信息：{}", msg);
        e.printStackTrace(); // 打印异常的跟踪信息，主要是为了在开发阶段更好的检查出现异常的原因
        return JsonResult.fail(ServiceCode.ERROR_UNKNOWN, msg);
    }


}