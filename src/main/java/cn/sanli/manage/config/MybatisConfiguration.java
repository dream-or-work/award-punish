package cn.sanli.manage.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Mybatis配置类
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Configuration
@MapperScan("cn.sanli.manage.mapper.**.**")
public class MybatisConfiguration {
}