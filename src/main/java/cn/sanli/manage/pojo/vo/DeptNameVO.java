package cn.sanli.manage.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DeptNameVO {

    @ApiModelProperty("唯一id")
    public Integer id;

    @ApiModelProperty("部门名称")
    public String deptName;
}
