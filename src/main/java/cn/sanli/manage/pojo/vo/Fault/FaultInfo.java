package cn.sanli.manage.pojo.vo.Fault;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Author wzy
 * @Date 2023/12/18 11:26
 * @Description: 考核信息
 * @Version 1.0
 */
@Data
public class FaultInfo {

    @ApiModelProperty("责任人姓名")
    private String resName;

    @ApiModelProperty("责任人工号")
    private String resNum;

    @ApiModelProperty("考核时间")
    private Date faultTime;

    @ApiModelProperty("考核类型")
    private String faultType;

    @ApiModelProperty("考核级别")
    private String faultLevel;

    @ApiModelProperty("考核内容")
    private String faultContent;

    @ApiModelProperty("处理部门")
    private String disposalDept;

    @ApiModelProperty("处理人")
    private String disposalName;

    @ApiModelProperty("处理结果")
    private String disposalResults;


}
