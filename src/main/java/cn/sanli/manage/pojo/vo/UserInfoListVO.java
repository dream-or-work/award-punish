package cn.sanli.manage.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * ClassName:UserInfoListVO
 * Package:cn.sanli.manage.pojo.vo
 * Description:
 *
 * @Author Lu
 * @Create 2024/4/5 8:54
 * @Version 1.0
 */
@Data
public class UserInfoListVO {
    /**
     * ID
     */
    @ApiModelProperty(value="ID")
    private Integer id;

    /**
     * 用户名
     */
    @ApiModelProperty(value="用户名")
    private String username;

    /**
     * 大口Id集合
     */
    @ApiModelProperty(value="大口Id集合")
    private List<String> centerIdList;

    /**
     * 部门Id集合
     */
    @ApiModelProperty(value="部门Id集合")
    private List<String> deptIdList;
    /**
     * 角色id集合
     */
    @ApiModelProperty(value="用户角色Id列表")
    private List<String> roleIdList;


    @ApiModelProperty("大口名称")
    private List<String> centerNameList;

    @ApiModelProperty("部门名称")
    private List<String> deptNameList;

    @ApiModelProperty("权限名称")
    private List<String> roleNameList;


    @ApiModelProperty(value="大口Id列表")
    private String centerId;

    @ApiModelProperty(value="部门Id列表")
    private String deptId;

    @ApiModelProperty(value="用户角色Id列表")
    private String roleId;


    @ApiModelProperty("大口名称")
    private String centerName;

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty("权限名称")
    private String roleName;
    /**
     * 部门内线
     */
    @ApiModelProperty(value="部门内线")
    private String call;

    /**
     * 手机号
     */
    @ApiModelProperty(value="手机号")
    private String phone;

    /**
     * 微信号
     */
    @ApiModelProperty(value="微信号")
    private String wechat;

    /**
     * 工号
     */
    @ApiModelProperty(value="工号")
    private String num;

    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date creatTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private Date updateTime;

    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String remark;

    /**
     * 用户账号有无被锁定【1：正常使用，2：账号被锁定】
     */
    @ApiModelProperty(value="用户账号有无被锁定【1：正常使用，2：账号被锁定】")
    private Integer isLock;
}
