package cn.sanli.manage.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class TreeVO<T> {

    @ApiModelProperty(value = "三级分类树")
    private List<T> categories;
}
