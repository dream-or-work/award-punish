package cn.sanli.manage.pojo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ClassName:UserOrganizationVO
 * Package:cn.sanli.manage.pojo.vo
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/13 16:05
 * @Version 1.0
 */
@Data
public class UserOrganizationVO {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="id")
    private Integer id;

    /**
     * 工号
     */
    @TableField(value = "num")
    @ApiModelProperty(value="工号")
    private String num;

    /**
     * 对应大口（部门）id
     */
    /*@TableField(value = "organ_id")
    @ApiModelProperty(value="对应大口（部门）id")
    private Integer organId;*/
    @ApiModelProperty("大口名称")
    private String centerName;

    @ApiModelProperty("部门名称")
    private String deptName;
    /**
     * 是否为主显部门（0:代管部门（大口）,1:主显部门（大口））
     */
    @TableField(value = "lv")
    @ApiModelProperty(value="是否为主显部门（0:代管部门（大口）,1:主显部门（大口））")
    private Boolean lv;
    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value="备注")
    private String remark;
}
