package cn.sanli.manage.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RegionNameVO {

    @ApiModelProperty("大区id")
    public Integer region_id;

    @ApiModelProperty("大区名称")
    public String regionName;
}
