package cn.sanli.manage.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * ClassName:UserInfoVo
 * Package:cn.sanli.manage.pojo.vo
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/8 11:14
 * @Version 1.0
 */
@Data
public class UserInfoVO {
    /**
     * ID
     */
    @ApiModelProperty(value="ID")
    private Integer id;

    /**
     * 用户名
     */
    @ApiModelProperty(value="用户名")
    private String username;

    @ApiModelProperty("大口名称")
    private String centerName;

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty("权限名称")
    private String roleName;
    @ApiModelProperty("权限id")
    private String roleId;
    /**
     * 大口id（organization_id）
     */
    @ApiModelProperty(value="大口id（organization_id）")
    private Integer centerId;

    /**
     * 部门id（organization_id）
     */
    @ApiModelProperty(value="部门id（organization_id）")
    private Integer deptId;

    /**
     * 部门内线
     */
    @ApiModelProperty(value="部门内线")
    private String call;

    /**
     * 手机号
     */
    @ApiModelProperty(value="手机号")
    private String phone;

    /**
     * 微信号
     */
    @ApiModelProperty(value="微信号")
    private String wechat;

    /**
     * 工号
     */
    @ApiModelProperty(value="工号")
    private String num;

    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date creatTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private Date updateTime;

    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String remark;

    /**
     * 用户账号有无被锁定【1：正常使用，2：账号被锁定】
     */
    @ApiModelProperty(value="用户账号有无被锁定【1：正常使用，2：账号被锁定】")
    private Integer isLock;

}
