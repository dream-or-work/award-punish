package cn.sanli.manage.pojo.vo.Rewards;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author wzy
 * @Date 2023/12/19 16:09
 * @Description: 奖励统计
 * @Version 1.0
 */
@Data
public class RewardsStatisticsVO {

    @ApiModelProperty("被奖励人")
    private String name;

    @ApiModelProperty("被奖励人工号")
    private String num;

    @ApiModelProperty("被奖励部门")
    private String dept;

    @ApiModelProperty("被奖励大口")
    private String center;

    @ApiModelProperty("奖励时间")
    private Date rewardsTime;

    @ApiModelProperty("记分奖励")
    private BigDecimal bonusPoints;

    @ApiModelProperty("本周记分")
    private BigDecimal statisticsOfWeek;

    @ApiModelProperty("本月记分")
    private BigDecimal statisticsOfMonth;

    @ApiModelProperty("本季记分")
    private BigDecimal statisticsOfQuarter;

    @ApiModelProperty("半年记分")
    private BigDecimal statisticsOfHalf;

    @ApiModelProperty("本年记分")
    private BigDecimal statisticsOfYear;

}
