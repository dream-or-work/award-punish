package cn.sanli.manage.pojo.vo.Rewards;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

import java.util.Date;

/**
 * ClassName:RewardsCollectiveVO
 * Package:cn.sanli.manage.pojo.vo.Rewards
 * Description:
 *
 * @Author Lu
 * @Create 2024/4/11 11:21
 * @Version 1.0
 */
@ApiModel("集体奖励信息entity")
@Data
@ExcelIgnoreUnannotated
// 单元格字体样式
@ContentFontStyle(fontHeightInPoints = 9)
// 标题字体样式
@HeadFontStyle(fontHeightInPoints = 9)
@ContentRowHeight(value = -1)
@ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)
@HeadStyle(fillBackgroundColor = 70)
public class RewardsCollectiveVO {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private Integer id;
    /**
     * 奖励编号
     */
    @ApiModelProperty("奖励编号")
    @ExcelProperty(value = {"", "编号"}, index = 0)
    private String rid;

    /**
     * 被奖励集体
     */
    @ApiModelProperty("被奖励集体")
    @ExcelProperty(value = {"", "被奖励集体"}, index = 1)
    @ColumnWidth(value = 23)
    private String rewardsCollective;

    /**
     * 奖励类别
     */
    @ApiModelProperty("奖励类别")
    private String rewardsType;

    /**
     * 奖励类别
     */
    @ApiModelProperty("奖励类别")
    @ExcelProperty(value = {"", "奖励类别"}, index = 2)
    @ColumnWidth(value = 16)
    private String rewardsTypeText;

    /**
     * 奖励时间
     */
    @ApiModelProperty("奖励时间")
    @ExcelProperty(value = {"", "奖励时间"}, index = 3)
    @ColumnWidth(value = 16)
    private Date rewardsTime;

    /**
     * 奖励事由
     */
    @ApiModelProperty("奖励事由")
    @ExcelProperty(value = {"", "奖励事由"}, index = 4)
    @ColumnWidth(value = 23)
    private String reasons;

    /**
     * 奖励部门
     */
    @ApiModelProperty("奖励部门")
    private Integer rewardsDept;

    /**
     * 奖励部门
     */
    @ApiModelProperty("奖励部门")
    @ExcelProperty(value = {"", "奖励部门"}, index = 5)
    @ColumnWidth(value = 16)
    private String rewardsDeptText;
    /**
     * 奖励人
     */
    @ApiModelProperty("奖励人")
    @ExcelProperty(value = {"", "奖励人"}, index = 6)
    @ColumnWidth(value = 12)
    private String rewardsName;

    /**
     * 奖励信息
     */
    @ExcelProperty(value = {"", "奖励信息"}, index = 7)
    @ColumnWidth(value = 16)
    @ApiModelProperty("奖励信息")
    private String rewards;

    /**
     * 奖励资料编号
     */
    @ApiModelProperty("奖励资料编号")
    @ExcelProperty(value = {"", "奖励资料编号"}, index = 8)
    @ColumnWidth(value = 28)
    private String rewardsNum;

    /**
     * 现金奖励
     */
    @ApiModelProperty("现金奖励")
    @ExcelProperty(value = {"", "现金奖励"}, index = 9)
    @ColumnWidth(value = 16)
    private String rewardsCash;

    /**
     * 物品奖励
     */
    @ApiModelProperty("物品奖励")
    @ExcelProperty(value = {"", "物品奖励"}, index = 10)
    @ColumnWidth(value = 18)
    private String rewardsArticles;

    /**
     * 其他奖励
     */
    @ApiModelProperty("其他奖励")
    @ExcelProperty(value = {"", "其他奖励"}, index = 11)
    @ColumnWidth(value = 18)
    private String rewardsOthers;

    /**
     * 是否审核
     */
//    @ApiModelProperty("是否审核")
//    @ExcelProperty(value = {"", "是否审核"}, index = 12, converter = CheckConverter.class)
//    private Integer isCheck;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    @ExcelProperty(value = {"", "备注"}, index = 12)
    private String remark;
    /**
     * 添加人
     */
    @ApiModelProperty("添加人")
    @ExcelProperty(value = {"", "添加人"}, index = 13)
    @ColumnWidth(value = 12)
    private String createName;

    /**
     * 添加日期
     */
    @ApiModelProperty("添加日期")
    @JsonFormat(pattern = "YYYY-MM-dd")
    @ExcelProperty(value = {"", "添加日期"}, index = 14)
    @ColumnWidth(value = 12)
    @DateTimeFormat(value = "yyyy-MM-dd")
    private Date createTime;
}
