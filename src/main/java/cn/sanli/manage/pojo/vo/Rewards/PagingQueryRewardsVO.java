package cn.sanli.manage.pojo.vo.Rewards;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 奖励信息表(Rewards)实体类
 *
 * @author wzy
 * @since 2023-12-07 16:06:45
 */

@Data
@ApiModel("奖励信息entity")
@ExcelIgnoreUnannotated
// 单元格字体样式
@ContentFontStyle(fontHeightInPoints = 9)
// 标题字体样式
@HeadFontStyle(fontHeightInPoints = 9)
@ContentRowHeight(value = -1)
@ContentStyle(horizontalAlignment = HorizontalAlignment.CENTER)
@HeadStyle(fillBackgroundColor = 70)
public class PagingQueryRewardsVO implements Serializable {
    private static final long serialVersionUID = -12207362842778498L;
    /**
     * ID
     */
    @ApiModelProperty("ID")
    private Integer id;
    /**
     * 奖励编号
     */
    @ApiModelProperty("奖励编号")
    @ExcelProperty(value = {"", "编号"}, index = 0)
    private String rid;
    /**
     * 所属大口
     */
    @ApiModelProperty("所属大口")
    @ExcelProperty(value = {"", "所属大口"}, index = 1)
    @ColumnWidth(value = 16)
    private String center;



    private String centerOfNum;
    /**
     * 所属部门
     */
    @ApiModelProperty("所属部门")
    @ExcelProperty(value = {"", "所属部门"}, index = 2)
    @ColumnWidth(value = 16)
    private String dept;


    private String deptOfNum;
    /**
     * 被奖励人
     */
    @ApiModelProperty("被奖励人")
    @ExcelProperty(value = {"", "被奖励人"}, index = 4)
    @ColumnWidth(value = 10)
    private String name;
    /**
     * 工号
     */
    @ApiModelProperty("工号")
    @ExcelProperty(value = {"", "工号"}, index = 3)
    @ColumnWidth(value = 9)
    private String number;
    /**
     * 奖励类别
     */
    @ApiModelProperty("奖励类别")
    private String rewardsType;
    /**
     * 奖励类别文本
     */
    @ApiModelProperty("奖励类别")
    @ExcelProperty(value = {"", "奖励类别"}, index = 8)
    private String rewardsTypeText;
    /**
     * 奖励时间
     */
    @ApiModelProperty("奖励时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(value = {"", "奖励时间"}, index = 9)
    @ColumnWidth(value = 12)
    @DateTimeFormat(value = "yyyy-MM-dd")
    private Date rewardsTime;
    /**
     * 奖励事由
     */
    @ApiModelProperty("奖励事由")
    @ExcelProperty(value = {"", "奖励事由"}, index = 5)
    @ColumnWidth(value = 23)
    private String reasons;
    /**
     * 奖励部门
     */
    @ApiModelProperty("奖励部门")
    @ExcelProperty(value = {"", "奖励部门"}, index = 6)
    private String rewardsDept;


    private String rewardsDeptOfNum;
    /**
     * 奖励人
     */
    @ApiModelProperty("奖励人")
    @ExcelProperty(value = {"", "奖励人"}, index = 7)
    private String rewardsName;
    /**
     * 添加人
     */
    @ApiModelProperty("添加人")
    @ExcelProperty(value = {"", "添加人"}, index = 16)
    @ColumnWidth(value = 12)
    private String createName;
    /**
     * 添加日期
     */
    @ApiModelProperty("添加日期")
    @JsonFormat(pattern = "YYYY-MM-dd")
    @ExcelProperty(value = {"", "添加日期"}, index = 17)
    @ColumnWidth(value = 12)
    @DateTimeFormat(value = "yyyy-MM-dd")
    private Date createTime;

    /**
     * 修改人(最后操作人员)
     */
    @ApiModelProperty("修改人(最后操作人员)")
    private String updateName;

    /**
     * 添加日期
     */
    @ApiModelProperty("修改时间")
    @DateTimeFormat(value = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 奖励资料编号
     */
    @ExcelProperty(value = {"", "奖励资料编号"}, index = 14)
    @ApiModelProperty("奖励资料编号")
    @ColumnWidth(value = 28)
    private String rewardsNum;
    /**
     * 现金奖励
     */
    @ExcelProperty(value = {"", "现金奖励"}, index = 10)
    @ApiModelProperty("现金奖励")
    @ColumnWidth(value = 12)
    private String rewardsCash;
    /**
     * 现金奖励
     */
    @ExcelProperty(value = {"", "物品奖励"}, index = 11)
    @ApiModelProperty("物品奖励")
    @ColumnWidth(value = 18)
    private String rewardsArticles;
    /**
     * 现金奖励
     */
    @ExcelProperty(value = {"", "其他奖励"}, index = 12)
    @ApiModelProperty("其他奖励")
    @ColumnWidth(value = 14)
    private String othersRewards;
    /**
     * 积分奖励
     */
    @ApiModelProperty("积分奖励")
    @ExcelProperty(value = {"", "积分奖励"}, index = 13)
    private BigDecimal bonusPoints;
//    /**
//     * 奖励信息
//     */
//    @ApiModelProperty("奖励信息")
//    @ExcelProperty(value = {"", "奖励信息"}, index = 10)
//    @ColumnWidth(value = 13)
//    private String rewards;
    /**
     * 是否审核
     */
    @ApiModelProperty("是否审核")
    @ExcelProperty(value = {"", "是否审核"}, index = 18, converter = CheckConverter.class)
    private String isCheck;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    @ExcelProperty(value = {"", "备注"}, index = 15)
    private String remark;



}

