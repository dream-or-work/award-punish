package cn.sanli.manage.pojo.vo.FaultAssess;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author wzy
 * @Date 2024/3/22 0:57
 * @Description: 考核处理信息相关
 * @Version 1.0
 */
@Data
public class FaultAssessInfoVO {

    /**
     * 过错id
     */
    @ApiModelProperty("过错id")
    private String faultId;

    /**
     * 处理方式（1026.保证金，1027：损失费，1028.荣誉记分，1029.月考核分，1030：年考核分）
     */
    @ApiModelProperty("处理方式（1026.保证金，1027：损失费，1028.荣誉记分，1029.月考核分，1030：年考核分）")
    @ExcelProperty(value = {"", "处理方式"}, index = 16)
    private String faultAssessDisposalForm;

    /**
     * 处理方式id（1026.保证金，1027：损失费，1028.荣誉记分，1029.月考核分，1030：年考核分）
     */
    @ApiModelProperty("处理方式id（1026.保证金，1027：损失费，1028.荣誉记分，1029.月考核分，1030：年考核分）")
    private Integer faultAssessDisposalFormId;

    /**
     * 申诉处理时间
     */
    @ApiModelProperty("考核内容")
    private BigDecimal disposalDetails;

    /**
     * 申诉处理时间
     */
    @ApiModelProperty("考核内容编号")
    private String assessNum;

    /**
     * 申诉处理时间
     */
    @ApiModelProperty("考核结果")
    private String assessResults;

    /**
     * 申诉处理时间
     */
    @ApiModelProperty("补充")
    private String faultAssessRemark;


}
