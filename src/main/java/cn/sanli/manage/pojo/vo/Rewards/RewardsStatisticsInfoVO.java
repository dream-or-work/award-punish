package cn.sanli.manage.pojo.vo.Rewards;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author wzy
 * @Date 2023/12/19 16:09
 * @Description: 奖励统计信息封装类
 * @Version 1.0
 */
@Data
public class RewardsStatisticsInfoVO {

    @ApiModelProperty("被奖励人")
    @ExcelProperty(value = "被奖励人", index = 0)
    private String name;

    @ApiModelProperty("被奖励人工号")
    @ExcelProperty(value = "被奖励人工号", index = 1)
    private String num;

    @ApiModelProperty("被奖励部门")
    @ExcelProperty(value = "被奖励部门", index = 2)
    private String dept;

    @ApiModelProperty("被奖励大口")
    @ExcelProperty(value = "被奖励大口", index = 3)
    private String center;

    @ApiModelProperty("本周记分")
    @ExcelProperty(value = "本周记分", index = 4)
    private BigDecimal statisticsOfWeek;

    @ApiModelProperty("本月记分")
    @ExcelProperty(value = "本月记分", index = 5)
    private BigDecimal statisticsOfMonth;

    @ApiModelProperty("本季记分")
    @ExcelProperty(value = "本季记分", index = 6)
    private BigDecimal statisticsOfQuarter;

    @ApiModelProperty("半年记分")
    @ExcelProperty(value = "半年记分", index = 7)
    private BigDecimal statisticsOfHalf;

    @ApiModelProperty("本年记分")
    @ExcelProperty(value = "本年记分", index = 8)
    private BigDecimal statisticsOfYear;

}
