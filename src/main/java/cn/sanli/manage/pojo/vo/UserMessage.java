package cn.sanli.manage.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserMessage {

    @ApiModelProperty("用户名称")
    public String userName;

    @ApiModelProperty("所属大区id")
    public int center_id;

    @ApiModelProperty("所属部门id")
    public int dept_id;

    @ApiModelProperty("部门内线")
    public String call;

    @ApiModelProperty("个人电话")
    public String phone;

    @ApiModelProperty("微信号")
    public String weChat;

    @ApiModelProperty("工号")
    public String num;

    @ApiModelProperty("积分提醒标准")
    public int standard_id;

    @ApiModelProperty("备注")
    public String remark;

    @ApiModelProperty("所在大区名称")
    public String centerName;

    @ApiModelProperty("所在部门名称")
    public String deptName;

    @ApiModelProperty("账号权限名称")
    public String roleName;

    @ApiModelProperty("账号权限【数值】")
    public int roleId;
}
