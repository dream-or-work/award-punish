package cn.sanli.manage.pojo.vo.Fault;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author wzy
 * @Date 2023/12/18 10:28
 * @Description: 计分标准封装类
 * @Version 1.0
 */
@Data
@ApiModel("计分标准封装类")
public class FaultStandardVO {

    @ApiModelProperty("wechat码")
    private String weChat;

    @ApiModelProperty("周标准")
    private BigDecimal standardWeek;

    @ApiModelProperty("月标准")
    private BigDecimal standardMonth;

    @ApiModelProperty("季标准")
    private BigDecimal standardQuarter;

    @ApiModelProperty("半年标准")
    private BigDecimal standardHalf;

    @ApiModelProperty("年标准")
    private BigDecimal standardYear;



}
