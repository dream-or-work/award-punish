package cn.sanli.manage.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ceshi1 {
    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 父id
     */
    @ApiModelProperty(value = "父id")
    private Integer pid;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;
}
