package cn.sanli.manage.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 此类用于登录成功后响应给前端的内容
 */
@Data
public class LoginUserVO {

    @ApiModelProperty("登录工号")
    public String num;

    @ApiModelProperty("响应给前端的jwt")
    public String token;

    @ApiModelProperty("账号主显大区/部门")
    public String organName;

    @ApiModelProperty("账号主显权限")
    public String roleName;

    @ApiModelProperty("账号主显权限【数值】")
    public int roleId;

    @ApiModelProperty("账号状态【1：状态正常 2：逻辑删除】")
    public Integer status;

    @ApiModelProperty("账号是否被锁定【1：账号正常 2：账号被锁定不可用】")
    public Integer isLock;
}
