package cn.sanli.manage.pojo.vo.Fault;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author wzy
 * @Date 2023/12/13 16:45
 * @Description: 考核统计信息
 * @Version 1.0
 */
@Data
@ApiModel("考核统计信息")
public class FaultStatisticsInfoOfCenterVO implements Serializable {

    @ApiModelProperty("责任大口")
    @ExcelProperty(value = "责任大口", index = 0)
    private String resCenter;

    @ApiModelProperty("本周记分")
    @ExcelProperty(value = "本周记分", index = 1)
    private BigDecimal statisticsOfWeek;

    @ApiModelProperty("本月记分")
    @ExcelProperty(value = "本月记分", index = 2)
    private BigDecimal statisticsOfMonth;

    @ApiModelProperty("本季记分")
    @ExcelProperty(value = "本季记分", index = 3)
    private BigDecimal statisticsOfQuarter;

    @ApiModelProperty("半年记分")
    @ExcelProperty(value = "半年记分", index = 4)
    private BigDecimal statisticsOfHalf;

    @ApiModelProperty("本年记分")
    @ExcelProperty(value = "本年记分", index = 5)
    private BigDecimal statisticsOfYear;


}
