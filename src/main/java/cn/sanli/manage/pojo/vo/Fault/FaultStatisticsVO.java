package cn.sanli.manage.pojo.vo.Fault;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author wzy
 * @Date 2023/12/13 16:45
 * @Description: 考核统计信息
 * @Version 1.0
 */
@Data
@ApiModel("考核统计信息")
public class FaultStatisticsVO implements Serializable {

    @ApiModelProperty("责任人")
    private String resName;

    @ApiModelProperty("责任工号")
    private String resNum;

    @ApiModelProperty("责任部门")
    private String resDept;

    @ApiModelProperty("责任大口")
    private String resCenter;

    @ApiModelProperty("过错时间")
    private Date faultTime;

    @ApiModelProperty("考核内容")
    private BigDecimal disposalDetails;

    @ApiModelProperty("本周记分")
    private BigDecimal statisticsOfWeek;

    @ApiModelProperty("本月记分")
    private BigDecimal statisticsOfMonth;

    @ApiModelProperty("本季记分")
    private BigDecimal statisticsOfQuarter;

    @ApiModelProperty("半年记分")
    private BigDecimal statisticsOfHalf;

    @ApiModelProperty("本年记分")
    private BigDecimal statisticsOfYear;


}
