package cn.sanli.manage.pojo.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 集体奖励表
 * @TableName rewards_collective
 */

@ApiModel("集体奖励信息entity")
@TableName(value ="rewards_collective")
@Data
public class RewardsCollective implements Serializable {
    /**
     * ID
     */
    @ApiModelProperty("ID")
    private Integer id;
    /**
     * 奖励编号
     */
    @ApiModelProperty("奖励编号")
    private String rid;

    /**
     * 被奖励集体
     */
    @ApiModelProperty("被奖励集体")
    private String rewardsCollective;

    /**
     * 奖励类别
     */
    @ApiModelProperty("奖励类别")
    private String rewardsType;

    /**
     * 奖励时间
     */
    @ApiModelProperty("奖励时间")
    private Date rewardsTime;

    /**
     * 奖励事由
     */
    @ApiModelProperty("奖励事由")
    private String reasons;

    /**
     * 奖励部门
     */
    @ApiModelProperty("奖励部门")
    private Integer rewardsDept;

    /**
     * 奖励人
     */
    @ApiModelProperty("奖励人")
    private String rewardsName;

    /**
     * 添加人
     */
    @ApiModelProperty("添加人")
    private String createName;

    /**
     * 添加日期
     */
    @ApiModelProperty("添加日期")
    private Date createTime;

    /**
     * 奖励信息
     */
    @ApiModelProperty("奖励信息")
    private String rewards;

    /**
     * 奖励资料编号
     */
    @ApiModelProperty("奖励资料编号")
    private String rewardsNum;

    /**
     * 现金奖励
     */
    @ApiModelProperty("现金奖励")
    private String rewardsCash;

    /**
     * 物品奖励
     */
    @ApiModelProperty("物品奖励")
    private String rewardsArticles;

    /**
     * 其他奖励
     */
    @ApiModelProperty("其他奖励")
    private String rewardsOthers;

    /**
     * 是否审核
     */
    private Integer isCheck;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}