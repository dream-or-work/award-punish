package cn.sanli.manage.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**

* @program: award-punish
*
*
*
* @description:
*
* @author: lsk
*
* @version 1.0.0
*
* @since jdk1.8
*
* @create: 2023-12-04 15:52
**/
    /**
    * 用户信息表
    */
@ApiModel(description="用户信息表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "dbo.userInfo")
public class UserInfo {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="ID")
    private Integer id;

    /**
     * 用户名
     */
    @TableField(value = "userName")
    @ApiModelProperty(value="用户名")
    private String username;

    /**
     * 密码
     */
    @TableField(value = "[passWord]")
    @ApiModelProperty(value="密码")
    private String password;

    /**
     * 大口id（organization_id）
     */
    @TableField(value = "center_id")
    @ApiModelProperty(value="大口id（organization_id）")
    private Integer centerId;

    /**
     * 部门id（organization_id）
     */
    @TableField(value = "dept_id")
    @ApiModelProperty(value="部门id（organization_id）")
    private Integer deptId;

    /**center
     * 部门内线
     */
    @TableField(value = "[call]")
    @ApiModelProperty(value="部门内线")
    private String call;

    /**
     * 手机号
     */
    @TableField(value = "phone")
    @ApiModelProperty(value="手机号")
    private String phone;

    /**
     * 微信号
     */
    @TableField(value = "weChat")
    @ApiModelProperty(value="微信号")
    private String wechat;

    /**
     * 工号
     */
    @TableField(value = "num")
    @ApiModelProperty(value="工号")
    private String num;

    /**
     * 创建时间
     */
    @TableField(value = "creat_time")
    @ApiModelProperty(value="创建时间")
    private Date creatTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value="更新时间")
    private Date updateTime;

    /**
     * 记分提醒标准
     */
    @TableField(value = "standard_id")
    @ApiModelProperty(value="记分提醒标准")
    private Integer standardId;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value="备注")
    private String remark;

    /**
     * 用户状态【1：状态正常；2：逻辑删除】
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value="用户状态【1：状态正常；2：逻辑删除】")
    private Integer isDelete;

    /**
     * 用户账号有无被锁定【1：正常使用，2：账号被锁定】
     */
    @TableField(value = "is_lock")
    @ApiModelProperty(value="用户账号有无被锁定【1：正常使用，2：账号被锁定】")
    private Integer isLock;
}