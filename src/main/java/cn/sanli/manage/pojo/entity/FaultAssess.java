package cn.sanli.manage.pojo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 过错关联表(FaultAssess)实体类
 *
 * @author wzy
 * @since 2023-12-07 16:42:54
 */
@Data
@ApiModel("过错关联表(FaultAssess)实体类")
public class FaultAssess implements Serializable {
    private static final long serialVersionUID = 925312675376799085L;
    /**
     * id
     */
    @ApiModelProperty("id")
    private Integer id;
    /**
     * 过错id
     */
    @ApiModelProperty("过错id")
    private String faultId;
    /**
     * 处理方式（1.保证金，2：损失费，3荣誉记分，4月考核分，5：年考核分）
     */
    @ApiModelProperty("处理方式（1026.保证金，1027：损失费，1028.荣誉记分，1029.月考核分，1030：年考核分）")
    private Integer disposalForm;
    /**
     * 考核内容
     */
    @ApiModelProperty("考核内容")
    private BigDecimal disposalDetails;
    /**
     * 考核内容编号
     */
    @ApiModelProperty("考核内容编号")
    private String assessNum;
    /**
     * 考核结果
     */
    @ApiModelProperty("考核结果")
    private String assessResults;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private Date creatTime;
    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    private Date updateTime;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;




}

