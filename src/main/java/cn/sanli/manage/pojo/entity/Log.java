package cn.sanli.manage.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**

* @program: award-punish
* @description:
* @author: lsk
* @version 1.0.0
* @since jdk1.8
*
* @create: 2023-12-04 15:52
**/
/**
    * 日志信息表
    */
@ApiModel(description="日志信息表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "dbo.log")
@Accessors(chain = true)
public class Log implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * log_id,唯一标识
     */
    @ApiModelProperty(value = "唯一标识")
    @TableId(value = "log_id", type = IdType.AUTO)
    private Long logId;

    /**
     * 操作人id
     */
    @ApiModelProperty(value = "操作人id")
    private String operatorId;

    /**
     * 参数信息
     */
    @ApiModelProperty(value = "参数信息")
    private String params;

    /**
     * 日志内容(具体操作内容)
     */
    @ApiModelProperty(value = "日志内容(具体操作内容)")
    private String logMessage;

    /**
     * 操作人名称(职务)
     */
    @ApiModelProperty(value = "操作人名称(职务)")
    private String operatorInfo;

    /**
     * 操作时间
     */
    @ApiModelProperty(value = "操作时间")
    private LocalDateTime updateTime;

    /**
     * 操作状态(0:成功, 1:失败)
     */
    @ApiModelProperty(value = "操作状态(0:成功, 1:失败)")
    private Integer status;

    /**
     * 运行消耗时间
     */
    @ApiModelProperty(value = "运行消耗时间")
    private Date runTime;

}