package cn.sanli.manage.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**

* @program: award-punish
*
*
*
* @description:
*
* @author: lsk
*
* @version 1.0.0
*
* @since jdk1.8
*
* @create: 2023-12-04 15:52
**/
/**
    * 职务(权限)信息表
    */
@ApiModel(description="职务(权限)信息表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "dbo.[role]")
public class Role {
    /**
     * 唯一标识
     */
    @TableId(value = "role_id", type = IdType.INPUT)
    @ApiModelProperty(value="唯一标识")
    private Integer roleId;

    /**
     * 职务名称
     */
    @TableField(value = "role_name")
    @ApiModelProperty(value="职务名称")
    private String roleName;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value="创建时间")
    private Date createTime;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value="备注")
    private String remark;

    /**
     * 权限值
     */
    @TableField(value = "role_key")
    @ApiModelProperty(value="权限值")
    private String roleKey;

    /**
     * 对应链路的id
     */
    @TableField(value = "dynamic_id")
    @ApiModelProperty(value="对应链路的id")
    private Integer dynamicId;
}