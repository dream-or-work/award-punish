package cn.sanli.manage.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**

* @program: award-punish
*
*
*
* @description:
*
* @author: lsk
*
* @version 1.0.0
*
* @since jdk1.8
*
* @create: 2023-12-04 15:52
**/
/**
    * 用户组织关联表
    */
@ApiModel(description="用户组织关联表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "dbo.userRole")
public class UserRole {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="id")
    private Integer id;

    /**
     * 工号
     */
    @TableField(value = "num")
    @ApiModelProperty(value="工号")
    private String num;

    /**
     * 对应大口（部门）id
     */
    @TableField(value = "role_id")
    @ApiModelProperty(value="对应大口（部门）id")
    private Integer roleId;

    /**
     * 是否为主显ID（0:其他权限,1:主权限）
     */
    @TableField(value = "lv")
    @ApiModelProperty(value="是否为主显ID（0:其他权限,1:主权限）")
    private Integer lv;

    /**
     * 创建时间
     */
    @TableField(value = "creat_time")
    @ApiModelProperty(value="创建时间")
    private Date creatTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value="修改时间")
    private Date updateTime;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value="备注")
    private String remark;
}