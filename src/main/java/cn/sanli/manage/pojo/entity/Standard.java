package cn.sanli.manage.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**

* @program: award-punish
*
*
*
* @description:
*
* @author: lsk
*
* @version 1.0.0
*
* @since jdk1.8
*
* @create: 2023-12-04 15:52
**/
/**
    * 记分提醒标准表
    */
@ApiModel(description="记分提醒标准表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "dbo.[standard]")
public class Standard {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="id")
    private Integer id;

    /**
     * 周标准
     */
    @TableField(value = "week")
    @ApiModelProperty(value="周标准")
    private Integer week;

    /**
     * 月标准
     */
    @TableField(value = "[month]")
    @ApiModelProperty(value="月标准")
    private Integer month;

    /**
     * 季度标准
     */
    @TableField(value = "quarter")
    @ApiModelProperty(value="季度标准")
    private Integer quarter;

    /**
     * 半年标准
     */
    @TableField(value = "[year]")
    @ApiModelProperty(value="半年标准")
    private Integer half;
    /**
     * 年标准
     */
    @TableField(value = "[year]")
    @ApiModelProperty(value="年标准")
    private Integer year;

    /**
     * 是否启用(1:启用,0:关闭)
     */
    @TableField(value = "is_enable")
    @ApiModelProperty(value="是否启用(1:启用,0:关闭)")
    private Boolean isEnable;
}