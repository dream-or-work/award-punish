package cn.sanli.manage.pojo.entity;

import lombok.Data;

/**
 * @Author wzy
 * @Date 2023/12/25 11:55
 * @Description:
 * @Version 1.0
 */
@Data
public class IdAndName {


    private Integer id;

    private String name;



}
