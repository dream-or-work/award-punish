package cn.sanli.manage.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**

* @program: award-punish
*
*
*
* @description:
*
* @author: lsk
*
* @version 1.0.0
*
* @since jdk1.8
*
* @create: 2023-12-04 15:52
**/

/**
 * 奖惩组织机构
 */
@ApiModel(description = "奖惩组织机构")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "dbo.organization")
public class Organization {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 名称
     */
    @TableField(value = "[name]")
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 父id
     */
    @TableField(value = "parent_id")
    @ApiModelProperty(value = "父id")
    private Integer parentId;

    /**
     * 创建时间
     */
    @TableField(value = "creat_time")
    @ApiModelProperty(value = "创建时间")
    private Date creatTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 状态【1：状态正常；2：逻辑删除】
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value="状态【1：状态正常；2：逻辑删除】")
    private Integer isDelete;

    @ApiModelProperty(value="子节点")
    @TableField(value="children", exist = false)
    private List<Organization> children;
}