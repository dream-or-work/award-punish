package cn.sanli.manage.pojo.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class UserRoles {

    @ApiModelProperty("登录工号")
    public String num;

    @ApiModelProperty("非主显权限id")
    public Integer role_id;

    @ApiModelProperty("非主显id")
    public Integer main_role;

    @ApiModelProperty("数据创建时间")
    public Date creat_time;

    @ApiModelProperty("数据修改时间")
    public Date update_time;

    @ApiModelProperty("备注")
    public String remark;
}
