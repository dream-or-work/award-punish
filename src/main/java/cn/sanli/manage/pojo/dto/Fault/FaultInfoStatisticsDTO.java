package cn.sanli.manage.pojo.dto.Fault;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Author wzy
 * @Date 2023/12/13 16:18
 * @Description: 考核信息统计参数封装类
 * @Version 1.0
 */
@Data
@ApiModel("考核信息统计参数封装类")
public class FaultInfoStatisticsDTO {

    @ApiModelProperty("责任大口")
    private List<Integer> resCenter;

    @ApiModelProperty("责任部门")
    private List<Integer> resDept;

    @ApiModelProperty("责任人")
    private String resName;

    @ApiModelProperty("责任人工号")
    private Integer resNum;

    @ApiModelProperty("处理部门")
    private String disposalDept;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    @ApiModelProperty("开始时间")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    @ApiModelProperty("结束时间")
    private Date endTime;

    @ApiModelProperty("是否只显示超标人员(1代表是, 0代表否)")
    private Integer onlyExceedStandard;





}
