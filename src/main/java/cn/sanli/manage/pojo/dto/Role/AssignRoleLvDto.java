package cn.sanli.manage.pojo.dto.Role;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ClassName:assignRoleLvDto
 * Package:cn.sanli.manage.pojo.dto
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/7 17:14
 * @Version 1.0
 */
@Data
public class AssignRoleLvDto {

    @ApiModelProperty("用户id")
    private Integer userId;

    @ApiModelProperty("角色id")
    private Integer roleId;
}
