package cn.sanli.manage.pojo.dto.System;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * ClassName:CategoryDTO
 * Package:cn.sanli.manage.pojo.dto.System
 * Description:
 *
 * @Author Lu
 * @Create 2024/3/21 15:04
 * @Version 1.0
 */

@Data
public class CategoryDTO {

    /**
     * 名称
     */
    @NotBlank
    @TableField(value = "[name]")
    @ApiModelProperty(value="名称")
    private String name;

    @ApiModelProperty(value="处理类型（金额0/分数1）")
    private String value;
}
