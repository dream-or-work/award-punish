package cn.sanli.manage.pojo.dto.Fault;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author wzy
 * @Date 2023/12/14 17:08
 * @Description: 修改考核信息请求参数
 * @Version 1.0
 */
@Data

//@JsonIgnoreProperties(value = {"faultNum"})
public class FaultUpdateRequest {

//    /**
//     * 过错id
//     */
//    @ApiModelProperty("id")
//    private Integer id;

    @ApiModelProperty("faultNum")
    private String faultNum;

    /**
     * 责任大口
     */
    @ApiModelProperty("责任大口")
    @NotNull(message = "责任大口不能为空")
    private Integer resCenter;

    /**
     * 责任部门
     */
    @ApiModelProperty("责任部门")
    @NotNull(message = "责任部门不能为空")
    private Integer resDept;

    /**
     * 责任人工号
     */
    @ApiModelProperty("责任人工号")
    @NotNull(message = "责任人工号不能为空")
    private Integer resNum;

    /**
     * 责任人
     */
    @ApiModelProperty("责任人")
    @NotBlank(message = "责任人不能为空")
    private String resName;

    /**
     * 过错类别(1.信誉,2.质量,3档次.,4形象.,5.效率,6.安全,7.素质,8.纪律)
     */
    @ApiModelProperty("过错类别(9.信誉,10.质量,11.档次.,12.形象.,13.安全,14.纪律,15.素质)")
    @NotNull(message = "过错类别不能为空")
    private Integer faultType;

    /**
     * 过错时间
     */
    @ApiModelProperty("过错时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "过错时间不能为空")
    private Date faultTime;

    /**
     * 过错事实
     */
    @ApiModelProperty("过错事实")
    @NotBlank(message = "过错事实不能为空")
    private String faultContent;

    /**
     * 过错级别(1.警告,2.轻微,3一般.,4较重.,5.严重,6.非常严重,7.恶劣)
     */
    @ApiModelProperty("过错级别(17.警告,18.轻微,19.一般.,20.较重.,21.严重,22.非常严重,23.恶劣)")
    @NotNull(message = "过错级别不能为空")
    private Integer faultLevel;

    /**
     * 处理部门
     */
    @ApiModelProperty("处理部门")
    @NotBlank(message = "处理部门不能为空")
    private String disposalDept;

    /**
     * 处理人
     */
    @ApiModelProperty("处理人")
    @NotBlank(message = "处理人不能为空")
    private String disposalName;

    /**
     * 处理时间
     */
    @ApiModelProperty("处理时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "处理时间不能为空")
    private Date disposalTime;

    /**
     * 处理形式(1.通报,2.警告单,3.责任追究单,4问题整改告知书.,5.其他形式)
     */
    @ApiModelProperty("处理形式(25.通报,26.警告单,27.责任追究单,28.问题整改告知书.,29.其他形式)")
    @NotNull(message = "处理形式不能为空")
    private Integer faultDisposalForm;

    /**
     * 其他结果(1.待岗,2.开除,3.开除留用查看,4降薪.,5.降职,6.记过,7.其他形式)
     */
    @ApiModelProperty("其他结果(31.待岗,32.开除,33.开除留用查看,34.降薪.,35.降职,36.记过,37.其他形式)")
    private Integer otherResults;

    /**
     * 其他荣誉处理(1.检查,2.整改,3.现场会,4申请工资扣除.,5.批评,6.曝光,7.体力劳动,8.警告,9.往来账.,10.预奖励抵顶)
     */
    @ApiModelProperty("其他荣誉处理(39.检查,40.整改,41.现场会,42.申请工资扣除.,43.批评,44.曝光,45.体力劳动,46.警告,47.往来账.,48.预奖励抵顶)")
    private Integer otherDisposal;


    /**
     * 处理资料编号
     */
    @ApiModelProperty("处理资料编号")
    @NotBlank(message = "处理资料编号不能为空")
    private String disposalNum;

    /**
     * 执行完毕时间
     */
    @ApiModelProperty("执行完毕时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;


    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("处理层次（2:公司型考核, 3:大口性考核, 4:部门性考核）")
    @NotNull(message = "处理层次不能为空")
    private Integer disposalLevel;



//    /**
//     * 处理方式（1.保证金，2：损失费，3荣誉记分，4月考核分，5：年考核分）
//     */
//    @ApiModelProperty("处理方式（1026.保证金，1027：损失费，1028.荣誉记分，1029.月考核分，1030：年考核分）")
//    private Integer faultAssessDisposalForm;
//
//    /**
//     * 考核内容
//     */
//    @ApiModelProperty("考核内容")
//    private BigDecimal disposalDetails;
//
//    /**
//     * 考核内容编号
//     */
//    @ApiModelProperty("考核内容编号")
//    private String assessNum;

    @ApiModelProperty("考核信息数组")
    private FaultAssessInfoDTO[] faultAssessInfos;

    /**
     * 补充
     */
    @ApiModelProperty("补充")
    private String remark;

}
