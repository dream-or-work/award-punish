package cn.sanli.manage.pojo.dto.System;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * ClassName:CorresDTO
 * Package:cn.sanli.manage.pojo.dto.System
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/12 11:59
 * @Version 1.0
 */
@Data
public class CorresDTO {
    /**
     * 名称
     */
    @NotBlank
    @TableField(value = "[name]")
    @ApiModelProperty(value="名称")
    private String name;

    /**
     * 父id
     */
    @NotNull
    @Min(0)
    @TableField(value = "parent_id")
    @ApiModelProperty(value="父id")
    private Integer parentId;

    @ApiModelProperty(value="分值标准（仅添加计分标准时用）")
    private String value;
}
