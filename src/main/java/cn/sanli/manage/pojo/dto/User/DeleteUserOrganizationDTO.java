package cn.sanli.manage.pojo.dto.User;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ClassName:DeleteUserOrganizationDTO
 * Package:cn.sanli.manage.pojo.dto.User
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/16 10:21
 * @Version 1.0
 */
@Data
public class DeleteUserOrganizationDTO {
    /**
     * 工号
     */
    @TableField(value = "num")
    @ApiModelProperty(value="工号")
    private String num;

    /**
     * 对应大口（部门）id
     */
    @TableField(value = "organ_id")
    @ApiModelProperty(value="对应大口（部门）id")
    private Integer organId;

}
