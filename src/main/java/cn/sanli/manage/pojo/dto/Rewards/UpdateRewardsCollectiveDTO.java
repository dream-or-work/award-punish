package cn.sanli.manage.pojo.dto.Rewards;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * ClassName:UpdateRewardsCollectiveDTO
 * Package:cn.sanli.manage.pojo.dto.Rewards
 * Description:
 *
 * @Author Lu
 * @Create 2024/4/12 14:17
 * @Version 1.0
 */
@ApiModel("修改集体奖励信息entity")
@Data
public class UpdateRewardsCollectiveDTO {

    /**
     * ID
     */
    @ApiModelProperty("id")
    private Integer id;
    /**
     * 被奖励集体
     */
    @ApiModelProperty("被奖励集体")
    private String rewardsCollective;

    /**
     * 奖励类别
     */
    @ApiModelProperty("奖励类别")
    private String rewardsType;
    /**
     * 奖励时间
     */
    @ApiModelProperty("奖励时间")
    private Date rewardsTime;

    /**
     * 奖励事由
     */
    @ApiModelProperty("奖励事由")
    private String reasons;

    /**
     * 奖励人
     */
    @ApiModelProperty("奖励人")
    private String rewardsName;

    /**
     * 奖励信息
     */
    @ApiModelProperty("奖励信息")
    private String rewards;

    /**
     * 奖励资料编号
     */
    @ApiModelProperty("奖励资料编号")
    private String rewardsNum;

    /**
     * 现金奖励
     */
    @ApiModelProperty("现金奖励")
    private String rewardsCash;

    /**
     * 物品奖励
     */
    @ApiModelProperty("物品奖励")
    private String rewardsArticles;

    /**
     * 其他奖励
     */
    @ApiModelProperty("其他奖励")
    private String rewardsOthers;

    /**
     * 是否审核
     */
//    private Integer isCheck;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;
}
