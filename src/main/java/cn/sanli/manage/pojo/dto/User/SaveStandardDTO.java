package cn.sanli.manage.pojo.dto.User;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * ClassName:SaveStandardDTO
 * Package:cn.sanli.manage.pojo.dto
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/9 11:34
 * @Version 1.0
 */
@Data
public class SaveStandardDTO {
    /**
     * 周标准
     */
    @NotNull
    @Positive
    @ApiModelProperty(value="周标准")
    private Integer week;

    /**
     * 月标准
     */
    @NotNull
    @Positive
    @ApiModelProperty(value="月标准")
    private Integer month;

    /**
     * 季度标准
     */
    @NotNull
    @Positive
    @ApiModelProperty(value="季度标准")
    private Integer quarter;
    /**
     * 半年标准
     */
    @NotNull
    @Positive
    @ApiModelProperty(value="半年标准")
    private Integer half;

    /**
     * 年标准
     */
    @NotNull
    @Positive
    @ApiModelProperty(value="年标准")
    private Integer year;
}
