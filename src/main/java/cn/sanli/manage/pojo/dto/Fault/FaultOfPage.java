package cn.sanli.manage.pojo.dto.Fault;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 过错信息表(Fault)实体类
 *
 * @author wzy
 * @since 2023-12-07 16:58:16
 */
@Data
@ApiModel("过错信息表(Fault)实体类")
public class FaultOfPage implements Serializable {
    private static final long serialVersionUID = 176443866858547154L;
    /**
     * id
     */
    @ApiModelProperty("id")
    private Integer id;

    /**
     * 过错编号
     */
    @ApiModelProperty("过错编号")
    private String faultNum;

    /**
     * 责任大口
     */
    @ApiModelProperty("责任大口")
    private String resCenter;

    /**
     * 责任人工号
     */
    @ApiModelProperty("责任人工号")
    private Integer resNum;

    /**
     * 责任人
     */
    @ApiModelProperty("责任人")
    private String resName;

    /**
     * 过错类别(9.信誉,10.质量,11.档次.,12.形象.,13.安全,14.纪律,15.素质)
     */
    @ApiModelProperty("过错类别(9.信誉,10.质量,11.档次.,12.形象.,13.安全,14.纪律,15.素质)")
    private Integer faultType;

    /**
     * 过错时间
     */
    @ApiModelProperty("过错时间")
    private Date faultTime;

    /**
     * 过错事实
     */
    @ApiModelProperty("过错事实")
    private String faultContent;

    /**
     * 过错级别(17.警告,18.轻微,19.一般.,20.较重.,21.严重,22.非常严重,23.恶劣)
     */
    @ApiModelProperty("过错级别(17.警告,18.轻微,19.一般.,20.较重.,21.严重,22.非常严重,23.恶劣)")
    private Integer faultLevel;

    /**
     * 处理部门
     */
    @ApiModelProperty("处理部门")
    private String disposalDept;

    /**
     * 处理人
     */
    @ApiModelProperty("处理人")
    private String disposalName;

    /**
     * 处理时间
     */
    @ApiModelProperty("处理时间")
    private Date disposalTime;

    /**
     * 处理形式(25.通报,26.警告单,27.责任追究单,28.问题整改告知书.,29.其他形式)
     */
    @ApiModelProperty("处理形式(25.通报,26.警告单,27.责任追究单,28.问题整改告知书.,29.其他形式)")
    private Integer disposalForm;

    /**
     * 其他结果(31.待岗,32.开除,33.开除留用查看,34.降薪.,35.降职,36.记过,37.其他形式)
     */
    @ApiModelProperty("其他结果(31.待岗,32.开除,33.开除留用查看,34.降薪.,35.降职,36.记过,37.其他形式)")
    private Integer otherResults;

    /**
     * 其他荣誉处理(39.检查,40.整改,41.现场会,42.申请工资扣除.,43.批评,44.曝光,45.体力劳动,46.警告,47.往来账.,48.预奖励抵顶)
     */
    @ApiModelProperty("其他荣誉处理(39.检查,40.整改,41.现场会,42.申请工资扣除.,43.批评,44.曝光,45.体力劳动,46.警告,47.往来账.,48.预奖励抵顶)")
    private Integer otherDisposal;

    /**
     * 处理结果
     */
    @ApiModelProperty("处理结果")
    private String disposalResults;

    /**
     * 处理资料编号
     */
    @ApiModelProperty("处理资料编号")
    private String disposalNum;

    /**
     * 执行完毕时间
     */
    @ApiModelProperty("执行完毕时间")
    private Date endTime;

    /**
     * 添加人
     */
    @ApiModelProperty("添加人")
    private String creatName;

    /**
     * 添加时间
     */
    @ApiModelProperty("添加时间")
    private Date creatTime;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    private Date updateTime;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 处理层次（2:公司型考核, 3:大口性考核, 4:部门性考核）
     */
    @ApiModelProperty("处理层次（2:公司型考核, 3:大口性考核, 4:部门性考核）")
    private Integer disposalLevel;

    /**
     * 责任部门
     */
    @ApiModelProperty("责任部门")
    private String resDept;

    /**
     * 审批状态(暂无)
     */
    @ApiModelProperty("审批状态(暂无)")
    private String approvalType;

    /**
     * 审批人
     */
    @ApiModelProperty("审批人")
    private String approvalName;

    /**
     * 审批时间
     */
    @ApiModelProperty("审批时间")
    private Date approvalTime;

    /**
     * 申诉时间
     */
    @ApiModelProperty("申诉时间")
    private Date complaintTime;

    /**
     * 申诉处理人
     */
    @ApiModelProperty("申诉处理人")
    private String complaintName;

    /**
     * 申诉处理时间
     */
    @ApiModelProperty("申诉处理时间")
    private Date complaintDisposalTime;





}

