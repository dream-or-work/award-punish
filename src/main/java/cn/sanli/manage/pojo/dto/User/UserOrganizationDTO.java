package cn.sanli.manage.pojo.dto.User;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ClassName:UserOrganizationDTO
 * Package:cn.sanli.manage.pojo.dto.User
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/13 16:00
 * @Version 1.0
 */
@Data
public class UserOrganizationDTO {

    @ApiModelProperty("当前页码")
    private Integer pageNum;

    @ApiModelProperty("每页记录数")
    private Integer pageSize;

    @ApiModelProperty(value="工号")
    private String num;
    /**
     * 对应大口（部门）id
     */
    /*@ApiModelProperty(value="对应大口（部门）id")
    private Integer organId;*/
    @ApiModelProperty("大口名称")
    private String centerName;

    @ApiModelProperty("部门名称")
    private String deptName;
}
