package cn.sanli.manage.pojo.dto.Rewards;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author wzy
 * @Date 2023/12/14 20:26
 * @Description: 奖励信息修改请求参数实体类
 * @Version 1.0
 */
@Data
@ApiModel("奖励信息修改请求参数实体类")
public class RewardsUpdateRequest {

    /**
     * ID
     */
    @ApiModelProperty("rid")
    private String rid;
    /**
     * 所属大口
     */
    @ApiModelProperty("所属大口")
    private Integer center;
    /**
     * 所属部门
     */
    @ApiModelProperty("所属部门")
    private Integer dept;
    /**
     * 被奖励人
     */
    @ApiModelProperty("被奖励人")
    private String name;
    /**
     * 工号
     */
    @ApiModelProperty("工号")
    private String num;
    /**
     * 奖励类别
     */
    @ApiModelProperty("奖励类别")
    private Integer rewardsType;

    /**
     * 奖励时间
     */
    @ApiModelProperty("奖励时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date rewardsTime;


    /**
     * 奖励时间
     */
    @ApiModelProperty("修改人(最后操作人员)")
    private String updateName;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    @JsonFormat(pattern = "YYYY-MM-dd")
    private Date updateTime;

    /**
     * 奖励事由
     */
    @ApiModelProperty("奖励事由")
    private String reasons;
    /**
     * 奖励部门
     */
    @ApiModelProperty("奖励部门")
    private String rewardsDept;
    /**
     * 奖励人
     */
    @ApiModelProperty("奖励人")
    private String rewardsName;
    /**
     * 奖励资料编号
     */
    @ApiModelProperty("奖励资料编号")
    private String rewardsNum;

//    /**
//     * 奖励信息
//     */
//    @ApiModelProperty("奖励信息")
//    private String rewards;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 现金奖励
     */
    @ApiModelProperty("现金奖励")
    private String rewardsCash;
    /**
     * 现金奖励
     */
    @ApiModelProperty("物品奖励")
    private String rewardsArticles;
    /**
     * 现金奖励
     */
    @ApiModelProperty("其他奖励")
    private String othersRewards;
    /**
     * 积分奖励
     */
    @ApiModelProperty("积分奖励")
    private BigDecimal bonusPoints;

}
