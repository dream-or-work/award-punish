package cn.sanli.manage.pojo.dto.System;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * ClassName:OrganizationDTO
 * Package:cn.sanli.manage.pojo.dto.System
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/16 17:28
 * @Version 1.0
 */
@Data
public class OrganizationDTO {
    /**
     * 名称
     */
    @NotBlank
    @TableField(value = "[name]")
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 父id
     */
    @NotNull
    @Min(1)
    @TableField(value = "parent_id")
    @ApiModelProperty(value = "父id")
    private Integer parentId;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;
}
