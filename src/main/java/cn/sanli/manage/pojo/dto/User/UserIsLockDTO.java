package cn.sanli.manage.pojo.dto.User;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ClassName:UserIsLockDto
 * Package:cn.sanli.manage.pojo.dto
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/8 17:31
 * @Version 1.0
 */
@Data
public class UserIsLockDTO {
    @ApiModelProperty("用户id")
    private Integer id;

    @ApiModelProperty("用户账号有无被锁定【1：正常使用，2：账号被锁定】")
    private Integer isLock;
}
