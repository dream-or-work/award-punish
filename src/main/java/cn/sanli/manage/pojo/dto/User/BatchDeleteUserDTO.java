package cn.sanli.manage.pojo.dto.User;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * ClassName:BatchDeleteUserDTO
 * Package:cn.sanli.manage.pojo.dto.User
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/9 23:27
 * @Version 1.0
 */
@Data
public class BatchDeleteUserDTO {

    @ApiModelProperty(value="id列表集合")
    private List<Integer> idList;
}
