package cn.sanli.manage.pojo.dto.System;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * ClassName:UpdateCategoryDTO
 * Package:cn.sanli.manage.pojo.dto.System
 * Description:
 *
 * @Author Lu
 * @Create 2024/3/21 15:33
 * @Version 1.0
 */
@Data
public class UpdateCategoryDTO {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="id")
    private Integer id;
    /**
     * 名称
     */
    @NotBlank
    @TableField(value = "[name]")
    @ApiModelProperty(value="名称")
    private String name;

    @ApiModelProperty(value="处理类型（金额0/分数1）")
    private String value;
}
