package cn.sanli.manage.pojo.dto.Rewards;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author wzy
 * @Date 2023/12/14 20:26
 * @Description: 导出批量新增奖励信息模版
 * @Version 1.0
 */
@Data
@ApiModel("导出批量新增奖励信息模版")
@JsonIgnoreProperties(value = {"rid"})
@ExcelIgnoreUnannotated
@ColumnWidth(value = 16)
public class RewardsInsertOfTextpromptRequest {

    /**
     * rId
     */
    @ApiModelProperty("Rid")
    private String Rid;
    /**
     * 所属大口
     */
    @ApiModelProperty("所属大口")
    @ExcelProperty(value = "所属大口", index = 0)
    private String center;
    /**
     * 所属部门
     */
    @ApiModelProperty("所属部门")
    @ExcelProperty(value = "所属部门", index = 1)
    private String dept;
    /**
     * 被奖励人
     */
    @ApiModelProperty("被奖励人")
    @ExcelProperty(value = "被奖励人", index = 2)
    private String name;
    /**
     * 工号
     */
    @ApiModelProperty("工号")
    @ExcelProperty(value = "工号", index = 3)
    private String num;
    /**
     * 奖励类别
     */
    @ApiModelProperty("奖励类别(9.信誉,10.质量,11.档次.,12.形象.,13.安全,14.纪律,15.素质)")
    @ExcelProperty(value = "奖励类别", index = 4)
    private String rewardsType;

    /**
     * 奖励时间
     */
    @ApiModelProperty("奖励时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(value = "奖励时间", index = 5)
    private String rewardsTime;
    /**
     * 奖励事由
     */
    @ApiModelProperty("奖励事由")
    @ExcelProperty(value = "奖励事由", index = 6)
    private String reasons;
    /**
     * 奖励部门
     */
    @ApiModelProperty("奖励部门")
    @ExcelProperty(value = "奖励部门", index = 7)
    private String rewardsDept;
    /**
     * 奖励人
     */
    @ApiModelProperty("奖励人")
    @ExcelProperty(value = "奖励人", index = 8)
    private String rewardsName;
    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    @ExcelProperty(value = "创建人", index = 9)
    private String createName;
    /**
     * 奖励资料编号
     */
    @ApiModelProperty("奖励资料编号")
    @ExcelProperty(value = "奖励资料编号", index = 10)
    private String rewardsNum;
    /**
     * 现金奖励
     */
    @ApiModelProperty("现金奖励")
    @ExcelProperty(value = "现金奖励", index = 11)
    private String rewardsCash;
    /**
     * 现金奖励
     */
    @ApiModelProperty("物品奖励")
    @ExcelProperty(value = "物品奖励", index = 12)
    private String rewardsArticles;
//    /**
//     * 现金奖励
//     */
//    @ApiModelProperty("其他奖励")
//    @ExcelProperty(value = "其他奖励", index = 13)
//    private String othersRewards;
    /**
     * 积分奖励
     */
    @ApiModelProperty("积分奖励")
    @ExcelProperty(value = "积分奖励", index = 13)
    private String bonusPoints;

//    /**
//     * 奖励信息
//     */
//    @ApiModelProperty("奖励信息")
//    @ExcelProperty(value = "奖励信息", index = 15)
//    private String rewards;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    @ExcelProperty(value = "备注", index = 14)
    private String remark;


}
