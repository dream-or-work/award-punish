package cn.sanli.manage.pojo.dto.System;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ClassName:UpdateCorresDTO
 * Package:cn.sanli.manage.pojo.dto.System
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/13 8:40
 * @Version 1.0
 */
@Data
public class UpdateCorresDTO {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="id")
    private Integer id;
    /**
     * 名称
     */
    @TableField(value = "[name]")
    @ApiModelProperty(value="名称")
    private String name;

    /**
     * 父id
     */
    /*@TableField(value = "parent_id")
    @ApiModelProperty(value="父id")
    private Integer parentId;*/

    @ApiModelProperty(value="分值标准（仅添加计分标准时用）")
    private String value;
}
