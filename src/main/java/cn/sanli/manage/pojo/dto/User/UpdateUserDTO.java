package cn.sanli.manage.pojo.dto.User;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * ClassName:UpdateUserDTO
 * Package:cn.sanli.manage.pojo.dto.Wechat
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/9 10:00
 * @Version 1.0
 */
@ApiModel(description="修改用户信息表")
@Data
public class UpdateUserDTO {

    @ApiModelProperty(value="工号")
    private String num;
    /**
     * 用户名
     */
    @ApiModelProperty(value="用户名")
    private String username;

    /*@ApiModelProperty(value="大口名称")
    private String centerName;

    @ApiModelProperty(value="部门名称")
    private String deptName;*/

    /**
     * 大口Id集合
     */
    @ApiModelProperty(value="大口Id集合")
    private List<Integer> centerIdList;

    /**
     * 部门Id集合
     */
    @ApiModelProperty(value="部门Id集合")
    private List<Integer> deptIdList;

    /**
     * 所属部门Id
     */
    @ApiModelProperty("所属部门id")
    private Integer deptId;

    /**center
     * 部门内线
     */
    @ApiModelProperty(value="部门内线")
    private String call;

    /**
     * 手机号
     */
    @ApiModelProperty(value="手机号")
    private String phone;

    /**
     * 微信号
     */
    @ApiModelProperty(value="微信号")
    private String wechat;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value="备注")
    private String remark;

    @ApiModelProperty("角色id的List集合")
    private List<Integer> roleIdList;
}
