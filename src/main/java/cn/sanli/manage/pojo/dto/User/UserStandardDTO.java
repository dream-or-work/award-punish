package cn.sanli.manage.pojo.dto.User;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ClassName:UserStandardDTO
 * Package:cn.sanli.manage.pojo.dto.User
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/11 10:20
 * @Version 1.0
 */
@Data
public class UserStandardDTO {

    @ApiModelProperty(value="工号")
    private String num;
    
    @ApiModelProperty(value="记分标准id")
    private Integer standardId;
}
