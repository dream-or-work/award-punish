package cn.sanli.manage.pojo.dto.Login;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LoginDTO {

    @ApiModelProperty("登录账号，用户名")
    public String userName;

    @ApiModelProperty("密码")
    public String cryptograph;

    @ApiModelProperty("账号状态【1：状态正常 2：逻辑删除】")
    public Integer status;

    @ApiModelProperty("账号是否被锁定【1：账号正常 2：账号被锁定不可用")
    public Integer isLock;

    @ApiModelProperty("登录工号")
    public String num;

    @ApiModelProperty("所属大区id")
    public int centerId;

    @ApiModelProperty("所属部门id")
    public int deptId;

    @ApiModelProperty("主显权限")
    public int roleId;

}
