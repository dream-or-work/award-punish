package cn.sanli.manage.pojo.dto.System;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * ClassName:CenterDTO
 * Package:cn.sanli.manage.pojo.dto.System
 * Description:
 *
 * @Author Lu
 * @Create 2024/3/29 10:07
 * @Version 1.0
 */
@Data
public class CenterDTO {
    @ApiModelProperty(value="大口id集合")
    private List<Integer> centerIdList;
}
