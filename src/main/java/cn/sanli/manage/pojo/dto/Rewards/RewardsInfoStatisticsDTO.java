package cn.sanli.manage.pojo.dto.Rewards;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Author wzy
 * @Date 2023/12/13 16:18
 * @Description: 考核信息统计参数封装类
 * @Version 1.0
 */
@Data
@ApiModel("考核信息统计参数封装类")
public class RewardsInfoStatisticsDTO {

    @ApiModelProperty("被奖励大口")
    private List<Integer> Center;

    @ApiModelProperty("被奖励部门")
    private List<Integer> Dept;

    @ApiModelProperty("被奖励人")
    private String Name;

    @ApiModelProperty("被奖励人工号")
    private Integer Num;

    @ApiModelProperty("奖励部门")
    private Integer rewardsDept;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    @ApiModelProperty("开始时间")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    @ApiModelProperty("结束时间")
    private Date endTime;

    @ApiModelProperty("统计类型( 1:个人统计// 2:部门统计// 3:大口统计)")
    private Integer type;





}
