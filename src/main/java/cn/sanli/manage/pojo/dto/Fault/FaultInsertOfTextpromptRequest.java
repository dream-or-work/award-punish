package cn.sanli.manage.pojo.dto.Fault;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author wzy
 * @Date 2023/12/14 16:44
 * @Description: 新增过错信息请求参数实体类
 * @Version 1.0
 */
@Data
@ApiModel("新增过错信息请求参数实体类")
@JsonIgnoreProperties
@ExcelIgnoreUnannotated
@ContentFontStyle(color = 10)
@ColumnWidth(value = 10)
public class FaultInsertOfTextpromptRequest {

    /**
     * 责任大口
     */
    @ApiModelProperty("责任大口")
    @ExcelProperty(value = "责任大口", index = 0)
    private String resCenter;

    /**
     * 责任部门
     */
    @ApiModelProperty("责任部门")
    @ExcelProperty(value = "责任部门", index = 1)
    private String resDept;

    /**
     * 责任人工号
     */
    @ApiModelProperty("责任人工号")
    @ExcelProperty(value = "责任人工号", index = 2)
    private Integer resNum;

    /**
     * 责任人
     */
    @ApiModelProperty("责任人")
    @ExcelProperty(value = "责任人", index = 3)
    private String resName;

    /**
     * 过错类别(1.信誉,2.质量,3档次.,4形象.,5.效率,6.安全,7.素质,8.纪律)
     */
    @ApiModelProperty("过错类别(9.信誉,10.质量,11.档次.,12.形象.,13.安全,14.纪律,15.素质)")
    @ExcelProperty(value = "过错类别", index = 4)
    private String faultType;

    /**
     * 过错时间
     */
    @ApiModelProperty("过错时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(value = "yyyy-MM-dd")
    @ExcelProperty(value = "过错时间", index = 5)
    private Date faultTime;

    /**
     * 过错事实
     */
    @ApiModelProperty("过错事实")
    @ExcelProperty(value = "过错事实", index = 6)
    private String faultContent;

    /**
     * 过错级别(1.警告,2.轻微,3一般.,4较重.,5.严重,6.非常严重,7.恶劣)
     */
    @ApiModelProperty("过错级别(17.警告,18.轻微,19.一般.,20.较重.,21.严重,22.非常严重,23.恶劣)")
    @ExcelProperty(value = "过错级别", index = 7)
    private String faultLevel;

    /**
     * 处理部门
     */
    @ApiModelProperty("处理部门")
    @ExcelProperty(value = "处理部门", index = 8)
    private String disposalDept;

    /**
     * 处理人
     */
    @ApiModelProperty("处理人")
    @ExcelProperty(value = "处理人", index = 9)
    private String disposalName;

    /**
     * 处理时间
     */
    @ApiModelProperty("处理时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(value = "yyyy-MM-dd")
    @ExcelProperty(value = "处理时间", index = 10)
    private Date disposalTime;

    /**
     * 处理形式(1.通报,2.警告单,3.责任追究单,4问题整改告知书.,5.其他形式)
     */
    @ApiModelProperty("处理形式(25.通报,26.警告单,27.责任追究单,28.问题整改告知书.,29.其他形式)")
    @ExcelProperty(value = "处理形式", index = 11)
    private String faultDisposalForm;

    /**
     * 其他结果(1.待岗,2.开除,3.开除留用查看,4降薪.,5.降职,6.记过,7.其他形式)
     */
    @ApiModelProperty("其他结果(31.待岗,32.开除,33.开除留用查看,34.降薪.,35.降职,36.记过,37.其他形式)")
    @ExcelProperty(value = "其他结果", index = 12)
    private String otherResults;

    /**
     * 其他荣誉处理(1.检查,2.整改,3.现场会,4申请工资扣除.,5.批评,6.曝光,7.体力劳动,8.警告,9.往来账.,10.预奖励抵顶)
     */
    @ApiModelProperty("其他荣誉处理(39.检查,40.整改,41.现场会,42.申请工资扣除.,43.批评,44.曝光,45.体力劳动,46.警告,47.往来账.,48.预奖励抵顶)")
    @ExcelProperty(value = "其他荣誉处理", index = 13)
    private String otherDisposal;


    /**
     * 处理资料编号
     */
    @ApiModelProperty("处理资料编号")
    @ExcelProperty(value = "处理资料编号", index = 14)
    private String disposalNum;

    /**
     * 执行完毕时间
     */
    @ApiModelProperty("执行完毕时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(value = "yyyy-MM-dd")
    @ExcelProperty(value = "执行完毕时间", index = 15)
    private Date endTime;


    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("处理层次（2:公司性考核, 3:大口性考核, 4:部门性考核）")
    @ExcelProperty(value = "处理层次", index = 16)
    private String disposalLevel;

    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("保证金")
    @ExcelProperty(value = "保证金", index = 17)
    private BigDecimal earnestMoney;

    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("保证金编号")
    @ExcelProperty(value = "保证金编号", index = 18)
    private String earnestMoneyNum;

    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("损失费")
    @ExcelProperty(value = "损失费", index = 19)
    private BigDecimal lossExpense;

    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("损失费编号")
    @ExcelProperty(value = "损失费编号", index = 20)
    private String lossExpenseNum;

    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("荣誉记分")
    @ExcelProperty(value = "荣誉记分", index = 21)
    private BigDecimal score;

    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("荣誉记分编号")
    @ExcelProperty(value = "荣誉记分编号", index = 22)
    private String scoreNum;

    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("月考核分")
    @ExcelProperty(value = "月考核分", index = 23)
    private BigDecimal scoreOfMonth;

    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("月考核分编号")
    @ExcelProperty(value = "月考核分编号", index = 24)
    private String scoreOfMonthNum;

    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("年考核分")
    @ExcelProperty(value = "年考核分", index = 25)
    private BigDecimal scoreOfYear;

    /**
     * 处理层次（1：部门，2：大口，3：公司）
     */
    @ApiModelProperty("年考核分编号")
    @ExcelProperty(value = "年考核分编号", index = 26)
    private String scoreOfYearNum;

    /**
     * KPI考核分
     */
    @ApiModelProperty("kpi")
    @ExcelProperty(value = "kpi", index = 27)
    private BigDecimal kpi;

    /**
     * KPI考核分单号
     */
    @ApiModelProperty("kpi考核编号")
    @ExcelProperty(value = "kpi考核编号", index = 28)
    private String kpiNum;


    @ApiModelProperty("备注")
    @ExcelProperty(value = "备注", index = 29)
    private String remark;





}
