package cn.sanli.manage.pojo.dto.Role;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * ClassName:AssignRoleDto
 * Package:cn.sanli.manage.pojo.dto
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/7 16:28
 * @Version 1.0
 */
@Data
public class AssignRoleDto {

    @ApiModelProperty("用户id")
    private Integer userId;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("角色id的List集合")
    private List<Integer> roleIdList;

}
