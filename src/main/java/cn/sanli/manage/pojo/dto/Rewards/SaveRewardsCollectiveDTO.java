package cn.sanli.manage.pojo.dto.Rewards;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * ClassName:SaveRewardsCollectiveDTO
 * Package:cn.sanli.manage.pojo.dto.Rewards
 * Description:
 *
 * @Author Lu
 * @Create 2024/4/11 17:05
 * @Version 1.0
 */
@ApiModel("添加集体奖励信息entity")
@Data
public class SaveRewardsCollectiveDTO {

    @ApiModelProperty("奖励编号")
    private String rid;

    @ApiModelProperty("被奖励集体")
    @ExcelProperty(value = "被奖励集体", index = 0)
    @ColumnWidth(value = 23)
    private String rewardsCollective;

    /**
     * 奖励类别
     */
    @ApiModelProperty("奖励类别")
    @ExcelProperty(value = "奖励类别", index = 1)
    @ColumnWidth(value = 16)
    private String rewardsType;
    /**
     * 奖励时间
     */
    @ApiModelProperty("奖励时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(value = "奖励时间", index = 2)
    @ColumnWidth(value = 16)
    private Date rewardsTime;

    /**
     * 奖励事由
     */
    @ApiModelProperty("奖励事由")
    @ExcelProperty(value = {"", "奖励事由"}, index = 3)
    @ColumnWidth(value = 23)
    private String reasons;

    /**
     * 奖励部门
     */
    @ApiModelProperty("奖励部门")
    @ExcelProperty(value = {"", "奖励部门"}, index = 4)
    @ColumnWidth(value = 16)
    private String rewardsDept;

    /**
     * 奖励人
     */
    @ApiModelProperty("奖励人")
    @ExcelProperty(value = {"", "奖励人"}, index = 5)
    @ColumnWidth(value = 12)
    private String rewardsName;



    /**
     * 奖励信息
     */
    @ApiModelProperty("奖励信息")
    @ExcelProperty(value = {"", "奖励信息"}, index = 6)
    @ColumnWidth(value = 16)
    private String rewards;

    /**
     * 奖励资料编号
     */
    @ApiModelProperty("奖励资料编号")
    @ExcelProperty(value = {"", "奖励资料编号"}, index = 7)
    @ColumnWidth(value = 28)
    private String rewardsNum;

    /**
     * 现金奖励
     */
    @ApiModelProperty("现金奖励")
    @ExcelProperty(value = {"", "现金奖励"}, index = 8)
    @ColumnWidth(value = 16)
    private String rewardsCash;

    /**
     * 物品奖励
     */
    @ApiModelProperty("物品奖励")
    @ExcelProperty(value = {"", "物品奖励"}, index = 9)
    @ColumnWidth(value = 18)
    private String rewardsArticles;

    /**
     * 其他奖励
     */
    @ApiModelProperty("其他奖励")
    @ExcelProperty(value = {"", "其他奖励"}, index = 10)
    @ColumnWidth(value = 18)
    private String rewardsOthers;

    /**
     * 是否审核
     */
//    private Integer isCheck;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    @ExcelProperty(value = {"", "备注"}, index = 11)
    private String remark;
    /**
     * 添加人
     */
    @ApiModelProperty("添加人")
    @ExcelProperty(value = {"", "添加人"}, index = 12)
    @ColumnWidth(value = 12)
    private String createName;

    /**
     * 添加日期
     */
//    private Date createTime;
}
