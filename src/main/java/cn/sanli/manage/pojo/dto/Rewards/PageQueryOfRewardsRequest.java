package cn.sanli.manage.pojo.dto.Rewards;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author wzy
 * @Date 2023/12/8 14:25
 * @Description: 分页查询奖励信息封装类
 * @Version 1.0
 */
@Data
@ApiModel("分页查询奖励信息封装类")
public class PageQueryOfRewardsRequest implements Serializable {
    private static final long serialVersionUID = 176443866858547154L;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    @ApiModelProperty("开始时间")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    @ApiModelProperty("结束时间")
    private Date endTime;

    @ApiModelProperty("奖励编号")
    private String rid;

    @ApiModelProperty("工号")
    private String num;

    @ApiModelProperty("被奖励人姓名")
    private String name;

    @ApiModelProperty("所属大口")
    private Integer center;

    @ApiModelProperty("所属部门")
    private Integer dept;

    @ApiModelProperty("奖励类型")
    private Integer rewardsType;

    @ApiModelProperty("奖励形式")
    private String rewardsForm;

    @ApiModelProperty("是否审核")
    private Integer isCheck;

    @ApiModelProperty("页码")
    private Integer pageNum;

    @ApiModelProperty("每页条数")
    private Integer pageSize;

}
