package cn.sanli.manage.pojo.dto.User;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;
import java.util.List;

/**
 * ClassName:SaveUserInfo
 * Package:cn.sanli.manage.pojo.dto
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/9 8:54
 * @Version 1.0
 */
@ApiModel(description="添加用户信息表")
@Data
public class UserDTO {
    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空")
    @Pattern(regexp = "^[\\u4e00-\\u9fa5]{0,}$", message = "只能输入汉字")
    @ApiModelProperty(value="用户名")
    private String username;

    /*@ApiModelProperty(value="密码")
    private String password;*/

    /*@NotBlank
    @ApiModelProperty(value="大口名称")
    private String centerName;

    @NotBlank
    @ApiModelProperty(value="部门名称")
    private String deptName;*/

    /**
     * 大口Id集合
     */
    @ApiModelProperty(value="大口Id集合")
    private List<Integer> centerIdList;

    /**
     * 部门Id集合
     */
    @ApiModelProperty(value="所在部门Id")
    private Integer deptId;

    /**
     * 部门Id集合
     */
    @ApiModelProperty(value="所辖部门Id集合")
    private List<Integer> deptIdList;

    /**
     * 部门内线
     */
    @ApiModelProperty(value="部门内线")
    private String call;

    /**
     * 手机号
     */
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    @ApiModelProperty(value="手机号")
    private String phone;

    /**
     * 微信号
     */
    @ApiModelProperty(value="微信号")
    private String wechat;

    /**
     * 工号
     */
    @NotBlank(message = "工号不能为空")
    @Pattern(regexp = "^\\d{1,6}$", message = "只能输入1~6位的数字")
    @TableField(value = "num")
    @ApiModelProperty(value="工号")
    private String num;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value="备注")
    private String remark;

    @ApiModelProperty(value="用户角色Id列表")
    private List<Integer> roleIdList;

    //@ApiModelProperty(value="主权限Id")
    //private Integer roleId;
}
