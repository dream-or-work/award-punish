package cn.sanli.manage.pojo.dto.System;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ClassName:UpdateOrganizationDTO
 * Package:cn.sanli.manage.pojo.dto.System
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/16 17:46
 * @Version 1.0
 */
@Data
public class UpdateOrganizationDTO {

    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 名称
     */
    @TableField(value = "[name]")
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 父id
     */
    /*@TableField(value = "parent_id")
    @ApiModelProperty(value = "父id")
    private Integer parentId;*/

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;
}
