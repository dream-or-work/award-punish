package cn.sanli.manage.pojo.dto.Rewards;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * ClassName:RewardsCollectiveDTO
 * Package:cn.sanli.manage.pojo.dto.Rewards
 * Description:
 *
 * @Author Lu
 * @Create 2024/4/11 14:43
 * @Version 1.0
 */
@Data
@ApiModel("分页查询集体奖励信息封装类")
public class RewardsCollectiveDTO {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    @ApiModelProperty("开始时间")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    @ApiModelProperty("结束时间")
    private Date endTime;

    @ApiModelProperty("奖励编号")
    private String rid;

    @ApiModelProperty("被奖励集体")
    private String rewardsCollective;

    @ApiModelProperty("奖励类型")
    private Integer rewardsType;

    @ApiModelProperty("奖励形式")
    private String rewardsForm;

    /*@ApiModelProperty("是否审核")
    private Integer isCheck;*/

    @ApiModelProperty("页码")
    private Integer pageNum;

    @ApiModelProperty("每页条数")
    private Integer pageSize;
}
