package cn.sanli.manage.pojo.dto.User;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ClassName:UserInfoDto
 * Package:cn.sanli.manage.pojo.dto
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/6 8:12
 * @Version 1.0
 */
@Data
public class UserInfoDTO {

    @ApiModelProperty("当前页码")
    private Integer pageNum;

    @ApiModelProperty("每页记录数")
    private Integer pageSize;

    @ApiModelProperty("工号")
    private String num;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("大口名称")
    private String centerName;

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty("权限名称")
    private String roleName;
}
