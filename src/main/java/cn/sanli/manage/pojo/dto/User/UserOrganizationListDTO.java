package cn.sanli.manage.pojo.dto.User;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * ClassName:SaveUserOrganizationDTO
 * Package:cn.sanli.manage.pojo.dto.User
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/13 17:45
 * @Version 1.0
 */
@Data
public class UserOrganizationListDTO {

    /**
     * 工号
     */
    @TableField(value = "num")
    @ApiModelProperty(value="工号")
    private String num;

    /**
     * 大口Id集合
     */
    @ApiModelProperty(value="大口Id集合")
    private List<Integer> centerIdList;

    /**
     * 部门Id集合
     */
    @ApiModelProperty(value="部门Id集合")
    private List<Integer> deptIdList;
}
