package cn.sanli.manage.pojo.dto.User;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * ClassName:StandardEnableDTO
 * Package:cn.sanli.manage.pojo.dto.User
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/11 10:50
 * @Version 1.0
 */
@Data
public class StandardEnableDTO {
    @NotNull
    @ApiModelProperty("记分标准id")
    private Integer id;

    @NotNull
    @ApiModelProperty("记分标准是否启用(1:启用,0:关闭)")
    private Boolean isEnable;
}
