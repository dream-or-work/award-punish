package cn.sanli.manage.pojo.dto.Fault;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Author wzy
 * @Date 2023/12/8 14:25
 * @Description: 分页查询封装类
 * @Version 1.0
 */
@Data
@ApiModel("分页查询封装类")
public class PageQueryOfFaultRequest {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    @ApiModelProperty("开始时间")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    @ApiModelProperty("结束时间")
    private Date endTime;

    @ApiModelProperty(name = "number", value = "工号", position = 1)
    private String number;

    @ApiModelProperty(name = "username", value = "责任人姓名", position = 2)
    private String username;

    @ApiModelProperty("责任人大口")
    private Integer center;

    @ApiModelProperty("责任人部门")
    private Integer dept;

    @ApiModelProperty("过错类别(9.信誉,10.质量,11.档次.,12.形象.,13.安全,14.纪律,15.素质)")
    private Integer faultType;

    @ApiModelProperty("过错级别(17.警告,18.轻微,19.一般.,20.较重.,21.严重,22.非常严重,23.恶劣)")
    private Integer faultLevel;

    @ApiModelProperty("处理层次(考核分级) 2.公司型考核 3.大口性考核 4.部门性考核")
    private Integer disposalLevel;

    @ApiModelProperty("页码")
    private Integer pageNum;

    @ApiModelProperty("每页条数")
    private Integer pageSize;


}
