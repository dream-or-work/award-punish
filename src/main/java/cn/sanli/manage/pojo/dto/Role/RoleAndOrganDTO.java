package cn.sanli.manage.pojo.dto.Role;

import lombok.Data;

import java.util.List;

/**
 * @Author wzy
 * @Date 2024/4/3 16:54
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class RoleAndOrganDTO {

    /**
     * 用户角色列表
     */
    List<Integer> roleList;

    /**
     * 用户所辖部门列表
     */
    List<Integer> organOfDept;

    /**
     * 用户所辖大口列表
     */
    List<Integer> organOfCenter;


}
