package cn.sanli.manage.pojo.dto.User;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;

/**
 * ClassName:UpdateLoginUserDTO
 * Package:cn.sanli.manage.pojo.dto.User
 * Description:
 *
 * @Author Lu
 * @Create 2023/12/9 20:37
 * @Version 1.0
 */
@ApiModel(description="修改登录用户信息表")
@Data
public class UpdateLoginUserDTO {
    /**
     * 用户名
     */
    @ApiModelProperty(value="用户名")
    private String username;

    @ApiModelProperty(value="密码")
    private String password;

    /*@ApiModelProperty(value="大口名称")
    private String centerName;

    @ApiModelProperty(value="部门名称")
    private String deptName;*/

    /**center
     * 部门内线
     */
    @ApiModelProperty(value="部门内线")
    private String call;

    /**
     * 手机号
     */
    @ApiModelProperty(value="手机号")
    private String phone;

    /**
     * 微信号
     */
    @ApiModelProperty(value="微信号")
    private String wechat;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value="备注")
    private String remark;
}
