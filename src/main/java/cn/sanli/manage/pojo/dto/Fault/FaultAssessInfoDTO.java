package cn.sanli.manage.pojo.dto.Fault;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author wzy
 * @Date 2024/3/23 0:10
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class FaultAssessInfoDTO {

    /**
     * 过错编号
     */
    @ApiModelProperty("过错编号")
    @ExcelProperty(value = {"", "过错编号"}, index = 0)
    private String faultNum;

        /**
     * 处理方式（1.保证金，2：损失费，3荣誉记分，4月考核分，5：年考核分）
     */
    @ApiModelProperty("处理方式（1026.保证金，1027：损失费，1028.荣誉记分，1029.月考核分，1030：年考核分）")
    @ExcelProperty(value = "处理方式", index = 17)
    private Integer faultAssessDisposalForm;

    /**
     * 考核内容
     */
    @ApiModelProperty("考核内容")
    @ExcelProperty(value = "考核内容", index = 18)
    private BigDecimal disposalDetails;

    /**
     * 考核内容编号
     */
    @ApiModelProperty("考核内容编号")
    @ExcelProperty(value = "考核内容编号", index = 19)
    private String assessNum;

    /**
     * 考核结果
     */
    @ApiModelProperty("考核结果")
    private String assessResults;


    @ApiModelProperty("备注")
    private String faultAssessRemark;



}
