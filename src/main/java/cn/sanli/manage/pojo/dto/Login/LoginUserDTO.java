package cn.sanli.manage.pojo.dto.Login;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LoginUserDTO {

    @ApiModelProperty("工号")
    public String num;

    @ApiModelProperty("密码")
    public String password;

}
