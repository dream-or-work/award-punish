package cn.sanli.manage.pojo.Wechat;

import lombok.Data;

/**
 *  微信文本消息
 */
@Data
public class MessageText {

    /**
     * 消息内容，最长不超过2048个字节，超过将截断（支持id转译）
     * 必填
     */
    private String content;
}
