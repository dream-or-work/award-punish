package cn.sanli.manage.pojo.Wechat;

import lombok.Data;

import java.util.Date;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description: 企业微信Token缓存
 * @author: lsk
 * @create: 2023-12-11 16:14
 * @since jdk1.8
 **/
@Data
public class TWxAccessToken {

    // 企业ID
    private String corpId;

    // 企业密钥
    private String corpSecret;

    // 过期时间
    private Date expiresTime;

    // 访问令牌
    private String accessToken;


    // 标记
    private String flag;
}

