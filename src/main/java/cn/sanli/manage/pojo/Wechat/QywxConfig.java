package cn.sanli.manage.pojo.Wechat;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 企业微信配置
 */
@Component
@Configuration
@ConfigurationProperties(prefix = QywxConfig.prefix)
@Data
public class QywxConfig {

    public final static String prefix = "qywx";

    /**
     * 企业微信请求地址
     */
    @Value("${qywx.endpoint}")
    private String endpoint;
	
    /**
     * 企业id
     */
    @Value("${qywx.corpId}")
    private String corpId;
    /**
     * 服务id
     */
    @Value("${qywx.agentId}")
    private String agentId;
    /**
     * 服务Secret
     */
    @Value("${qywx.corpSecret}")
    private String corpSecret;


}
