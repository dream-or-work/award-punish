package cn.sanli.manage.web;

/**
 * 业务状态码枚举类型
 */
public enum ServiceCode {

    /**
     * 成功
     */
    OK(200),

    /**
     * 修改密码
     */
    JWT_EXPIRED(201),
    /**
     * 错误：请求参数格式有误
     */
    ERROR_BAD_REQUEST(400),
    /**
     * 错误：数据不存在
     */
    ERROR_NOT_FOUND(404),
    /**
     * 错误：数据冲突
     */
    ERROR_CONFLICT(409),
    /**
     * 错误：未通过认证，或未找到认证信息
     */
    ERROR_UNAUTHORIZED(401),
    /**
     * 错误：未通过认证，因为账号被禁用
     */
    ERROR_UNAUTHORIZED_DISABLED(402),
    /**
     * 错误：禁止访问，无此操作权限
     */
    ERROR_FORBIDDEN(403),

    /**
     * 错误：未知错误
     */
    ERROR_UNKNOWN(405),

    /**
     * 错误:账号被顶号
     */
    ERROR_SATISFIABLE(416),
    /**
     * 错误：插入数据错误
     */
    ERROR_INSERT(500),
    /**
     * 错误：删除数据错误
     */
    ERROR_DELETE(501),
    /**
     * 错误：修改数据错误
     */
    ERROR_UPDATE(502),
    /**
     * 错误：JWT已过期
     */
    ERROR_JWT_EXPIRED(600),
    /**
     * 错误：JWT格式错误
     */
    ERROR_JWT_MALFORMED(601),
    /**
     * 错误：JWT验证签名失败
     */
    ERROR_JWT_SIGNATURE(602);


    private Integer value;

    ServiceCode(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

}
