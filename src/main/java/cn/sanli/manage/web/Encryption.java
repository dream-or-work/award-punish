package cn.sanli.manage.web;

import java.util.Base64;

/**
 * 对原密码进行加码或者是解密
 */
public class Encryption {
    /**
     * base64加密
     * @param content 待加密内容
     * @return byte[]
     */
    public static String base64Encrypt(String content) {
        byte[] encode = Base64.getEncoder().encode(content.getBytes());
        String password=new String(encode);
        return password;
    }

    /**
     * base64解密
     * @param passWord 已加密内容
     * @return byte[]
     */
    public static String base64Decrypt(String passWord) {
        byte[] bytes = passWord.getBytes();
        byte[] decode = Base64.getDecoder().decode(bytes);
        String cryptanalysis=new String(decode);
        return cryptanalysis;
    }
}
