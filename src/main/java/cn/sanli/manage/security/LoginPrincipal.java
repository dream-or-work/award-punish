package cn.sanli.manage.security;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LoginPrincipal {

    @ApiModelProperty("登录工号")
    private String num;

    @ApiModelProperty("该账号所拥有的权限")
    public String roles;

    @ApiModelProperty("登录名称")
    private String userName;

    @ApiModelProperty("所属大区id")
    public int centerId;

    @ApiModelProperty("所属部门id")
    public int deptId;

    @ApiModelProperty("主显权限")
    public int roleId;

}
