package cn.sanli.manage.security;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import java.util.Collection;

public class UserDetail extends User {

    @Getter
    @ApiModelProperty("用户名")
    public String userName;

    @Getter
    @ApiModelProperty("主显id")
    public int roleId;

    @Getter
    @ApiModelProperty("所属大区")
    public int centerId;

    @Getter
    @ApiModelProperty("所属部门")
    public int deptId;


    /**
     *  通过此构造方法,添加所需要的属性
     */
    public UserDetail(String userName, int roleId, int centerId,  int deptId, String num,String passWord, boolean status, boolean isLock, Collection<? extends GrantedAuthority> authorities) {
        super(num, passWord,  status, true, true, isLock, authorities);
        this.userName=userName;
        this.roleId=roleId;
        this.centerId=centerId;
        this.deptId=deptId;
    }
}
