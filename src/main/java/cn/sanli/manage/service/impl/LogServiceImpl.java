package cn.sanli.manage.service.impl;


import cn.sanli.manage.mapper.data1.LogMapper;
import cn.sanli.manage.pojo.entity.Log;
import cn.sanli.manage.service.LogService;
import cn.sanli.manage.web.JsonResult;
import cn.sanli.manage.web.ServiceCode;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 日志信息表(log)表服务实现类
 *
 * @author makejava
 * @since 2023-12-04 15:33:14
 */
@Service("logService")
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements LogService {


    @Override
    public void addLog(Log l) {
        int rows = this.baseMapper.addLog(l);
        if (rows <= 0) {
            JsonResult.fail(ServiceCode.ERROR_FORBIDDEN, "日志存储错误");
        }

    }

}