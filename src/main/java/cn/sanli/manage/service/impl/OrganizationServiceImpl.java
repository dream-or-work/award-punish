package cn.sanli.manage.service.impl;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.mapper.data1.OrganizationMapper;
import cn.sanli.manage.pojo.dto.System.OrganizationDTO;
import cn.sanli.manage.pojo.dto.System.UpdateOrganizationDTO;
import cn.sanli.manage.pojo.entity.IdAndName;
import cn.sanli.manage.pojo.entity.Organization;
import cn.sanli.manage.pojo.vo.DeptNameVO;
import cn.sanli.manage.service.OrganizationService;
import cn.sanli.manage.web.ServiceCode;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 奖惩组织机构(organization)表服务实现类
 *
 * @author makejava
 * @since 2023-12-04 15:33:22
 */
@Transactional
@Service("organizationService")
public class OrganizationServiceImpl implements OrganizationService {
    @Resource
    private OrganizationMapper organizationMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Organization queryById(Integer id) {
        return this.organizationMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增数据
     *
     * @param organization 实例对象
     * @return 实例对象
     */
    @Override
    public Organization insert(Organization organization) {
        this.organizationMapper.insert(organization);
        return organization;
    }

    /**
     * 修改数据
     *
     * @param organization 实例对象
     * @return 实例对象
     */
    @Override
    public Organization update(Organization organization) {
        this.organizationMapper.update(organization);
        return this.queryById(organization.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.organizationMapper.deleteByPrimaryKey(id) > 0;
    }

    /**
     * 查询多个大口所辖部门
     * @param centerIdList
     * @return
     */
    @Override
    public List<DeptNameVO> selectMoreDept(List<Integer> centerIdList) {
        List<DeptNameVO> deptNameList = organizationMapper.selectMoreDept(centerIdList);
        return deptNameList;
    }
    /**
     * 查询全部大口和部门
     * @return
     */
    @Override
    public List<Organization> findNodes() {
       //查询所有组织信息
     List<Organization> organizationList = organizationMapper.selectAll();
        //如果查询结果为空，则返回null
        if (CollectionUtils.isEmpty(organizationList)) return null;
        //构建树形数据
//        List<Organization> treeList = OrganizationUtils.buildTree(organizationList);
        //确定所有对象的父分类Id，将所有相同父分类id的分类对象都放在一起
        //创建map，map中使用父分类id为key，遍历当前参数集合，将相同父分类id对象存在一个key下
        Map<Integer,List<Organization>> map=new HashMap<>();
        for (Organization node :organizationList){
            Organization newNode=new Organization();
            BeanUtils.copyProperties(node,newNode);
            Integer pid = newNode.getParentId();
            //判断当前map中是否已经存在这个父分类id的key
            if (map.containsKey(pid)){
                //如果已经存在，直接将当前对象添加到这个key对应的list中即可
                map.get(pid).add(newNode);
            }else{
                //如果当前map中还没有这个父分类id的key，就要创建新的key-value元素，要先把value准备好，即实例化List，并将分类对象添加到集合中
                List<Organization> list=new ArrayList<>();
                list.add(newNode);
                map.put(pid,list);
            }
        }
        //第二步构建三级分类树，将子分类集合添加到对应的分类对象的children属性中，
        //从一级分类开始我们程序设计父分类id为1的就是一级分类
        List<Organization> treeList = map.get(1);
        //判断是否为空TODO
        //遍历一级分类集合
        for (Organization oneLevel: treeList){
            //一级分类对象的id就是二级分类对象的父id
            Integer id = oneLevel.getId();
            List<Organization> twoList = map.get(id);
//            后续判断都是如此，根据id等于父id来写程序，也可进行判断，continue跳过此次循环。。。。。
//            if(twoList==null || twoList.isEmpty()){
//                throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,"查询数据不存在，请重新操作");
//            }
            //每个一级分类对象的children属性，就是通过一级分类的id查询出来list集合，一级分类的id就是二级分类的父id
            oneLevel.setChildren(twoList);
        }
        return treeList;
    }


    /**
     * 添加组织结构
     * @param organizationDTO 添加信息
     */
    @Transactional
    @Override
    public void saveOrganization(OrganizationDTO organizationDTO) {
        if (organizationDTO.getName()==null ||organizationDTO.getParentId()==null
                ||organizationDTO.getName()=="" || organizationDTO.getParentId()==0){
            throw new ServiceException(ServiceCode.ERROR_CONFLICT,"请传入必要参数");
        }
        Organization select = organizationMapper.selectByNameAndParentId(organizationDTO.getName(), organizationDTO.getParentId());
        if (select!=null){
            throw new ServiceException(ServiceCode.ERROR_CONFLICT,"该组织结构已存在！");
        }
        Organization organization = new Organization();
        organization.setName(organizationDTO.getName());
        organization.setParentId(organizationDTO.getParentId());
        organization.setRemark(organizationDTO.getRemark());
        organizationMapper.insertAll(organization);
    }

    /**
     * 修改组织结构
     * @param organizationDTO 修改信息
     */
    @Transactional
    @Override
    public void updateOrganization(UpdateOrganizationDTO organizationDTO) {
        if (organizationDTO.getId() == null || organizationDTO.getId() == 0 ){
            throw new ServiceException(ServiceCode.ERROR_UPDATE, "请传入必要参数");
        }
        Organization organization = organizationMapper.selectByPrimaryKey(organizationDTO.getId());
        organization.setName(organizationDTO.getName());
        organization.setRemark(organizationDTO.getRemark());
        organizationMapper.updateSelective(organization);
    }

    /**
     * 删除组织结构
     * @param id 组织结构id
     */
    @Transactional
    @Override
    public void deleteOrganization(Integer id) {
        if (id == null || id == 0){
            throw new ServiceException(ServiceCode.ERROR_DELETE,"请传入必要参数");
        }
        int count = organizationMapper.countByParentId(id);   // 先查询是否存在子节点，如果存在不允许进行删除
        if (count > 0){
            throw new ServiceException(ServiceCode.ERROR_DELETE, "该节点下有子节点，不可以删除");
        }
        // 不存在子节点直接删除
        organizationMapper.updateIsDeleteById(id);
    }

    @Override
    public List<IdAndName> queryIdAndName() {
        return organizationMapper.queryIdAndName();
    }

}

