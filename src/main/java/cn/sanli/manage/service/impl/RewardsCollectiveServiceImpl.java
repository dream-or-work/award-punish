package cn.sanli.manage.service.impl;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.mapper.data1.*;
import cn.sanli.manage.pojo.dto.Rewards.RewardsCollectiveDTO;
import cn.sanli.manage.pojo.dto.Rewards.RewardsInsertRequest;
import cn.sanli.manage.pojo.dto.Rewards.SaveRewardsCollectiveDTO;
import cn.sanli.manage.pojo.dto.Rewards.UpdateRewardsCollectiveDTO;
import cn.sanli.manage.pojo.dto.Role.RoleAndOrganDTO;
import cn.sanli.manage.pojo.dto.User.BatchDeleteUserDTO;
import cn.sanli.manage.pojo.entity.IdAndName;
import cn.sanli.manage.pojo.entity.UserInfo;
import cn.sanli.manage.pojo.vo.Rewards.RewardsCollectiveVO;
import cn.sanli.manage.pojo.vo.UserInfoVO;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.utils.MapUtils;
import cn.sanli.manage.web.ServiceCode;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.sanli.manage.pojo.entity.RewardsCollective;
import cn.sanli.manage.service.RewardsCollectiveService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * @author lu
 * @description 针对表【rewards_collective(集体奖励表)】的数据库操作Service实现
 * @createDate 2024-04-11 10:42:15
 */
@Slf4j
@Service
public class RewardsCollectiveServiceImpl extends ServiceImpl<RewardsCollectiveMapper, RewardsCollective>
        implements RewardsCollectiveService {
    @Resource
    private RewardsCollectiveMapper rewardsCollectiveMapper;
    @Resource
    private UserInfoMapper userInfoMapper;
    @Resource
    private UserOrganizationMapper userOrganizationMapper;
    @Resource
    private OrganizationMapper organizationMapper;
    @Resource
    private CorresMapper corresMapper;

    @Override
    public RewardsCollectiveVO selectAllById(Integer id) {
        return rewardsCollectiveMapper.selectAllById(id);
    }

    @Override
    public List<RewardsCollectiveVO> findByPage(RewardsCollectiveDTO rewardsCollectiveDTO, LoginPrincipal message) {
        List<Integer> roleList = UserInfoServiceImpl.getLoginRoles(message);
        // 查看全部
        if (roleList.contains(1) || roleList.contains(2) || roleList.contains(7)) {
            List<RewardsCollectiveVO> rewardsCollectiveList = rewardsCollectiveMapper.findByPage(rewardsCollectiveDTO);
            return rewardsCollectiveList;
            //查看大口
        } else if (roleList.contains(3) || roleList.contains(4)) {
            List<Integer> centerList = userOrganizationMapper.selectOrganIdByNum(message.getNum());
            List<RewardsCollectiveVO> rewardsCollectiveList = rewardsCollectiveMapper.findByCenter(rewardsCollectiveDTO, centerList);
            return rewardsCollectiveList;
            //查看部门
        } else if (roleList.contains(5) || roleList.contains(6) || roleList.contains(8)) {
            List<Integer> deptIdList = userOrganizationMapper.selectOrganIdByNum(message.getNum());
            List<RewardsCollectiveVO> rewardsCollectiveList = rewardsCollectiveMapper.findByDept(rewardsCollectiveDTO, deptIdList);
            return rewardsCollectiveList;
        }
        return null;
    }

    @Override
    public boolean saveRewardsCollective(SaveRewardsCollectiveDTO rewardsCollectiveDTO, LoginPrincipal message) {

        log.debug("开始处理新增奖励业务逻辑: {}", rewardsCollectiveDTO);
        List<UserInfo> userInfoList = userInfoMapper.selectByUsername(rewardsCollectiveDTO.getRewardsName());
        if (CollectionUtils.isEmpty(userInfoList)||userInfoList.size()==0) {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "奖励人名称不正确！");
        }
        // 截取字符串中的数字
        String maxRId = rewardsCollectiveMapper.getMaxRId();
        if (maxRId == null) {
            log.debug("获取数据库中最大奖励编号为null, 手动生成一个");
            maxRId = "T0";
        }
        String substring = maxRId.substring(1);
        log.debug("截取到的rid数值部分: {}", substring);
        // 数值加一生成新的rid并赋值
        rewardsCollectiveDTO.setRid("T" + (Integer.parseInt(substring) + 1));
        log.debug(rewardsCollectiveDTO.getRid());
        rewardsCollectiveDTO.setCreateName(message.getUserName());
        if (rewardsCollectiveDTO.getRewardsDept()==null ||rewardsCollectiveDTO.getRewardsDept()=="") {
            // 获取对应的部门
            UserInfo userInfo = userInfoMapper.selectByNum(message.getNum());
            String rewardsDept = String.valueOf(userInfo.getDeptId());
            log.debug("奖励部门: {}", rewardsCollectiveDTO.getRewardsDept());
            rewardsCollectiveDTO.setRewardsDept(rewardsDept);
        }
        int row = rewardsCollectiveMapper.insertAll(rewardsCollectiveDTO);
        if (row <= 0) {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "新增奖励信息失败！");
        }
        return true;
    }

    @Override
    public boolean updateRewardsCollective(UpdateRewardsCollectiveDTO rewardsCollectiveDTO, LoginPrincipal loginPrincipal) {

        rewardsCollectiveMapper.updateSelective(rewardsCollectiveDTO);
        return true;
    }

    @Override
    public boolean deleteById(Integer id) {
        rewardsCollectiveMapper.deleteById(id);
        return true;
    }

    @Override
    public boolean importData(List<SaveRewardsCollectiveDTO> list, LoginPrincipal loginPrincipal) {
        log.debug("开始执行批量新增数据, 参数: {}", list);

        String maxRId = rewardsCollectiveMapper.getMaxRId();
        if (maxRId == null) {
            log.debug("获取数据库中最大奖励编号为null, 手动生成一个");
            maxRId = "T0";
        }
        String substring = maxRId.substring(1);
        int rId = Integer.parseInt(substring) + 1;
        int sum = 0;
        List<IdAndName> idAndNamesOfDept = organizationMapper.queryIdAndNameOfDept();
        Map<String, Integer> mapOrganOfDept = cn.sanli.manage.utils.MapUtils.byName(idAndNamesOfDept);
        List<IdAndName> idAndNames1 = corresMapper.queryIdAndName();
        Map<String, Integer> mapOfCorres = MapUtils.byName(idAndNames1);

        for (SaveRewardsCollectiveDTO rewardsInsertRequest : list) {
            rewardsInsertRequest.setRid("T" + ( rId + sum++));
            rewardsInsertRequest.setCreateName(loginPrincipal.getUserName());
            rewardsInsertRequest.setRewardsDept(String.valueOf(mapOrganOfDept.get(rewardsInsertRequest.getRewardsDept())));
            rewardsInsertRequest.setRewardsCollective(rewardsInsertRequest.getRewardsCollective());
            rewardsInsertRequest.setRewardsType(String.valueOf(mapOfCorres.get(rewardsInsertRequest.getRewardsType())));
            log.debug("新增数据: {}", rewardsInsertRequest);
        }


        int row = rewardsCollectiveMapper.insertBatch(list);
        if(!(list.size() == row)) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "批量新增数据有误");
        }
        return true;
    }

    @Override
    public boolean batchDelete(BatchDeleteUserDTO info) {
        rewardsCollectiveMapper.batchDelete(info.getIdList());
        return true;
    }

}




