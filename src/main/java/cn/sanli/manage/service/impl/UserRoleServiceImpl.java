package cn.sanli.manage.service.impl;

import cn.sanli.manage.mapper.data1.RoleMapper;
import cn.sanli.manage.mapper.data1.UserInfoMapper;
import cn.sanli.manage.mapper.data1.UserRoleMapper;
import cn.sanli.manage.pojo.dto.Role.AssignRoleDto;
import cn.sanli.manage.pojo.dto.Role.AssignRoleLvDto;
import cn.sanli.manage.pojo.entity.Role;
import cn.sanli.manage.pojo.entity.UserInfo;
import cn.sanli.manage.pojo.entity.UserRole;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.UserRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户组织关联表(userRole)表服务实现类
 *
 * @author makejava
 * @since 2023-12-04 15:34:39
 */
@Service("userroleService")
public class UserRoleServiceImpl implements UserRoleService {
    @Resource
    private UserRoleMapper userRoleMapper;
    @Resource
    private RoleMapper roleMapper;
    @Resource
    private UserInfoMapper userInfoMapper;
    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public UserRole queryById(Integer id) {
        return this.userRoleMapper.selectByPrimaryKey(id);
    }


    /**
     * 新增数据
     *
     * @param userRole 实例对象
     * @return 实例对象
     */
    @Override
    public UserRole insert(UserRole userRole) {
        this.userRoleMapper.insert(userRole);
        return userRole;
    }

    /**
     * 修改数据
     *
     * @param userRole 实例对象
     * @return 实例对象
     */
    @Override
    public UserRole update(UserRole userRole) {
        this.userRoleMapper.update(userRole);
        return this.queryById(userRole.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.userRoleMapper.deleteByPrimaryKey(id) > 0;
    }

    /**
     * 查询所有角色
     * @return 角色集合
     */
    @Override
    public Map<String, Object> findAllRoles() {
        // 查询所有的角色数据
        List<Role> roleList  = roleMapper.findAllRoles();
        Map<String , Object> resultMap = new HashMap<>() ;
        resultMap.put("roleList" , roleList) ;  //所有的角色数据
        return resultMap;
    }

    /**
     * 查询所有角色 -角色数据回显
     * @return 角色集合
     */
    @Override
    public Map<String, Object> findAllRoles(String num) {

        // 查询所有的角色数据
        List<Role> roleList  = roleMapper.findAllRoles();
        // 查询当前登录用户的角色数据
        UserInfo userInfo = userInfoMapper.selectByNum(num);
        List<Integer> userRoleList = userRoleMapper.findUserRole(userInfo.getNum());

        Map<String , Object> resultMap = new HashMap<>() ;
        resultMap.put("roleList" , roleList) ;  //所有的角色数据
        resultMap.put("userRoleList", userRoleList);    //当前登录用户的角色数据
        return resultMap;
    }

    /**
     * 用户分配角色
     * @param assignRoleDto 请求参数
     */
    @Transactional
    @Override
    public void assignRole(AssignRoleDto assignRoleDto) {
        // 删除之前用户所对应的角色数据
        UserInfo userInfo = userInfoMapper.selectByPrimaryKey(assignRoleDto.getUserId());

        userRoleMapper.deleteByNum(userInfo.getNum());
        // 分配新的角色数据
        List<Integer> roleIdList = assignRoleDto.getRoleIdList();
        roleIdList.forEach(roleId -> userRoleMapper.assignRole(userInfo.getNum(),roleId,assignRoleDto.getRemark()));
    }

    /**
     * 指定主权限
     * @param assignRoleLvDto 请求参数
     */
    @Override
    public void assignRoleLv(AssignRoleLvDto assignRoleLvDto) {
        UserInfo userInfo = userInfoMapper.selectByPrimaryKey(assignRoleLvDto.getUserId());
        userRoleMapper.updateLvByNumAndRoleId(userInfo.getNum(),assignRoleLvDto.getRoleId());
    }

    @Override
    public List<Role> findRoles(LoginPrincipal message) {
        int roleId = message.getRoleId();
        List<Role> roleList = null;
        if (roleId==1){
            roleList = roleMapper.selectAllRoleList(roleId);
        } else {
            roleList = roleMapper.selectByRoleId(roleId);
        }
        return roleList;
    }
}

