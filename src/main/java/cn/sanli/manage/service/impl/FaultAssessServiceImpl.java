//package cn.sanli.manage.service.impl;
//
//import cn.sanli.manage.mapper.data1.FaultAssessMapper;
//import cn.sanli.manage.pojo.entity.FaultAssess;
//import cn.sanli.manage.service.FaultAssessService;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//
///**
// * 过错关联表(FaultAssess)表服务实现类
// *
// * @author wzy
// * @since 2023-12-07 16:42:56
// */
//@Service("faultAssessService")
//public class FaultAssessServiceImpl implements FaultAssessService {
//    @Resource
//    private FaultAssessMapper faultAssessMapper;
//
//    /**
//     * 通过ID查询单条数据
//     *
//     * @param id 主键
//     * @return 实例对象
//     */
//    @Override
//    public FaultAssess queryById(Integer id) {
//        return faultAssessMapper.queryById(id);
//    }
//
//    /**
//     * 新增数据
//     *
//     * @param faultAssess 实例对象
//     * @return 实例对象
//     */
//    @Override
//    public boolean insert(FaultAssess faultAssess) {
//        return faultAssessMapper.insert(faultAssess) > 0;
//    }
//
//    /**
//     * 修改数据
//     *
//     * @param faultAssess 实例对象
//     * @return 实例对象
//     */
//    @Override
//    public boolean update(FaultAssess faultAssess) {
//        return faultAssessMapper.update(faultAssess) > 0;
//    }
//
//    /**
//     * 通过主键删除数据
//     *
//     * @param id 主键
//     * @return 是否成功
//     */
//    @Override
//    public boolean deleteById(Integer id) {
//        return faultAssessMapper.deleteById(id) > 0;
//    }
//}
