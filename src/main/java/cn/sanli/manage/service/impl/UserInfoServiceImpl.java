package cn.sanli.manage.service.impl;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.mapper.data1.*;
import cn.sanli.manage.pojo.dto.User.*;
import cn.sanli.manage.pojo.entity.Organization;
import cn.sanli.manage.pojo.entity.UserInfo;
import cn.sanli.manage.pojo.entity.UserOrganization;
import cn.sanli.manage.pojo.entity.UserRole;
import cn.sanli.manage.pojo.vo.UserInfoListVO;
import cn.sanli.manage.pojo.vo.UserInfoVO;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.UserInfoService;
import cn.sanli.manage.web.ServiceCode;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 用户信息表(userInfo)表服务实现类
 *
 * @author makejava
 * @since 2023-12-04 15:34:25
 */
@Slf4j
@Transactional
@Service("userinfoService")
public class UserInfoServiceImpl implements UserInfoService {
    @Resource
    private UserInfoMapper userInfoMapper;
    @Resource
    private UserRoleMapper userRoleMapper;
    @Resource
    private OrganizationMapper organizationMapper;
    @Resource
    private UserOrganizationMapper userOrganizationMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public UserInfo queryById(Integer id) {
        return this.userInfoMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增数据
     *
     * @param userInfo 实例对象
     * @return 实例对象
     */
    @Override
    public UserInfo insert(UserInfo userInfo) {
        this.userInfoMapper.insert(userInfo);
        return userInfo;
    }

    /**
     * 用户分页列表
     *
     * @param userInfoDto 查询对象
     * @return
     */
    @Override
    public PageInfo<UserInfoVO> findByPage(UserInfoDTO userInfoDto, LoginPrincipal message) {
//        int roleId = message.getRoleId();
        List<Integer> roleList = getLoginRoles(message);
        // 根据角色获取用户列表
        // 查看全部
        if (roleList.contains(1) || roleList.contains(2) || roleList.contains(7)) {
            PageHelper.startPage(userInfoDto.getPageNum(), userInfoDto.getPageSize());
            List<UserInfoVO> userInfoList = userInfoMapper.findByPage(userInfoDto);
            return new PageInfo<>(userInfoList);
            //查看大口
        } else if (roleList.contains(3) || roleList.contains(4)) {
            List<Integer> centerIdList = userOrganizationMapper.selectOrganIdByNum(message.getNum());
            PageHelper.startPage(userInfoDto.getPageNum(), userInfoDto.getPageSize()); // 设置分页参数
            List<UserInfoVO> userInfoList = userInfoMapper.findByCenter(userInfoDto, centerIdList);
            return new PageInfo<>(userInfoList);
            //查看部门
        } else if (roleList.contains(5) || roleList.contains(6)) {
            List<Integer> deptIdList = userOrganizationMapper.selectOrganIdByNum(message.getNum());
            PageHelper.startPage(userInfoDto.getPageNum(), userInfoDto.getPageSize()); // 设置分页参数
            List<UserInfoVO> userInfoList = userInfoMapper.findByDept(userInfoDto, deptIdList);
            return new PageInfo<>(userInfoList);
        }
        return null;
    }

    /**
     * 获取用户信息列表分页
     *
     * @param userInfoDto
     * @param message
     * @return
     */
    @Override
    public PageInfo<UserInfoListVO> findListByPage(UserInfoDTO userInfoDto, LoginPrincipal message) {
        List<Integer> roleList = getLoginRoles(message);
//        UserInfo userInfo = userInfoMapper.selectByNum(userInfoDto.getNum());
//        List<Integer> userRoles = userRoleMapper.findUserRole(userInfoDto.getNum());
        // 根据角色获取用户列表
        // 查看全部
        if (roleList.contains(1) || roleList.contains(2) || roleList.contains(7)) {
            PageHelper.startPage(userInfoDto.getPageNum(), userInfoDto.getPageSize());
            List<UserInfoListVO> userList = userInfoMapper.findListByPage(userInfoDto);
            return getUserInfoListVOPageInfo(userList);
            //查看大口
        } else if (roleList.contains(3) || roleList.contains(4)) {
            List<Integer> centerList = userOrganizationMapper.selectOrganIdByNum(message.getNum());
            PageHelper.startPage(userInfoDto.getPageNum(), userInfoDto.getPageSize()); // 设置分页参数
            List<UserInfoListVO> userList = userInfoMapper.findListByCenter(userInfoDto, centerList);
            return getUserInfoListVOPageInfo(userList);
            //查看部门
        } else if (roleList.contains(5) || roleList.contains(6)) {
            List<Integer> deptList = userOrganizationMapper.selectOrganIdByNum(message.getNum());
            PageHelper.startPage(userInfoDto.getPageNum(), userInfoDto.getPageSize()); // 设置分页参数
            List<UserInfoListVO> userList = userInfoMapper.findListByDept(userInfoDto, deptList);
            return getUserInfoListVOPageInfo(userList);
        }
        return null;
    }

    private PageInfo<UserInfoListVO> getUserInfoListVOPageInfo(List<UserInfoListVO> userList) {
        List<UserInfoListVO> userInfoList = userList.stream()
                .peek(user -> {
//                        List<String> centerIdList = userInfoMapper.selectCenterIdList(user.getNum());
//                        List<String> deptIdList = userInfoMapper.selectDeptIdList(user.getNum());
//                        List<String> roleIdList = userInfoMapper.selectRoleIdList(user.getNum());
                    List<String> centerNameList = userInfoMapper.selectCenterNameList(user.getNum());
                    List<String> deptNameList = userInfoMapper.selectDeptNameList(user.getNum());
                    List<String> roleNameList = userInfoMapper.selectRoleNameList(user.getNum());
//                        user.setCenterIdList(centerIdList);
//                        user.setDeptIdList(deptIdList);
//                        user.setRoleIdList(roleIdList);
                    user.setCenterNameList(centerNameList);
                    user.setDeptNameList(deptNameList);
                    user.setRoleNameList(roleNameList);
                })
                .collect(Collectors.toList());
        PageInfo<UserInfoListVO> pageInfo = new PageInfo<>(userList);
        pageInfo.setList(userInfoList);
        log.debug(userInfoList.toString());
        return pageInfo;
    }
    /**
     * 修改用户数据
     *
     * @param info 修改信息
     * @return
     */
    @Transactional
    @Override
    public UserInfo update(UpdateUserDTO info, LoginPrincipal message) {
        // 判断被修改人是否存在
        UserInfo userInfo = userInfoMapper.selectByNum(info.getNum());
        if (userInfo==null){
            throw new ServiceException(ServiceCode.ERROR_UPDATE, "修改的人员工号不存在！");
        }
        Integer centerId = userInfo.getCenterId();                                                   // 声明大口id变量
        Integer deptId = info.getDeptId();                                                       // 声明部门id变量
        //获取大区id列表
        List<Integer> centerIdList = info.getCenterIdList();                                         // 声明用户所辖大口列表

        List<Integer> newCenterIdList = new ArrayList<>();                                           // 声明一个新集合列表用来接收用户所辖大口列表
        if (!CollectionUtils.isEmpty(centerIdList) && centerIdList.size() > 0) {                     // 判断用户所辖列表不为空且长度不为0情况下, 赋值给新用户所限大口列表
            newCenterIdList = new ArrayList<>(centerIdList);
        }
        /*if (!CollectionUtils.isEmpty(centerIdList)) {
            centerId = centerIdList.get(0);
        }*/
        //获取部门id列表
        List<Integer> deptIdList = info.getDeptIdList();                                             // 声明用户所辖部门列表
        if (!CollectionUtils.isEmpty(centerIdList) && !CollectionUtils.isEmpty(deptIdList)) {        // 判断大口列表不为空并且部门列表不为空
            if (centerIdList.size() > 1 && deptIdList.size() > 1) {                                  // 判断大口/部门列表长度不小于一
                throw new ServiceException(ServiceCode.ERROR_UPDATE, "大口多选时,不可选多个部门！");
            }
        }
//        if (!CollectionUtils.isEmpty(deptIdList)) {                                                  // 判断如果所辖列表不为空
//            deptId = deptIdList.get(0);                                                              // 获取列表第一个部门
//            Organization dept = organizationMapper.selectCenterById(deptId);                         // 根据部门获取组织机构
//            centerId = dept.getParentId();                                                           // 根据组织机构获取大口id
//        }
        centerId = organizationMapper.selectCenterById(info.getDeptId()).getParentId();
        log.debug("根据部门id: {}获取到大口id: {}", info.getDeptId(), centerId);
        List<UserOrganization> userCenterList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(centerIdList) && centerIdList.size() > 0) {
            newCenterIdList.remove(Integer.valueOf(centerId));
            if (centerIdList.size() > 1) {
                for (int i = 0; i < newCenterIdList.size(); i++) {
                    UserOrganization userCenter = new UserOrganization();
                    userCenter.setNum(info.getNum());
                    userCenter.setOrganId(newCenterIdList.get(i));
                    userCenter.setLv(false);
                    userCenter.setUpdateTime(new Date());
                    userCenterList.add(userCenter);
                }
            }
        }
        List<UserOrganization> userDeptList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(deptIdList)) {
            if (deptIdList.size() > 1) {
                for (int i = 1; i < deptIdList.size(); i++) {
                    UserOrganization userDept = new UserOrganization();
                    userDept.setNum(info.getNum());
                    userDept.setOrganId(deptIdList.get(i));
                    userDept.setLv(false);
                    userDept.setUpdateTime(new Date());
                    userDeptList.add(userDept);
                }
            }
        }
        List<Integer> roleList = getLoginRoles(message);
        //修改用户信息
//        UserInfo userInfo = userInfoMapper.selectByNum(info.getNum());
        List<Integer> organIdList = userOrganizationMapper.selectOrganIdByNum(info.getNum());
        List<Integer> userRoles = userRoleMapper.findUserRole(info.getNum());
        List<UserRole> userRoleList = new ArrayList<>();
        List<Integer> roleIdList = info.getRoleIdList();
        if (!CollectionUtils.isEmpty(roleIdList)) {
            //对角色id集合进行排序
            Collections.sort(roleIdList);
        }
        //userInfo.setNum(info.getNum());
        userInfo.setUsername(info.getUsername());
        if (!CollectionUtils.isEmpty(centerIdList) || !CollectionUtils.isEmpty(deptIdList)) {
//            if ((!deptIdList.equals(organIdList)) || (!centerIdList.equals(organIdList))) {
            if (userRoles.contains(3) || userRoles.contains(4) ||((!CollectionUtils.isEmpty(roleIdList))&&(roleIdList.contains(3) || roleIdList.contains(4)))) {
                if (!CollectionUtils.isEmpty(centerIdList)) {
                    if ((!centerIdList.equals(organIdList))) {
                        if (roleList.contains(1)) {
                            //获取大口和部门id
                            Organization center = organizationMapper.selectCenterById(centerId);
                            if (center == null) {
                                throw new ServiceException(ServiceCode.ERROR_UPDATE, "大区名称有误！");
                            }
                            Organization dept = organizationMapper.selectDeptByIdAndParentId(deptId, centerId);
                            if (dept == null) {
                                throw new ServiceException(ServiceCode.ERROR_UPDATE, "部门名称有误！");
                            }
                            //封装用户数据信息
                            userInfo.setCenterId(centerId);
                            userInfo.setDeptId(deptId);
//                        if (userRoles.contains(3)) {
                            //添加用户组织结构
                            userOrganizationMapper.deleteByNum(userInfo.getNum());
                            UserOrganization userCenter = new UserOrganization();
                            userCenter.setNum(info.getNum());
                            userCenter.setOrganId(centerId);
                            userCenter.setLv(true);
                            userCenter.setUpdateTime(new Date());
                            userOrganizationMapper.insertSelective(userCenter);
                            userOrganizationMapper.insertBatch(userCenterList);
//                        }
                        } else {
                            throw new ServiceException(ServiceCode.ERROR_UPDATE, "没有修改大口权限！");
                        }
                    }
                } else if (!CollectionUtils.isEmpty(deptIdList)) {
                    if ((!deptIdList.equals(organIdList))) {
                        updateDept(info, userInfo, centerId, deptId, deptIdList, userDeptList, roleList, organIdList);
                    }
                }
            } else {
                if (centerIdList.size() > 1) {
                    throw new ServiceException(ServiceCode.ERROR_UPDATE, "该员工角色不可选择多个大区");
                }
                /*if ((!roleList.contains(1)) && (!(centerId.equals(userInfo.getCenterId())) || !centerIdList.get(0).equals(userInfo.getCenterId()))) {
                    throw new ServiceException(ServiceCode.ERROR_UPDATE, "没有修改大口权限！");
                }*/
                if (!CollectionUtils.isEmpty(centerIdList)) {
                    if ((!centerIdList.equals(organIdList))) {
                        if (roleList.contains(1)) {
                            //获取大口和部门id
                            Organization center = organizationMapper.selectCenterById(centerId);
                            if (center == null) {
                                throw new ServiceException(ServiceCode.ERROR_UPDATE, "大区名称有误！");
                            }
                            Organization dept = organizationMapper.selectDeptByIdAndParentId(deptId, centerId);
                            if (dept == null) {
                                throw new ServiceException(ServiceCode.ERROR_UPDATE, "部门名称有误！");
                            }
                            //封装用户数据信息
                            userInfo.setCenterId(centerId);
                            userInfo.setDeptId(deptId);
//                        if (userRoles.contains(3)) {
                            //添加用户组织结构
                            userOrganizationMapper.deleteByNum(userInfo.getNum());
                            UserOrganization userDept = new UserOrganization();
                            userDept.setNum(info.getNum());
                            userDept.setOrganId(deptId);
                            userDept.setLv(true);
                            userDept.setUpdateTime(new Date());
                            userOrganizationMapper.insertSelective(userDept);
                            userOrganizationMapper.insertBatch(userDeptList);
//                        }
                        } else {
                            throw new ServiceException(ServiceCode.ERROR_UPDATE, "没有修改大口权限！");
                        }
                    }
                } else if (!CollectionUtils.isEmpty(deptIdList)) {
                    if ((!deptIdList.equals(organIdList))) {
                        updateDept(info, userInfo, centerId, deptId, deptIdList, userDeptList, roleList, organIdList);
                    }
                }

//                updateDept(info, userInfo, centerId, deptId, deptIdList, userDeptList, roleList, organIdList);
            }
//            }
        }
        userInfo.setCall(info.getCall());
        userInfo.setPhone(info.getPhone());
        userInfo.setWechat(info.getWechat());
        userInfo.setRemark(info.getRemark());
        userInfo.setUpdateTime(new Date());
        userInfo.setDeptId(info.getDeptId());
        userInfo.setCenterId(centerId);

        if ((!userRoles.equals(info.getRoleIdList())) && (info.getRoleIdList() != null && info.getRoleIdList().size() > 0)) {
            // 删除之前用户所对应的角色数据
            //UserInfo user = userInfoMapper.selectByNum(info.getNum());
            userRoleMapper.deleteByNum(info.getNum());
            // 分配新的角色数据
            if (roleIdList.contains(3) || roleIdList.contains(4)) {
                //根据角色分配记分标准
                if (roleIdList.contains(3)) {
                    //高层领导
                    userInfo.setStandardId(1);
                }
                //添加用户组织结构
                updateUserOrganization(info, userInfo, centerId, centerIdList, userCenterList, organIdList);
            } else {
                if (!CollectionUtils.isEmpty(centerIdList)) {
                    if (centerIdList.size() > 1) {
                        throw new ServiceException(ServiceCode.ERROR_UPDATE, "该员工角色不可选择多个大区");
                    }
                }
                if (roleIdList.contains(5)) {
                    //中层领导
                    userInfo.setStandardId(2);
                }
                userInfo.setStandardId(3);
                //添加用户组织结构
                updateUserOrganization(info, userInfo, deptId, deptIdList, userDeptList, organIdList);
            }
            this.userInfoMapper.update(userInfo);
            //指定第一个角色id为主权限
            Integer master = roleIdList.get(0);
            UserRole masterRole = new UserRole();
            masterRole.setNum(info.getNum());
            masterRole.setRoleId(master);
            masterRole.setLv(1);
            masterRole.setUpdateTime(new Date());
            //遍历角色id集合
            for (int i = 1; i < roleIdList.size(); i++) {
                UserRole userRole = new UserRole();
                userRole.setNum(info.getNum());
                userRole.setRoleId(roleIdList.get(i));
                userRole.setLv(0);
                userRole.setUpdateTime(new Date());
                userRoleList.add(userRole);
            }
            if (roleIdList.size() == 1) {
                // 分配新的角色数据
                userRoleMapper.insertUserRole(masterRole);
            } else if (roleIdList.size() > 1) {
                // 分配新的角色数据
                userRoleMapper.insertUserRole(masterRole);
                userRoleMapper.insertUserRoleList(userRoleList);
            }
        }
        this.userInfoMapper.update(userInfo);
        return this.queryById(userInfo.getId());
    }

    private void updateDept(UpdateUserDTO info, UserInfo userInfo, Integer centerId, Integer deptId, List<Integer> deptIdList, List<UserOrganization> userDeptList, List<Integer> roleList, List<Integer> organIdList) {
        List<Integer> userRoles = userRoleMapper.findUserRole(userInfo.getNum());
        if ((!deptIdList.equals(organIdList)) && (!CollectionUtils.isEmpty(deptIdList))) {
            if (roleList.contains(1) || roleList.contains(3) || roleList.contains(4)) {
                Organization dept = organizationMapper.selectDeptByIdAndParentId(deptId, centerId);
                if (dept == null) {
                    throw new ServiceException(ServiceCode.ERROR_UPDATE, "部门名称有误！");
                }
                userInfo.setDeptId(deptId);

                if ((!userRoles.contains(3)) && (!userRoles.contains(4))) {
                    //添加用户组织结构
                    userOrganizationMapper.deleteByNum(userInfo.getNum());
                    UserOrganization userDept = new UserOrganization();
                    userDept.setNum(info.getNum());
                    userDept.setOrganId(deptId);
                    userDept.setLv(true);
                    userDept.setUpdateTime(new Date());
                    userOrganizationMapper.insertSelective(userDept);
                    userOrganizationMapper.insertBatch(userDeptList);
                }
            } else {
                throw new ServiceException(ServiceCode.ERROR_UPDATE, "没有修改部门权限！");
            }
        }
    }

    /**
     * 修改用户组织结构
     *
     * @param info
     * @param userInfo
     * @param deptId
     * @param deptIdList
     * @param userDeptList
     * @param organIdList
     */
    private void updateUserOrganization(UpdateUserDTO info, UserInfo userInfo, Integer deptId, List<Integer> deptIdList, List<UserOrganization> userDeptList, List<Integer> organIdList) {
        userOrganizationMapper.deleteByNum(userInfo.getNum());
        UserOrganization userDept = new UserOrganization();
        userDept.setNum(info.getNum());
        userDept.setOrganId(deptId);
        userDept.setLv(true);
        userDept.setCreatTime(new Date());
        userDept.setUpdateTime(new Date());
        userOrganizationMapper.insertSelective(userDept);
        userOrganizationMapper.insertBatch(userDeptList);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        if (id == null) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "请传入必要参数");
        }
        return this.userInfoMapper.deleteByPrimaryKey(id) > 0;
    }

    /**
     * 添加用户
     *
     * @param info 用户信息
     */
    @Transactional
    @Override
    public void saveUser(UserDTO info, LoginPrincipal message) {
        //获取大区id列表
        List<Integer> centerIdList = info.getCenterIdList();
//        Integer centerId = centerIdList.get(0);
        List<Integer> newCenterIdList = new ArrayList<>(centerIdList);
        //获取部门id列表
        List<Integer> deptIdList = info.getDeptIdList();

        if(deptIdList == null) {
            deptIdList = new ArrayList<>();
        }

//        Integer deptId = deptIdList.get(0);
        Integer deptId = info.getDeptId();
//        deptIdList.remove(deptId);
        if(centerIdList.size() > 1 && !info.getRoleIdList().contains(1) && !info.getRoleIdList().contains(3) && !info.getRoleIdList().contains(4)) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "只有高层领导或大口管理员可以选择多个大口");
        }
        if (centerIdList.size() > 1 && deptIdList.size() > 0) {
            throw new ServiceException(ServiceCode.ERROR_UPDATE, "大口多选时,不可选多个部门！");
        }
        Organization dept = organizationMapper.selectCenterById(deptId);
        if (dept==null){
            throw new ServiceException(ServiceCode.ERROR_UPDATE, "所在部门有误！");
        }
        Integer centerId = dept.getParentId();
        List<UserOrganization> userDeptList = new ArrayList<>();
        if (deptIdList.size() > 0) {
            for (int i = 0; i < deptIdList.size(); i++) {
                UserOrganization userDept = new UserOrganization();
                userDept.setNum(info.getNum());
                userDept.setOrganId(deptIdList.get(i));
                userDept.setLv(false);
                userDept.setCreatTime(new Date());
                userDept.setUpdateTime(new Date());
                userDeptList.add(userDept);
            }
        }
        newCenterIdList.remove(Integer.valueOf(centerId));
        List<UserOrganization> userCenterList = new ArrayList<>();
        if (centerIdList.size() > 1) {
            for (int i = 0; i < newCenterIdList.size(); i++) {
                UserOrganization userCenter = new UserOrganization();
                userCenter.setNum(info.getNum());
                userCenter.setOrganId(newCenterIdList.get(i));
                userCenter.setLv(false);
                userCenter.setCreatTime(new Date());
                userCenter.setUpdateTime(new Date());
                userCenterList.add(userCenter);
            }
        }
        if (info.getNum() == null || "".equals(info.getNum()) || info.getUsername() == null || info.getUsername() == "") {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "用户名和工号不能为空");
        }
//        if (StringUtils.isAnyBlank(info.getCenterName(), info.getDeptName()/*,info.getCall(),info.getPhone()*/)) {
//        if (CollectionUtils.isEmpty(centerIdList) || CollectionUtils.isEmpty(deptIdList)) {
        if (CollectionUtils.isEmpty(centerIdList)) {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "请选择大口");
        }
        if (CollectionUtils.isEmpty(info.getRoleIdList()) || info.getRoleIdList().size() == 0) {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "请选择用户角色");
        }
        Organization center = organizationMapper.selectCenterById(centerId);
//        Organization dept = organizationMapper.selectDeptByIdAndParentId(deptId, centerId);
        if (center == null || dept == null) {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "大口或部门名称有误！");
        }
//        int roleId = message.getRoleId();
        List<Integer> roleList = getLoginRoles(message);
        // 根据输入的工号查询用户
        UserInfo user = userInfoMapper.selectByNum(info.getNum());
        if (user != null) {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "工号已存在！");
        }
        // 对密码进行加密
        String password = "123456";
        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
        String hashPassword = bcryptPasswordEncoder.encode(password);
        //封装用户信息
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername(info.getUsername());
        userInfo.setPassword(hashPassword);
        //封装用户数据信息
        userInfo.setCenterId(centerId);
        userInfo.setDeptId(deptId);
        userInfo.setCall(info.getCall());
        userInfo.setPhone(info.getPhone());
        userInfo.setWechat(info.getWechat());
        userInfo.setNum(info.getNum());
        userInfo.setRemark(info.getRemark());
        userInfo.setCreatTime(new Date());
        userInfo.setUpdateTime(new Date());
        userInfo.setStandardId(3);
        userInfo.setIsDelete(1);
        userInfo.setIsLock(1);
        List<UserRole> userRoleList = new ArrayList<>();
        //获取角色数据
        List<Integer> roleIdList = info.getRoleIdList();
        if (CollectionUtils.isEmpty(roleIdList) || roleIdList.size() == 0) {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "请选择用户角色");
        }
        //对角色id集合进行排序
        Collections.sort(roleIdList);
        //根据角色分配记分标准
        if (roleIdList.contains(3) || roleIdList.contains(4)) {
            if (roleIdList.contains(3)) {
                //高层领导
                userInfo.setStandardId(1);
            }
            //添加用户组织结构
            saveUserOrganization(info, centerId, userCenterList);
        } else {
            if (centerIdList.size() > 1) {
                throw new ServiceException(ServiceCode.ERROR_CONFLICT, "该员工角色不可选择多个大区");
            }
            if (roleIdList.contains(5)) {
                //中层领导
                userInfo.setStandardId(2);
            }
            //添加用户组织结构
            saveUserOrganization(info, deptId, userDeptList);
        }
        //指定第一个角色id为主权限
        Integer master = roleIdList.get(0);
        UserRole masterRole = new UserRole();
        masterRole.setNum(info.getNum());
        masterRole.setRoleId(master);
        masterRole.setLv(1);
        masterRole.setCreatTime(new Date());
        masterRole.setUpdateTime(new Date());
        //遍历角色id集合
        for (int i = 1; i < roleIdList.size(); i++) {
            UserRole userRole = new UserRole();
            userRole.setNum(info.getNum());
            userRole.setRoleId(roleIdList.get(i));
            userRole.setLv(0);
            userRole.setCreatTime(new Date());
            userRole.setUpdateTime(new Date());
            userRoleList.add(userRole);
        }

        //添加全部用户
//        if (roleId == 1) {
        if (roleList.contains(1)) {
            common(userInfo, userRoleList, roleIdList, masterRole);
            //添加大口用户
        } else if (roleList.contains(3) || roleList.contains(4)) {
            List<Integer> organIdList = userOrganizationMapper.selectOrganIdByNum(message.getNum());
            if (center.getId() != message.getCenterId()) {
                if (((roleList.contains(3) || roleList.contains(4))) && !CollectionUtils.isEmpty(organIdList) && organIdList.contains(center.getId())) {
                    common(userInfo, userRoleList, roleIdList, masterRole);
                } else {
                    throw new ServiceException(ServiceCode.ERROR_CONFLICT, "只能添加相同大口用户！");
                }
            } else {
                common(userInfo, userRoleList, roleIdList, masterRole);
            }
            //添加部门用户
        } else if (roleList.contains(5) || roleList.contains(6)) {
            List<Integer> organIdList = userOrganizationMapper.selectOrganIdByNum(message.getNum());
            if (center.getId() != message.getCenterId() || dept.getId() != message.getDeptId()) {
                if (((roleList.contains(5) || roleList.contains(6))) && center.getId() == message.getCenterId() &&
                        !CollectionUtils.isEmpty(organIdList) && organIdList.contains(dept.getId())) {
                    common(userInfo, userRoleList, roleIdList, masterRole);
                } else {
                    throw new ServiceException(ServiceCode.ERROR_CONFLICT, "只能添加相同部门用户！");
                }
            } else {
                common(userInfo, userRoleList, roleIdList, masterRole);
            }
        } else {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "没有添加用户权限！");
        }
    }

    public static List<Integer> getLoginRoles(LoginPrincipal message) {
        String roles = message.getRoles();
        List<String> authorityRole = JSON.parseArray(roles, String.class);
        List<Integer> roleList = new ArrayList<>();
        for (String auth : authorityRole) {
            System.out.println(auth);
            JSONObject json = JSONObject.parseObject(auth);
            System.out.println("json对象为" + json);
            String authority = json.getString("authority");
            System.out.println(authority);
            roleList.add(Integer.valueOf(authority));
        }
        return roleList;
    }

    /**
     * 添加用户组织结构
     *
     * @param info
     * @param centerId
     * @param userCenterList
     */
    private void saveUserOrganization(UserDTO info, Integer centerId, List<UserOrganization> userCenterList) {
        UserOrganization userCenter = new UserOrganization();
        userCenter.setNum(info.getNum());
        userCenter.setOrganId(centerId);
        userCenter.setLv(true);
        userCenter.setCreatTime(new Date());
        userCenter.setUpdateTime(new Date());
        userOrganizationMapper.insertSelective(userCenter);
        userOrganizationMapper.insertBatch(userCenterList);
    }

    /**
     * 添加用户的通用方法
     *
     * @param userInfo     用户信息
     * @param userRoleList 用户角色集合
     * @param roleIdList   角色id集合
     * @param masterRole   主权限
     */
    private void common(UserInfo userInfo, List<UserRole> userRoleList, List<Integer> roleIdList, UserRole masterRole) {
        if (roleIdList.size() == 1) {   //添加主权限
            //添加用户
            userInfoMapper.insertSelective(userInfo);
            //添加用户角色
            userRoleMapper.insertUserRole(masterRole);
        } else if (roleIdList.size() > 1) { //添加主权限和其它权限
            //添加用户
            userInfoMapper.insertSelective(userInfo);
            //添加用户角色
            userRoleMapper.insertUserRole(masterRole);
            userRoleMapper.insertUserRoleList(userRoleList);
        }
    }

    /**
     * 账号锁定
     *
     * @param userIsLockDto 用户账号有无被锁定
     */
    @Transactional
    @Override
    public void updateIsLock(UserIsLockDTO userIsLockDto) {
        userInfoMapper.updateIsLockById(userIsLockDto.getIsLock(), userIsLockDto.getId());
    }

    /**
     * 删除用户
     *
     * @param id 用户id
     */
    @Transactional
    @Override
    public void updateIsDelete(Integer id, LoginPrincipal message) {
        if (id == null || id == 0) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "请传入必要参数");
        }
        int roleId = message.getRoleId();
        //获取用户信息
        UserInfo userInfo = userInfoMapper.selectByPrimaryKey(id);
        //获取用户角色
        UserRole userRole = userRoleMapper.selectByNum(userInfo.getNum());
        if (message.getNum().equals(userInfo.getNum()) || ((userRole.getRoleId() <= roleId && roleId != 1))) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "不能删除自己或更高权限用户");
        } else if ((userRole.getRoleId() == 2 || userRole.getRoleId() == 7) && roleId != 1) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "不能删除更高权限用户");
        }
        //删除用户
        userInfoMapper.updateIsDeleteById(id);
        // 删除之前用户所对应的角色数据
        UserInfo user = userInfoMapper.selectByPrimaryKey(id);
        userRoleMapper.deleteByNum(user.getNum());
        //删除用户对应的组织结构
        userOrganizationMapper.deleteByNum(user.getNum());
    }

    /**
     * 批量删除
     *
     * @param info 用户id集合
     */
    @Transactional
    @Override
    public void batchDelete(BatchDeleteUserDTO info, LoginPrincipal message) {
        if (CollectionUtils.isEmpty(info.getIdList())) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "请传入必要参数");
        }
        int roleId = message.getRoleId();
        List<Integer> idList = info.getIdList();
        idList.forEach(id -> {
            //获取用户信息
            UserInfo userInfo = userInfoMapper.selectByPrimaryKey(id);
            //获取用户角色
            UserRole userRole = userRoleMapper.selectByNum(userInfo.getNum());
            if (message.getNum().equals(userInfo.getNum()) || ((userRole.getRoleId() <= roleId && roleId != 1))) {
                throw new ServiceException(ServiceCode.ERROR_DELETE, "不能删除自己或更高权限用户");
            } else if ((userRole.getRoleId() == 2 || userRole.getRoleId() == 7) && roleId != 1) {
                throw new ServiceException(ServiceCode.ERROR_DELETE, "不能删除更高权限用户");
            }
        });
        //批量删除
        userInfoMapper.batchDelete(idList);
        // 删除之前用户所对应的角色数据
        List<String> numList = userInfoMapper.selectNumById(idList);
        userRoleMapper.batchDeleteRole(numList);
        //删除用户对应的组织结构
        userOrganizationMapper.batchDelete(numList);
    }

    /**
     * 重置密码
     *
     * @param id 用户id
     */
    @Transactional
    @Override
    public void resetPassWord(Integer id) {
        if (id == null || id == 0) {
            throw new ServiceException(ServiceCode.ERROR_UPDATE, "请传入必要参数");
        }
        //重置初始密码123456
        String password = "123456";
        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
        String hashPassword = bcryptPasswordEncoder.encode(password);
        userInfoMapper.resetPassWord(hashPassword, id);
    }

    /**
     * 修改当前登录用户
     *
     * @param info    修改信息
     * @param message 登录用户信息
     * @return
     */
    @Transactional
    @Override
    public UserInfo updateLoginUser(UpdateLoginUserDTO info, LoginPrincipal message) {
        // 获取当前登录用户信息
        UserInfo userInfo = userInfoMapper.selectByNum(message.getNum());
        userInfo.setNum(message.getNum());
        userInfo.setUsername(info.getUsername());
        // 对密码进行加密
        String password = info.getPassword();
        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
        String hashPassword = bcryptPasswordEncoder.encode(password);
        userInfo.setPassword(hashPassword);
        //获取大口和部门id
        /*Organization center = organizationMapper.selectCenterByName(info.getCenterName());
        Organization dept = organizationMapper.selectDeptByName(info.getDeptName(), center.getId());
        if (center == null || dept == null) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "大口或部门名称有误！");
        }
        userInfo.setCenterId(center.getId());
        userInfo.setDeptId(dept.getId());*/
        //封装数据信息
        userInfo.setCall(info.getCall());
        userInfo.setPhone(info.getPhone());
        userInfo.setWechat(info.getWechat());
        userInfo.setRemark(info.getRemark());
        userInfo.setUpdateTime(new Date());
        this.userInfoMapper.update(userInfo);
        return this.queryById(userInfo.getId());
    }

    /**
     * 查询工号是否存在
     *
     * @param num 工号
     */
    @Override
    public void findByNum(String num) {
        // 根据输入的工号查询用户
        UserInfo user = userInfoMapper.selectByNum(num);
        if (user != null) {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "工号已存在！");
        }
    }

    /**
     * 查询用户信息
     *
     * @param num 工号
     * @return
     */
    @Override
    public UserInfoVO findUserInfo(String num) {
        UserInfoVO user = userInfoMapper.selectAllByNum(num);
        if (user == null) {
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, "用户不存在");
        }
        return user;
    }

    @Override
    public UserInfoListVO findListUserInfo(String num) {
        UserInfoListVO user = userInfoMapper.findListUserInfo(num);
        if (user == null) {
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, "用户不存在");
        }
        return user;
    }



    /*@Override
    public void updateUserStandard(UserStandardDTO info) {
        userInfoMapper.updateStandardIdByNum(info.getStandardId(), info.getNum());
    }*/
}

