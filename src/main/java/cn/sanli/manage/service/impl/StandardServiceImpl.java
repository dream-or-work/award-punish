package cn.sanli.manage.service.impl;


import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.mapper.data1.StandardMapper;
import cn.sanli.manage.pojo.dto.User.SaveStandardDTO;
import cn.sanli.manage.pojo.dto.User.StandardEnableDTO;
import cn.sanli.manage.pojo.entity.Standard;
import cn.sanli.manage.service.StandardService;
import cn.sanli.manage.web.ServiceCode;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 记分提醒标准表(standard)表服务实现类
 *
 * @author makejava
 * @since 2023-12-04 15:34:08
 */
@Service("standardService")
public class StandardServiceImpl implements StandardService {
    @Resource
    private StandardMapper standardMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Standard queryById(Integer id) {
        return this.standardMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增数据
     *
     * @param standard 实例对象
     * @return 实例对象
     */
    @Override
    public Standard insert(Standard standard) {
        this.standardMapper.insert(standard);
        return standard;
    }

    /**
     * 修改数据
     *
     * @param standard 实例对象
     * @return 实例对象
     */
    @Override
    public Standard update(Standard standard) {
        this.standardMapper.update(standard);
        return this.queryById(standard.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.standardMapper.deleteByPrimaryKey(id) > 0;
    }


    /**
     * 获取记分提醒标准列表
     * @return
     */
    @Override
    public List<Standard> findStandardList() {
        List<Standard> standardList = standardMapper.findAllStandard();
        return standardList;
    }

    /**
     * 添加记分提醒标准
     * @param info
     */
    @Override
    public void saveStandard(SaveStandardDTO info) {
        if (info.getWeek() == null || info.getMonth() ==null ||info.getQuarter() == null || info.getHalf() == null ||info.getYear() ==null
         ||info.getWeek() == 0 || info.getMonth() ==0 ||info.getQuarter() == 0 || info.getHalf() == 0 ||info.getYear() ==0){
            throw new ServiceException(ServiceCode.ERROR_CONFLICT,"请传入必要参数");
        }
        standardMapper.saveStandard(info);
    }

    /**
     * 修改记分提醒标准
     * @param standard
     */
    @Override
    public void updateStandard(Standard standard) {
        if (standard.getId() == null || standard.getId() == 0){
            throw new ServiceException(ServiceCode.ERROR_UPDATE,"请传入必要参数");
        }
        standardMapper.updateSelective(standard);
    }

    /**
     * 修改是否启用
     * @param isEnable
     */
    @Override
    public void updateIsEnable(StandardEnableDTO isEnable) {
        if (isEnable.getId() == null || isEnable.getId() == 0 ){
            throw new ServiceException(ServiceCode.ERROR_UPDATE,"请传入必要参数");
        }
        standardMapper.updateIsEnableById(isEnable.getIsEnable(),isEnable.getId());
    }

    @Override
    public Standard findStandard(Integer id) {
        return standardMapper.selectByPrimaryKey(id);
    }

}

