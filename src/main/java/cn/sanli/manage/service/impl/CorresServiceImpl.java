package cn.sanli.manage.service.impl;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.mapper.data1.CorresMapper;
import cn.sanli.manage.pojo.dto.System.CategoryDTO;
import cn.sanli.manage.pojo.dto.System.CorresDTO;
import cn.sanli.manage.pojo.dto.System.UpdateCategoryDTO;
import cn.sanli.manage.pojo.dto.System.UpdateCorresDTO;
import cn.sanli.manage.pojo.entity.Corres;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.CorresService;
import cn.sanli.manage.web.ServiceCode;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 字段映射表(corres)表服务实现类
 *
 * @author makejava
 * @since 2023-12-04 15:33:00
 */
@Transactional
@Service("corresService")
public class CorresServiceImpl implements CorresService {
    @Resource
    private CorresMapper corresMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Corres queryById(Integer id) {
        return this.corresMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增数据
     *
     * @param corres 实例对象
     * @return 实例对象
     */
    @Override
    public Corres insert(Corres corres) {
        this.corresMapper.insert(corres);
        return corres;
    }

    /**
     * 修改数据
     *
     * @param corres 实例对象
     * @return 实例对象
     */
    @Override
    public Corres update(Corres corres) {
        this.corresMapper.update(corres);
        return this.queryById(corres.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.corresMapper.deleteByPrimaryKey(id) > 0;
    }

    /**
     * 查询菜单
     *
     * @param message 登录用户信息
     * @return 菜单列表
     */
    @Override
    public List<Corres> findNodes(LoginPrincipal message) {
        int roleId = message.getRoleId();
        List<Corres> sysMenuList = corresMapper.selectAll();
        if (CollectionUtils.isEmpty(sysMenuList)) return null;
        //确定所有对象的父分类Id，将所有相同父分类id的分类对象都放在一起
        //创建map，map中使用父分类id为key，遍历当前参数集合，将相同父分类id对象存在一个key下
        Map<Integer,List<Corres>> map=new HashMap<>();
        for (Corres node :sysMenuList){
            Corres newNode=new Corres();
            BeanUtils.copyProperties(node,newNode);
            Integer pid = newNode.getParentId();
            //判断当前map中是否已经存在这个父分类id的key
            if (map.containsKey(pid)){
                //如果已经存在，直接将当前对象添加到这个key对应的list中即可
                map.get(pid).add(newNode);
            }else{
                //如果当前map中还没有这个父分类id的key，就要创建新的key-value元素，要先把value准备好，即实例化List，并将分类对象添加到集合中
                List<Corres> list=new ArrayList<>();
                list.add(newNode);
                map.put(pid,list);
            }
        }
        //第二步构建三级分类树，将子分类集合添加到对应的分类对象的children属性中，
        //从一级分类开始我们程序设计父分类id为1的就是一级分类
        List<Corres> treeList = map.get(0);
        //判断是否为空TODO
        //遍历一级分类集合
        for (Corres oneLevel: treeList){
            //一级分类对象的id就是二级分类对象的父id
            Integer id = oneLevel.getId();
            List<Corres> twoList = map.get(id);
//            后续判断都是如此，根据id等于父id来写程序，也可进行判断，continue跳过此次循环。。。。。
//            if(twoList==null || twoList.isEmpty()){
//                throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,"查询数据不存在，请重新操作");
//            }
            //每个一级分类对象的children属性，就是通过一级分类的id查询出来list集合，一级分类的id就是二级分类的父id
            oneLevel.setChildren(twoList);
        }
        return treeList;
    }

    /**
     * 查询考核分级
     *
     * @param message 登录用户信息
     * @return
     */
    @Override
    public List<Corres> findAssess(LoginPrincipal message) {
        int roleId = message.getRoleId();
        List<Corres> list = null;
        if (roleId == 1 || roleId == 2 || roleId == 7) {
            //查看全部
            list = corresMapper.selectAssess();
        } else if (roleId == 3 || roleId == 4) {
            //查看大口
            list = corresMapper.selectCenterAssess();
        } else if (roleId == 5 || roleId == 6 /*|| roleId == 8*/) {
            //查看部门
            list = corresMapper.selectDeptAssess();
        }
        return list;
    }

    /**
     * 查询奖励分级
     *
     * @param message
     * @return
     */
    @Override
    public List<Corres> findReward(LoginPrincipal message) {
        int roleId = message.getRoleId();
        List<Corres> list = null;
        if (roleId == 1 || roleId == 2 || roleId == 7) {
            //查看全部
            list = corresMapper.selectReward();
        } else if (roleId == 3 || roleId == 4) {
            //查看大口
            list = corresMapper.selectCenterReward();
        } else if (roleId == 5 || roleId == 6 || roleId == 8) {
            //查看部门
            list = corresMapper.selectDeptReward();
        }
        return list;
    }

    /**
     * @return
     */
    @Override
    public List<Corres> findFaultLevel() {
        return corresMapper.selectFaultLevel();
    }

    @Override
    public List<Corres> findAssessCategory() {
        return corresMapper.selectAssessCategory();
    }

    /**
     * 查询处理形式
     *
     * @return
     */
    @Override
    public List<Corres> findAssessWay() {
        return corresMapper.selectAssessWay();
    }

    /**
     * 查询其他考核方式
     *
     * @return
     */
    @Override
    public List<Corres> findOtherAssessWay() {
        return corresMapper.selectOtherAssessWay();
    }

    /**
     * 查询其他荣誉处理
     *
     * @return
     */
    @Override
    public List<Corres> findOtherRewardWay() {
        return corresMapper.selectOtherRewardWay();
    }

    /**
     * 查询处理方式
     *
     * @return
     */
    @Override
    public List<Corres> findPunish() {
        return corresMapper.selectPunish();
    }

    /**
     * 查询奖励形式
     *
     * @return
     */
    @Override
    public List<Corres> findRewardWay() {
        return corresMapper.selectRewardWay();
    }

    /**
     * 添加菜单
     *
     * @param corresDTO 添加信息
     */
    @Transactional
    @Override
    public void saveMenu(CorresDTO corresDTO) {
        if (corresDTO.getName() == null || corresDTO.getParentId() == null || corresDTO.getName() == "") {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "请传入必要参数");
        }
        Corres select = corresMapper.selectByNameAndParentId(corresDTO.getName(), corresDTO.getParentId());
        if (select!=null){
            throw new ServiceException(ServiceCode.ERROR_CONFLICT,"该菜单项已存在！");
        }
        Corres corres = new Corres();
        corres.setName(corresDTO.getName());
        corres.setParentId(corresDTO.getParentId());
//        corres.setValue(corresDTO.getValue());
        corresMapper.insertMenu(corres);
    }

    /**
     * 添加奖励形式
     * @param categoryDTO
     */
    @Transactional
    @Override
    public void saveRewardWay(CategoryDTO categoryDTO) {
        if (categoryDTO.getName() == null || categoryDTO.getName() == ""||categoryDTO.getValue()==null||categoryDTO.getValue()=="") {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "请传入必要参数");
        }
        Corres select = corresMapper.selectByRewardWayName(categoryDTO.getName());
        if (select!=null){
            throw new ServiceException(ServiceCode.ERROR_CONFLICT,"该菜单项已存在！");
        }
        Corres corres = new Corres();
        corres.setName(categoryDTO.getName());
        corres.setValue(categoryDTO.getValue());
        corresMapper.insertRewardWay(corres);
    }

    /**
     * 添加处理方式
     * @param categoryDTO
     */
    @Transactional
    @Override
    public void savePunish(CategoryDTO categoryDTO) {
        if (categoryDTO.getName() == null || categoryDTO.getName() == ""||categoryDTO.getValue()==null||categoryDTO.getValue()=="") {
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, "请传入必要参数");
        }
        Corres select = corresMapper.selectByPunishName(categoryDTO.getName());
        if (select!=null){
            throw new ServiceException(ServiceCode.ERROR_CONFLICT,"该菜单项已存在！");
        }
        Corres corres = new Corres();
        corres.setName(categoryDTO.getName());
        corres.setValue(categoryDTO.getValue());
        corresMapper.insertPunish(corres);
    }
    /**
     * 修改菜单
     *
     * @param corresDTO 修改信息
     */
    @Transactional
    @Override
    public void updateMenu(UpdateCorresDTO corresDTO) {
        if (corresDTO.getId() == null || corresDTO.getId() == 0) {
            throw new ServiceException(ServiceCode.ERROR_UPDATE, "请传入必要参数");
        }
        Corres corres = corresMapper.selectByPrimaryKey(corresDTO.getId());
        corres.setName(corresDTO.getName());
//        corres.setValue(corresDTO.getValue());
        corresMapper.updateMenu(corres);
    }

    /**
     * 修改处理方式或奖励形式
     * @param categoryDTO
     */
    @Transactional
    @Override
    public void updateCategory(UpdateCategoryDTO categoryDTO) {
        if (categoryDTO.getId() == null || categoryDTO.getId() == 0) {
            throw new ServiceException(ServiceCode.ERROR_UPDATE, "请传入必要参数");
        }
        Corres corres = corresMapper.selectByPrimaryKey(categoryDTO.getId());
        corres.setName(categoryDTO.getName());
        corres.setValue(categoryDTO.getValue());
        corresMapper.updateCategory(corres);
    }
    /**
     * 删除菜单
     *
     * @param id 菜单id
     */
    @Transactional
    @Override
    public void deleteMenu(Integer id) {
        if (id == null || id == 0) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "请传入必要参数");
        }
        int count = corresMapper.countByParentId(id);   // 先查询是否存在子菜单，如果存在不允许进行删除
        if (count > 0) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "该节点下有子节点，不可以删除");
        }
        // 不存在子菜单直接删除
        corresMapper.updateIsDeleteById(id);
    }


}

