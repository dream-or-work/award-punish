package cn.sanli.manage.service.impl;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.mapper.data1.*;
import cn.sanli.manage.pojo.dto.Fault.*;
import cn.sanli.manage.pojo.entity.Fault;
import cn.sanli.manage.pojo.entity.IdAndName;
import cn.sanli.manage.pojo.vo.Fault.*;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.FaultService;
import cn.sanli.manage.service.WechatService;
import cn.sanli.manage.utils.DateUtils;
import cn.sanli.manage.utils.MapUtils;
import cn.sanli.manage.utils.StringUtils;
import cn.sanli.manage.web.ServiceCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * 过错信息表(Fault)表服务实现类
 *
 * @author wzy
 * @since 2023-12-07 16:58:18
 */
@Service("faultService")
@Slf4j
public class FaultServiceImpl implements FaultService {

    @Resource // 过错信息表Mapper层
    private FaultMapper faultMapper;

    @Resource // 过错关联表Mapper层
    private FaultAssessMapper faultAssessMapper;

    @Resource // 字段映射表Mapper层
    private CorresMapper corresMapper;

    @Resource // 微信推送Mapper层
    private WechatService wechatService;

    @Resource // 用户信息Mapper层
    UserInfoMapper userInfoMapper;

    @Resource
    private OrganizationMapper organizationMapper;


    /**
     * 通过ID查询单条数据
     *
     * @param faultNum 主键
     * @return 实例对象
     */
    @Override
    public Fault queryById(String faultNum) {
        return faultMapper.queryById(faultNum);
    }

    /**
     * 根据条件分页查询
     *
     * @param queryRequest 查询条件
     * @return 符合条件的数据
     */
    @Override
    @Transactional
    public List<FaultPagingQueryVO> pagingQuery(PageQueryOfFaultRequestDTO queryRequest) {
        // 根据条件从数据库获取符合条件的数据
        List<FaultPagingQueryVO> faultInfos = faultMapper.pagingQueryOfFault(queryRequest);
        if(faultInfos == null || faultInfos.isEmpty()) {
            return new ArrayList<>();
        }
        log.debug("根据条件查询到的Fault数据: {}条数据", faultInfos.size());



        return faultInfos;
    }

    /**
     * 考核信息积分统计
     *
     * @return 结果集
     */
    @Override
    public List<FaultStatisticsInfoVO> statisticsFaultOfPersonage(LoginPrincipal loginPrincipal, FaultInfoStatisticsDTO statisticsDTO) {
        // TODO: 2024/3/21 修改数据获取方式SQL语句
        int roleId = loginPrincipal.getRoleId();
        log.debug("识别到的当事人: {}  主显权限: {}", loginPrincipal, roleId);

        // 根据查询条件搜索数据库
        log.debug("根据条件获取到数据, 参数: {}", statisticsDTO);
        List<FaultStatisticsVO> faultStatisticsOfPersonageVOs
                = faultMapper.statisticsFaultOfPersonage(statisticsDTO);
        log.debug("根据条件获取到的数据: {}", faultStatisticsOfPersonageVOs);

        // 以工号为key(), 当前对象为value存入map
        Map<String, List<FaultStatisticsVO>> map = new HashMap<>();
        List<FaultStatisticsVO> listOfMap = new ArrayList<>();
        for (FaultStatisticsVO statisticsVO : faultStatisticsOfPersonageVOs) {
            if(map.containsKey(statisticsVO.getResNum())) {
                listOfMap = map.get(statisticsVO.getResNum());
                listOfMap.add(statisticsVO);
                map.put(statisticsVO.getResNum(), listOfMap);
            }else {
                List<FaultStatisticsVO> list = new ArrayList<>();
                list.add(statisticsVO);
                map.put(statisticsVO.getResNum(), list);
            }
        }

        // 创建List集合用于接受最终数据并返回
        List<FaultStatisticsInfoVO> list = new ArrayList<>();
        if(statisticsDTO.getOnlyExceedStandard() == 1) {
            log.debug("只获取超标人员数据");
            for (String key : map.keySet()) {
                FaultStatisticsInfoVO statisticsInfo = MapUtils.getStatisticsInfoOnlyExceedStandard(map.get(key), DateUtils.getStatisticsTime());
                if(statisticsInfo != null) {
                    list.add(statisticsInfo);
                }
            }
        } else {
            // 遍历map集合, 调用数据统计方法
            log.debug("获取全部人员数据");
            for (String key : map.keySet()) {
                FaultStatisticsInfoVO statisticsInfo = MapUtils.getStatisticsInfo(map.get(key), DateUtils.getStatisticsTime());
                list.add(statisticsInfo);
            }
        }
        log.debug("将统计完的数据集合返回: {}", list);

        // 返回
        return list;
    }


    /**
     * 部门积分统计
     * @param loginPrincipal 当事人
     * @return 经过处理的结果集
     */
    public List<FaultStatisticsInfoOfDeptVO> statisticsFaultOfDept(LoginPrincipal loginPrincipal, FaultInfoStatisticsDTO statisticsDTO){
        int roleId = loginPrincipal.getRoleId();
        log.debug("识别到的当事人: {}  主显权限: {}", loginPrincipal, roleId);
        // 根据查询条件搜索数据库
        List<FaultStatisticsVO> faultStatisticsOfPersonageVOs
                = faultMapper.statisticsFaultOfPersonage(statisticsDTO);
        log.debug("根据条件获取到{}条数据", faultStatisticsOfPersonageVOs.size());
        log.debug("根据条件获取到的数据: {}", faultStatisticsOfPersonageVOs);

        // 以部门id为key(), 当前对象为value存入map
        Map<String, List<FaultStatisticsVO>> map = new HashMap<>();
        List<FaultStatisticsVO> listOfMap = new ArrayList<>();
        for (FaultStatisticsVO statisticsVO : faultStatisticsOfPersonageVOs) {
            if(map.containsKey(statisticsVO.getResDept())) {
                listOfMap = map.get(statisticsVO.getResDept());
                listOfMap.add(statisticsVO);
                map.put(statisticsVO.getResDept(), listOfMap);
            }else {
                List<FaultStatisticsVO> list = new ArrayList<>();
                list.add(statisticsVO);
                map.put(statisticsVO.getResDept(), list);
            }
        }

        // 创建List集合用于接受最终数据并返回
        List<FaultStatisticsInfoOfDeptVO> list = new ArrayList<>();
        // 遍历map集合, 调用数据统计方法
        for (String key : map.keySet()) {
            FaultStatisticsInfoVO statisticsInfo = MapUtils.getStatisticsInfo(map.get(key), DateUtils.getStatisticsTime());
            log.debug("从map中获取key为{}的数据: {}", key, statisticsInfo);
            FaultStatisticsInfoOfDeptVO deptVO = new FaultStatisticsInfoOfDeptVO();
            BeanUtils.copyProperties(statisticsInfo, deptVO);
            list.add(deptVO);
        }
        log.debug("将统计完的数据集合返回: {}", list);

        // 返回
        return list;
    }


    /**
     * 大口积分统计
     * @param loginPrincipal 当事人
     * @return 经过处理的结果集
     */
    public List<FaultStatisticsInfoOfCenterVO> statisticsFaultOfCenter(LoginPrincipal loginPrincipal, FaultInfoStatisticsDTO statisticsDTO){
        int roleId = loginPrincipal.getRoleId();
        log.debug("识别到的当事人: {}  主显权限: {}", loginPrincipal, roleId);

        // 根据查询条件搜索数据库
        List<FaultStatisticsVO> faultStatisticsOfPersonageVOs
                = faultMapper.statisticsFaultOfPersonage(statisticsDTO);
        log.debug("根据条件获取到{}条数据", faultStatisticsOfPersonageVOs.size());
        log.debug("根据条件获取到的数据: {}", faultStatisticsOfPersonageVOs);

        // 以大口id为key(), 当前对象为value存入map
        Map<String, List<FaultStatisticsVO>> map = new HashMap<>();
        List<FaultStatisticsVO> listOfMap = new ArrayList<>();
        for (FaultStatisticsVO statisticsVO : faultStatisticsOfPersonageVOs) {
            if(map.containsKey(statisticsVO.getResCenter())) {
                listOfMap = map.get(statisticsVO.getResCenter());
                listOfMap.add(statisticsVO);
                map.put(statisticsVO.getResCenter(), listOfMap);
            }else {
                List<FaultStatisticsVO> list = new ArrayList<>();
                list.add(statisticsVO);
                map.put(statisticsVO.getResCenter(), list);
            }
        }

        // 创建List集合用于接受最终数据并返回
        List<FaultStatisticsInfoOfCenterVO> list = new ArrayList<>();
        // 遍历map集合, 调用数据统计方法
        for (String key : map.keySet()) {
            FaultStatisticsInfoVO statisticsInfo = MapUtils.getStatisticsInfo(map.get(key), DateUtils.getStatisticsTime());
            log.debug("从map中获取key为{}的数据: {}", key, statisticsInfo);
            FaultStatisticsInfoOfCenterVO centerVO = new FaultStatisticsInfoOfCenterVO();
            BeanUtils.copyProperties(statisticsInfo, centerVO);
            list.add(centerVO);
        }
        log.debug("将统计完的数据集合返回: {}", list);

        // 返回
        return list;
    }

    /**
     * 新增数据
     *
     * @param faultInsertRequest 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public boolean insert(FaultInsertRequest faultInsertRequest, LoginPrincipal loginPrincipal) {
        // TODO: 2024/3/21 修改数据读取方式以及数据添加方式 
        log.debug("service层接收到的参数: {}", faultInsertRequest);
        log.debug("获取到的当事人类型数据:{}", loginPrincipal);
        FaultInsertDTO faultInsertDTO = new FaultInsertDTO();
        BeanUtils.copyProperties(faultInsertRequest, faultInsertDTO);
        faultInsertDTO.setFaultAssessRemark(faultInsertRequest.getFaultAssessRemark());
        faultInsertDTO.setResNum(StringUtils.fillStringBeforeString(String.valueOf(faultInsertRequest.getResNum()), "0", 4));
        log.debug("同名属性赋值后L{}", faultInsertDTO.toString());
        log.debug("开始处理新增过错信息业务逻辑, 参数: {}", faultInsertDTO);

        // 获取最近的一个过错编号, 并往后延续赋值
        String maxFaultNum = faultMapper.getMaxFaultNum();
        if(maxFaultNum == null) {
            log.debug("获取数据库中最大考核编号为null, 手动生成一个");
            maxFaultNum = "G0";
        }
        String substring = maxFaultNum.substring(1);
        log.debug("截取到的rid数值部分: {}", substring);
        faultInsertDTO.setFaultNum("G" + (Integer.parseInt(substring) + 1));

        List<IdAndName> idAndNames = corresMapper.queryIdAndName();
        Map<Integer, String> mapOfCorres = MapUtils.byId(idAndNames);

        faultInsertDTO.setFaultLevelOfNum(faultInsertRequest.getFaultLevel());
        faultInsertDTO.setCreatName(loginPrincipal.getUserName());
        faultInsertDTO.setDisposalResults("");
        // 向过错信息表新增数据
        log.debug("补充完参数属性后: {}", faultInsertDTO);

        if(faultInsertDTO.getFaultAssessInfos() != null && faultInsertDTO.getFaultAssessInfos().length != 0) {
            for(FaultAssessInfoDTO faultAssessInfoDTO : faultInsertDTO.getFaultAssessInfos()) {
                if(faultAssessInfoDTO.getDisposalDetails() == null) {
                    throw new ServiceException(ServiceCode.ERROR_INSERT, "考核分值/金额不能为空");
                } else if(faultAssessInfoDTO.getFaultAssessDisposalForm() == null) {
                    throw new ServiceException(ServiceCode.ERROR_INSERT, "考核类型不能为空");
                }
                String corresName = mapOfCorres.get(faultAssessInfoDTO.getFaultAssessDisposalForm());
                faultInsertDTO.setDisposalResults( faultInsertDTO.getDisposalResults().concat(corresName + ":" + faultAssessInfoDTO.getDisposalDetails() + "/"));
                log.debug("获取到处理结果: {}", faultInsertDTO.getDisposalResults());
                faultAssessInfoDTO.setAssessResults(corresName + ":" + faultAssessInfoDTO.getDisposalDetails() + "/" + faultAssessInfoDTO.getAssessNum());
                faultAssessInfoDTO.setFaultNum(faultInsertDTO.getFaultNum());
                faultAssessInfoDTO.setDisposalDetails(new BigDecimal(String.valueOf(faultAssessInfoDTO.getDisposalDetails())).setScale(2, BigDecimal.ROUND_HALF_UP));
                faultAssessInfoDTO.setFaultAssessRemark(faultAssessInfoDTO.getFaultAssessRemark());
                log.debug("本次考核内容: {}", faultAssessInfoDTO.getDisposalDetails());
            }

            log.debug("开始向过错关联表中新增关联信息, 参数: {}", faultInsertDTO);
            int row = faultAssessMapper.insert(faultInsertDTO.getFaultAssessInfos());
            if(row <= 0) {
                throw new ServiceException(ServiceCode.ERROR_INSERT, "新增过错关联信息失败");
            }

        }

        int insert = faultMapper.insert(faultInsertDTO);
        Integer id = faultInsertDTO.getId();
        if(insert <= 0) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "过错信息新增数据失败");
        }

        Integer resNum = faultInsertRequest.getResNum();
        // 获取时间段
        Map<String, Date[]> statisticsTime = DateUtils.getStatisticsTime();
        // 根据工号搜索用户的信息
        FaultStandardVO faultStandardVO = userInfoMapper.selectStandardByNumber(resNum);
        // 根据工号搜索员工所有计分信息
        List<FaultStatisticsVO> statisticsVOS = faultMapper.queryScoreByNumber(String.valueOf(resNum));
        log.debug("根据工号搜索员工所有计分信息:{}", statisticsVOS);

        String message = "";
        // 获取字段名
        FaultInfo faultInfo = faultMapper.queryFaultInfoById(faultInsertDTO.getFaultNum());
        if(faultStandardVO == null) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "数据库中没有当前考核对象的用户信息");
        }

        if(!statisticsVOS.isEmpty()) {
            // 对用户计分数据进行整理
            FaultStatisticsInfoVO statisticsInfo = MapUtils.getStatisticsInfo(statisticsVOS, statisticsTime);
            log.debug("对用户计分数据进行整理: {}", statisticsInfo);
            message = "尊敬的" + faultInfo.getResName() + "(工号:" + faultInfo.getResNum() + "), 您好 " + "\r\n" +
                    "您在" + DateUtils.date(faultInfo.getFaultTime()) + "因" + faultInfo.getFaultType() + "发生" + faultInfo.getFaultLevel() + "过错, " +
                    "具体事实:" + faultInfo.getFaultContent()+ ", 处理部门:" + faultInfo.getDisposalDept() + ", 处理人:" + faultInfo.getDisposalName() +
                    ", 处理结果:" + faultInfo.getDisposalResults() + "\r\n" +
                    "[统计]:" + "\r\n" +
                    "本周计分:" + statisticsInfo.getStatisticsOfWeek() + ",\r\n" + "本月计分:" + statisticsInfo.getStatisticsOfMonth() + ",\r\n" +
                    "本季度计分:" + statisticsInfo.getStatisticsOfQuarter() + ",\r\n" + "本半年计分:" + statisticsInfo.getStatisticsOfHalf() + ",\r\n" +
                    "本年度计分:" + statisticsInfo.getStatisticsOfYear() + "\r\n" +
                    "请您前往记分奖惩管理软件进行查看，如有异议请及时与相关部门沟通！";
        }else {
            message = "尊敬的" + faultInfo.getResName() + "(工号:" + faultInfo.getResNum() + "), 您好 " + "\r\n" +
                    "您在" + DateUtils.date(faultInfo.getFaultTime()) + "因" + faultInfo.getFaultType() + "发生" + faultInfo.getFaultLevel() + "过错, " +
                    "具体事实:" + faultInfo.getFaultContent()+ ", 处理部门:" + faultInfo.getDisposalDept() + ", 处理人:" + faultInfo.getDisposalName() + "\r\n" +
                    "请您前往记分奖惩管理软件进行查看，如有异议请及时与相关部门沟通！";
        }

        log.debug("向{}推送消息", faultStandardVO.getWeChat());
        // 像这个倒霉蛋推送考核消息消息
        wechatService.sendMessage(faultStandardVO.getWeChat(), message);

        return true;
    }


    /**
     * 修改数据
     *
     * @param faultUpdateRequest 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public boolean update(FaultUpdateRequest faultUpdateRequest, LoginPrincipal loginPrincipal) {

        log.debug("service层接收到的参数: {}", faultUpdateRequest);
        FaultDTO faultDTO = new FaultDTO();
        BeanUtils.copyProperties(faultUpdateRequest, faultDTO);
        faultDTO.setFaultAssessRemark(faultUpdateRequest.getRemark());
        faultDTO.setResNum(StringUtils.fillStringBeforeString(String.valueOf(faultUpdateRequest.getResNum()), "0", 4));

        List<IdAndName> idAndNames = corresMapper.queryIdAndName();
        Map<Integer, String> mapOfCorres = MapUtils.byId(idAndNames);

        faultDTO.setFaultLevelOfNum(faultUpdateRequest.getFaultLevel());
        faultDTO.setUpdateName(loginPrincipal.getUserName());
        faultDTO.setFaultAssessRemark(faultUpdateRequest.getRemark());
        faultDTO.setDisposalResults("");
        faultDTO.setFaultRemark(faultUpdateRequest.getRemark());



        if(faultUpdateRequest.getFaultAssessInfos() != null) {
            for(FaultAssessInfoDTO faultAssessInfoDTO : faultUpdateRequest.getFaultAssessInfos()) {
                if(faultAssessInfoDTO.getDisposalDetails() == null) {
                    throw new ServiceException(ServiceCode.ERROR_INSERT, "考核分值/金额不能为空");
                } else if(faultAssessInfoDTO.getFaultAssessDisposalForm() == null) {
                    throw new ServiceException(ServiceCode.ERROR_INSERT, "考核类型不能为空");
                }
                String corresName = mapOfCorres.get(faultAssessInfoDTO.getFaultAssessDisposalForm());
                faultDTO.setDisposalResults( faultDTO.getDisposalResults().concat(corresName + ":" + faultAssessInfoDTO.getDisposalDetails() + "/"));
                faultAssessInfoDTO.setAssessResults(corresName + ":" + faultAssessInfoDTO.getDisposalDetails() + "/" + faultAssessInfoDTO.getAssessNum());
                faultAssessInfoDTO.setDisposalDetails(new BigDecimal(String.valueOf(faultAssessInfoDTO.getDisposalDetails())).setScale(2, BigDecimal.ROUND_HALF_UP));
                faultAssessInfoDTO.setFaultNum(faultDTO.getFaultNum());
            }
        }

        log.debug("开始处理修改过错信息业务逻辑, 参数: {}", faultDTO);
        // 向过错信息表新增数据
        int updateFault = faultMapper.update(faultDTO);
        if(updateFault <= 0) {
            throw new ServiceException(ServiceCode.ERROR_UPDATE, "向过错信息表(Fault)修改数据失败");
        }

        log.debug("开始向过错关联表中修改关联信息, 参数: {}", faultUpdateRequest);
        log.debug("执行修改FaultAssess信息前先删除对应已存在的信息,再将新的信息存入");
        faultAssessMapper.deleteById(faultDTO.getFaultNum());
        if(faultUpdateRequest.getFaultAssessInfos() != null && faultUpdateRequest.getFaultAssessInfos().length != 0) {
            int updateFaultAssess = faultAssessMapper.insert(faultUpdateRequest.getFaultAssessInfos());
            if(updateFaultAssess <= 0) {
                throw new ServiceException(ServiceCode.ERROR_UPDATE, "向过错关联表(FaultAssess)修改数据失败");
            }
        }
        return true;
    }

    /**
     * 通过主键删除数据
     *
     * @param faultNum 考核编号
     * @return 是否成功
     */
    @Override
    @Transactional
    public boolean deleteById(String faultNum) {
        int row1 = faultMapper.deleteById(faultNum);
        log.debug("删除过错信息完毕, 受影响行数:{}", row1);
        if(row1 <=0) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "删除过错信息失败");
        }
        int row2 = faultAssessMapper.deleteById(faultNum);
        log.debug("删除过错关联信息完毕, 受影响行数: {}", row2);

        return  true;
    }


    /**
     * 批量删除考核信息
     * @param faultNums
     * @return
     */
    @Transactional
    public boolean deleteBatch(String[] faultNums) {
        // TODO: 2024/3/21 修改删除方式 
        log.debug("开始执行批量删除考核信息Service层, 参数: {}, 参数大小:{}", faultNums, faultNums.length);
        int i = faultMapper.deleteBatch(faultNums);
        log.debug("考核信息表批量删除完毕, 受影响行数:{}", i);
        if(i != faultNums.length) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "批量删除考核信息失败, 数据不存在");
        }
        return faultAssessMapper.deleteBatch(faultNums);
    }


    /**
     *
     * @param list
     * @param loginPrincipal
     * @return
     */
    @Override
    @Transactional
    public boolean upload(List<FaultInsertOfTextpromptRequest> list, LoginPrincipal loginPrincipal) {

        List<IdAndName> idAndNames = corresMapper.queryIdAndName();
        Map<String, Integer> mapOfCorres = MapUtils.byName(idAndNames);
        List<IdAndName> idAndNamesOfCenter = organizationMapper.queryIdAndNameOfCenter();
        Map<String, Integer> mapOrganOfCenter = MapUtils.byName(idAndNamesOfCenter);
        List<IdAndName> idAndNamesOfDept = organizationMapper.queryIdAndNameOfDept();
        Map<String, Integer> mapOrganOfDept = MapUtils.byName(idAndNamesOfDept);

        // 获取最近的一个过错编号, 并往后延续赋值
        String maxFaultNum = faultMapper.getMaxFaultNum();
        if(maxFaultNum == null) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "数据库有误, 新增失败");
        }
        String substring = maxFaultNum.substring(1);
        log.debug("截取到的rid数值部分: {}", substring);
        int faultNum = Integer.parseInt(substring) + 1;
        int sum = 0;

        if("某某中心".equals(list.get(0).getResCenter())) {
            log.debug("数据第一条为示例数据, 不将其存入数据库, 移除前:{}", list);
            list = list.subList(1, list.size());
            log.debug("移除示例数据后: {}", list);
        }

        List<FaultInsertBatchDTO> insertBatchDTOS = new ArrayList<>();
        List<FaultAssessInfoDTO> faultAssessInfos = new ArrayList<>();

        for(FaultInsertOfTextpromptRequest request : list) {
            FaultInsertBatchDTO faultInsertBatchDTO = new FaultInsertBatchDTO();
            log.debug("被复制的数据: {}", request);
            BeanUtils.copyProperties(request, faultInsertBatchDTO);
            log.debug("将同名属性复制后: {}", faultInsertBatchDTO);
            // 先将处理结果赋值为空字符串, 方便后续字符串拼接不会出现开头为null的情况
            faultInsertBatchDTO.setDisposalResults("");
            faultInsertBatchDTO.setCreatName(loginPrincipal.getUserName());
            faultInsertBatchDTO.setResCenter(mapOrganOfCenter.get(request.getResCenter()));
            faultInsertBatchDTO.setResDept(mapOrganOfDept.get(request.getResDept()));
            faultInsertBatchDTO.setFaultType(mapOfCorres.get(request.getFaultType()));
            faultInsertBatchDTO.setFaultLevel(mapOfCorres.get(request.getFaultLevel()));
            faultInsertBatchDTO.setFaultDisposalForm(mapOfCorres.get(request.getFaultDisposalForm()));
            faultInsertBatchDTO.setOtherResults(mapOfCorres.get(request.getOtherResults()));
            faultInsertBatchDTO.setOtherDisposal(mapOfCorres.get(request.getOtherDisposal()));
            faultInsertBatchDTO.setDisposalLevel(mapOfCorres.get(request.getDisposalLevel()));
            faultInsertBatchDTO.setFaultNum("G" + (faultNum + sum++));
            faultInsertBatchDTO.setResNum(StringUtils.fillStringBeforeString(String.valueOf(request.getResNum()), "0", 4));

            if(request.getEarnestMoney() != null) {

                FaultAssessInfoDTO infoDTO = new FaultAssessInfoDTO();
                infoDTO.setFaultNum(faultInsertBatchDTO.getFaultNum());
                infoDTO.setFaultAssessDisposalForm(1026);
                infoDTO.setDisposalDetails(request.getEarnestMoney());
                infoDTO.setAssessNum(request.getEarnestMoneyNum());
                infoDTO.setAssessResults("保证金(元):" + request.getEarnestMoney() + "/" + request.getEarnestMoneyNum());
                faultInsertBatchDTO.setDisposalResults(faultInsertBatchDTO.getDisposalResults() + infoDTO.getAssessResults() + "/");
                faultAssessInfos.add(infoDTO);

            }
            if(request.getLossExpense() != null) {

                FaultAssessInfoDTO infoDTO = new FaultAssessInfoDTO();
                infoDTO.setFaultNum(faultInsertBatchDTO.getFaultNum());
                infoDTO.setFaultAssessDisposalForm(1027);
                infoDTO.setDisposalDetails(request.getLossExpense());
                infoDTO.setAssessNum(request.getLossExpenseNum());
                infoDTO.setAssessResults("损失费(元):" + request.getLossExpense() + "/" + request.getLossExpenseNum());
                faultInsertBatchDTO.setDisposalResults(faultInsertBatchDTO.getDisposalResults() + infoDTO.getAssessResults() + "/");
                faultAssessInfos.add(infoDTO);

            }
            if(request.getScore() != null) {

                FaultAssessInfoDTO infoDTO = new FaultAssessInfoDTO();
                infoDTO.setFaultNum(faultInsertBatchDTO.getFaultNum());
                infoDTO.setFaultAssessDisposalForm(1028);
                infoDTO.setDisposalDetails(request.getScore());
                infoDTO.setAssessNum(request.getScoreNum());
                infoDTO.setAssessResults("荣誉记分(分):" + request.getScore() + "/" + request.getScoreNum());
                faultInsertBatchDTO.setDisposalResults(faultInsertBatchDTO.getDisposalResults() + infoDTO.getAssessResults() + "/");
                faultAssessInfos.add(infoDTO);

            }
            if(request.getScoreOfMonth() != null) {

                FaultAssessInfoDTO infoDTO = new FaultAssessInfoDTO();
                infoDTO.setFaultNum(faultInsertBatchDTO.getFaultNum());
                infoDTO.setFaultAssessDisposalForm(1029);
                infoDTO.setDisposalDetails(request.getScoreOfMonth());
                infoDTO.setAssessNum(request.getScoreOfMonthNum());
                infoDTO.setAssessResults("月考核分(分):" + request.getScoreOfMonth() + "/" + request.getScoreOfMonthNum());
                faultInsertBatchDTO.setDisposalResults(faultInsertBatchDTO.getDisposalResults() + infoDTO.getAssessResults() + "/");
                faultAssessInfos.add(infoDTO);

            }
            if(request.getScoreOfYear() != null) {

                FaultAssessInfoDTO infoDTO = new FaultAssessInfoDTO();
                infoDTO.setFaultNum(faultInsertBatchDTO.getFaultNum());
                infoDTO.setFaultAssessDisposalForm(1030);
                infoDTO.setDisposalDetails(request.getScoreOfYear());
                infoDTO.setAssessNum(request.getScoreOfYearNum());
                infoDTO.setAssessResults("年考核分(分):" + request.getScoreOfYear() + "/" + request.getScoreOfYearNum());
                faultInsertBatchDTO.setDisposalResults(faultInsertBatchDTO.getDisposalResults() + infoDTO.getAssessResults() + "/");
                faultAssessInfos.add(infoDTO);

            }
            if(request.getKpi() != null) {

                FaultAssessInfoDTO infoDTO = new FaultAssessInfoDTO();
                infoDTO.setFaultNum(faultInsertBatchDTO.getFaultNum());
                infoDTO.setFaultAssessDisposalForm(1058);
                infoDTO.setDisposalDetails(request.getKpi());
                infoDTO.setAssessNum(request.getKpiNum());
                infoDTO.setAssessResults("kpi考核分(分):" + request.getKpi() + "/" + request.getKpiNum());
                faultInsertBatchDTO.setDisposalResults(faultInsertBatchDTO.getDisposalResults() + infoDTO.getAssessResults() + "/");
                faultAssessInfos.add(infoDTO);

            }

            insertBatchDTOS.add(faultInsertBatchDTO);

        }

        boolean i = true;
        boolean row = faultMapper.insertBatch(insertBatchDTOS);
        log.debug("向FaultAssess批量导入数据: {}", faultAssessInfos);
        if(!faultAssessInfos.isEmpty()) {
            i = faultAssessMapper.insertBatch(faultAssessInfos);
        }
        if(!row && !i){
            throw new ServiceException(ServiceCode.ERROR_INSERT, "向数据库批量导入数据失败");
        }
        return row;
    }
}
