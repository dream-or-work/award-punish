package cn.sanli.manage.service.impl;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.mapper.data1.CorresMapper;
import cn.sanli.manage.mapper.data1.OrganizationMapper;
import cn.sanli.manage.mapper.data1.RewardsMapper;
import cn.sanli.manage.mapper.data1.UserInfoMapper;
import cn.sanli.manage.pojo.dto.Rewards.*;
import cn.sanli.manage.pojo.entity.IdAndName;
import cn.sanli.manage.pojo.entity.Rewards;
import cn.sanli.manage.pojo.entity.UserInfo;
import cn.sanli.manage.pojo.vo.Rewards.*;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.RewardsService;
import cn.sanli.manage.service.WechatService;
import cn.sanli.manage.utils.DateUtils;
import cn.sanli.manage.utils.MapUtils;
import cn.sanli.manage.utils.StringUtils;
import cn.sanli.manage.web.ServiceCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * 奖励信息表(Rewards)表服务实现类
 *
 * @author wzy
 * @since 2023-12-07 16:06:49
 */
@Service("rewardsService")
@Slf4j
public class RewardsServiceImpl implements RewardsService {
    @Resource
    private RewardsMapper rewardsMapper;

    @Resource
    private OrganizationMapper organizationMapper;

    @Resource
    private CorresMapper corresMapper;

    @Resource
    private WechatService wechatService;

    @Resource
    private UserInfoMapper userInfoMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Rewards queryById(Integer id) {
        return rewardsMapper.queryById(id);
    }

    /**
     * 根据条件分页查询
     *
     * @param queryRequest 查询条件
     * @return 符合条件的数据
     */
    @Override
    public List<PagingQueryRewardsVO> pagingQuery(PageQueryOfRewardsDTO queryRequest) {
        return rewardsMapper.pagingQuery(queryRequest);
    }


    /**
     * 奖励信息积分统计
     *
     * @return 结果集
     */
    @Override
    public List<RewardsStatisticsInfoVO> statisticsRewardsOfPersonage(RewardsInfoStatisticsDTO rewardsInfoStatisticsDTO, LoginPrincipal loginPrincipal) {
        int roleId = loginPrincipal.getRoleId();
        log.debug("识别到的当事人: {}  主显权限: {}", loginPrincipal, roleId);

        // 根据查询条件搜索数据库
        log.debug("开始执行个人奖励记分统计, 参数:{}", rewardsInfoStatisticsDTO);
        List<RewardsStatisticsVO> rewardsStatisticsVOS
                = rewardsMapper.statisticsRewardsOfPersonage(rewardsInfoStatisticsDTO);
        log.debug("根据条件获取到{}条数据", rewardsStatisticsVOS.size());
        log.debug("根据条件获取到的数据: {}", rewardsStatisticsVOS);

        // 以工号为key(), 当前对象为value存入map
        Map<String, List<RewardsStatisticsVO>> map = new HashMap<>();
        List<RewardsStatisticsVO> listOfMap = new ArrayList<>();
        for (RewardsStatisticsVO rewardsStatisticsVO : rewardsStatisticsVOS) {
            if(map.containsKey(rewardsStatisticsVO.getNum())) {
                listOfMap = map.get(rewardsStatisticsVO.getNum());
                listOfMap.add(rewardsStatisticsVO);
                map.put(rewardsStatisticsVO.getNum(), listOfMap);
            }else {
                List<RewardsStatisticsVO> list = new ArrayList<>();
                list.add(rewardsStatisticsVO);
                map.put(rewardsStatisticsVO.getNum(), list);
            }
        }

        // 创建List集合用于接受最终数据并返回
        List<RewardsStatisticsInfoVO> list = new ArrayList<>();
        // 遍历map集合, 调用数据统计方法
        for (String key : map.keySet()) {
            RewardsStatisticsInfoVO statisticsInfo = this.getStatisticsInfo(map.get(key), DateUtils.getStatisticsTime());
            log.debug("从map中获取key为{}的数据: {}", key, statisticsInfo);
            list.add(statisticsInfo);
        }
        log.debug("将统计完的数据集合返回: {}", list);

        // 返回
        return list;
    }


    /**
     * 部门积分统计
     * @param loginPrincipal 当事人
     * @return 经过处理的结果集
     */
    public List<RewardsStatisticsInfoOfDeptVO> statisticsRewardsOfDept(RewardsInfoStatisticsDTO rewardsInfoStatisticsDTO, LoginPrincipal loginPrincipal){
        int roleId = loginPrincipal.getRoleId();
        log.debug("识别到的当事人: {}  主显权限: {}", loginPrincipal, roleId);
        // 根据查询条件搜索数据库
        List<RewardsStatisticsVO> rewardsStatisticsVOS
                = rewardsMapper.statisticsRewardsOfPersonage(rewardsInfoStatisticsDTO);
        log.debug("根据条件获取到{}条数据", rewardsStatisticsVOS.size());
        log.debug("根据条件获取到的数据: {}", rewardsStatisticsVOS);

        // 以部门id为key(), 当前对象为value存入map
        Map<String, List<RewardsStatisticsVO>> map = new HashMap<>();
        List<RewardsStatisticsVO> listOfMap = new ArrayList<>();
        for (RewardsStatisticsVO rewardsStatisticsVO : rewardsStatisticsVOS) {
            if(map.containsKey(rewardsStatisticsVO.getDept())) {
                listOfMap = map.get(rewardsStatisticsVO.getDept());
                listOfMap.add(rewardsStatisticsVO);
                map.put(rewardsStatisticsVO.getDept(), listOfMap);
            }else {
                List<RewardsStatisticsVO> list = new ArrayList<>();
                list.add(rewardsStatisticsVO);
                map.put(rewardsStatisticsVO.getDept(), list);
            }
        }

        // 创建List集合用于接受最终数据并返回
        List<RewardsStatisticsInfoOfDeptVO> list = new ArrayList<>();
        // 遍历map集合, 调用数据统计方法
        for (String key : map.keySet()) {
            RewardsStatisticsInfoVO statisticsInfo = this.getStatisticsInfo(map.get(key), DateUtils.getStatisticsTime());
            log.debug("从map中获取key为{}的数据: {}", key, statisticsInfo);
            RewardsStatisticsInfoOfDeptVO deptVO = new RewardsStatisticsInfoOfDeptVO();
            BeanUtils.copyProperties(statisticsInfo, deptVO);
            list.add(deptVO);
        }
        log.debug("将统计完的数据集合返回: {}", list);

        // 返回
        return list;
    }


    /**
     * 大口积分统计
     * @param loginPrincipal 当事人
     * @return 经过处理的结果集
     */
    public List<RewardsStatisticsInfoOfCenterVO> statisticsRewardsOfCenter(RewardsInfoStatisticsDTO rewardsInfoStatisticsDTO, LoginPrincipal loginPrincipal){
        int roleId = loginPrincipal.getRoleId();
        log.debug("识别到的当事人: {}  主显权限: {}", loginPrincipal, roleId);
        // 根据查询条件搜索数据库
        List<RewardsStatisticsVO> rewardsStatisticsVOS
                = rewardsMapper.statisticsRewardsOfPersonage(rewardsInfoStatisticsDTO);
        log.debug("根据条件获取到{}条数据", rewardsStatisticsVOS.size());
        log.debug("根据条件获取到的数据: {}", rewardsStatisticsVOS);

        // 以部门id为key(), 当前对象为value存入map
        Map<String, List<RewardsStatisticsVO>> map = new HashMap<>();
        List<RewardsStatisticsVO> listOfMap = new ArrayList<>();
        for (RewardsStatisticsVO rewardsStatisticsVO : rewardsStatisticsVOS) {
            if(map.containsKey(rewardsStatisticsVO.getCenter())) {
                listOfMap = map.get(rewardsStatisticsVO.getCenter());
                listOfMap.add(rewardsStatisticsVO);
                map.put(rewardsStatisticsVO.getCenter(), listOfMap);
            }else {
                List<RewardsStatisticsVO> list = new ArrayList<>();
                list.add(rewardsStatisticsVO);
                map.put(rewardsStatisticsVO.getCenter(), list);
            }
        }

        // 创建List集合用于接受最终数据并返回
        List<RewardsStatisticsInfoOfCenterVO> list = new ArrayList<>();
        // 遍历map集合, 调用数据统计方法
        for (String key : map.keySet()) {
            RewardsStatisticsInfoVO statisticsInfo = this.getStatisticsInfo(map.get(key), DateUtils.getStatisticsTime());
            log.debug("从map中获取key为{}的数据: {}", key, statisticsInfo);
             RewardsStatisticsInfoOfCenterVO deptVO = new RewardsStatisticsInfoOfCenterVO();
            BeanUtils.copyProperties(statisticsInfo, deptVO);
            list.add(deptVO);
        }
        log.debug("将统计完的数据集合返回: {}", list);

        // 返回
        return list;
    }


    private RewardsStatisticsInfoVO getStatisticsInfo(List<RewardsStatisticsVO> list, Map<String, Date[]> statisticsTime) {
        log.debug("调用getStatisticsInfo统计数据, 需处理的数据: {}  时间参数: {}", list, statisticsTime);
        RewardsStatisticsInfoVO rewardsStatisticsInfoVO = new RewardsStatisticsInfoVO();
        // 声明并初始化五个变量后续使用
        BigDecimal ofWeek = new BigDecimal("0.0");
        BigDecimal ofMonth = new BigDecimal("0.0");
        BigDecimal ofQuarter = new BigDecimal("0.0");
        BigDecimal ofHalf = new BigDecimal("0.0");
        BigDecimal ofYear = new BigDecimal("0.0");
        // 遍历数据集合
        for (RewardsStatisticsVO rewardsStatisticsVO : list) {
            log.debug("本次循环的数据: {}", rewardsStatisticsVO);
            // 根据考核时间判断
            if(rewardsStatisticsVO.getRewardsTime().getTime() >= statisticsTime.get(DateUtils.THISWEEK)[0].getTime()
                    &&
                    rewardsStatisticsVO.getRewardsTime().getTime() <= statisticsTime.get(DateUtils.THISWEEK)[1].getTime()) {
                ofWeek = ofWeek.add(rewardsStatisticsVO.getBonusPoints());
                ofMonth = ofMonth.add(rewardsStatisticsVO.getBonusPoints());
                ofQuarter = ofQuarter.add(rewardsStatisticsVO.getBonusPoints());
                ofHalf = ofHalf.add(rewardsStatisticsVO.getBonusPoints());
                ofYear = ofYear.add(rewardsStatisticsVO.getBonusPoints());
                continue;
            }else if(rewardsStatisticsVO.getRewardsTime().getTime() >= statisticsTime.get(DateUtils.THISMONTH)[0].getTime()
                    &&
                    rewardsStatisticsVO.getRewardsTime().getTime() <= statisticsTime.get(DateUtils.THISMONTH)[1].getTime()) {
                ofMonth = ofMonth.add(rewardsStatisticsVO.getBonusPoints());
                ofQuarter = ofQuarter.add(rewardsStatisticsVO.getBonusPoints());
                ofHalf = ofHalf.add(rewardsStatisticsVO.getBonusPoints());
                ofYear = ofYear.add(rewardsStatisticsVO.getBonusPoints());
                continue;
            }else if(rewardsStatisticsVO.getRewardsTime().getTime() >= statisticsTime.get(DateUtils.THISQUARTER)[0].getTime()
                    &&
                    rewardsStatisticsVO.getRewardsTime().getTime() <= statisticsTime.get(DateUtils.THISQUARTER)[1].getTime()) {
                ofQuarter = ofQuarter.add(rewardsStatisticsVO.getBonusPoints());
                ofHalf = ofHalf.add(rewardsStatisticsVO.getBonusPoints());
                ofYear = ofYear.add(rewardsStatisticsVO.getBonusPoints());
                continue;
            }else if(rewardsStatisticsVO.getRewardsTime().getTime() >= statisticsTime.get(DateUtils.THISHALF)[0].getTime()
                    &&
                    rewardsStatisticsVO.getRewardsTime().getTime() <= statisticsTime.get(DateUtils.THISHALF)[1].getTime()) {
                ofHalf = ofHalf.add(rewardsStatisticsVO.getBonusPoints());
                ofYear = ofYear.add(rewardsStatisticsVO.getBonusPoints());
                continue;
            }else if(rewardsStatisticsVO.getRewardsTime().getTime() >= statisticsTime.get(DateUtils.THISYEAR)[0].getTime()
                    &&
                    rewardsStatisticsVO.getRewardsTime().getTime() <= statisticsTime.get(DateUtils.THISYEAR)[1].getTime()) {
                ofYear = ofYear.add(rewardsStatisticsVO.getBonusPoints());
                continue;
            }
        }
        // 基本信息属性赋值
        rewardsStatisticsInfoVO.setName(list.get(0).getName());
        rewardsStatisticsInfoVO.setNum(list.get(0).getNum());
        rewardsStatisticsInfoVO.setDept(list.get(0).getDept());
        rewardsStatisticsInfoVO.setCenter(list.get(0).getCenter());
        // 积分信息赋值, 四舍五入保留一位小数
        rewardsStatisticsInfoVO.setStatisticsOfWeek(ofWeek.setScale(1, RoundingMode.HALF_UP));
        rewardsStatisticsInfoVO.setStatisticsOfMonth(ofMonth.setScale(1, RoundingMode.HALF_UP));
        rewardsStatisticsInfoVO.setStatisticsOfQuarter(ofQuarter.setScale(1, RoundingMode.HALF_UP));
        rewardsStatisticsInfoVO.setStatisticsOfHalf(ofHalf.setScale(1, RoundingMode.HALF_UP));
        rewardsStatisticsInfoVO.setStatisticsOfYear(ofYear.setScale(1, RoundingMode.HALF_UP));

        return rewardsStatisticsInfoVO;
    }

    /**
     * 新增数据
     *
     * @param rewardsInsertRequest 实例对象
     * @return 实例对象
     */
    @Override
    @Transactional
    public boolean insert(RewardsInsertRequest rewardsInsertRequest, LoginPrincipal loginPrincipal) {


//        List<IdAndName> idAndNames = organizationMapper.queryIdAndNameOfDept();
//        log.debug("部门信息表查询的内容: {}", idAndNames);
//        Map<String, Integer> map = MapUtils.toMap(idAndNames);

        log.debug("开始处理新增奖励业务逻辑: {}", rewardsInsertRequest);
        // 截取字符串中的数字
        String maxRId = rewardsMapper.getMaxRId();
        if(maxRId == null) {
            log.debug("获取数据库中最大奖励编号为null, 手动生成一个");
            maxRId = "R0";
        }
        String substring = maxRId.substring(1);
        log.debug("截取到的rid数值部分: {}", substring);
        // 数值加一生成新的rid并赋值
        rewardsInsertRequest.setRid("R" + (Integer.parseInt(substring) + 1));
        rewardsInsertRequest.setCreateName(loginPrincipal.getUserName());
        rewardsInsertRequest.setNum(StringUtils.fillStringBeforeString(rewardsInsertRequest.getNum(), "0", 4));
        // 获取对应的部门
//        String rewardsDept = String.valueOf(map.get(rewardsInsertRequest.getRewardsDept()));
//        log.debug("奖励部门: {}", rewardsInsertRequest.getRewardsDept());
//        rewardsInsertRequest.setRewardsDept(rewardsDept);
        int row = rewardsMapper.insert(rewardsInsertRequest);
        if(row <= 0) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "新增奖励信息失败");
        }

        List<IdAndName> idAndNamesOfDept = organizationMapper.queryIdAndNameOfDept();
        Map<Integer, String> mapOfOrg = MapUtils.byId(idAndNamesOfDept);
        List<IdAndName> idAndNamesOfType = corresMapper.queryIdAndName();
        Map<Integer, String> mapOfCor = MapUtils.byId(idAndNamesOfType);


        String rewardsCash = !("".equals(rewardsInsertRequest.getRewardsCash())) ?
                             "现金奖励: "+ rewardsInsertRequest.getRewardsCash() + "\r\n" :
                             "";
        String rewardsArticles = !("".equals(rewardsInsertRequest.getRewardsArticles())) ?
                "物品奖励: "+ rewardsInsertRequest.getRewardsArticles() + "\r\n" :
                "";
        String bonusPoints = rewardsInsertRequest.getBonusPoints().compareTo(BigDecimal.ZERO) != 0 ?
                "记分奖励: "+ rewardsInsertRequest.getBonusPoints() + "分\r\n" :
                "";

        UserInfo userInfo = userInfoMapper.selectByNum(rewardsInsertRequest.getNum());
        if(userInfo == null) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "数据库中没有当前奖励对象的用户信息");
        }
        log.debug("向{}推送消息", userInfo.getWechat());
        wechatService.sendMessage(userInfo.getWechat(), "尊敬的" + rewardsInsertRequest.getName() + "(工号:" + rewardsInsertRequest.getNum() + "), 您好 " +"\r\n" +
                                                                 "您在" + DateUtils.date(rewardsInsertRequest.getRewardsTime()) + "因" + mapOfCor.get(Integer.valueOf(rewardsInsertRequest.getRewardsType())) + "方面表现突出被奖励: "+ "\r\n" +
                                                                 "奖励事由: " + rewardsInsertRequest.getReasons() + ", 奖励部门: " + mapOfOrg.get(Integer.valueOf(rewardsInsertRequest.getRewardsDept())) +
                                                                 ", 奖励人: " + rewardsInsertRequest.getRewardsName() + "\r\n" +
                                                                 rewardsCash + rewardsArticles + bonusPoints);

        return true;
    }

    /**
     * 修改数据
     *
     * @param updateRequest 实例对象
     * @return 实例对象
     */
    @Override
    public boolean update(RewardsUpdateRequest updateRequest, LoginPrincipal loginPrincipal) {
//        List<IdAndName> idAndNames = organizationMapper.queryIdAndName();
//        Map<String, Integer> map = MapUtils.toMap(idAndNames);
//        updateRequest.setRewardsDept(String.valueOf(map.get(updateRequest.getRewardsDept())));
        updateRequest.setUpdateName(loginPrincipal.getUserName());
        updateRequest.setNum(StringUtils.fillStringBeforeString(updateRequest.getNum(), "0", 4));

        return rewardsMapper.update(updateRequest) > 0;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    @Transactional
    public boolean deleteById(Integer id) {
        return rewardsMapper.deleteById(id) > 0;
    }

    @Override
    public boolean upload(List<RewardsInsertRequest> list, LoginPrincipal loginPrincipal) {
        log.debug("开始执行批量新增数据, 参数: {}", list);

        String maxRId = rewardsMapper.getMaxRId();
        String substring = maxRId.substring(1);
        int rId = Integer.parseInt(substring) + 1;
        int sum = 0;
        List<IdAndName> idAndNamesOfCenter = organizationMapper.queryIdAndNameOfCenter();
        Map<String, Integer> mapOrganOfCenter = MapUtils.byName(idAndNamesOfCenter);
        List<IdAndName> idAndNamesOfDept = organizationMapper.queryIdAndNameOfDept();
        Map<String, Integer> mapOrganOfDept = MapUtils.byName(idAndNamesOfDept);
        List<IdAndName> idAndNames1 = corresMapper.queryIdAndName();
        Map<String, Integer> mapOfCorres = MapUtils.byName(idAndNames1);

        for (RewardsInsertRequest rewardsInsertRequest : list) {
            rewardsInsertRequest.setRid("R" + ( rId + sum++));
            rewardsInsertRequest.setCreateName(loginPrincipal.getUserName());
            rewardsInsertRequest.setRewardsDept(String.valueOf(mapOrganOfDept.get(rewardsInsertRequest.getRewardsDept())));
            rewardsInsertRequest.setDept(String.valueOf(mapOrganOfDept.get(rewardsInsertRequest.getDept())));
            rewardsInsertRequest.setCenter(String.valueOf(mapOrganOfCenter.get(rewardsInsertRequest.getCenter())));
            rewardsInsertRequest.setRewardsType(String.valueOf(mapOfCorres.get(rewardsInsertRequest.getRewardsType())));
            log.debug("阿萨德烤炉好的附属卡等哈: {}", rewardsInsertRequest);
        }


        int row = rewardsMapper.upload(list);
        if(!(list.size() == row)) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "批量新增数据有误");
        }
        return true;
    }
}
