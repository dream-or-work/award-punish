package cn.sanli.manage.service;


import cn.sanli.manage.pojo.dto.System.OrganizationDTO;
import cn.sanli.manage.pojo.dto.System.UpdateOrganizationDTO;
import cn.sanli.manage.pojo.entity.IdAndName;
import cn.sanli.manage.pojo.entity.Organization;
import cn.sanli.manage.pojo.vo.DeptNameVO;

import java.util.List;

/**
 * 奖惩组织机构(organization)表服务接口
 *
 * @author makejava
 * @since 2023-12-04 15:33:22
 */
public interface OrganizationService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Organization queryById(Integer id);

    /**
     * 新增数据
     *
     * @param organization 实例对象
     * @return 实例对象
     */
    Organization insert(Organization organization);

    /**
     * 修改数据
     *
     * @param organization 实例对象
     * @return 实例对象
     */
    Organization update(Organization organization);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    List<DeptNameVO> selectMoreDept(List<Integer> centerIdList);
    /**
     * 查询全部大口和部门
     * @return
     */
    List<Organization> findNodes();

    /**
     * 添加组织结构
     * @param organizationDTO 添加信息
     */
    void saveOrganization(OrganizationDTO organizationDTO);

    /**
     * 修改组织结构
     * @param organizationDTO 修改信息
     */
    void updateOrganization(UpdateOrganizationDTO organizationDTO);

    /**
     * 删除组织结构
     * @param id 组织结构id
     */
    void deleteOrganization(Integer id);

    List<IdAndName> queryIdAndName();
}

