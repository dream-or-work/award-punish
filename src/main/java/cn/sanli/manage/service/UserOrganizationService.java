package cn.sanli.manage.service;

import cn.sanli.manage.pojo.dto.User.DeleteUserOrganizationDTO;
import cn.sanli.manage.pojo.dto.User.UserOrganizationListDTO;
import cn.sanli.manage.pojo.dto.User.UserOrganizationDTO;
import cn.sanli.manage.pojo.entity.UserOrganization;
import cn.sanli.manage.pojo.vo.UserOrganizationVO;
import com.github.pagehelper.PageInfo;


/**
 * 用户组织关联表(userOrganization)表服务接口
 *
 * @author makejava
 * @since 2023-12-04 15:34:31
 */
public interface UserOrganizationService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserOrganization queryById(Integer id);

    /**
     * 新增数据
     *
     * @param userorganization 实例对象
     * @return 实例对象
     */
    UserOrganization insert(UserOrganization userorganization);

    /**
     * 修改数据
     *
     * @param userorganization 实例对象
     * @return 实例对象
     */
    UserOrganization update(UserOrganization userorganization);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 获取用户组织结构列表
     * @param userOrganizationDTO
     * @return
     */
    PageInfo<UserOrganizationVO> findByPage(UserOrganizationDTO userOrganizationDTO);

    void saveUserOrganization(UserOrganizationListDTO info);

    void updateUserOrganization(UserOrganizationListDTO info);

    void deleteUserOrganization(DeleteUserOrganizationDTO info);
}

