package cn.sanli.manage.service;

import cn.sanli.manage.pojo.entity.FaultAssess;


/**
 * 过错关联表(FaultAssess)表服务接口
 *
 * @author wzy
 * @since 2023-12-07 16:42:56
 */
public interface FaultAssessService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    FaultAssess queryById(Integer id);

    /**
     * 新增数据
     *
     * @param faultAssess 实例对象
     * @return 实例对象
     */
    boolean insert(FaultAssess faultAssess);

    /**
     * 修改数据
     *
     * @param faultAssess 实例对象
     * @return 实例对象
     */
    boolean update(FaultAssess faultAssess);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}
