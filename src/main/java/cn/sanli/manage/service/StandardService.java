package cn.sanli.manage.service;


import cn.sanli.manage.pojo.dto.User.SaveStandardDTO;
import cn.sanli.manage.pojo.dto.User.StandardEnableDTO;
import cn.sanli.manage.pojo.entity.Standard;

import java.util.List;

/**
 * 记分提醒标准表(standard)表服务接口
 *
 * @author makejava
 * @since 2023-12-04 15:34:08
 */
public interface StandardService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Standard queryById(Integer id);

    /**
     * 新增数据
     *
     * @param standard 实例对象
     * @return 实例对象
     */
    Standard insert(Standard standard);

    /**
     * 修改数据
     *
     * @param standard 实例对象
     * @return 实例对象
     */
    Standard update(Standard standard);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 添加记分提醒标准
     * @param info
     */
    void saveStandard(SaveStandardDTO info);

    /**
     * 修改记分提醒标准
     * @param standard
     */
    void updateStandard(Standard standard);

    /**
     * 修改是否启用
     * @param isEnable
     */
    void updateIsEnable(StandardEnableDTO isEnable);

    Standard findStandard(Integer id);

    /**
     * 获取记分提醒标准列表
     * @return
     */
    List<Standard> findStandardList();
}

