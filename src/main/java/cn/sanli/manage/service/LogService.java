package cn.sanli.manage.service;


import cn.sanli.manage.pojo.entity.Log;

/**
 * 日志信息表(log)表服务接口
 *
 * @author makejava
 * @since 2023-12-04 15:33:14
 */
public interface LogService {


    /**
     * 添加日志信息
     *
     * @param l 日志信息对象
     */
    void addLog(Log l);




}

