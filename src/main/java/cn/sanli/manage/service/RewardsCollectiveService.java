package cn.sanli.manage.service;

import cn.sanli.manage.pojo.dto.Rewards.RewardsCollectiveDTO;
import cn.sanli.manage.pojo.dto.Rewards.RewardsInsertRequest;
import cn.sanli.manage.pojo.dto.Rewards.SaveRewardsCollectiveDTO;
import cn.sanli.manage.pojo.dto.Rewards.UpdateRewardsCollectiveDTO;
import cn.sanli.manage.pojo.dto.Role.RoleAndOrganDTO;
import cn.sanli.manage.pojo.dto.User.BatchDeleteUserDTO;
import cn.sanli.manage.pojo.entity.RewardsCollective;
import cn.sanli.manage.pojo.vo.Rewards.RewardsCollectiveVO;
import cn.sanli.manage.security.LoginPrincipal;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author lu
* @description 针对表【rewards_collective(集体奖励表)】的数据库操作Service
* @createDate 2024-04-11 10:42:15
*/
public interface RewardsCollectiveService extends IService<RewardsCollective> {

    RewardsCollectiveVO selectAllById(@Param("id") Integer id);

    List<RewardsCollectiveVO> findByPage(RewardsCollectiveDTO rewardsCollectiveDTO, LoginPrincipal loginPrincipal);

    boolean saveRewardsCollective(SaveRewardsCollectiveDTO rewardsCollectiveDTO, LoginPrincipal loginPrincipal);

    boolean updateRewardsCollective(UpdateRewardsCollectiveDTO rewardsCollectiveDTO, LoginPrincipal loginPrincipal);

    boolean deleteById(Integer id);

    boolean importData(List<SaveRewardsCollectiveDTO> faultInsertRequests, LoginPrincipal loginPrincipal);

    boolean batchDelete(BatchDeleteUserDTO info);
}
