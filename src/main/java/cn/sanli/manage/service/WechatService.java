package cn.sanli.manage.service;

import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description:
 * @author: lsk
 * @create: 2023-12-07 16:53
 * @since jdk1.8
 **/
public interface WechatService {

     Map<String, Object> sendMessage(String touser, String content);
}

