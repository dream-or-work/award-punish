package cn.sanli.manage.service;


import cn.sanli.manage.pojo.dto.Rewards.*;
import cn.sanli.manage.pojo.entity.Rewards;
import cn.sanli.manage.pojo.vo.Rewards.PagingQueryRewardsVO;
import cn.sanli.manage.pojo.vo.Rewards.RewardsStatisticsInfoOfCenterVO;
import cn.sanli.manage.pojo.vo.Rewards.RewardsStatisticsInfoOfDeptVO;
import cn.sanli.manage.pojo.vo.Rewards.RewardsStatisticsInfoVO;
import cn.sanli.manage.security.LoginPrincipal;

import java.util.List;

/**
 * 奖励信息表(Rewards)表服务接口
 *
 * @author wzy
 * @since 2023-12-07 16:06:46
 */
public interface RewardsService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Rewards queryById(Integer id);

    /**
     * 根据条件分页查询奖励信息
     *
     * @param queryRequest 查询条件
     * @return 符合条件的奖励信息集合
     */
    List<PagingQueryRewardsVO> pagingQuery(PageQueryOfRewardsDTO queryRequest);

    /**
     * 个人奖励统计
     * @param loginPrincipal
     * @return
     */
    List<RewardsStatisticsInfoVO> statisticsRewardsOfPersonage(RewardsInfoStatisticsDTO rewardsInfoStatisticsDTO, LoginPrincipal loginPrincipal);

    /**
     * 部门奖励统计
     * @param loginPrincipal
     * @return
     */
    List<RewardsStatisticsInfoOfDeptVO> statisticsRewardsOfDept(RewardsInfoStatisticsDTO rewardsInfoStatisticsDTO, LoginPrincipal loginPrincipal);

    /**
     * 大口奖励统计
     * @param loginPrincipal
     * @return
     */
    List<RewardsStatisticsInfoOfCenterVO> statisticsRewardsOfCenter(RewardsInfoStatisticsDTO rewardsInfoStatisticsDTO, LoginPrincipal loginPrincipal);

    /**
     * 新增数据
     *
     * @param rewardsInsertRequest 实例对象
     * @return 实例对象
     */
    boolean insert(RewardsInsertRequest rewardsInsertRequest, LoginPrincipal loginPrincipal);

    /**
     * 修改数据
     *
     * @param updateRequest 实例对象
     * @return 实例对象
     */
    boolean update(RewardsUpdateRequest updateRequest, LoginPrincipal loginPrincipal);


    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);


    /**
     * 批量导入奖励信息
     *
     * @param list
     * @return 是否成功
     */
    boolean upload(List<RewardsInsertRequest> list, LoginPrincipal loginPrincipal);

}
