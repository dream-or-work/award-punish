package cn.sanli.manage.service;


import cn.sanli.manage.pojo.dto.Role.AssignRoleDto;
import cn.sanli.manage.pojo.dto.Role.AssignRoleLvDto;
import cn.sanli.manage.pojo.entity.Role;
import cn.sanli.manage.pojo.entity.UserRole;
import cn.sanli.manage.security.LoginPrincipal;

import java.util.List;
import java.util.Map;

/**
 * 用户组织关联表(userRole)表服务接口
 *
 * @author makejava
 * @since 2023-12-04 15:34:39
 */
public interface UserRoleService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserRole queryById(Integer id);

    /**
     * 新增数据
     *
     * @param userrole 实例对象
     * @return 实例对象
     */
    UserRole insert(UserRole userrole);

    /**
     * 修改数据
     *
     * @param userrole 实例对象
     * @return 实例对象
     */
    UserRole update(UserRole userrole);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 查询所有角色
     * @return 角色集合
     */
    Map<String, Object> findAllRoles();

    /**
     * 查询所有角色 -角色数据回显
     * @return 角色集合
     */
    Map<String, Object> findAllRoles(String num);

    /**
     * 用户分配角色
     * @param assignRoleDto 请求参数
     */
    void assignRole(AssignRoleDto assignRoleDto);

    /**
     * 指定主权限
     * @param assignRoleLvDto 请求参数
     */
    void assignRoleLv(AssignRoleLvDto assignRoleLvDto);

    List<Role> findRoles(LoginPrincipal message);
}

