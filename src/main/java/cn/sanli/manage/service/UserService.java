package cn.sanli.manage.service;

import cn.sanli.manage.pojo.dto.Login.LoginUserDTO;
import cn.sanli.manage.pojo.vo.LoginUserVO;
import cn.sanli.manage.pojo.vo.UserMessage;
import org.springframework.stereotype.Service;

/**
 * 处理管理员相关操作
 */
@Service
public interface UserService {

    /**
     * 管理员登录查询的service层
     * @param loginUserDTO
     * @return
     */
    String  login(LoginUserDTO loginUserDTO);

    /**
     * 查询用户信息的service层
     * @param num
     * @return
     */
    UserMessage message(String num);


}
