package cn.sanli.manage.service;

import cn.sanli.manage.pojo.dto.System.CategoryDTO;
import cn.sanli.manage.pojo.dto.System.CorresDTO;
import cn.sanli.manage.pojo.dto.System.UpdateCategoryDTO;
import cn.sanli.manage.pojo.dto.System.UpdateCorresDTO;
import cn.sanli.manage.pojo.entity.Corres;
import cn.sanli.manage.security.LoginPrincipal;

import java.util.List;


/**
 * 字段映射表(corres)表服务接口
 *
 * @author makejava
 * @since 2023-12-04 15:32:56
 */
public interface CorresService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Corres queryById(Integer id);

    /**
     * 新增数据
     *
     * @param corres 实例对象
     * @return 实例对象
     */
    Corres insert(Corres corres);

    /**
     * 修改数据
     *
     * @param corres 实例对象
     * @return 实例对象
     */
    Corres update(Corres corres);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 查询菜单
     * @param message 登录用户信息
     * @return 菜单列表
     */
    List<Corres> findNodes(LoginPrincipal message);

    /**
     * 查询考核分级
     * @param message 登录用户信息
     * @return
     */
    List<Corres> findAssess(LoginPrincipal message);

    /**
     * 查询处理类别
     * @return
     */
    List<Corres> findAssessCategory();

    /**
     * 查询处理形式
     * @return
     */
    List<Corres> findAssessWay();

    /**
     * 查询其他考核方式
     * @return
     */
    List<Corres> findOtherAssessWay();

    /**
     * 查询其他荣誉处理
     * @return
     */
    List<Corres> findOtherRewardWay();

    /**
     * 查询奖励分级
     * @param message
     * @return
     */
    List<Corres> findReward(LoginPrincipal message);

    /**
     * 查询过错级别
     * @return
     */
    List<Corres> findFaultLevel();

    /**
     * 查询处理方式
     * @return
     */
    List<Corres> findPunish();

    /**
     * 查询奖励形式
     * @return
     */
    List<Corres> findRewardWay();

    /**
     * 添加菜单
     * @param corresDTO 添加信息
     */
    void saveMenu(CorresDTO corresDTO);

    /**
     * 添加奖励形式
     * @param categoryDTO
     */
    void saveRewardWay(CategoryDTO categoryDTO);

    /**
     * 添加处理方式
     * @param categoryDTO
     */
    void savePunish(CategoryDTO categoryDTO);
    /**
     * 修改菜单
     * @param corresDTO 修改信息
     */
    void updateMenu(UpdateCorresDTO corresDTO);

    /**
     * 修改处理方式或奖励形式
     * @param categoryDTO
     */
    void updateCategory(UpdateCategoryDTO categoryDTO);
    /**
     * 删除菜单
     * @param id 菜单id
     */
    void deleteMenu(Integer id);


}

