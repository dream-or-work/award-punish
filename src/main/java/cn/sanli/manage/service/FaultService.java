package cn.sanli.manage.service;

import cn.sanli.manage.pojo.dto.Fault.*;
import cn.sanli.manage.pojo.dto.Rewards.RewardsInsertRequest;
import cn.sanli.manage.pojo.entity.Fault;
import cn.sanli.manage.pojo.vo.Fault.*;
import cn.sanli.manage.security.LoginPrincipal;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 过错信息表(Fault)表服务接口
 *
 * @author wzy
 * @since 2023-12-07 16:58:18
 */
@Service
public interface FaultService {

    /**
     * 通过ID查询单条数据
     *
     * @param faultNum 主键
     * @return 实例对象
     */
    Fault queryById(String faultNum);


    /**
     * 根据条件分页查询过错信息
     *
     * @param queryRequest 查询条件
     * @return 符合条件的过错信息集合
     */
    List<FaultPagingQueryVO> pagingQuery(PageQueryOfFaultRequestDTO queryRequest);

    /**
     * 统计个人积分
     * @param loginPrincipal 当事人
     * @return 合适的数据
     */
    List<FaultStatisticsInfoVO> statisticsFaultOfPersonage(LoginPrincipal loginPrincipal, FaultInfoStatisticsDTO statisticsRequest);


    /**
     * 统计部门积分
     * @param loginPrincipal 当事人
     * @return 合适的数据
     */
    List<FaultStatisticsInfoOfDeptVO> statisticsFaultOfDept(LoginPrincipal loginPrincipal, FaultInfoStatisticsDTO statisticsRequest);


    /**
     * 统计大口积分
     * @param loginPrincipal 当事人
     * @return 合适的数据
     */
    List<FaultStatisticsInfoOfCenterVO> statisticsFaultOfCenter(LoginPrincipal loginPrincipal, FaultInfoStatisticsDTO statisticsRequest);


    /**
     * 新增数据
     *
     * @param faultInsertRequest 实例对象
     * @return 实例对象
     */
    boolean insert(FaultInsertRequest faultInsertRequest, LoginPrincipal loginPrincipal);


    /**
     * 修改数据
     *
     * @param faultUpdateRequest 实例对象
     * @return 实例对象
     */
    boolean update(FaultUpdateRequest faultUpdateRequest, LoginPrincipal loginPrincipal);

//    /**
//     * 修改数据
//     *
//     * @param faultAndFaultAssessRequest 实例对象
//     * @return 实例对象
//     */
//    boolean update(FaultAndFaultAssessRequest faultAndFaultAssessRequest);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);


    boolean deleteBatch(String[] faultNums);



    /**
     * 批量导入考核信息
     *
     * @param list
     * @return 是否成功
     */
    boolean upload(List<FaultInsertOfTextpromptRequest> list, LoginPrincipal loginPrincipal);



}
