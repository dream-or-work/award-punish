package cn.sanli.manage.service;


import cn.sanli.manage.pojo.dto.User.*;
import cn.sanli.manage.pojo.entity.UserInfo;
import cn.sanli.manage.pojo.vo.UserInfoListVO;
import cn.sanli.manage.pojo.vo.UserInfoVO;
import cn.sanli.manage.security.LoginPrincipal;
import com.github.pagehelper.PageInfo;

/**
 * 用户信息表(userInfo)表服务接口
 *
 * @author makejava
 * @since 2023-12-04 15:34:25
 */
public interface UserInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserInfo queryById(Integer id);

    /**
     * 新增数据
     *
     * @param  userinfo
     * @return 实例对象
     */
    UserInfo insert(UserInfo userinfo);

    /**
     * 修改数据
     *
     * @param  info 修改信息
     * @return
     */
    UserInfo update(UpdateUserDTO info, LoginPrincipal message);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    PageInfo<UserInfoVO> findByPage(UserInfoDTO userInfoDto, LoginPrincipal message);

    PageInfo<UserInfoListVO> findListByPage(UserInfoDTO userInfoDto, LoginPrincipal loginPrincipal);

    /**
     * 添加用户
     *
     * @param info 用户信息
     */
    void saveUser(UserDTO info, LoginPrincipal message);

    /**
     * 账号锁定
     *
     * @param userIsLockDto 用户账号有无被锁定【1：正常使用，2：账号被锁定】
     */
    void updateIsLock(UserIsLockDTO userIsLockDto);

    /**
     * 删除用户
     *
     * @param id 用户id
     */
    void updateIsDelete(Integer id, LoginPrincipal message);

    /**
     * 批量删除
     *
     * @param info 用户id集合
     */
    void batchDelete(BatchDeleteUserDTO info, LoginPrincipal message);

    /**
     * 重置密码
     *
     * @param id 用户id
     */
    void resetPassWord(Integer id);

    /**
     * 修改当前登录用户
     *
     * @param info    修改信息
     * @param message 登录用户信息
     * @return
     */
    UserInfo updateLoginUser(UpdateLoginUserDTO info, LoginPrincipal message);

    /**
     * 查询工号是否存在
     * @param num 工号
     */
    void findByNum(String num);

    /**
     * 查询用户信息
     *
     * @param num 工号
     * @return
     */
    UserInfoVO findUserInfo(String num);

    UserInfoListVO findListUserInfo(String num);


    //void updateUserStandard(UserStandardDTO info);
}

