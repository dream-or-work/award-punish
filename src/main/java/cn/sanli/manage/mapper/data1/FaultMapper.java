package cn.sanli.manage.mapper.data1;

import cn.sanli.manage.pojo.dto.Fault.*;
import cn.sanli.manage.pojo.entity.Fault;
import cn.sanli.manage.pojo.vo.Fault.FaultInfo;
import cn.sanli.manage.pojo.vo.Fault.FaultPagingQueryVO;
import cn.sanli.manage.pojo.vo.Fault.FaultStatisticsVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectKey;

import java.util.List;

/**
 * 过错信息表(Fault)表数据库访问层
 *
 * @author wzy
 * @since 2023-12-07 16:58:13
 */
@Mapper
public interface FaultMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param faultNum 主键
     * @return 实例对象
     */
    Fault queryById(String faultNum);


    /**
     * 通过工号查询多条数据
     *
     * @param faultNum 主键
     * @return 实例对象
     */
    FaultInfo queryFaultInfoById(String faultNum);


    /**
     * 统计总行数
     *
     * @param fault 查询条件
     * @return 总行数
     */
    long count(Fault fault);

    /**
     * 查询最新上一条fault_num的值
     * @return 最新上一条fault_num的值
     */
    String getMaxFaultNum();

    // 根据工号查询所有过错信息
    List<FaultStatisticsVO> queryByNumber(Integer number);

    // 根据工号查询所有过错信息
    List<FaultStatisticsVO> queryScoreByNumber(String number);

    /**
     * 根据条件分页查询过错信息
     *
     * @param queryRequest 查询条件
     * @return 符合条件的过错信息集合
     */
    List<FaultPagingQueryVO> pagingQuery(PageQueryOfFaultRequestDTO queryRequest);

    /**
     * 根据条件分页查询过错信息
     *
     * @param queryRequest 查询条件
     * @return 符合条件的过错信息集合
     */
    List<FaultPagingQueryVO> pagingQueryOfFault(PageQueryOfFaultRequestDTO queryRequest);

    /**
     * 根据条件查询统计所需的数据
     *
     * @param statisticsRequest 当事人
     * @return 符合条件的数据
     */
    List<FaultStatisticsVO> statisticsFaultOfPersonage(FaultInfoStatisticsDTO statisticsRequest);

    /**
     * 新增过错信息
     * @param faultDTO 实例对象
     * @return 受影响行数
     */
    @SelectKey(statement="select LAST_INSERT_ID()",keyProperty = "id",before = false,resultType = Integer.class)
    int insert(FaultInsertDTO faultDTO);


    boolean insertBatch(List<FaultInsertBatchDTO> list);

    /**
     * 修改过错信息
     * @param faultDTO 实例对象
     * @return 受影响行数
     */
    int update(FaultDTO faultDTO);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    /**
     * 批量删除
     * @param faultNums
     * @return
     */
    int deleteBatch(String[] faultNums);

//    /**
//     * 新增数据
//     *
//     * @param fault 实例对象
//     * @return 影响行数
//     */
//    @SelectKey(statement="select LAST_INSERT_ID()",keyProperty = "id",before = false,resultType = Integer.class)
//    int insert(Fault fault);

//    /**
//     * 批量新增数据（MyBatis原生foreach方法）
//     *
//     * @param entities List<Fault> 实例对象列表
//     * @return 影响行数
//     */
//    int insertBatch(@Param("entities") List<Fault> entities);

//    /**
//     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
//     *
//     * @param entities List<Fault> 实例对象列表
//     * @return 影响行数
//     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
//     */
//    int insertOrUpdateBatch(@Param("entities") List<Fault> entities);

//    /**
//     * 修改数据
//     *
//     * @param fault 实例对象
//     * @return 影响行数
//     */
//    int update(Fault fault);



}

