package cn.sanli.manage.mapper.data1;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import cn.sanli.manage.pojo.dto.User.SaveStandardDTO;
import cn.sanli.manage.pojo.entity.Standard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description:
 * @author: lsk
 * @create: 2023-12-04 15:52
 * @since jdk1.8
 **/
@Mapper
public interface StandardMapper extends BaseMapper<Standard> {

    void update(Standard standard);

    int deleteByPrimaryKey(Integer id);

    Standard selectByPrimaryKey(Integer id);

    //获取记分提醒标准列表
    List<Standard> findAllStandard();

    void saveStandard(SaveStandardDTO standard);

    void updateSelective(Standard standard);

    void updateIsEnableById(@Param("isEnable") Boolean isEnable, @Param("id") Integer id);
}