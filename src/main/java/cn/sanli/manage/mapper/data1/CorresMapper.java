package cn.sanli.manage.mapper.data1;
import cn.sanli.manage.pojo.entity.IdAndName;
import org.apache.ibatis.annotations.Param;

import cn.sanli.manage.pojo.entity.Corres;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description:
 * @author: lsk
 * @create: 2023-12-04 15:52
 * @since jdk1.8
 **/
@Mapper
public interface CorresMapper extends BaseMapper<Corres> {
    Corres selectByPrimaryKey(Integer id);

    void update(Corres corres);

    int deleteByPrimaryKey(Integer id);

    //查看全部
    List<Corres> selectAll();

    //查看全部考核和奖励
    List<Corres> selectAwardPunish();

    //查看大口考核和奖励
    List<Corres> selectCenter();

    //查看部门考核和奖励
    List<Corres> selectDept();
    //查询考核分级
    List<Corres> selectAssess();
    //查询大口考核分级
    List<Corres> selectCenterAssess();
    //查询部门考核分级
    List<Corres> selectDeptAssess();
    //查询奖励分级
    List<Corres> selectReward();
    //查询大口奖励分级
    List<Corres> selectCenterReward();
    //查询部门奖励分级
    List<Corres> selectDeptReward();

    //查询处理类别
    List<Corres> selectAssessCategory();
    //查询过错级别
    List<Corres> selectFaultLevel();
    //查询处理形式
    List<Corres> selectAssessWay();
    //查询其他考核方式
    List<Corres> selectOtherAssessWay();
    //查询其他荣誉处理
    List<Corres> selectOtherRewardWay();
    //查询处理方式
    List<Corres> selectPunish();

    //查询奖励形式
    List<Corres> selectRewardWay();

    Corres selectByNameAndParentId(@Param("name") String name, @Param("parentId") Integer parentId);

    Corres selectByPunishName(@Param("name") String name);
    Corres selectByRewardWayName(@Param("name") String name);
    //添加菜单
    void insertMenu(Corres corres);

    //添加处理方式
    void insertPunish(Corres corres);

    //添加奖励形式
    void insertRewardWay(Corres corres);
    //修改菜单
    void updateMenu(Corres corres);

    //修改处理方式或奖励形式
    void updateCategory(Corres corres);
    //查询子节点个数
    int countByParentId(@Param("parentId") Integer parentId);

    //删除菜单
    void updateIsDeleteById(@Param("id") Integer id);

    List<IdAndName> queryIdAndName();

}