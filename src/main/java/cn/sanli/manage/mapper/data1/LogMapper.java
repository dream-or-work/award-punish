package cn.sanli.manage.mapper.data1;

import cn.sanli.manage.pojo.entity.Log;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description:
 * @author: lsk
 * @create: 2023-12-04 15:52
 * @since jdk1.8
 **/

public interface LogMapper extends BaseMapper<Log> {

    /**
     * 添加日志信息
     *
     * @param l 日志信息对象
     * @return 影响行数
     */
    int addLog(Log l);
}