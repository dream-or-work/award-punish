package cn.sanli.manage.mapper.data1;
import cn.sanli.manage.pojo.dto.User.UserOrganizationDTO;
import cn.sanli.manage.pojo.vo.UserOrganizationVO;
import org.apache.ibatis.annotations.Param;

import cn.sanli.manage.pojo.entity.UserOrganization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description:
 * @author: lsk
 * @create: 2023-12-04 15:52
 * @since jdk1.8
 **/
@Mapper
public interface UserOrganizationMapper extends BaseMapper<UserOrganization> {

    UserOrganization selectByPrimaryKey(Integer id);

    void update(UserOrganization userorganization);

    int deleteByPrimaryKey(Integer id);

    //根据工号和组织id查询用户组织
    UserOrganization selectByNumAndOrganId(@Param("num") String num, @Param("organId") Integer organId);

    List<Integer> selectOrganIdByNum(@Param("num") String num);

    List<Integer> selectOrganIdByNumOfCenter(@Param("num") String num);

    List<Integer> selectOrganIdByNumOfDept(@Param("num") String num);

    //添加用户组织结构
    void insertSelective(UserOrganization userOrganization);

    void insertAll(@Param("num") String num, @Param("organId") Integer organId);

    //修改用户组织结构
    void updateSelective(UserOrganization userOrganization);

    void updateOrganId(@Param("organId") Integer organId, @Param("num") String num);

    //删除用户组织结构
    void deleteByNum(@Param("num") String num);
    void deleteByNumAndOrganId(@Param("num") String num, @Param("organId") Integer organId);
    void deleteOrganization(@Param("num") String num, @Param("organId") Integer organId);

    //批量删除
    void batchDelete(@Param("numList") List<String> numList);
    //查询用户组织结构
    List<UserOrganizationVO> findByPage(@Param("dto") UserOrganizationDTO userOrganizationDTO);

    //添加用户组织结构
    void insertBatch(@Param("list") List<UserOrganization> list);


}