package cn.sanli.manage.mapper.data1;
import org.apache.ibatis.annotations.Param;

import cn.sanli.manage.pojo.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description:
 * @author: lsk
 * @create: 2023-12-04 15:52
 * @since jdk1.8
 **/
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

    int deleteByPrimaryKey(Integer roleId);

    void update(Role role);

    Role selectByPrimaryKey(Integer roleId);

    List<Role> selectAllRoleList(@Param("roleId") Integer roleId);

    List<Role> selectByRoleId(@Param("roleId") Integer roleId);

    List<Role> findAllRoles();

}