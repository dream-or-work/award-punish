package cn.sanli.manage.mapper.data1;

import cn.sanli.manage.pojo.dto.Fault.FaultAssessInfoDTO;
import cn.sanli.manage.pojo.dto.Fault.FaultDTO;
import cn.sanli.manage.pojo.dto.Fault.FaultInsertDTO;
import cn.sanli.manage.pojo.entity.FaultAssess;
import cn.sanli.manage.pojo.vo.FaultAssess.FaultAssessInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectKey;

import java.util.List;

/**
 * 过错关联表(FaultAssess)表数据库访问层
 *
 * @author wzy
 * @since 2023-12-07 16:42:50
 */
@Mapper
public interface FaultAssessMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    FaultAssess queryById(Integer id);

    /**
     * 通过ID查询单条数据
     *
     * @param ids 主键数组
     * @return 实例对象
     */
    List<FaultAssessInfoVO> queryByIds(List<String> ids);


    List<FaultAssessInfoVO> queryByIdsToEduce(String Str);


    /**
     * 统计总行数
     *
     * @param faultAssess 查询条件
     * @return 总行数
     */
    long count(FaultAssess faultAssess);


    /**
     * 新增或修改过错关联信息
     * @param faultAssessInfos 实例对象
     * @return 受影响行数
     */
    @SelectKey(statement="select LAST_INSERT_ID()",keyProperty = "id",before = false,resultType = Integer.class)
    int insert(FaultAssessInfoDTO[] faultAssessInfos);

//    /**
//     * 新增或修改过错关联信息
//     * @param entities 实例对象数组
//     * @return 受影响行数
//     */
//    int insertOfArray(FaultAssessArr[] entities);

    /**
     * 修改数据
     *
     * @param faultAssessInfos 实例对象
     * @return 影响行数
     */
    int update(FaultAssessInfoDTO[] faultAssessInfos);



    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    /**
     * 批量删除
     * @param faultNums
     * @return
     */
    boolean deleteBatch(String[] faultNums);

    boolean insertBatch(List<FaultAssessInfoDTO> list);


    //    /**
//     * 新增数据
//     *
//     * @param faultAssess 实例对象
//     * @return 影响行数
//     */
//    int insert(FaultAssess faultAssess);
//
//    /**
//     * 批量新增数据（MyBatis原生foreach方法）
//     *
//     * @param entities List<FaultAssess> 实例对象列表
//     * @return 影响行数
//     */
//    int insertBatch(@Param("entities") List<FaultAssess> entities);
//
//    /**
//     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
//     *
//     * @param entities List<FaultAssess> 实例对象列表
//     * @return 影响行数
//     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
//     */
//    int insertOrUpdateBatch(@Param("entities") List<FaultAssess> entities);
//


}

