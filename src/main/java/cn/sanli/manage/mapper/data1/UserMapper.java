package cn.sanli.manage.mapper.data1;

import cn.sanli.manage.pojo.dto.Login.LoginDTO;
import cn.sanli.manage.pojo.vo.LoginUserVO;
import cn.sanli.manage.pojo.vo.UserMessage;

import java.util.List;

public interface UserMapper {
    //仅做测试
    int selectUser1();

    /*根据登录账号查询登录信息的mapper层*/
    LoginDTO selectUserMapper(String num);

    /*根据工号查询前端主显部门/大区*/
    String selectDistrictMapper(String num);

    /*根据工号查询前端主显权限*/
    LoginUserVO selectRole(String num);

    /*根据工号查询所有权限【为生成jwt数据准备】*/
    List<Integer> selectRoles(String num);

    /**
     * 根据工号查询登录用户详情
     * @param num
     * @return
     */
    UserMessage selectMessage(String num);

    /**
     * 根据不同id查询所属部门或者大区
     * @param  id
     * @return
     */
    String selectRegion(int id);
}
