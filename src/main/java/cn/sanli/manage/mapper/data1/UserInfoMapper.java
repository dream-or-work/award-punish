package cn.sanli.manage.mapper.data1;


import cn.sanli.manage.pojo.dto.User.UserInfoDTO;
import cn.sanli.manage.pojo.vo.Fault.FaultStandardVO;
import cn.sanli.manage.pojo.vo.UserInfoListVO;
import cn.sanli.manage.pojo.vo.UserInfoVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import cn.sanli.manage.pojo.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description:
 * @author: lsk
 * @create: 2023-12-04 15:52
 * @since jdk1.8
 **/
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    //获取全部用户列表
    List<UserInfoListVO> findListByPage(@Param("dto") UserInfoDTO userInfoDto);
    List<UserInfoVO> findByPage(@Param("dto") UserInfoDTO userInfoDto);
    List<String> selectCenterIdList(@Param("num") String num);
    List<String> selectDeptIdList(@Param("num") String num);
    List<String> selectRoleIdList(@Param("num") String num);

    List<String> selectCenterNameList(@Param("num") String num);

    List<String> selectDeptNameList(@Param("num") String num);

    List<String> selectRoleNameList(@Param("num") String num);
    //获取大口用户列表
//    List<UserInfoVO> findByCenter(@Param("dto") UserInfoDTO userInfoDto, @Param("centerId") int centerId);
    List<UserInfoListVO> findListByCenter(@Param("dto") UserInfoDTO userInfoDto, @Param("idList") List<Integer> idList);
    List<UserInfoVO> findByCenter(@Param("dto") UserInfoDTO userInfoDto, @Param("idList") List<Integer> idList);
    //获取部门用户列表
//    List<UserInfoVO> findByDept(@Param("dto") UserInfoDTO userInfoDto, @Param("deptId") int deptId);
    List<UserInfoListVO> findListByDept(@Param("dto") UserInfoDTO userInfoDto, @Param("idList") List<Integer> idList);
    List<UserInfoVO> findByDept(@Param("dto") UserInfoDTO userInfoDto, @Param("idList") List<Integer> idList);

    //根据工号查询用户
    UserInfo selectByNum(@Param("num") String num);
    UserInfoVO selectAllByNum(@Param("num") String num);
    UserInfoListVO findListUserInfo(@Param("num") String num);
    //根据用户名查询用户
    List<UserInfo> selectByUsername(@Param("username") String username);
    /**
     * 根据工号查询计分标准信息
     * @param number
     * @return
     */
    FaultStandardVO selectStandardByNumber(Integer number);

    //修改用户信息
    void update(UserInfo userInfo);

    //void updateStandardIdByNum(@Param("standardId") Integer standardId, @Param("num") String num);

    int deleteByPrimaryKey(Integer id);

    UserInfo selectByPrimaryKey(Integer id);

    //添加用户信息
    void insertSelective(UserInfo userInfo);

    void insertCenterUser(UserInfo userInfo, @Param("centerId") int centerId);
    //账号锁定
    void updateIsLockById(@Param("isLock") Integer isLock, @Param("id") Integer id);

    //逻辑删除
    void updateIsDeleteById(@Param("id") Integer id);

    List<String> selectNumById(@Param("idList") List<Integer> idList);
    //批量删除
    void batchDelete(@Param("idList") List<Integer> idList);


    //重置密码
    void resetPassWord(@Param("password") String password, @Param("id") Integer id);



}