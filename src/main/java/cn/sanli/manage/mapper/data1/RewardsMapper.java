package cn.sanli.manage.mapper.data1;

import cn.sanli.manage.pojo.dto.Rewards.*;
import cn.sanli.manage.pojo.entity.Rewards;
import cn.sanli.manage.pojo.vo.Rewards.PagingQueryRewardsVO;
import cn.sanli.manage.pojo.vo.Rewards.RewardsStatisticsVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 奖励信息表(Rewards)表数据库访问层
 *
 * @author makejava
 * @since 2023-12-07 16:06:42
 */
@Mapper
public interface RewardsMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Rewards queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param rewards 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
   // List<Rewards> queryAllByLimit(Rewards rewards, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param rewards 查询条件
     * @return 总行数
     */
    long count(Rewards rewards);

    /**
     * 查询最新上一条rid的值
     * @return 最新上一条rid的值
     */
    String getMaxRId();

    /**
     * 根据条件分页查询奖励信息
     *
     * @param queryRequest 查询条件
     * @return 符合条件的奖励信息集合
     */
    List<PagingQueryRewardsVO> pagingQuery(PageQueryOfRewardsDTO queryRequest);


    /**
     * 根据条件查询统计所需的数据
     *
     * @param statisticsRequest 当事人
     * @return 符合条件的数据
     */
    List<RewardsStatisticsVO> statisticsRewardsOfPersonage(RewardsInfoStatisticsDTO statisticsRequest);

    /**
     * 新增数据
     *
     * @param rewardsInsertRequest 实例对象
     * @return 影响行数
     */
    int insert(RewardsInsertRequest rewardsInsertRequest);

    /**
     * 修改数据
     *
     * @param updateRequest 实例对象
     * @return 影响行数
     */
    int update(RewardsUpdateRequest updateRequest);



    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);


    int upload(List<RewardsInsertRequest> entities);


//    /**
//     * 批量新增数据（MyBatis原生foreach方法）
//     *
//     * @param entities List<Rewards> 实例对象列表
//     * @return 影响行数
//     */
//    int insertBatch(@Param("entities") List<Rewards> entities);
//
//    /**
//     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
//     *
//     * @param entities List<Rewards> 实例对象列表
//     * @return 影响行数
//     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
//     */
//    int insertOrUpdateBatch(@Param("entities") List<Rewards> entities);
//
//    /**
//     * 修改数据
//     *
//     * @param rewards 实例对象
//     * @return 影响行数
//     */
//    int update(Rewards rewards);

}

