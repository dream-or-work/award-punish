package cn.sanli.manage.mapper.data1;

import cn.sanli.manage.pojo.Wechat.TWxAccessToken;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description:
 * @author: lsk
 * @create: 2023-12-11 16:19
 * @since jdk1.8
 **/
@Mapper
@Transactional
public interface TWxAccessTokenMapper extends BaseMapper<TWxAccessToken> {
    TWxAccessToken getAccessToken(@Param("corpId") String corpId, @Param("corpSecret")String corpSecret, @Param("flag") String flag);

    void updateAccessToken(TWxAccessToken wxAccessToken);

    int insert(TWxAccessToken wxAccessToken);
}

