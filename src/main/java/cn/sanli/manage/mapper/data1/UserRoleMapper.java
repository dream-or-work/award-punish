package cn.sanli.manage.mapper.data1;

import cn.sanli.manage.pojo.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description:
 * @author: lsk
 * @create: 2023-12-04 15:52
 * @since jdk1.8
 **/
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {

    //根据id查询角色
    UserRole selectByPrimaryKey(Integer id);

    UserRole selectByNum(@Param("num") String num);

    List<UserRole> selectRoles(@Param("num") String num);
    //修改用户角色
    void update(UserRole userRole);

    int deleteByPrimaryKey(Integer id);

    //添加用户角色
    void insertSelective(UserRole userRole);

    void insertUserRole(UserRole userRole);
    void insertUserRoleList(@Param("userRoleList") List<UserRole> userRoleList);

    //根据工号获取用户角色列表
    List<Integer> findUserRole(@Param("num") String num);

    List<UserRole> findAllByNum(@Param("num") String num);
    //根据工号删除角色
    void deleteByNum(@Param("num") String num);

    //批量删除
    void batchDeleteRole(@Param("numList") List<String> numList);

    //用户分配角色
    void assignRole(@Param("num") String num, @Param("roleId")Integer roleId, @Param("remark") String remark);

    //指定主权限
    void updateLvByNumAndRoleId(@Param("num") String num, @Param("roleId") Integer roleId);
}