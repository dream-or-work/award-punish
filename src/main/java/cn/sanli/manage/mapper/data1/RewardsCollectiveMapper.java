package cn.sanli.manage.mapper.data1;
import java.util.Collection;
import java.util.List;

import cn.sanli.manage.pojo.dto.Rewards.RewardsCollectiveDTO;
import cn.sanli.manage.pojo.dto.Rewards.SaveRewardsCollectiveDTO;
import cn.sanli.manage.pojo.dto.Rewards.UpdateRewardsCollectiveDTO;
import cn.sanli.manage.pojo.dto.User.UserInfoDTO;
import cn.sanli.manage.pojo.vo.Rewards.RewardsCollectiveVO;
import cn.sanli.manage.pojo.vo.UserInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.sanli.manage.pojo.entity.RewardsCollective;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lu
* @description 针对表【rewards_collective(集体奖励表)】的数据库操作Mapper
* @createDate 2024-04-11 10:42:15
* @Entity cn.sanli.manage.pojo.entity.RewardsCollective
*/
@Mapper
public interface RewardsCollectiveMapper extends BaseMapper<RewardsCollective> {
    RewardsCollectiveVO selectAllById(@Param("id") Integer id);
    List<RewardsCollectiveVO> findByPage(@Param("dto") RewardsCollectiveDTO rewardsCollectiveDTO);
    List<RewardsCollectiveVO> findByCenter(@Param("dto") RewardsCollectiveDTO rewardsCollectiveDTO, @Param("idList") List<Integer> idList);
    List<RewardsCollectiveVO> findByDept(@Param("dto") RewardsCollectiveDTO rewardsCollectiveDTO, @Param("idList") List<Integer> idList);

    String getMaxRId();

    int insertAll(SaveRewardsCollectiveDTO rewardsCollectiveDTO);

    int updateSelective(UpdateRewardsCollectiveDTO rewardsCollectiveDTO);

    int deleteById(@Param("id") Integer id);

    int insertBatch(@Param("list") List<SaveRewardsCollectiveDTO> list);

    void batchDelete(@Param("idList") List<Integer> idList);
}




