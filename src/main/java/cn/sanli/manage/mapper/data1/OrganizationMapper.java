package cn.sanli.manage.mapper.data1;
import cn.sanli.manage.pojo.entity.IdAndName;
import cn.sanli.manage.pojo.vo.ceshi;
import cn.sanli.manage.pojo.vo.ceshi1;
import org.apache.ibatis.annotations.Param;

import cn.sanli.manage.pojo.entity.Organization;
import cn.sanli.manage.pojo.vo.DeptNameVO;
import cn.sanli.manage.pojo.vo.RegionNameVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description:
 * @author: lsk
 * @create: 2023-12-04 15:52
 * @since jdk1.8
 **/
@Mapper
public interface OrganizationMapper extends BaseMapper<Organization> {
    Organization selectByPrimaryKey(Integer id);

    void update(Organization organization);

    int deleteByPrimaryKey(Integer id);

    /**
     * 查询所有大区
     * @return
     */
    List<RegionNameVO> regionName();


    /**
     * 查询所有大区
     * @return
     */
    List<ceshi> regionName1();



    /**
     * 根据大区查询对应部门1
     */
    List<DeptNameVO> deptName(int id);

    /**
     * 根据大区查询对应部门1
     */
    List<ceshi1> deptName1(int id);

    List<ceshi1> selectAll1();


    List<DeptNameVO> selectMoreDept(@Param("idList") List<Integer> centerIdList);
    Organization selectCenterById(@Param("id") Integer id);

    Organization selectCenterByName(@Param("name") String name);

    Organization selectDeptByIdAndParentId(@Param("id") Integer id, @Param("parentId") Integer parentId);
    Organization selectDeptByName(@Param("name") String name, @Param("parentId") Integer parentId);

    Organization selectByNameAndParentId(@Param("name") String name, @Param("parentId") Integer parentId);
    //查询全部大口和部门
    List<Organization> selectAll();

    //添加组织结构
    void insertAll(Organization organization);

    //修改组织结构
    void updateSelective(Organization organization);

    //查询子节点个数
    int countByParentId(@Param("parentId") Integer parentId);

    //删除组织结构
    void updateIsDeleteById(@Param("id") Integer id);

    List<IdAndName> queryIdAndName();

    List<IdAndName> queryIdAndNameOfCenter();

    List<IdAndName> queryIdAndNameOfDept();

    List<RegionNameVO> listALlDept();
}