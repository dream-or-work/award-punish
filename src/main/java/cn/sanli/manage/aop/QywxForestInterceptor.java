package cn.sanli.manage.aop;

import cn.sanli.manage.pojo.Wechat.QywxConfig;
import com.dtflys.forest.http.ForestRequest;
import com.dtflys.forest.interceptor.Interceptor;
import com.dtflys.forest.reflection.ForestMethod;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class QywxForestInterceptor implements Interceptor<String> {

    @Resource
    private QywxConfig qywxConfig;

    @Override
    public void onInvokeMethod(ForestRequest req, ForestMethod method, Object[] args) {
        req.setBasePath(qywxConfig.getEndpoint());
    }
}
