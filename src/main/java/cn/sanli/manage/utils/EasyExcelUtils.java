package cn.sanli.manage.utils;

import cn.sanli.manage.utils.text.Hcs;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;

/**
 * @Author wzy
 * @Date 2023/12/21 0:13
 * @Description: 关于Excel操作的工具类
 * @Version 1.0
 */
@Slf4j
public class EasyExcelUtils {
    private static final int MAX_USER_IMPORT = 1000;

    /**
     * 单sheet导出
     *
     * @param response  HttpServletResponse
     * @param list      数据 list
     * @param fileName  导出的文件名
     * @param sheetName 导入文件的 sheet 名
     * @param model     Excel 模型class对象
     */
    public static void writeExcel(HttpServletResponse response, List<?> list,
                                  String fileName, String sheetName, Class model) {
        try {
            //表头样式
            WriteCellStyle headWriteCellStyle = new WriteCellStyle();
            // 头部背景色
            headWriteCellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
            WriteFont headWriteFont = new WriteFont();
            // 字体大小
            headWriteFont.setFontHeightInPoints((short) 9);
            headWriteCellStyle.setWriteFont(headWriteFont);
            //设置表头居中对齐
            headWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
            //内容样式
            WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
            // 水平居中
            contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
            // 垂直居中
            contentWriteCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            // 设置自动换行，前提内容中需要加「\n」才有效
            contentWriteCellStyle.setWrapped(true);
            //设置内容靠左对齐
//            contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.LEFT);
            HorizontalCellStyleStrategy horizontalCellStyleStrategy = new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle);
            EasyExcel.write(getOutputStream(fileName, response), model)
                    .excelType(ExcelTypeEnum.XLSX)
                    .sheet(sheetName)
                    .registerWriteHandler(Hcs.getHorizontalCellStyleStrategy((short)12))
//                    .registerWriteHandler(new CustomCellWeightWeightConfig())
                    .doWrite(list);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("excel导出失败");
        }
    }








    /**
     * 单sheet导出
     *
     * @param response  HttpServletResponse
     * @param list      数据 list
     * @param fileName  导出的文件名
     * @param sheetName 导入文件的 sheet 名
     * @param head      表头
     */
    public static void writeHeadExcel(HttpServletResponse response, List<?> list,
                                      String fileName, String sheetName, List<List<String>> head) {
        try {
            //表头样式
            WriteCellStyle headWriteCellStyle = new WriteCellStyle();
            //设置表头居中对齐
            headWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
            //内容样式
            WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
            //设置内容靠左对齐
            contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.LEFT);
            HorizontalCellStyleStrategy horizontalCellStyleStrategy = new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle);
            EasyExcel.write(getOutputStream(fileName, response))
                    .excelType(ExcelTypeEnum.XLSX)
                    .sheet(sheetName)
                    .head(head)
                    .registerWriteHandler(horizontalCellStyleStrategy)
                    .doWrite(list);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("excel导出失败");
        }
    }

    /**
     * 导出文件时为Writer生成OutputStream
     *
     * @param fileName
     * @param response
     * @return
     */
    public static OutputStream getOutputStream(String fileName, HttpServletResponse response) {
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8");
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf8");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xlsx");
            response.setHeader("Pragma", "public");
            response.setHeader("Cache-Control", "no-store");
            response.addHeader("Cache-Control", "max-age=0");
            return response.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
            log.info("创建头文件失败");
        }
        return null;
    }


    /**
     * 导入：同步读，单sheet
     *
     * @param file excel文件
     * @param t    excel导入的model对象
     */
    public static <T> List<T> importData(MultipartFile file, Class<T> t) {
        List<T> userExcelList = null;
        // 1.excel同步读取数据
        try {
            userExcelList = EasyExcel.read(new BufferedInputStream(file.getInputStream()))
                    .head(t)
                    .sheet()
                    .doReadSync();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 2.检查是否大于1000条
        if (userExcelList.size() > MAX_USER_IMPORT) {
        }
        return userExcelList;
    }
    public static <T> List<T> importDataSheetIndex(File file, Class<T> t, Integer index) {
        List<T> userExcelList = null;
        // 1.excel同步读取数据
        try {
            userExcelList = EasyExcel.read(new BufferedInputStream(new FileInputStream(file)))
                    .head(t)
                    .sheet(index)
                    .doReadSync();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 2.检查是否大于1000条
        if (userExcelList.size() > MAX_USER_IMPORT) {
        }
        return userExcelList;
    }
    public static <T> List<T> importData2(File file, Class<T> t,String name) {
        List<T> userExcelList = null;
        // 1.excel同步读取数据
        try {
            userExcelList = EasyExcel.read(new BufferedInputStream(new FileInputStream(file)))
                    .head(t)
                    .sheet(name)
                    .doReadSync();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 2.检查是否大于1000条
        if (userExcelList.size() > MAX_USER_IMPORT) {
        }
        return userExcelList;
    }
    /**
     * 多sheet导出
     *
     * @param response  HttpServletResponse
     * @param fileName  导出的文件名
     */
    public static void writeExcelAll(HttpServletResponse response,
                                     String fileName, List<ExcelObject> obj) throws Exception{
        fileName = URLEncoder.encode(fileName, "UTF-8");
        OutputStream outputStream = response.getOutputStream();
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
            ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
            for(int i=0;i<obj.size();i++){
                WriteSheet writeSheet1 = EasyExcel.writerSheet(i, obj.get(i).getSheetName()).head(obj.get(0).getT()).build();
                excelWriter.write(obj.get(i).getData(), writeSheet1);
            }
            excelWriter.finish();
        } catch (IOException e) {
            response.reset();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            Map<String, String> map = new HashMap();
            map.put("status", "failure");
            map.put("message", "下载文件失败" + e.getMessage());
            response.getWriter().println(JSON.toJSONString(map));
        }finally {
            outputStream.close();
        }
    }

    /**
     * 根据Echarts的byteList以及表格相关数据生成Ecxel表
     * @param response
     * @param buffer                                EchartsString
     * @param name                                  Excel表名
     * @param title                                 sheet名称
     * @param header                                表头
     * @param data                                  表数据
     * @param mapParamsList                         data数据中的字段名称
     * @return
     * @throws IOException
     */
    public static String bufferStreamAndData2Excel (HttpServletResponse response, byte[] buffer ,
                                                    String name, String title, List<String> header,
                                                    List<HashMap<String,Object>> data, List<String> mapParamsList) throws IOException {

        response.setCharacterEncoding("utf-8");

        //设置响应内容类型
        response.setContentType("text/plain");

        //设置文件名称和格式
        response.addHeader("Content-Disposition", "attachment;filename="
                +genAttachmentFileName(name,"JSON_FOR_UCC_")
                +".xls");        //设置名称格式,没有中文名称无法显示

        //创建HSSFWorkbook对象
        HSSFWorkbook wb = new HSSFWorkbook();
        //创建sheet对象
        HSSFSheet sheet1 = wb.createSheet(title);

        if (data != null && data.size() != 0) {
            //左右居中
            HSSFCellStyle style = wb.createCellStyle();
            style.setAlignment(HorizontalAlignment.CENTER);
            style.setBorderBottom(BorderStyle.THIN); // 下边框
            style.setBorderLeft(BorderStyle.THIN);// 左边框
            style.setBorderTop(BorderStyle.THIN);// 上边框
            style.setBorderRight(BorderStyle.THIN);// 右边框

            //写入表格数据
            //在sheet中创建第一行写入title
            HSSFRow rowTitle = sheet1.createRow(0);

            // 这里是合并excel中title的列为一列
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, header.size()-1);
            sheet1.addMergedRegion(region);
            rowTitle.createCell(0).setCellValue(title);
            for (int i = 0; i < header.size(); i++) {

                if(i == (header.size()-1)) {
                    rowTitle.createCell(i);
                }else {
                    rowTitle.createCell(i+1);
                }
            }
            for (Cell cell : rowTitle) {
                cell.setCellStyle(style);
            }

            //在sheet中第二行写入表头数据
            HSSFRow rowheader = sheet1.createRow(1);
            for (int i = 0; i < header.size(); i++) {
                //创建单元格写入数据
                HSSFCell cellheader = rowheader.createCell(i);
                cellheader.setCellStyle(style);
                cellheader.setCellValue(header.get(i));
            }

            //在sheet中第三行开始写入表数据
            for (int i = 0; i < data.size(); i++) {
                //创建新行数据
                HSSFRow rowData = sheet1.createRow(i+2);
                //排名
                HSSFCell cellRank = rowData.createCell(0);
                cellRank.setCellStyle(style);
                cellRank.setCellValue(String.valueOf(i+1));
                for (int j = 0; j < mapParamsList.size(); j++ ) {
                    HSSFCell cellData = rowData.createCell(j+1);
                    cellData.setCellStyle(style);
                    cellData.setCellValue(String.valueOf(data.get(i).get(mapParamsList.get(j))));
                }
            }
        }

        //根据buffer流把图片写入到excel中
        BufferedImage bufferedImage = null;

        ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();

        bufferedImage = ImageIO.read(new ByteArrayInputStream(buffer));
        ImageIO.write(bufferedImage, "png", byteArrayOut);

        //画图的顶级管理器，一个sheet只能获取一个（一定要注意这点）
        HSSFPatriarch patriarch = sheet1.createDrawingPatriarch();
        //anchor主要用于设置图片的属性
        HSSFClientAnchor anchor = null;
        if(header != null && header.size() !=0) {
            anchor = new HSSFClientAnchor(0, 0, 255, 255,(short) (header.size()+3), 0,(short) (header.size()+15), 18);
        }else {
            anchor = new HSSFClientAnchor(0, 0, 255, 255,(short) 3, 0,(short) 15, 18);
        }

        anchor.setAnchorType(ClientAnchor.AnchorType.DONT_MOVE_DO_RESIZE);
        //插入图片
        patriarch.createPicture(anchor, wb.addPicture(byteArrayOut.toByteArray(), HSSFWorkbook.PICTURE_TYPE_JPEG));

        //写出excel表数据
        ServletOutputStream sos = response.getOutputStream();
        BufferedOutputStream bos = new BufferedOutputStream(sos);
        wb.write(bos);
        bos.flush();
        bos.close();
        sos.close();

        return null;
    }

    /**
     * 防止中文文件名显示错误
     * @param cnName
     * @param defaultName
     * @return
     */
    public static String genAttachmentFileName(String cnName,String defaultName){

        try {
            cnName = new String (cnName.getBytes("GB2312"),"ISO-8859-1");
        } catch (Exception e) {
            e.printStackTrace();
            cnName = defaultName;
        }

        return cnName;
    }

    /**
     * jsonArray转List<HashMap<String,String>>
     * @param pics
     * @param mapParamsList
     * @return
     */
    public static List<HashMap<String,Object>> getPics(String pics,List<String> mapParamsList) {
        List<HashMap<String,Object>> res = new ArrayList<>();
        if(pics==null||pics.equals("")||!pics.startsWith("[")||!pics.endsWith("]")){
            return res;
        }
        JSONArray jsons = JSONArray.parseArray(pics);
        JSONObject json = null;
        HashMap<String,Object> map = null;
        Object data = null;
        for(int i=0;i<jsons.size();i++){
            json = jsons.getJSONObject(i);
            map = new HashMap<String,Object>();
            for (int j=0;j<mapParamsList.size();j++) {
                data = json.get((String)mapParamsList.get(j));
                if(data!=null&&!"".equals(data)) {
                    map.put((String)mapParamsList.get(j),data);
                }
            }
            res.add(map);
        }
        return res;
    }


    /**
     * 单sheet导出-忽略某个表头
     *
     * @param response  HttpServletResponse
     * @param list      数据 list
     * @param fileName  导出的文件名
     * @param sheetName 导入文件的 sheet 名
     * @param model     Excel 模型class对象
     */
    public static void writeExcelWithoutLine(HttpServletResponse response, List<?> list,
                                             String fileName, String sheetName, Class model, String line) {
        try {
            //表头样式
            WriteCellStyle headWriteCellStyle = new WriteCellStyle();
            //设置表头居中对齐
            headWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
            //内容样式
            WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
            //忽略某个表头
            Set<String> excludeColumnFiledNames = new HashSet<String>();
            excludeColumnFiledNames.add(line);
            //设置内容靠左对齐
            contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.LEFT);
            HorizontalCellStyleStrategy horizontalCellStyleStrategy = new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle);
            EasyExcel.write(getOutputStream(fileName, response), model)
                    .excludeColumnFiledNames(excludeColumnFiledNames)
                    .excelType(ExcelTypeEnum.XLSX)
                    .sheet(sheetName)
                    .registerWriteHandler(horizontalCellStyleStrategy)
                    .doWrite(list);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("excel导出失败");
        }
    }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class ExcelObject{
    private String sheetName;
    private List<?> data;
    private Class T;
}


