package cn.sanli.manage.utils.handler;


import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.write.handler.RowWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import com.alibaba.excel.write.property.ExcelWriteHeadProperty;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.util.Map;

/**
 * 自定义EasyExcel Row
 *
 * @author zk
 */
public class CustomRowWriteHandler implements RowWriteHandler {

    /**
     * excel创建序号列标题
     */
    String TITLE = "序号";

    private static CustomRowWriteHandler instance;

    /**
     * 获取excel行处理对象
     *
     * @return 行处理对象
     */
    public static CustomRowWriteHandler getInstance() {

        return new CustomRowWriteHandler();
    }

    /**
     * 自由化构造参数
     */
    private CustomRowWriteHandler() {

    }

    private boolean init = true;

    /**
     * 行创建前的执行方法
     *
     * @param writeSheetHolder sheet
     * @param writeTableHolder table
     * @param rowIndex         行索引
     * @param relativeRowIndex 执行行索引
     * @param isHead           表头
     */
    @Override
    public void beforeRowCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Integer rowIndex,
                                Integer relativeRowIndex, Boolean isHead) {
        if (init) {
            // 修改存储头部及对应字段信息的 map, 将其中的内容均右移一位, 给新增的序列号预留为第一列
            ExcelWriteHeadProperty excelWriteHeadProperty = writeSheetHolder.excelWriteHeadProperty();
            Map<Integer, Head> headMap = excelWriteHeadProperty.getHeadMap();
            int size = headMap.size();
            for (int current = size; current > 0; current--) {
                int previous = current - 1;
                headMap.put(current, headMap.get(previous));
            }
            // 空出第一列
            headMap.remove(0);
            // 只需要修改一次 map 即可, 故使用 init 变量进行控制
            init = false;
        }
    }

    /**
     * 行创建后的执行方法
     *
     * @param writeSheetHolder sheet
     * @param writeTableHolder table
     * @param row              行
     * @param relativeRowIndex 执行行索引
     * @param isHead           表头
     */
    @Override
    public void afterRowCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row,
                               Integer relativeRowIndex, Boolean isHead) {
        // 在行创建完成后添加序号列
        Cell cell = row.createCell(0);
        int rowNum = row.getRowNum();
        if (rowNum == 0) {
            cell.setCellValue(TITLE);
        } else {
            cell.setCellValue(rowNum);
        }
    }

    @Override
    public void afterRowDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row,
                                Integer relativeRowIndex, Boolean isHead) {
        if (row.getLastCellNum() > 1) {
            // 将自定义新增的序号列的样式设置与默认的样式一致
            row.getCell(0).setCellStyle(row.getCell(1).getCellStyle());
        }
    }

}