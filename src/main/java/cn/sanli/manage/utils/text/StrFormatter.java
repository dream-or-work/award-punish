package cn.sanli.manage.utils.text;


import cn.sanli.manage.utils.StringUtils;

/**
 * 字符串格式化
 *
 * @author zk
 * @since 2022/11/02
 */
public class StrFormatter {
    public static final String EMPTY_JSON = "{}";
    public static final char C_BACKSLASH = '\\';
    public static final char C_DELI_START = '{';
    public static final char C_DELI_END = '}';

    /**
     * 格式化字符串<br>
     * 此方法只是简单将占位符 {} 按照顺序替换为参数<br>
     * 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可<br>
     * 例：<br>
     * 通常使用：format("this is {} for {}", "a", "b") - this is a for b<br>
     * 转义{}： format("this is \\{} for {}", "a", "b") - this is \{} for a<br>
     * 转义\： format("this is \\\\{} for {}", "a", "b") - this is \a for b<br>
     *
     * @param strPattern 字符串模板
     * @param argArray   参数列表
     * @return 结果
     */
    public static String format(final String strPattern, final Object... argArray) {
        // 判断字符串是否为空，或者参数数组是否为空
        if (StringUtils.isEmpty(strPattern)
                || StringUtils.isEmpty(argArray)) {
            // 如果为空，返回原字符串
            return strPattern;
        }
        // 获取字符串长度
        final int strPatternLength = strPattern.length();

        // 初始化定义好的长度以获得更好的性能
        StringBuilder stuff = new StringBuilder(strPatternLength + 50);

        int handledPosition = 0;
        int deliIndex;// 占位符所在位置
        for (int argIndex = 0; argIndex < argArray.length; argIndex++) {
            // 获取占位符位置
            deliIndex = strPattern.indexOf(EMPTY_JSON, handledPosition);
            if (deliIndex == -1) {
                // 没有找到占位符，判断是否是最后一个字符
                if (handledPosition == 0) {
                    // 是最后一个字符，直接返回
                    return strPattern;
                } else { // 字符串模板剩余部分不再包含占位符，加入剩余部分后返回结果
                    stuff.append(strPattern, handledPosition, strPatternLength);
                    return stuff.toString();
                }
            } else {
                // 找到占位符
                if (deliIndex > 0 && strPattern.charAt(deliIndex - 1) == C_BACKSLASH) {
                    if (deliIndex > 1 && strPattern.charAt(deliIndex - 2) == C_BACKSLASH) {
                        // 转义符之前还有一个转义符，占位符依旧有效
                        stuff.append(strPattern, handledPosition, deliIndex - 1);
                        stuff.append(Convert.utf8Str(argArray[argIndex]));
                        handledPosition = deliIndex + 2;
                    } else {
                        // 占位符被转义
                        argIndex--;
                        stuff.append(strPattern, handledPosition, deliIndex - 1);
                        stuff.append(C_DELI_START);
                        handledPosition = deliIndex + 1;
                        // 正常占位符
                        stuff.append(strPattern, handledPosition, deliIndex);
                        stuff.append(Convert.utf8Str(argArray[argIndex]));
                        handledPosition = deliIndex + 2;
                    }
                } else {
                    // 正常占位符
                    stuff.append(strPattern, handledPosition, deliIndex);
                    stuff.append(Convert.utf8Str(argArray[argIndex]));
                    handledPosition = deliIndex + 2;
                }
            }
            // 加入最后一个占位符后所有的字符
            stuff.append(strPattern, handledPosition, strPattern.length());

            return stuff.toString();
        }
        // 加入最后一个占位符后所有的字符
        stuff.append(strPattern, handledPosition, strPattern.length());

        return stuff.toString();
    }
}
