package cn.sanli.manage.utils;

import cn.sanli.manage.pojo.entity.IdAndName;
import cn.sanli.manage.pojo.entity.Organization;
import cn.sanli.manage.pojo.vo.Fault.FaultStatisticsInfoVO;
import cn.sanli.manage.pojo.vo.Fault.FaultStatisticsVO;
import cn.sanli.manage.security.LoginPrincipal;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author wzy
 * @Date 2023/12/25 11:58
 * @Description: 关于Map操作的工具类
 * @Version 1.0
 */
@Slf4j
public class MapUtils {

    public static Map<String, Integer> byName(List<IdAndName> list){
        Map<String, Integer> map = new HashMap<>();
        for (IdAndName idAndName : list) {
            map.put(idAndName.getName(), idAndName.getId());
        }
        return map;
    }

    public static Map<Integer, String> byId(List<IdAndName> list){
        Map<Integer, String> map = new HashMap<>();
        for (IdAndName idAndName : list) {
            map.put(idAndName.getId(), idAndName.getName());
        }
        return map;
    }

    public static FaultStatisticsInfoVO getStatisticsInfo(List<FaultStatisticsVO> list, Map<String, Date[]> statisticsTime) { // 参数onlyExceedStandard为1的情况下, loginPrincipal不能为null
        log.debug("调用getStatisticsInfo统计数据, 需处理的数据: {}  时间参数: {}", list, statisticsTime);
        FaultStatisticsInfoVO personageVO = new FaultStatisticsInfoVO();
        // 声明并初始化五个变量后续使用
        BigDecimal ofWeek = new BigDecimal("0.0");
        BigDecimal ofMonth = new BigDecimal("0.0");
        BigDecimal ofQuarter = new BigDecimal("0.0");
        BigDecimal ofHalf = new BigDecimal("0.0");
        BigDecimal ofYear = new BigDecimal("0.0");
        // 遍历数据集合
        for (FaultStatisticsVO statisticsOfPersonageVO : list) {
            log.debug("本次循环的数据: {}", statisticsOfPersonageVO);
            if(statisticsOfPersonageVO.getDisposalDetails() != null) {
                // 根据考核时间判断
                if(statisticsOfPersonageVO.getFaultTime().getTime() >= statisticsTime.get(DateUtils.THISWEEK)[0].getTime()
                        &&
                        statisticsOfPersonageVO.getFaultTime().getTime() <= statisticsTime.get(DateUtils.THISWEEK)[1].getTime()) {
                    ofWeek = ofWeek.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofMonth = ofMonth.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofQuarter = ofQuarter.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofHalf = ofHalf.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofYear = ofYear.add(statisticsOfPersonageVO.getDisposalDetails());
                    continue;
                }else if(statisticsOfPersonageVO.getFaultTime().getTime() >= statisticsTime.get(DateUtils.THISMONTH)[0].getTime()
                        &&
                        statisticsOfPersonageVO.getFaultTime().getTime() <= statisticsTime.get(DateUtils.THISMONTH)[1].getTime()) {
                    ofMonth = ofMonth.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofQuarter = ofQuarter.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofHalf = ofHalf.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofYear = ofYear.add(statisticsOfPersonageVO.getDisposalDetails());
                    continue;
                }else if(statisticsOfPersonageVO.getFaultTime().getTime() >= statisticsTime.get(DateUtils.THISQUARTER)[0].getTime()
                        &&
                        statisticsOfPersonageVO.getFaultTime().getTime() <= statisticsTime.get(DateUtils.THISQUARTER)[1].getTime()) {
                    ofQuarter = ofQuarter.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofHalf = ofHalf.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofYear = ofYear.add(statisticsOfPersonageVO.getDisposalDetails());
                    continue;
                }else if(statisticsOfPersonageVO.getFaultTime().getTime() >= statisticsTime.get(DateUtils.THISHALF)[0].getTime()
                        &&
                        statisticsOfPersonageVO.getFaultTime().getTime() <= statisticsTime.get(DateUtils.THISHALF)[1].getTime()) {
                    ofHalf = ofHalf.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofYear = ofYear.add(statisticsOfPersonageVO.getDisposalDetails());
                    continue;
                }else if(statisticsOfPersonageVO.getFaultTime().getTime() >= statisticsTime.get(DateUtils.THISYEAR)[0].getTime()
                        &&
                        statisticsOfPersonageVO.getFaultTime().getTime() <= statisticsTime.get(DateUtils.THISYEAR)[1].getTime()) {
                    ofYear = ofYear.add(statisticsOfPersonageVO.getDisposalDetails());
                    continue;
                }
            }
        }
        // 基本信息属性赋值
        personageVO.setResName(list.get(0).getResName());
        personageVO.setResNum(list.get(0).getResNum());
        personageVO.setResDept(list.get(0).getResDept());
        personageVO.setResCenter(list.get(0).getResCenter());
        // 积分信息赋值, 四舍五入保留两位小数
        personageVO.setStatisticsOfWeek(ofWeek.setScale(2, RoundingMode.HALF_UP));
        personageVO.setStatisticsOfMonth(ofMonth.setScale(2, RoundingMode.HALF_UP));
        personageVO.setStatisticsOfQuarter(ofQuarter.setScale(2, RoundingMode.HALF_UP));
        personageVO.setStatisticsOfHalf(ofHalf.setScale(2, RoundingMode.HALF_UP));
        personageVO.setStatisticsOfYear(ofYear.setScale(2, RoundingMode.HALF_UP));

        return personageVO;
    }

    public static FaultStatisticsInfoVO getStatisticsInfoOnlyExceedStandard(List<FaultStatisticsVO> list, Map<String, Date[]> statisticsTime) { // 参数onlyExceedStandard为1的情况下, loginPrincipal不能为null
        log.debug("调用getStatisticsInfo统计数据并只返回超标人员数据, 需处理的数据: {}  时间参数: {}", list, statisticsTime);
        FaultStatisticsInfoVO personageVO = new FaultStatisticsInfoVO();
        // 声明并初始化五个变量后续使用
        BigDecimal ofWeek = new BigDecimal("0.0");
        BigDecimal ofMonth = new BigDecimal("0.0");
        BigDecimal ofQuarter = new BigDecimal("0.0");
        BigDecimal ofHalf = new BigDecimal("0.0");
        BigDecimal ofYear = new BigDecimal("0.0");
        // 遍历数据集合
        for (FaultStatisticsVO statisticsOfPersonageVO : list) {
            log.debug("本次循环的数据: {}", statisticsOfPersonageVO);
            if(statisticsOfPersonageVO.getDisposalDetails() != null) {
                // 根据考核时间判断
                if(statisticsOfPersonageVO.getFaultTime().getTime() >= statisticsTime.get(DateUtils.THISWEEK)[0].getTime()
                        &&
                        statisticsOfPersonageVO.getFaultTime().getTime() <= statisticsTime.get(DateUtils.THISWEEK)[1].getTime()) {
                    ofWeek = ofWeek.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofMonth = ofMonth.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofQuarter = ofQuarter.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofHalf = ofHalf.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofYear = ofYear.add(statisticsOfPersonageVO.getDisposalDetails());
                    continue;
                }else if(statisticsOfPersonageVO.getFaultTime().getTime() >= statisticsTime.get(DateUtils.THISMONTH)[0].getTime()
                        &&
                        statisticsOfPersonageVO.getFaultTime().getTime() <= statisticsTime.get(DateUtils.THISMONTH)[1].getTime()) {
                    ofMonth = ofMonth.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofQuarter = ofQuarter.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofHalf = ofHalf.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofYear = ofYear.add(statisticsOfPersonageVO.getDisposalDetails());
                    continue;
                }else if(statisticsOfPersonageVO.getFaultTime().getTime() >= statisticsTime.get(DateUtils.THISQUARTER)[0].getTime()
                        &&
                        statisticsOfPersonageVO.getFaultTime().getTime() <= statisticsTime.get(DateUtils.THISQUARTER)[1].getTime()) {
                    ofQuarter = ofQuarter.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofHalf = ofHalf.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofYear = ofYear.add(statisticsOfPersonageVO.getDisposalDetails());
                    continue;
                }else if(statisticsOfPersonageVO.getFaultTime().getTime() >= statisticsTime.get(DateUtils.THISHALF)[0].getTime()
                        &&
                        statisticsOfPersonageVO.getFaultTime().getTime() <= statisticsTime.get(DateUtils.THISHALF)[1].getTime()) {
                    ofHalf = ofHalf.add(statisticsOfPersonageVO.getDisposalDetails());
                    ofYear = ofYear.add(statisticsOfPersonageVO.getDisposalDetails());
                    continue;
                }else if(statisticsOfPersonageVO.getFaultTime().getTime() >= statisticsTime.get(DateUtils.THISYEAR)[0].getTime()
                        &&
                        statisticsOfPersonageVO.getFaultTime().getTime() <= statisticsTime.get(DateUtils.THISYEAR)[1].getTime()) {
                    ofYear = ofYear.add(statisticsOfPersonageVO.getDisposalDetails());
                    continue;
                }
            }
        }
        // 比较分值是否超出标准
        int resultOfWeek = ofWeek.compareTo(new BigDecimal("2.0"));
        int resultOfMonth = ofMonth.compareTo(new BigDecimal("4.0"));
        int resultOfQuarter = ofQuarter.compareTo(new BigDecimal("14.0"));
        int resultOfHalf = ofHalf.compareTo(new BigDecimal("25.0"));
        int resultOfYear = ofYear.compareTo(new BigDecimal("45.0"));
        if(resultOfWeek >= 0 || resultOfMonth >= 0 || resultOfQuarter >= 0 || resultOfHalf >= 0 || resultOfYear >= 0) {
            // 有分项超出分值标准
            // 基本信息属性赋值
            personageVO.setResName(list.get(0).getResName());
            personageVO.setResNum(list.get(0).getResNum());
            personageVO.setResDept(list.get(0).getResDept());
            personageVO.setResCenter(list.get(0).getResCenter());
            // 积分信息赋值, 四舍五入保留两位小数
            personageVO.setStatisticsOfWeek(ofWeek.setScale(2, RoundingMode.HALF_UP));
            personageVO.setStatisticsOfMonth(ofMonth.setScale(2, RoundingMode.HALF_UP));
            personageVO.setStatisticsOfQuarter(ofQuarter.setScale(2, RoundingMode.HALF_UP));
            personageVO.setStatisticsOfHalf(ofHalf.setScale(2, RoundingMode.HALF_UP));
            personageVO.setStatisticsOfYear(ofYear.setScale(2, RoundingMode.HALF_UP));
            return personageVO;
        }


        return null;
    }


}
