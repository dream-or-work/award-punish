package cn.sanli.manage.utils.converter;


import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * @Author wzy
 * @Date 2023/12/12 4:26
 * @Description: Excel表格内容(其他荣誉处理)转换器
 * @Version 1.0
 */
public class OtherDisposalConverter implements Converter<Integer> {
    @Override
    public Class supportJavaTypeKey() {
        return null;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return null;
    }

    @Override
    public Integer convertToJavaData(CellData cellData, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        return null;
    }

    // 写的时候会调用
    @Override
    public CellData convertToExcelData(Integer integer,
                                       ExcelContentProperty excelContentProperty,
                                       GlobalConfiguration globalConfiguration) throws Exception {
        switch(integer){
            case 1:
                return new CellData<Integer>("检查");
            case 2:
                return new CellData<Integer>("整改");
            case 3:
                return new CellData<Integer>("现场会");
            case 4:
                return new CellData<Integer>("申请工资扣除");
            case 5:
                return new CellData<Integer>("批评");
            case 6:
                return new CellData<Integer>("曝光");
            case 7:
                return new CellData<Integer>("体力劳动");
            case 8:
                return new CellData<Integer>("警告");
            case 9:
                return new CellData<Integer>("往来账");
            case 10:
                return new CellData<Integer>("预奖励抵顶");
            default:
                return new CellData<Integer>("错误信息");
        }
    }
}
