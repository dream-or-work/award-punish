package cn.sanli.manage.utils.converter;


import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * @Author wzy
 * @Date 2023/12/12 4:26
 * @Description: Excel表格内容(过错类别)转换器
 * @Version 1.0
 */
public class FaultTypeConverter implements Converter<Integer> {
    @Override
    public Class supportJavaTypeKey() {
        return null;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return null;
    }

    @Override
    public Integer convertToJavaData(CellData cellData, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        return null;
    }

    // 写的时候会调用
    @Override
    public CellData convertToExcelData(Integer integer,
                                       ExcelContentProperty excelContentProperty,
                                       GlobalConfiguration globalConfiguration) throws Exception {
        switch(integer){
            case 1:
                return new CellData<Integer>("信誉");
            case 2:
                return new CellData<Integer>("质量");
            case 3:
                return new CellData<Integer>("档次");
            case 4:
                return new CellData<Integer>("形象");
            case 5:
                return new CellData<Integer>("效率");
            case 6:
                return new CellData<Integer>("安全");
            case 7:
                return new CellData<Integer>("素质");
            case 8:
                return new CellData<Integer>("纪律");
            default:
                return new CellData<Integer>("错误信息");
        }
    }
}
