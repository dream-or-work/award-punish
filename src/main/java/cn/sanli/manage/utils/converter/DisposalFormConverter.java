package cn.sanli.manage.utils.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * @Author wzy
 * @Date 2023/12/12 4:36
 * @Description: Excel表格内容(考核形式)转换器
 * @Version 1.0
 */
public class DisposalFormConverter implements Converter<Integer> {
    @Override
    public Class supportJavaTypeKey() {
        return null;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return null;
    }

    @Override
    public Integer convertToJavaData(CellData cellData, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        return null;
    }

    // 写的时候会调用
    @Override
    public CellData convertToExcelData(Integer integer, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        switch(integer){
            case 1:
                return new CellData<Integer>("通报");
            case 2:
                return new CellData<Integer>("警告单");
            case 3:
                return new CellData<Integer>("责任追究单");
            case 4:
                return new CellData<Integer>("问题整改告知书");
            case 5:
                return new CellData<Integer>("其他形式");
            default:
                return new CellData<Integer>("错误信息");
        }
    }
}
