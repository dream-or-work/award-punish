package cn.sanli.manage.utils;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.pojo.dto.Fault.FaultInfoStatisticsDTO;
import cn.sanli.manage.pojo.dto.Fault.FaultInfoStatisticsRequest;
import cn.sanli.manage.pojo.dto.Fault.PageQueryOfFaultRequest;
import cn.sanli.manage.pojo.dto.Fault.PageQueryOfFaultRequestDTO;
import cn.sanli.manage.pojo.dto.Rewards.PageQueryOfRewardsDTO;
import cn.sanli.manage.pojo.dto.Rewards.PageQueryOfRewardsRequest;
import cn.sanli.manage.pojo.dto.Rewards.RewardsInfoStatisticsDTO;
import cn.sanli.manage.pojo.dto.Rewards.RewardsInfoStatisticsRequest;
import cn.sanli.manage.pojo.dto.Role.RoleAndOrganDTO;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.web.ServiceCode;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author wzy
 * @Date 2024/4/2 10:06
 * @Description: 考核或奖励相关的权限验证操作
 * @Version 1.0
 */
@Slf4j
@Component
public class RoleUtils {


    /**
     *
     * @param queryRequest 查询请求条件
     * @param loginPrincipal 当事人
     * @param organOfCenter 当事人所辖大口
     * @param organOfDept 当事人所辖部门
     * @param name 当事人相关所在部门名称
     * @param roleList 当事人权限列表
     * @return 重组后的查询条件
     */
    // 根据请求查询条件, 用户所辖大口, 部门, 用户权限, 重组考核请求条件
    public static PageQueryOfFaultRequestDTO initByRoleList(PageQueryOfFaultRequest queryRequest,
                                                     LoginPrincipal loginPrincipal,
                                                     List<Integer> organOfCenter,
                                                     List<Integer> organOfDept,
                                                     String name,
                                                     List<Integer> roleList) {
        PageQueryOfFaultRequestDTO requestDTO = new PageQueryOfFaultRequestDTO();
        BeanUtils.copyProperties(queryRequest, requestDTO);
        log.debug("两对象大口与部门类型不同, 所以手动赋值, queryRequest: {}, requestDTO: {}", queryRequest, requestDTO);
        if(queryRequest.getCenter() != null && queryRequest.getCenter() != 0) {
            List<Integer> c = new ArrayList<>();
            c.add(queryRequest.getCenter());
            requestDTO.setCenter(c);
        }
        if(queryRequest.getDept() != null && queryRequest.getDept() != 0) {
            List<Integer> d = new ArrayList<>();
            d.add(queryRequest.getDept());
            requestDTO.setDept(d);
        }

        // 手动设置部分属性初始值, 确保初始值不会出错
        requestDTO.setDisposalDept(null);
        requestDTO.setResDept(null);
        requestDTO.setRoleOfCenter(null);

        List<Integer> listOfCenter = new ArrayList<>();
        List<Integer> listOfDept = new ArrayList<>();

        if(roleList.contains(1) || roleList.contains(7)) {
            // 权限为1, 7则开放所有数据, 用户可以查看所有人员考核信息

            return requestDTO;
        } else if(roleList.contains(2)) {

            // 权限为2则开放部分, 用户仅可查看与自己部门相关的数据
            requestDTO.setResDept(loginPrincipal.getDeptId());
            requestDTO.setDisposalDept(name);

            if (roleList.contains(3) || roleList.contains(4)){ // 如果用户角色为考核管理和高层领导以及大口管理员共存
//                if(queryRequest.getCenter() != null && queryRequest.getCenter() != 0) {
//                    requestDTO.setDisposalDept(null);
//                    requestDTO.setResDept(null);
//                }

                // 考核管理与大口管理员并存, 为硬条件赋值
                requestDTO.setRoleOfCenter(organOfCenter);

                // 判断用户目的, 根据参数给条件赋值

                if(organOfCenter.isEmpty()) { // 判断所辖大口是否为空

                    // 用户所辖大口为空, roleOfCenter设置为当前用户所在大口
                    listOfCenter.add(loginPrincipal.getCenterId());
                    requestDTO.setRoleOfCenter(listOfCenter);
                } else { // 如果所辖大口不为空
                    // 设置查询范围为用户所辖大口范围
                    requestDTO.getRoleOfCenter().addAll(organOfCenter);

                    if(!organOfCenter.contains(loginPrincipal.getCenterId())) { // 判断所在大口是否包含在所辖大口中, 如果不在则添加

                        listOfCenter.add(loginPrincipal.getCenterId());
                        requestDTO.setCenter(listOfCenter);
                    }
                }

                return requestDTO;

            }


            return requestDTO;

        }else if(roleList.contains(3) || roleList.contains(4)) {
            // 权限为3, 4则开放部分, 用户可以查看自己所辖大口的所有数据
            if(organOfCenter.isEmpty()) {
                if(queryRequest.getCenter() == null || queryRequest.getCenter() == 0) {
                    // 如果用户没有所辖大口, 并且条件中大口没有要求, 默认使用所在大口
                    listOfCenter.add(loginPrincipal.getCenterId());
                    requestDTO.setCenter(listOfCenter);
                    return requestDTO;
                } else {
                    // 在条件中对大口明确要求, 判断是否与所在大口相同
                    if(queryRequest.getCenter() == loginPrincipal.getCenterId()) {
                        // 条件中明确的大口为用户所在大口, 给予放行
                        listOfCenter.add(loginPrincipal.getCenterId());
                        requestDTO.setCenter(listOfCenter);
                        return requestDTO;
                    } else {
                        // 条件中明确的大口与用户所在大口不相同, 传入负数阻止查询成功
                        listOfCenter.add(-1);
                        requestDTO.setCenter(listOfCenter);
                        return requestDTO;
                    }
                }

            } else {
                // 如果用户所辖大口内容不为空, 则判断条件中是否明确大口
                if(queryRequest.getCenter() == null || queryRequest.getCenter() == 0) {
                    // 如果条件中没有明确大口, 默认为用户所辖所有大口
                    requestDTO.setCenter(organOfCenter);
                    return requestDTO;
                } else {
                    // 如果条件中明确了想要查询的大口, 则判断是否在用户所辖范围内
                    if(organOfCenter.contains(queryRequest.getCenter())) {
                        // 如果条件中明确的大口在用户所辖范围内, 则赋值给查询条件
                        listOfCenter.add(queryRequest.getCenter());
                        requestDTO.setCenter(listOfCenter);
                        return requestDTO;
                    } else {
                        // 如果条件中明确的大口不在用户所辖范围内, 赋值附属阻止查询成功
                        listOfCenter.add(-1);
                        requestDTO.setCenter(listOfCenter);
                        return requestDTO;
                    }
                }

            }

        } else if(roleList.contains(5) || roleList.contains(6)) {
            // 权限为5, 6则开放部分, 用户仅可查看自己所辖部门的所有数据
            if(organOfDept.isEmpty()) {
                if(queryRequest.getDept() == null || queryRequest.getDept() == 0) {
                    // 如果用户没有所辖部门, 并且条件中大口没有要求, 默认使用所在部门
                    listOfDept.add(loginPrincipal.getDeptId());
                    requestDTO.setDept(listOfDept);
                    return requestDTO;
                } else {
                    // 在条件中对部门明确要求, 判断是否与所在部门相同
                    if(queryRequest.getDept() == loginPrincipal.getDeptId()) {
                        // 条件中明确的部门为用户所在部门, 给予放行
                        listOfDept.add(loginPrincipal.getDeptId());
                        requestDTO.setDept(listOfDept);
                        return requestDTO;
                    } else {
                        // 条件中明确的部门与用户所在部门不相同, 传入负数阻止查询成功
                        listOfDept.add(-1);
                        requestDTO.setDept(listOfDept);
                        return requestDTO;
                    }
                }

            } else {
                // 如果用户所辖部门内容不为空, 则判断条件中是否明确部门
                if(queryRequest.getDept() == null || queryRequest.getDept() == 0) {
                    // 如果条件中没有明确部门, 默认为用户所辖所有部门
                    requestDTO.setDept(organOfDept);
                    return requestDTO;
                } else {
                    // 如果条件中明确了想要查询的部门, 则判断是否在用户所辖范围内
                    if(organOfDept.contains(queryRequest.getDept())) {
                        // 如果条件中明确的部门在用户所辖范围内, 则赋值给查询条件
                        listOfDept.add(queryRequest.getDept());
                        requestDTO.setDept(listOfDept);
                        return requestDTO;
                    } else {
                        // 如果条件中明确的部门不在用户所辖范围内, 赋值附属阻止查询成功
                        listOfDept.add(-1);
                        requestDTO.setDept(listOfDept);
                        return requestDTO;
                    }
                }

            }
        } else if(roleList.contains(8)) {
            // 权限为8则开放部分数据, 用户仅可查看自己被考核的相关数据
            requestDTO.setNumber(loginPrincipal.getNum());
            return requestDTO;
        }

        return null;
    }

    /**
     *
     * @param queryRequest 分页查询条件
     * @param loginPrincipal 当事人
     * @param organOfCenter 当事人所辖大口
     * @param organOfDept 当事人所辖部门
     * @param roleList 当事人角色列表
     * @return PageQueryOfRewardsDTO
     */
    // 根据请求查询条件, 用户所辖大口, 部门, 用户权限, 重组奖励请求条件
    public static PageQueryOfRewardsDTO initByRoleList(PageQueryOfRewardsRequest queryRequest,
                                                            LoginPrincipal loginPrincipal,
                                                            List<Integer> organOfCenter,
                                                            List<Integer> organOfDept,
                                                            List<Integer> roleList,
                                                            Integer rewardsDept) {
        PageQueryOfRewardsDTO rewardsRequestDTO = new PageQueryOfRewardsDTO();
        BeanUtils.copyProperties(queryRequest, rewardsRequestDTO);
        log.debug("两对象大口与部门类型不同, 所以手动赋值, queryRequest: {}, requestDTO: {}", queryRequest, rewardsRequestDTO);
        if(queryRequest.getCenter() != null && queryRequest.getCenter() != 0) {
            List<Integer> c = new ArrayList<>();
            c.add(queryRequest.getCenter());
            rewardsRequestDTO.setCenter(c);
        }
        if(queryRequest.getDept() != null && queryRequest.getDept() != 0) {
            List<Integer> d = new ArrayList<>();
            d.add(queryRequest.getDept());
            rewardsRequestDTO.setDept(d);
        }

        List<Integer> listOfCenter = new ArrayList<>();
        List<Integer> listOfDept = new ArrayList<>();

        if(roleList.contains(1) || roleList.contains(7)) {
            // 权限为1, 7则开放所有数据, 用户可以查看所有人员考核信息

            return rewardsRequestDTO;
        } else if(roleList.contains(2)) {

            if (roleList.contains(3) || roleList.contains(4)){ // 如果用户角色为考核管理和高层领导以及大口管理员共存
                if(queryRequest.getCenter() != null && queryRequest.getCenter() != 0) {
                    rewardsRequestDTO.setResDept(null);
                    rewardsRequestDTO.setRewardsDept(null);
                }
                rewardsRequestDTO.setResDept(null);
                rewardsRequestDTO.setRewardsDept(null);
                if(organOfCenter.isEmpty()) { // 判断所辖大口是否为空
                    listOfCenter.add(loginPrincipal.getCenterId());
                    rewardsRequestDTO.setCenter(listOfCenter);
                } else { // 如果所辖大口不为空
                    if(organOfCenter.contains(loginPrincipal.getCenterId())) { // 判断所在大口是否包含在所辖大口中, 如果不在则添加
                        rewardsRequestDTO.setCenter(organOfCenter);
                    } else {
                        listOfCenter.add(loginPrincipal.getCenterId());
                        rewardsRequestDTO.setCenter(listOfCenter);
                    }
                }
                return rewardsRequestDTO;

            }
            // 权限为2则开放部分, 用户仅可查看与自己部门相关的数据
            rewardsRequestDTO.setResDept(loginPrincipal.getDeptId());
            rewardsRequestDTO.setRewardsDept(rewardsDept);
            return rewardsRequestDTO;

        }else if(roleList.contains(3) || roleList.contains(4)) {
            // 权限为3, 4则开放部分, 用户可以查看自己所辖大口的所有数据
            if(organOfCenter.isEmpty()) {
                if(queryRequest.getCenter() == null || queryRequest.getCenter() == 0) {
                    // 如果用户没有所辖大口, 并且条件中大口没有要求, 默认使用所在大口
                    listOfCenter.add(loginPrincipal.getCenterId());
                    rewardsRequestDTO.setCenter(listOfCenter);
                    return rewardsRequestDTO;
                } else {
                    // 在条件中对大口明确要求, 判断是否与所在大口相同
                    if(queryRequest.getCenter() == loginPrincipal.getCenterId()) {
                        // 条件中明确的大口为用户所在大口, 给予放行
                        listOfCenter.add(loginPrincipal.getCenterId());
                        rewardsRequestDTO.setCenter(listOfCenter);
                        return rewardsRequestDTO;
                    } else {
                        // 条件中明确的大口与用户所在大口不相同, 传入负数阻止查询成功
                        listOfCenter.add(-1);
                        rewardsRequestDTO.setCenter(listOfCenter);
                        return rewardsRequestDTO;
                    }
                }

            } else {
                // 如果用户所辖大口内容不为空, 则判断条件中是否明确大口
                if(queryRequest.getCenter() == null || queryRequest.getCenter() == 0) {
                    // 如果条件中没有明确大口, 默认为用户所辖所有大口
                    rewardsRequestDTO.setCenter(organOfCenter);
                    return rewardsRequestDTO;
                } else {
                    // 如果条件中明确了想要查询的大口, 则判断是否在用户所辖范围内
                    if(organOfCenter.contains(queryRequest.getCenter())) {
                        // 如果条件中明确的大口在用户所辖范围内, 则赋值给查询条件
                        listOfCenter.add(queryRequest.getCenter());
                        rewardsRequestDTO.setCenter(listOfCenter);
                        return rewardsRequestDTO;
                    } else {
                        // 如果条件中明确的大口不在用户所辖范围内, 赋值附属阻止查询成功
                        listOfCenter.add(-1);
                        rewardsRequestDTO.setCenter(listOfCenter);
                        return rewardsRequestDTO;
                    }
                }

            }

        } else if(roleList.contains(5) || roleList.contains(6)) {
            // 权限为5, 6则开放部分, 用户仅可查看自己所辖部门的所有数据
            if(organOfDept.isEmpty()) {
                if(queryRequest.getDept() == null || queryRequest.getDept() == 0) {
                    // 如果用户没有所辖部门, 并且条件中大口没有要求, 默认使用所在部门
                    listOfDept.add(loginPrincipal.getDeptId());
                    rewardsRequestDTO.setDept(listOfDept);
                    return rewardsRequestDTO;
                } else {
                    // 在条件中对部门明确要求, 判断是否与所在部门相同
                    if(queryRequest.getDept() == loginPrincipal.getDeptId()) {
                        // 条件中明确的部门为用户所在部门, 给予放行
                        listOfDept.add(loginPrincipal.getDeptId());
                        rewardsRequestDTO.setDept(listOfDept);
                        return rewardsRequestDTO;
                    } else {
                        // 条件中明确的部门与用户所在部门不相同, 传入负数阻止查询成功
                        listOfDept.add(-1);
                        rewardsRequestDTO.setDept(listOfDept);
                        return rewardsRequestDTO;
                    }
                }

            } else {
                // 如果用户所辖部门内容不为空, 则判断条件中是否明确部门
                if(queryRequest.getDept() == null || queryRequest.getDept() == 0) {
                    // 如果条件中没有明确部门, 默认为用户所辖所有部门
                    rewardsRequestDTO.setDept(organOfDept);
                    return rewardsRequestDTO;
                } else {
                    // 如果条件中明确了想要查询的部门, 则判断是否在用户所辖范围内
                    if(organOfDept.contains(queryRequest.getDept())) {
                        // 如果条件中明确的部门在用户所辖范围内, 则赋值给查询条件
                        listOfDept.add(queryRequest.getDept());
                        rewardsRequestDTO.setDept(listOfDept);
                        return rewardsRequestDTO;
                    } else {
                        // 如果条件中明确的部门不在用户所辖范围内, 赋值附属阻止查询成功
                        listOfDept.add(-1);
                        rewardsRequestDTO.setDept(listOfDept);
                        return rewardsRequestDTO;
                    }
                }

            }
        } else if(roleList.contains(8)) {
            // 权限为8则开放部分数据, 用户仅可查看自己被考核的相关数据
            rewardsRequestDTO.setNum(loginPrincipal.getNum());
            return rewardsRequestDTO;
        }

        return null;
    }


    /**
     * 统计相关功能所用根据当事人信息重组统计查询请求(考核)
     *
     * @param statisticsRequest 统计查询请求
     * @param loginPrincipal 当事人
     * @param organOfCenter 当事人所辖大口
     * @param organOfDept 当事人所辖部门
     * @param roleList 当事人角色列表
     * @param disposalDept 当事人部门名称
     * @return FaultInfoStatisticsRequest重组后的查询请求条件
     */
    public static FaultInfoStatisticsDTO initByRoleList(FaultInfoStatisticsRequest statisticsRequest,
                                                        LoginPrincipal loginPrincipal,
                                                        List<Integer> organOfCenter,
                                                        List<Integer> organOfDept,
                                                        List<Integer> roleList,
                                                        String disposalDept) {
        FaultInfoStatisticsDTO statisticsDTO = new FaultInfoStatisticsDTO();
        BeanUtils.copyProperties(statisticsRequest, statisticsDTO);
        log.debug("同名属性赋值后: request: {}  DTO: {}", statisticsRequest, statisticsDTO);

        // 前后对象属性类型不同, 进行判断并手动复制
        if(statisticsRequest.getResCenter() != null && statisticsRequest.getResCenter() != 0) {
            List<Integer> listOfCenter = new ArrayList<>();
            listOfCenter.add(statisticsRequest.getResCenter());
            statisticsDTO.setResCenter(listOfCenter);
        }
        if(statisticsRequest.getResDept() != null && statisticsRequest.getResDept() != 0) {
            List<Integer> listOfDept = new ArrayList<>();
            listOfDept.add(statisticsRequest.getResDept());
            statisticsDTO.setResDept(listOfDept);
        }

        // 判断时间参数是否为null, null则赋值只搜索今年的数据
        if(statisticsRequest.getStartTime() == null && statisticsRequest.getEndTime() == null) {
            // 分别获取本周,本月,本季,半年,本年的时间段
            Map<String, Date[]> statisticsTime = DateUtils.getStatisticsTime();
            // 首先限制查询时间为当前年份的数据
            statisticsDTO.setStartTime(statisticsTime.get(DateUtils.THISYEAR)[0]);
            statisticsDTO.setEndTime(statisticsTime.get(DateUtils.THISYEAR)[1]);
        }

        // 根据不同角色做不同处理
        if(roleList.contains(1) || roleList.contains(7)) {
            // 权限为1, 7则开放所有数据, 用户可以查看所有人员考核信息

            return statisticsDTO;
        } else if(roleList.contains(2)) {
            // 权限为2则开放部分, 用户仅可查看与自己部门相关的数据
            List<Integer> listOfDept = new ArrayList<>();
            listOfDept.add(loginPrincipal.getDeptId());
            statisticsDTO.setResDept(listOfDept);
            statisticsDTO.setDisposalDept(disposalDept);
            return statisticsDTO;

        }else if(roleList.contains(3) || roleList.contains(4)) {
            // 权限为3, 4则开放部分, 用户可以查看自己所辖大口的所有数据
            if(organOfCenter.isEmpty()) {
                if(statisticsRequest.getResCenter() == null || statisticsRequest.getResCenter() == 0) {
                    // 如果用户没有所辖大口, 并且条件中大口没有要求, 默认使用所在大口
                    List<Integer> listOfCenter = new ArrayList<>();
                    listOfCenter.add(loginPrincipal.getCenterId());
                    statisticsDTO.setResCenter(listOfCenter);
                    return statisticsDTO;
                } else {
                    // 在条件中对大口明确要求, 判断是否与所在大口相同
                    if(statisticsRequest.getResCenter() == loginPrincipal.getCenterId()) {
                        // 条件中明确的大口为用户所在大口, 给予放行
                        List<Integer> listOfCenter = new ArrayList<>();
                        listOfCenter.add(loginPrincipal.getCenterId());
                        statisticsDTO.setResCenter(listOfCenter);
                        return statisticsDTO;
                    } else {
                        // 条件中明确的大口与用户所在大口不相同, 传入负数阻止查询成功
                        List<Integer> listOfCenter = new ArrayList<>();
                        listOfCenter.add(-1);
                        statisticsDTO.setResCenter(listOfCenter);
                        return statisticsDTO;
                    }
                }

            } else {
                // 如果用户所辖大口内容不为空, 则判断条件中是否明确大口
                if(statisticsRequest.getResCenter() == null || statisticsRequest.getResCenter() == 0) {
                    // 如果条件中没有明确大口, 默认为用户所辖所有大口
                    statisticsDTO.setResCenter(organOfCenter);
                    return statisticsDTO;
                } else {
                    // 如果条件中明确了想要查询的大口, 则判断是否在用户所辖范围内
                    if(organOfCenter.contains(statisticsRequest.getResCenter())) {
                        // 如果条件中明确的大口在用户所辖范围内, 则赋值给查询条件
                        List<Integer> listOfCenter = new ArrayList<>();
                        listOfCenter.add(statisticsRequest.getResCenter());
                        statisticsDTO.setResCenter(listOfCenter);
                        return statisticsDTO;
                    } else {
                        // 如果条件中明确的大口不在用户所辖范围内, 赋值附属阻止查询成功
                        List<Integer> listOfCenter = new ArrayList<>();
                        listOfCenter.add(-1);
                        statisticsDTO.setResCenter(listOfCenter);
                        return statisticsDTO;
                    }
                }

            }

        } else if(roleList.contains(5) || roleList.contains(6)) {
            // 权限为5, 6则开放部分, 用户仅可查看自己所辖部门的所有数据
            if(organOfDept.isEmpty()) {
                if(statisticsRequest.getResDept() == null || statisticsRequest.getResDept() == 0) {
                    // 如果用户没有所辖部门, 并且条件中大口没有要求, 默认使用所在部门
                    List<Integer> listOfDept = new ArrayList<>();
                    listOfDept.add(loginPrincipal.getDeptId());
                    statisticsDTO.setResDept(listOfDept);
                    return statisticsDTO;
                } else {
                    // 在条件中对部门明确要求, 判断是否与所在部门相同
                    if(statisticsRequest.getResDept() == loginPrincipal.getDeptId()) {
                        // 条件中明确的部门为用户所在部门, 给予放行
                        List<Integer> listOfDept = new ArrayList<>();
                        listOfDept.add(loginPrincipal.getDeptId());
                        statisticsDTO.setResDept(listOfDept);
                        return statisticsDTO;
                    } else {
                        // 条件中明确的部门与用户所在部门不相同, 传入负数阻止查询成功
                        List<Integer> listOfDept = new ArrayList<>();
                        listOfDept.add(-1);
                        statisticsDTO.setResDept(listOfDept);
                        return statisticsDTO;
                    }
                }

            } else {
                // 如果用户所辖部门内容不为空, 则判断条件中是否明确部门
                if(statisticsRequest.getResDept() == null || statisticsRequest.getResDept() == 0) {
                    // 如果条件中没有明确部门, 默认为用户所辖所有部门
                    statisticsDTO.setResDept(organOfDept);
                    return statisticsDTO;
                } else {
                    // 如果条件中明确了想要查询的部门, 则判断是否在用户所辖范围内
                    if(organOfDept.contains(statisticsRequest.getResDept())) {
                        // 如果条件中明确的部门在用户所辖范围内, 则赋值给查询条件
                        List<Integer> listOfDept = new ArrayList<>();
                        listOfDept.add(statisticsRequest.getResDept());
                        statisticsDTO.setResDept(listOfDept);
                        return statisticsDTO;
                    } else {
                        // 如果条件中明确的部门不在用户所辖范围内, 赋值附属阻止查询成功
                        List<Integer> listOfDept = new ArrayList<>();
                        listOfDept.add(-1);
                        statisticsDTO.setResDept(listOfDept);
                        return statisticsDTO;
                    }
                }

            }
        } else if(roleList.contains(8)) {
            // 权限为8则开放部分数据, 用户仅可查看自己被考核的相关数据
            statisticsDTO.setResNum(Integer.valueOf(loginPrincipal.getNum()));
            return statisticsDTO;
        }

        return null;
    }


    /**
     * 统计相关功能所用根据当事人信息重组统计查询请求(奖励)
     *
     * @param statisticsRequest 统计查询请求
     * @param loginPrincipal 当事人
     * @param organOfCenter 当事人所辖大口
     * @param organOfDept 当事人所辖部门
     * @param roleList 当事人角色列表
     * @return FaultInfoStatisticsRequest重组后的查询请求条件
     */
    public static RewardsInfoStatisticsDTO initByRoleList(RewardsInfoStatisticsRequest statisticsRequest,
                                                        LoginPrincipal loginPrincipal,
                                                        List<Integer> organOfCenter,
                                                        List<Integer> organOfDept,
                                                        List<Integer> roleList) {
        RewardsInfoStatisticsDTO statisticsDTO = new RewardsInfoStatisticsDTO();
        BeanUtils.copyProperties(statisticsRequest, statisticsDTO);
        log.debug("同名属性赋值后: request: {}  DTO: {}", statisticsRequest, statisticsDTO);

        // 前后对象属性类型不同, 进行判断并手动复制
        if(statisticsRequest.getCenter() != null && statisticsRequest.getCenter() != 0) {
            List<Integer> listOfCenter = new ArrayList<>();
            listOfCenter.add(statisticsRequest.getCenter());
            statisticsDTO.setCenter(listOfCenter);
        }
        if(statisticsRequest.getDept() != null && statisticsRequest.getDept() != 0) {
            List<Integer> listOfDept = new ArrayList<>();
            listOfDept.add(statisticsRequest.getDept());
            statisticsDTO.setDept(listOfDept);
        }

        // 判断时间参数是否为null, null则赋值只搜索今年的数据
        if(statisticsRequest.getStartTime() == null && statisticsRequest.getEndTime() == null) {
            // 分别获取本周,本月,本季,半年,本年的时间段
            Map<String, Date[]> statisticsTime = DateUtils.getStatisticsTime();
            // 首先限制查询时间为当前年份的数据
            statisticsDTO.setStartTime(statisticsTime.get(DateUtils.THISYEAR)[0]);
            statisticsDTO.setEndTime(statisticsTime.get(DateUtils.THISYEAR)[1]);
        }

        // 根据不同角色做不同处理
        if(roleList.contains(1) || roleList.contains(7)) {
            // 权限为1, 7则开放所有数据, 用户可以查看所有人员考核信息

            return statisticsDTO;
        } else if(roleList.contains(2)) {
            // 权限为2则开放部分, 用户仅可查看与自己部门相关的数据
            List<Integer> listOfDept = new ArrayList<>();
            listOfDept.add(loginPrincipal.getDeptId());
            statisticsDTO.setDept(listOfDept);
            statisticsDTO.setRewardsDept(loginPrincipal.getDeptId());
            return statisticsDTO;

        }else if(roleList.contains(3) || roleList.contains(4)) {
            // 权限为3, 4则开放部分, 用户可以查看自己所辖大口的所有数据
            if(organOfCenter.isEmpty()) {
                if(statisticsRequest.getCenter() == null || statisticsRequest.getCenter() == 0) {
                    // 如果用户没有所辖大口, 并且条件中大口没有要求, 默认使用所在大口
                    List<Integer> listOfCenter = new ArrayList<>();
                    listOfCenter.add(loginPrincipal.getCenterId());
                    statisticsDTO.setCenter(listOfCenter);
                    return statisticsDTO;
                } else {
                    // 在条件中对大口明确要求, 判断是否与所在大口相同
                    if(statisticsRequest.getCenter() == loginPrincipal.getCenterId()) {
                        // 条件中明确的大口为用户所在大口, 给予放行
                        List<Integer> listOfCenter = new ArrayList<>();
                        listOfCenter.add(loginPrincipal.getCenterId());
                        statisticsDTO.setCenter(listOfCenter);
                        return statisticsDTO;
                    } else {
                        // 条件中明确的大口与用户所在大口不相同, 传入负数阻止查询成功
                        List<Integer> listOfCenter = new ArrayList<>();
                        listOfCenter.add(-1);
                        statisticsDTO.setCenter(listOfCenter);
                        return statisticsDTO;
                    }
                }

            } else {
                // 如果用户所辖大口内容不为空, 则判断条件中是否明确大口
                if(statisticsRequest.getCenter() == null || statisticsRequest.getCenter() == 0) {
                    // 如果条件中没有明确大口, 默认为用户所辖所有大口
                    statisticsDTO.setCenter(organOfCenter);
                    return statisticsDTO;
                } else {
                    // 如果条件中明确了想要查询的大口, 则判断是否在用户所辖范围内
                    if(organOfCenter.contains(statisticsRequest.getCenter())) {
                        // 如果条件中明确的大口在用户所辖范围内, 则赋值给查询条件
                        List<Integer> listOfCenter = new ArrayList<>();
                        listOfCenter.add(statisticsRequest.getCenter());
                        statisticsDTO.setCenter(listOfCenter);
                        return statisticsDTO;
                    } else {
                        // 如果条件中明确的大口不在用户所辖范围内, 赋值附属阻止查询成功
                        List<Integer> listOfCenter = new ArrayList<>();
                        listOfCenter.add(-1);
                        statisticsDTO.setCenter(listOfCenter);
                        return statisticsDTO;
                    }
                }

            }

        } else if(roleList.contains(5) || roleList.contains(6)) {
            // 权限为5, 6则开放部分, 用户仅可查看自己所辖部门的所有数据
            if(organOfDept.isEmpty()) {
                if(statisticsRequest.getDept() == null || statisticsRequest.getDept() == 0) {
                    // 如果用户没有所辖部门, 并且条件中大口没有要求, 默认使用所在部门
                    List<Integer> listOfDept = new ArrayList<>();
                    listOfDept.add(loginPrincipal.getDeptId());
                    statisticsDTO.setDept(listOfDept);
                    return statisticsDTO;
                } else {
                    // 在条件中对部门明确要求, 判断是否与所在部门相同
                    if(statisticsRequest.getDept() == loginPrincipal.getDeptId()) {
                        // 条件中明确的部门为用户所在部门, 给予放行
                        List<Integer> listOfDept = new ArrayList<>();
                        listOfDept.add(loginPrincipal.getDeptId());
                        statisticsDTO.setDept(listOfDept);
                        return statisticsDTO;
                    } else {
                        // 条件中明确的部门与用户所在部门不相同, 传入负数阻止查询成功
                        List<Integer> listOfDept = new ArrayList<>();
                        listOfDept.add(-1);
                        statisticsDTO.setDept(listOfDept);
                        return statisticsDTO;
                    }
                }

            } else {
                // 如果用户所辖部门内容不为空, 则判断条件中是否明确部门
                if(statisticsRequest.getDept() == null || statisticsRequest.getDept() == 0) {
                    // 如果条件中没有明确部门, 默认为用户所辖所有部门
                    statisticsDTO.setDept(organOfDept);
                    return statisticsDTO;
                } else {
                    // 如果条件中明确了想要查询的部门, 则判断是否在用户所辖范围内
                    if(organOfDept.contains(statisticsRequest.getDept())) {
                        // 如果条件中明确的部门在用户所辖范围内, 则赋值给查询条件
                        List<Integer> listOfDept = new ArrayList<>();
                        listOfDept.add(statisticsRequest.getDept());
                        statisticsDTO.setDept(listOfDept);
                        return statisticsDTO;
                    } else {
                        // 如果条件中明确的部门不在用户所辖范围内, 赋值附属阻止查询成功
                        List<Integer> listOfDept = new ArrayList<>();
                        listOfDept.add(-1);
                        statisticsDTO.setDept(listOfDept);
                        return statisticsDTO;
                    }
                }

            }
        } else if(roleList.contains(8)) {
            // 权限为8则开放部分数据, 用户仅可查看自己被考核的相关数据
            statisticsDTO.setNum(Integer.valueOf(loginPrincipal.getNum()));
            return statisticsDTO;
        }

        return null;
    }


    /**
     * 根据被考核或奖励人员所属大口, 部门判断操作者是否有权限
     *
     * @param dept 被考核/奖励人所属部门
     * @param center 被考核/奖励人所属大口
     * @param loginPrincipal 操作者
     * @param organOfCenter 当前用户所辖大口
     * @param organOfDept 当前用户所辖部门
     * @param roleList 当前用户权限列表
     */
    public static boolean passOrNot(String dept, String center,
                             LoginPrincipal loginPrincipal,
                             List<Integer> organOfCenter,
                             List<Integer> organOfDept,
                             List<Integer> roleList) {
        // 先判断当前用户权限
        if(roleList.contains(8) || (roleList.contains(7) && roleList.size() == 1)) {
            log.debug("当前用户角色为:{}或{}", 7, 8);
            // 权限为7,8则直接抛出错误, 信息查看员和普通员工没有操作数据的权限
            throw new ServiceException(ServiceCode.ERROR_INSERT, "信息查看员或普通员工用户没有操作数据权限");
        } else if(roleList.contains(1) || roleList.contains(2)) {
            // 权限为1,2则开放所有数据, 用户可以操作所有数据
            log.debug("当前用户角色为:{}或{}", 1, 2);
            return true;
        } else if(roleList.contains(3) || roleList.contains(4)) {
            // 权限为3, 4则开放部分, 用户可以查看自己所辖大口的所有数据
            log.debug("当前用户角色为:{}或{}", 3, 4);
            if(organOfCenter.isEmpty()) {
                // 如果所辖大口为空, 则判断被处理人员是否与处理人员相同大口
                if(Integer.parseInt(center) == loginPrincipal.getCenterId()) {
                    log.debug("当前用户与被处理人员在同一大口: {}", center);
                    // 允许大口管理员处理所在大口人员
                    return true;
                } else {
                    throw new ServiceException(ServiceCode.ERROR_INSERT, "您没有权限处理您所在大口之外的其他大口");
                }
            } else {
                // 如果所辖大口不为空, 判断被处理人员是否在用户所辖范围内
                if(organOfCenter.contains(Integer.parseInt(center))) {
                    // 被处理人在当前用户全县范围内
                    log.debug("被处理人大口在当前用户所辖大口范围内");
                    return true;
                } else {
                    throw new ServiceException(ServiceCode.ERROR_INSERT, "被处理人员大口不在您权限范围内");
                }
            }

        } else if(roleList.contains(5) || roleList.contains(6)) {
            // 权限为5, 6则开放部分, 用户可以查看自己所辖大口的所有数据
            log.debug("当前用户角色为:{}或{}", 5, 6);
            if(organOfDept.isEmpty()) {
                // 如果所辖部门为空, 则判断被处理人员是否与处理人员相同部门
                if(Integer.parseInt(dept) == loginPrincipal.getDeptId()) {
                    // 被处理人与当前用户同一部门, 在权限范围内
                    log.debug("被处理人与当前用户同一部门, 在权限范围内");
                    return true;
                } else {
                    throw new ServiceException(ServiceCode.ERROR_INSERT, "您没有权限处理您所在部门之外的其他部门");
                }
            } else {
                // 如果所辖部门不为空, 判断被处理人员是否在当前用户所辖范围内
                if(organOfDept.contains(Integer.parseInt(dept))) {
                    // 被处理人所在部门在当前用户所辖范围内, 允许操作
                    return true;
                } else {
                    throw new ServiceException(ServiceCode.ERROR_INSERT, "被处理人员所在部门不在您权限范围内");
                }
            }
        }
        return false;
    }



    /**
     * 根据当事人信息查询当事人权限列表
     *
     * @param loginPrincipal 当事人
     * @return 当事人权限列表
     */
    public static List<Integer> getRoleList(LoginPrincipal loginPrincipal) {
        // 获取用户权限列表
        String roles = loginPrincipal.getRoles();
        List<String> authorityRole= JSON.parseArray(roles, String.class);
        List<Integer> roleList = new ArrayList<>();
        for (String auth :authorityRole){
            System.out.println(auth);
            JSONObject json = JSONObject.parseObject(auth);
            System.out.println("json对象为"+json);
            String authority = json.getString("authority");
            System.out.println(authority);
            roleList.add(Integer.valueOf(authority));
        }
        log.debug("获取到的权限列表: {}", roleList);
        return roleList;
    }





}
