package cn.sanli.manage.controller;

import cn.sanli.manage.pojo.dto.Role.AssignRoleDto;
import cn.sanli.manage.pojo.dto.Role.AssignRoleLvDto;
import cn.sanli.manage.pojo.entity.Role;
import cn.sanli.manage.pojo.entity.UserRole;
import cn.sanli.manage.pojo.vo.UserMessage;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.UserRoleService;
import cn.sanli.manage.service.UserService;
import cn.sanli.manage.web.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 用户组织关联表(dbo.userRole)表控制层
 *
 * @author xxxxx
 */
@Api(tags = "用户角色管理")
@RestController
@RequestMapping("/userRole")
public class UserRoleController {
    /**
     * 服务对象
     */
    @Resource
    private UserRoleService userRoleService;
    @Resource
    private UserService userService;

    /**
     * 查询所有角色
     *
     * @return
     */
    @ApiOperation("查询所有角色")
    @GetMapping(value = "/findAllRoles")
    public JsonResult<Map<String, Object>> findAllRoles() {
        Map<String, Object> resultMap = userRoleService.findAllRoles();
        return JsonResult.ok(resultMap);
    }

    /**
     * 查询所有角色 -角色数据回显
     *
     * @param num 用户id
     * @return
     */
    @ApiOperation("查询所有角色和用户所有角色")
    @GetMapping(value = "/findRoles")
    public JsonResult<Map<String, Object>> findAllRoles(@ApiParam(name = "num", value = "工号", required = true)
                                                        @RequestParam String num) {
        Map<String, Object> resultMap = userRoleService.findAllRoles(num);
        return JsonResult.ok(resultMap);
    }

    @ApiOperation("查看当前登录用户以下角色")
    @GetMapping(value = "/findUserRoles")
    public JsonResult<List<Role>> findRoles(@ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        //获取登录用户信息
        /*String num = loginPrincipal.getNum();
        UserMessage message = userService.message(num);
        List<Role> roleList = userRoleService.findRoles(message);*/
        List<Role> roleList = userRoleService.findRoles(loginPrincipal);
        return JsonResult.ok(roleList);
    }

    /*@ApiOperation("用户分配角色")
    @PostMapping("/assignRole")
    public JsonResult assignRole(@RequestBody AssignRoleDto assignRoleDto) {
        userRoleService.assignRole(assignRoleDto);
        return JsonResult.ok();
    }*/

    /*@ApiOperation("指定主权限")
    @PostMapping("/assignRoleLv")
    public JsonResult assignRoleLv(@RequestBody AssignRoleLvDto assignRoleLvDto) {
        userRoleService.assignRoleLv(assignRoleLvDto);
        return JsonResult.ok();
    }*/

}
