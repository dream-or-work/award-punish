package cn.sanli.manage.controller;

import cn.sanli.manage.pojo.dto.User.DeleteUserOrganizationDTO;
import cn.sanli.manage.pojo.dto.User.UserOrganizationListDTO;
import cn.sanli.manage.pojo.dto.User.UserOrganizationDTO;
import cn.sanli.manage.pojo.vo.UserOrganizationVO;
import cn.sanli.manage.service.UserOrganizationService;
import cn.sanli.manage.web.JsonResult;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 用户组织关联表(dbo.userOrganization)表控制层
 *
 * @author xxxxx
 */
@Api(tags = "用户组织结构接口")
@RestController
@RequestMapping("system/userOrganization")
public class UserOrganizationController {
    /**
     * 服务对象
     */
    @Resource
    private UserOrganizationService userOrganizationService;

    /**
     * 获取用户组织结构列表
     * @param userOrganizationDTO
     * @return
     */
    @ApiOperation(value = "获取用户组织结构列表")
    @PreAuthorize("hasAnyAuthority('1')")
    @PostMapping("/findByPage")
    public JsonResult<PageInfo<UserOrganizationVO>> findByPage(
            @ApiParam(name = "userOrganizationDTO", value = "查询对象")
            @RequestBody UserOrganizationDTO userOrganizationDTO
    ) {
        PageInfo<UserOrganizationVO> pageInfo = userOrganizationService.findByPage(userOrganizationDTO);
        return JsonResult.ok(pageInfo);
    }

    /**
     * 添加用户组织机构
     * @param info 添加信息
     * @return
     */
    @ApiOperation(value = "添加用户组织结构")
    @PreAuthorize("hasAnyAuthority('1')")
    @PostMapping("/saveUserOrganization")
    public JsonResult saveUserOrganization(@RequestBody UserOrganizationListDTO info) {
        userOrganizationService.saveUserOrganization(info);
        return JsonResult.ok();
    }

    /**
     * 修改用户组织机构
     * @param info 修改信息
     * @return
     */
    @ApiOperation(value = "修改用户组织结构")
    @PreAuthorize("hasAnyAuthority('1')")
    @PostMapping("/updateUserOrganization")
    public JsonResult updateUserOrganization(@RequestBody UserOrganizationListDTO info) {
        userOrganizationService.updateUserOrganization(info);
        return JsonResult.ok();
    }

    /**
     * 删除用户组织机构
     * @param info 删除信息
     * @return
     */
    @ApiOperation(value = "删除用户组织结构")
    @PreAuthorize("hasAnyAuthority('1')")
    @PostMapping("/deleteUserOrganization")
    public JsonResult deleteUserOrganization(@RequestBody DeleteUserOrganizationDTO info) {
        userOrganizationService.deleteUserOrganization(info);
        return JsonResult.ok();
    }
}
