package cn.sanli.manage.controller;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.mapper.data1.UserOrganizationMapper;
import cn.sanli.manage.pojo.dto.Rewards.*;
import cn.sanli.manage.pojo.dto.Role.RoleAndOrganDTO;
import cn.sanli.manage.pojo.dto.User.BatchDeleteUserDTO;
import cn.sanli.manage.pojo.entity.RewardsCollective;
import cn.sanli.manage.pojo.vo.Rewards.PagingQueryRewardsVO;
import cn.sanli.manage.pojo.vo.Rewards.RewardsCollectiveVO;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.RewardsCollectiveService;
import cn.sanli.manage.service.impl.UserInfoServiceImpl;
import cn.sanli.manage.utils.DateUtils;
import cn.sanli.manage.utils.EasyExcelUtils;
import cn.sanli.manage.utils.RoleUtils;
import cn.sanli.manage.utils.StringUtils;
import cn.sanli.manage.web.JsonResult;
import cn.sanli.manage.web.ServiceCode;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * ClassName:RewardsCollectiveController
 * Package:cn.sanli.manage.controller
 * Description:
 *
 * @Author Lu
 * @Create 2024/4/11 10:43
 * @Version 1.0
 */
@RestController
@RequestMapping("/rewardsCollective")
@Slf4j
@Api(tags = "集体奖励信息管理接口")
public class RewardsCollectiveController {

    @Resource
    private RewardsCollectiveService rewardsCollectiveService;
    @Resource
    private UserOrganizationMapper userOrganizationMapper;

    @GetMapping("/queryOne")
    @ApiOperation("查询集体奖励信息")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public JsonResult<RewardsCollectiveVO> findById(Integer id) {
        log.debug("开始查询id为{}的奖励信息表中的数据", id);
        if (id == null || id == 0) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数不能为空");
        }
        RewardsCollectiveVO rewardsCollective = rewardsCollectiveService.selectAllById(id);
        return JsonResult.ok(rewardsCollective);
    }

    /**
     * 新增数据
     *
     * @param rewardsCollectiveDTO 实体
     * @return 新增结果
     */
    @PostMapping("/saveRewardsCollective")
    @ApiOperation(value = "添加集体奖励信息")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    public JsonResult<?> saveRewardsCollective(@RequestBody SaveRewardsCollectiveDTO rewardsCollectiveDTO,
                                               @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        log.debug("开始处理新增奖励信息请求, 参数: {}", rewardsCollectiveDTO.toString());
        if(StringUtils.isAnyBlank(
                rewardsCollectiveDTO.getReasons(), /*rewardsCollectiveDTO.getRewardsDept(),*/
                rewardsCollectiveDTO.getRewardsName(), rewardsCollectiveDTO.getRewardsNum()))
        {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数有误");
        }

        /*RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);
        log.debug("当前用户权限列表: {}, 及所辖大口: {}, 所辖部门:{}", roleAndOrganDTO.getRoleList(),
                roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept());*/
        boolean whether = rewardsCollectiveService.saveRewardsCollective(rewardsCollectiveDTO, loginPrincipal);

        if(!whether) {
            throw new ServiceException(ServiceCode.ERROR_FORBIDDEN, "新增数据失败");
        }
        return JsonResult.ok();
    }

    @PostMapping("/updateRewardsCollective")
    @ApiOperation("修改集体奖励信息")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    public JsonResult<?> updateRewardsCollective(@RequestBody UpdateRewardsCollectiveDTO rewardsCollectiveDTO,
                                                 @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        log.debug("开始处理修改奖励信息请求, 参数: {}", rewardsCollectiveDTO.toString());

        /*RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);
        log.debug("当前用户权限列表: {}, 及所辖大口: {}, 所辖部门:{}", roleAndOrganDTO.getRoleList(),
                roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept());*/

        boolean whether = rewardsCollectiveService.updateRewardsCollective(rewardsCollectiveDTO,loginPrincipal);
        if(!whether) {
            throw new ServiceException(ServiceCode.ERROR_FORBIDDEN, "修改数据失败");
        }
        return JsonResult.ok();
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @GetMapping("/deleteRewardsCollective")
    @ApiOperation("删除集体奖励信息")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    public JsonResult<?> deleteRewardsCollective(@ApiParam(name = "id", value = "奖励id", required = true)
                                    @RequestParam Integer id) {
        log.debug("开始删除id为{}的奖励信息表数据", id);
        if(id == null || id == 0) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数不匹配");
        }
        boolean whether = rewardsCollectiveService.deleteById(id);
        if(!whether) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "根据id删除数据失败");
        }
        return JsonResult.ok();
    }

    /**
     * 根据多条件分页查询奖励信息
     *
     * @param rewardsCollectiveDTO 查询条件封装类
     * @return 符合条件的奖励信息集合
     */
    @PostMapping("/findByPage")
    @ApiOperation("条件分页查询集体奖励信息")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public JsonResult<PageInfo<RewardsCollectiveVO>> findByPage(
            @ApiParam(name = "rewardsCollectiveDTO", value = "查询对象")
            @RequestBody RewardsCollectiveDTO rewardsCollectiveDTO,
            @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        // 判断时间是否准确, 如果开始时间大于现在时间, 以上方法结果不小于0
        if (rewardsCollectiveDTO.getStartTime() != null &&
                rewardsCollectiveDTO.getStartTime().getTime() > DateUtils.getNowDate().getTime()) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "起始时间不能大于当前时间");
        } else if(rewardsCollectiveDTO.getStartTime() != null &&
                rewardsCollectiveDTO.getStartTime().getTime() >= rewardsCollectiveDTO.getEndTime().getTime()) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "起始时间不能大于结束时间");
        }
        if (rewardsCollectiveDTO.getPageNum() == null || rewardsCollectiveDTO.getPageSize() == null
                || rewardsCollectiveDTO.getPageNum() == 0 || rewardsCollectiveDTO.getPageSize() == 0) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "分页条件不全");
        }
        PageHelper.startPage(rewardsCollectiveDTO.getPageNum(), rewardsCollectiveDTO.getPageSize());
        List<RewardsCollectiveVO> rewardsList = rewardsCollectiveService.findByPage(rewardsCollectiveDTO, loginPrincipal);
        PageInfo<RewardsCollectiveVO> pageInfo = new PageInfo<>(rewardsList);
        return JsonResult.ok(pageInfo);
    }
    /**
     * 奖惩信息导出到excel表格
     *
     * @param dataRequest 查询条件
     * @param response 响应
     */
    @PostMapping ("/exportData")
    @ApiOperation("集体奖励信息数据导出excel")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    public void exportData(@RequestBody RewardsCollectiveDTO dataRequest, HttpServletResponse response,
                          @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal)  {
        log.debug("开始处理将数据导出到excel的请求, 请求参数: {}", dataRequest.toString());
        PageQueryOfRewardsRequest pageQueryOfRewardsRequest = new PageQueryOfRewardsRequest();
        BeanUtils.copyProperties(dataRequest, pageQueryOfRewardsRequest);

        // 先将需要的数据查询出来
        RewardsCollectiveDTO rewardsRequest = new RewardsCollectiveDTO();
        BeanUtils.copyProperties(dataRequest, rewardsRequest);
        List<RewardsCollectiveVO> rewardsList = rewardsCollectiveService.findByPage(dataRequest, loginPrincipal);
        log.debug("获取到的奖励信息数据: {}", rewardsList);
        String fileName = "奖励信息";
        EasyExcelUtils.writeExcel(response, rewardsList, fileName, "sheet", RewardsCollectiveVO.class);

    }

    /**
     * 奖励信息模版导出
     *
     */
    @GetMapping ("/upload")
    @ApiOperation("集体奖励信息模版导出")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public void upload(HttpServletResponse response)  {
        List<RewardsCollectiveInsertRequest> rewardsInsertRequests = new ArrayList<>();
        RewardsCollectiveInsertRequest request = new RewardsCollectiveInsertRequest();
        request.setRewardsCollective("被奖励集体");
        request.setRewardsType("质量");
        request.setRewardsTime("2023-12-01");
        request.setReasons("质量新星积极带动");
        request.setRewardsDept("智慧水务项目部");
        request.setRewardsName("管理员");
        request.setCreateName("管理员");
        request.setRewards("奖励信息");
        request.setRewardsNum("No.124451");
        request.setRewardsCash("500");
        request.setRewardsArticles("笔记本一个");
        request.setRewardsOthers("");
        request.setRemark("");
        rewardsInsertRequests.add(request);
        EasyExcelUtils.writeExcel(response, rewardsInsertRequests, "集体奖励信息模版", "sheet", RewardsCollectiveInsertRequest.class);
    }

    @PostMapping("/importData")
    @ApiOperation("批量导入集体奖励信息")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    public JsonResult<?> importData(MultipartFile file, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        if (file == null){
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数不能为空");
        }
        log.debug("开始批量导入考核信息");
        List<SaveRewardsCollectiveDTO> faultInsertRequests = EasyExcelUtils.importData(file, SaveRewardsCollectiveDTO.class);
        if(faultInsertRequests.isEmpty()) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "参数有误");
        }
        boolean whether = rewardsCollectiveService.importData(faultInsertRequests, loginPrincipal);
        if(!whether) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "新增数据有误");
        }
        return JsonResult.ok();
    }

    /**
     * 批量删除
     *
     * @param info 用户id集合
     */
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public JsonResult batchDelete(
            @ApiParam(name = "info", value = "用户id集合", required = true)
            @RequestBody BatchDeleteUserDTO info) {
        boolean whether =rewardsCollectiveService.batchDelete(info);
        if(!whether) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "批量删除数据失败");
        }
        return JsonResult.ok();
    }
}
