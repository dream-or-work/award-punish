package cn.sanli.manage.controller;

import cn.sanli.manage.ex.ServiceException;

import cn.sanli.manage.mapper.data1.UserOrganizationMapper;
import cn.sanli.manage.pojo.dto.Fault.FaultInfoStatisticsDTO;
import cn.sanli.manage.pojo.dto.Fault.FaultInfoStatisticsRequest;
import cn.sanli.manage.pojo.dto.Rewards.*;
import cn.sanli.manage.pojo.dto.Role.RoleAndOrganDTO;
import cn.sanli.manage.pojo.entity.Rewards;
import cn.sanli.manage.pojo.vo.Fault.FaultStatisticsInfoOfCenterVO;
import cn.sanli.manage.pojo.vo.Rewards.PagingQueryRewardsVO;
import cn.sanli.manage.pojo.vo.Rewards.RewardsStatisticsInfoOfCenterVO;
import cn.sanli.manage.pojo.vo.Rewards.RewardsStatisticsInfoOfDeptVO;
import cn.sanli.manage.pojo.vo.Rewards.RewardsStatisticsInfoVO;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.OrganizationService;
import cn.sanli.manage.service.RewardsService;
import cn.sanli.manage.utils.DateUtils;
import cn.sanli.manage.utils.EasyExcelUtils;
import cn.sanli.manage.utils.RoleUtils;
import cn.sanli.manage.utils.StringUtils;
import cn.sanli.manage.web.JsonResult;
import cn.sanli.manage.web.ServiceCode;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 奖励信息表(Rewards)表控制层
 *
 * @author wzy
 * @since 2023-12-07 16:06:41
 */
@RestController
@RequestMapping("/rewards")
@Slf4j
@Api(tags = "奖励信息管理接口")
public class RewardsController {
    /**
     * 服务对象
     */
    @Resource
    private RewardsService rewardsService;

    @Resource
    private UserOrganizationMapper userOrganizationMapper;

    @Resource
    private OrganizationService organizationService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryOne")
    @ApiOperation("通过主键查询单条数据")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public JsonResult<Rewards> queryById(Integer id) {
        log.debug("开始查询id为{}的奖励信息表中的数据", id);
        if(id == null || id == 0) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数不能为空");
        }
        return JsonResult.ok(rewardsService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param rewardsInsertRequest 实体
     * @return 新增结果
     */
    @PostMapping("/insertOne")
    @ApiOperation("新增一条数据")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    public JsonResult<?> add(@RequestBody RewardsInsertRequest rewardsInsertRequest, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        log.debug("开始处理新增奖励信息请求, 参数: {}", rewardsInsertRequest.toString());
        if(StringUtils.isAnyBlank( rewardsInsertRequest.getName(), rewardsInsertRequest.getNum(),
                rewardsInsertRequest.getReasons(), rewardsInsertRequest.getRewardsDept(),
                rewardsInsertRequest.getRewardsName(), rewardsInsertRequest.getRewardsNum()))
        {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数有误");
        }

        RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);
        log.debug("当前用户权限列表: {}, 及所辖大口: {}, 所辖部门:{}", roleAndOrganDTO.getRoleList(),
                roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept());
        // 判断用户是否有权限添加该数据
        boolean b = RoleUtils.passOrNot(String.valueOf(rewardsInsertRequest.getDept()),
                String.valueOf(rewardsInsertRequest.getCenter()),
                loginPrincipal, roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept(), roleAndOrganDTO.getRoleList());

        if(!b) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "权限不足");
        }

        boolean whether = rewardsService.insert(rewardsInsertRequest, loginPrincipal);

        if(!whether) {
            throw new ServiceException(ServiceCode.ERROR_FORBIDDEN, "新增数据失败");
        }
        return JsonResult.ok();
    }


    /**     * 修改数据
     *
     * @param updateRequest 实体
     * @return 新增结果
     */
    @PostMapping("/updateOne")
    @ApiOperation("修改一条数据")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    public JsonResult<?> update(@RequestBody RewardsUpdateRequest updateRequest, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        log.debug("开始处理修改奖励信息请求, 参数: {}", updateRequest.toString());
        if(StringUtils.isAnyBlank(updateRequest.getName(), updateRequest.getNum(),
                updateRequest.getReasons(), updateRequest.getRewardsDept(),
                updateRequest.getRewardsName(), updateRequest.getRewardsNum()))
        {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数有误");
        }

        RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);
        log.debug("当前用户权限列表: {}, 及所辖大口: {}, 所辖部门:{}", roleAndOrganDTO.getRoleList(),
                                                                  roleAndOrganDTO.getOrganOfCenter(),
                                                                  roleAndOrganDTO.getOrganOfDept());
        // 判断用户是否有权限添加该数据
        boolean b = RoleUtils.passOrNot(String.valueOf(updateRequest.getDept()),
                String.valueOf(updateRequest.getCenter()),
                loginPrincipal, roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept(), roleAndOrganDTO.getRoleList());

        if(!b) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "权限不足");
        }

        boolean whether = rewardsService.update(updateRequest, loginPrincipal);
        if(!whether) {
            throw new ServiceException(ServiceCode.ERROR_FORBIDDEN, "修改数据失败");
        }
        return JsonResult.ok();
    }


    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @GetMapping("/deleteOne")
    @ApiOperation("根据id删除一条数据")
    @PreAuthorize("hasAnyAuthority('1')")
    public JsonResult<?> deleteById(@ApiParam(name = "id", value = "奖励id", required = true)
                                        @RequestParam Integer id) {
        log.debug("开始删除id为{}的奖励信息表数据", id);
        if(id == null || id == 0) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数不匹配");
        }
        boolean whether = rewardsService.deleteById(id);
        if(!whether) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "根据id删除数据失败");
        }
        return JsonResult.ok();
    }

    /**
     * 根据多条件分页查询奖励信息
     *
     * @param queryRequest 查询条件封装类
     * @return 符合条件的奖励信息集合
     */
    @PostMapping("/pagingQuery")
    @ApiOperation("根据多条件分页查询奖励信息")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public JsonResult<PageInfo<PagingQueryRewardsVO>> pagingQuery(@RequestBody PageQueryOfRewardsRequest queryRequest,
                                                                  @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        // 判断时间是否准确, 如果开始时间大于现在时间, 以上方法结果不小于0
        if (queryRequest.getStartTime() != null &&
                queryRequest.getStartTime().getTime() > DateUtils.getNowDate().getTime()) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "起始时间不能大于当前时间");
        }
        if(queryRequest.getPageNum() == null || queryRequest.getPageSize() == null
                || queryRequest.getPageNum() == 0 || queryRequest.getPageSize() == 0) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "分页条件不全");
        }

        RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);
        PageQueryOfRewardsDTO pageQueryOfRewardsDTO = RoleUtils.initByRoleList(queryRequest, loginPrincipal,
                                                        roleAndOrganDTO.getOrganOfCenter(),
                                                        roleAndOrganDTO.getOrganOfDept(),
                                                        roleAndOrganDTO.getRoleList(),
                                                        loginPrincipal.getDeptId());


        PageHelper.startPage(queryRequest.getPageNum(), queryRequest.getPageSize());
        List<PagingQueryRewardsVO> rewards = rewardsService.pagingQuery(pageQueryOfRewardsDTO);
        PageInfo<PagingQueryRewardsVO> pageInfo = new PageInfo<>(rewards);

        return JsonResult.ok(pageInfo);
    }


    /**
     * 奖惩信息导出到excel表格
     *
     * @param educeRewardsDataRequest 查询条件
     * @param response 响应
     */
    @PostMapping ("/dataEduce")
    @ApiOperation("数据导出excel")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public void dataEduce(@RequestBody EduceRewardsDataRequest educeRewardsDataRequest, HttpServletResponse response,
                          @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal)  {
        log.debug("开始处理将数据导出到excel的请求, 请求参数: {}", educeRewardsDataRequest.toString());
        PageQueryOfRewardsRequest pageQueryOfRewardsRequest = new PageQueryOfRewardsRequest();
        BeanUtils.copyProperties(educeRewardsDataRequest, pageQueryOfRewardsRequest);

        RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);
        PageQueryOfRewardsDTO pageQueryOfRewardsDTO = RoleUtils.initByRoleList(pageQueryOfRewardsRequest,
                loginPrincipal,
                roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept(),
                roleAndOrganDTO.getRoleList(),
                loginPrincipal.getDeptId());

        // 先将需要的数据查询出来
        PageQueryOfRewardsRequest rewardsRequest = new PageQueryOfRewardsRequest();
        BeanUtils.copyProperties(educeRewardsDataRequest, rewardsRequest);
        List<PagingQueryRewardsVO> rewards = rewardsService.pagingQuery(pageQueryOfRewardsDTO);
        log.debug("获取到的奖励信息数据: {}", rewards);
        String fileName = "奖励信息";
        EasyExcelUtils.writeExcel(response, rewards, fileName, "sheet", PagingQueryRewardsVO.class);

//        try {
//            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//            response.setCharacterEncoding("utf-8");
//            String finalFileName = URLEncoder.encode("奖励信息", "UTF-8").replaceAll("\\+", "%20");
//            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + finalFileName + ".xlsx");
//            EasyExcel.write(response.getOutputStream(), Rewards.class)
//                    .excelType(ExcelTypeEnum.XLSX)
//                    .sheet("")
//                    .doWrite(pageInfo.getList());
//        } catch (IOException e){
//            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "Excel表格导出过程中出现IOException");
//        }
    }


    /**
     * 个人奖励记分统计
     *
     * @param loginPrincipal 当事人
     */
    @PostMapping ("/statistics")
    @ApiOperation("个人奖励记分统计")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public JsonResult<List<?>> statistics(@RequestBody RewardsInfoStatisticsRequest statisticsRequest, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal)  {
        if(statisticsRequest == null || statisticsRequest.getType() == null){
            statisticsRequest = new RewardsInfoStatisticsRequest();
            statisticsRequest.setType(0);
        }

        RewardsInfoStatisticsDTO statisticsRequestInfo = getStatisticsRequestInfo(loginPrincipal, statisticsRequest);
        if(statisticsRequest.getType() == 1) {
            log.debug("开始执行个人奖励积分统计, 参数: {}", statisticsRequestInfo);
            List<RewardsStatisticsInfoVO> rewards = rewardsService.statisticsRewardsOfPersonage(statisticsRequestInfo, loginPrincipal);
            return JsonResult.ok(rewards);

        } else if(statisticsRequest.getType() == 2) {
            log.debug("开始执行部门奖励积分统计, 参数: {}", statisticsRequestInfo);
            List<RewardsStatisticsInfoOfDeptVO> rewards = rewardsService.statisticsRewardsOfDept(statisticsRequestInfo, loginPrincipal);
            return JsonResult.ok(rewards);

        } else if(statisticsRequest.getType() == 3) {
            log.debug("开始执行大口奖励积分统计, 参数: {}", statisticsRequestInfo);
            List<RewardsStatisticsInfoOfCenterVO> rewards = rewardsService.statisticsRewardsOfCenter(statisticsRequestInfo, loginPrincipal);
            return JsonResult.ok(rewards);

        } else {
            return JsonResult.ok(new ArrayList<>());
        }

    }

    /**
     * 个人奖励记分统计数据导出
     *
     * @param loginPrincipal 当事人
     */
    @PostMapping ("/statistics/educe")
    @ApiOperation("个人奖励记分统计数据导出")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public void statisticsEduce(@RequestBody RewardsInfoStatisticsRequest statisticsRequest, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal,
                                                                           HttpServletResponse response)  {
        if(statisticsRequest == null || statisticsRequest.getType() == null){
            statisticsRequest = new RewardsInfoStatisticsRequest();
            statisticsRequest.setType(0);
        }
        RewardsInfoStatisticsDTO statisticsRequestInfo = getStatisticsRequestInfo(loginPrincipal, statisticsRequest);
        if(statisticsRequest.getType() == 1) {
            log.debug("开始执行个人奖励积分统计导出, 参数: {}", statisticsRequestInfo);
            List<RewardsStatisticsInfoVO> rewards = rewardsService.statisticsRewardsOfPersonage(statisticsRequestInfo, loginPrincipal);
            EasyExcelUtils.writeExcel(response, rewards, "个人奖励记分统计", "sheet", RewardsStatisticsInfoVO.class);

        } else if(statisticsRequest.getType() == 2) {
            log.debug("开始执行部门奖励积分统计导出, 参数: {}", statisticsRequestInfo);
            List<RewardsStatisticsInfoOfDeptVO> rewards = rewardsService.statisticsRewardsOfDept(statisticsRequestInfo, loginPrincipal);
            EasyExcelUtils.writeExcel(response, rewards, "部门奖励记分统计", "sheet", RewardsStatisticsInfoOfDeptVO.class);

        } else if(statisticsRequest.getType() == 3) {
            log.debug("开始执行大口奖励积分统计导出, 参数: {}", statisticsRequestInfo);
            List<RewardsStatisticsInfoOfCenterVO> rewards = rewardsService.statisticsRewardsOfCenter(statisticsRequestInfo, loginPrincipal);
            EasyExcelUtils.writeExcel(response, rewards, "大口奖励记分统计", "sheet", RewardsStatisticsInfoOfCenterVO.class);

        } else {
            List<?> list = new ArrayList<>();
            EasyExcelUtils.writeExcel(response, list, "大口考核记分统计", "sheet", FaultStatisticsInfoOfCenterVO.class);
        }


    }


    /**
     * 奖励信息模版导出
     *
     */
    @GetMapping ("/educe")
    @ApiOperation("奖励信息模版导出")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public void educe(HttpServletResponse response)  {
        List<RewardsInsertOfTextpromptRequest> rewardsInsertRequests = new ArrayList<>();
        RewardsInsertOfTextpromptRequest request = new RewardsInsertOfTextpromptRequest();
        request.setCenter("技术中心");
        request.setDept("智慧水务项目部");
        request.setName("张三");
        request.setNum("10000");
        request.setRewardsType("素质");
        request.setRewardsTime("2023-12-01");
        request.setReasons("质量新星积极带动");
        request.setRewardsDept("智慧水务项目部");
        request.setRewardsName("李四");
        request.setCreateName("李四");
        request.setRewardsNum("No.124451");
        request.setRewardsCash("500");
        request.setRewardsArticles("电吹风一件");
        request.setBonusPoints("0.5");
        request.setRemark("");
        rewardsInsertRequests.add(request);
        EasyExcelUtils.writeExcel(response, rewardsInsertRequests, "奖励信息模版", "sheet", RewardsInsertOfTextpromptRequest.class);
    }



    @PostMapping("/upload")
    @ApiOperation("批量导入奖励信息")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    public JsonResult<?> upload(MultipartFile file, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        if (file == null){
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数不能为空");
        }
        log.debug("开始批量导入考核信息");
        List<RewardsInsertRequest> faultInsertRequests = EasyExcelUtils.importData(file, RewardsInsertRequest.class);
        if(faultInsertRequests.isEmpty()) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "参数有误");
        }
        boolean upload = rewardsService.upload(faultInsertRequests, loginPrincipal);
        if(!upload) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "新增数据有误");
        }
        return JsonResult.ok();
    }



    private RoleAndOrganDTO getRoleAndOrganBuLoginPrincipal(LoginPrincipal loginPrincipal) {
        // 获取用户权限列表
        String roles = loginPrincipal.getRoles();
        List<String> authorityRole= JSON.parseArray(roles, String.class);
        List<Integer> roleList = new ArrayList<>();
        for (String auth :authorityRole){
            System.out.println(auth);
            JSONObject json = JSONObject.parseObject(auth);
            System.out.println("json对象为"+json);
            String authority = json.getString("authority");
            System.out.println(authority);
            roleList.add(Integer.valueOf(authority));
        }
        log.debug("获取到的权限列表: {}", roleList);

        // 获取当事人所辖部门列表
        List<Integer> organOfDept = userOrganizationMapper.selectOrganIdByNumOfDept(loginPrincipal.getNum());
        log.debug("当前用户所辖部门: {}", organOfDept);
        // 获取当事人所辖大口列表
        List<Integer> organOfCenter = userOrganizationMapper.selectOrganIdByNumOfCenter(loginPrincipal.getNum());
        log.debug("当前用户所辖大口: {}", organOfCenter);

        RoleAndOrganDTO roleAndOrganDTO = new RoleAndOrganDTO();
        roleAndOrganDTO.setRoleList(roleList);
        roleAndOrganDTO.setOrganOfDept(organOfDept);
        roleAndOrganDTO.setOrganOfCenter(organOfCenter);

        return roleAndOrganDTO;
    }


    private RewardsInfoStatisticsDTO getStatisticsRequestInfo(LoginPrincipal loginPrincipal, RewardsInfoStatisticsRequest statisticsRequest) {
        // 根据当事人查询当事人的所辖大口、所辖部门以及角色列表
        RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);
        // 调用方法实现根据请求信息、当事人信息等重组请求信息
        return RoleUtils.initByRoleList(statisticsRequest, loginPrincipal,
                roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept(),
                roleAndOrganDTO.getRoleList());
    }


}