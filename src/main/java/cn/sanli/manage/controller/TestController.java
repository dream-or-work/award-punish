package cn.sanli.manage.controller;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.mapper.data1.UserMapper;
import cn.sanli.manage.mapper.data2.InformationMapper;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.web.Encryption;
import cn.sanli.manage.web.JsonResult;
import cn.sanli.manage.web.ServiceCode;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import java.util.List;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/test")
//@PreAuthorize("hasAuthority('1')")
@Api(tags = "测试接口")
public class TestController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private InformationMapper informationMapper;


    @GetMapping("/list1")
    @ApiOperation(value = "测试权限")
    //@PreAuthorize("hasAuthority('1')")
    @PreAuthorize("hasAnyAuthority('1','2','3','4')")
    public String list1(){
        System.out.println("1111111111");
        throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST,"测试");
       //return "测试成功";
    }

    @GetMapping("/list2")
    @ApiOperation(value = "测试jwt登录")
    public JsonResult<String> list2(){
        System.out.println("2222");
        return JsonResult.ok("sdwcdscdsacnjksacbnhsdajbchjsadbchjdsabvchfdsajbvchjfdsabvhjf");
    }

    @GetMapping("/mapper1")
    @ApiOperation(value = "测试mapper1")
    public int mapper1(){
        int i = userMapper.selectUser1();
        return i;
    }

    @GetMapping("/mapper2")
    @ApiOperation(value = "测试mapper2")
    public int mapper2(){
        int i = informationMapper.selectUser1();
        return i;
    }

    Encryption encryption=new Encryption();
    String res;
    @GetMapping("/encryption")
    @ApiOperation(value = "密码加密")
    public String encryption(){
        res = encryption.base64Encrypt("123456");
        System.out.println(res);
        return "完成";
    }

    @GetMapping("/cryptanalysis")
    @ApiOperation(value = "密码解析")
    public String cryptanalysis(){
        String s = encryption.base64Decrypt("MTIzNDU2");
        System.out.println(s);
        return "完成";
    }

    @GetMapping("/shuzu")
    @ApiOperation(value = "数组接收Get请求")
    public String ceshi(Integer[] a){
        System.out.println(a);
        for(Integer b:a){
            System.out.println("传入的数组参数为"+b);
        }
        return "完成";
    }

    @PostMapping("/shuzu1")
    @ApiOperation(value = "数组接收post请求")
    public String ceshi1(@RequestBody Integer[] a){
        System.out.println(a);
        for(Integer b:a){
            System.out.println("传入的数组参数为"+b);
        }
        return "完成";
    }

    @GetMapping ("/shuzu2")
    @ApiOperation(value = "数组接收RequestParam请求")
    public String ceshi2(@RequestParam Integer[] a){
        System.out.println(a);
        for(Integer b:a){
            System.out.println("传入的数组参数为"+b);
        }
        return "完成";
    }

    @GetMapping("/select")
    @ApiOperation(value = "查询登录后当事人信息")
    public String select(@ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal){
        log.debug("登录后当事人信息为：{}",loginPrincipal);
        String userName = loginPrincipal.getUserName();
        System.out.println(userName);
        String roles = loginPrincipal.getRoles();
        log.debug("查询到的权限信息为{}",roles);
        List<String> role= JSON.parseArray(roles, String.class);
        for (String a:role){
            System.out.println(a);
            JSONObject json = JSONObject.parseObject(a);
            System.out.println("json对象为"+json);
            String authority = json.getString("authority");
            System.out.println(authority);
        }
        return "完成";
    }

//    @GetMapping("/ceshi")
//    @ApiOperation(value = "查询大区以及所有部门11111")
//    @ApiOperationSupport(order = 2)
//    public JsonResult<TreeVO<ceshi>> ceshi(){
//        List<ceshi1> ceshi1s = organizationMapper.selectAll1();
//        //确定所有对象的父分类Id，将所有相同父分类id的分类对象都放在一起
//        //创建map，map中使用父分类id为key，遍历当前参数集合，将相同父分类id对象存在一个key下
//        Map<Integer,List<ceshi>> map=new HashMap<>();
//        log.debug("三级分类树元素总和{}",ceshi1s.size());
//        for (ceshi1 ceshi2:ceshi1s){
//            ceshi ceshi3=new ceshi();
//            BeanUtils.copyProperties(ceshi2,ceshi3);
//            Integer pid = ceshi3.getPid();
//            //判断当前map中是否已经存在这个父分类id的key
//            if (map.containsKey(pid)){
//                //如果已经存在，直接将当前对象添加到这个key对应的list中即可
//                map.get(pid).add(ceshi3);
//            }else{
//                //如果当前map中还没有这个父分类id的key，就要创建新的key-value元素，要先把value准备好，即实例化List，并将分类对象添加到集合中
//                List<ceshi> list=new ArrayList<>();
//                list.add(ceshi3);
//                map.put(pid,list);
//            }
//        }
//        //第二步构建三级分类树，将子分类集合添加到对应的分类对象的children属性中，
//        //从一级分类开始我们程序设计父分类id为1的就是一级分类
//        List<ceshi> firstList = map.get(1);
//        //判断是否为空TODO
//        //遍历一级分类集合
//        for (ceshi oneLevel:firstList){
//            //一级分类对象的id就是二级分类对象的父id
//            Integer id = oneLevel.getId();
//            List<ceshi> twoList = map.get(id);
////            后续判断都是如此，根据id等于父id来写程序，也可进行判断，continue跳过此次循环。。。。。
////            if(twoList==null || twoList.isEmpty()){
////                throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,"查询数据不存在，请重新操作");
////            }
//            //每个一级分类对象的children属性，就是通过一级分类的id查询出来list集合，一级分类的id就是二级分类的父id
//            oneLevel.setChildren(twoList);
//        }
//        TreeVO<ceshi> treeVO=new TreeVO<>();
//        treeVO.setCategories(firstList);
//        return JsonResult.ok(treeVO);
//    }

}
