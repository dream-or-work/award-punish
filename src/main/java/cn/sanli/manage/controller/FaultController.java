package cn.sanli.manage.controller;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.mapper.data1.CorresMapper;
import cn.sanli.manage.mapper.data1.FaultAssessMapper;
import cn.sanli.manage.mapper.data1.UserOrganizationMapper;
import cn.sanli.manage.pojo.dto.Fault.*;
import cn.sanli.manage.pojo.dto.Role.RoleAndOrganDTO;
import cn.sanli.manage.pojo.entity.Corres;
import cn.sanli.manage.pojo.entity.Fault;
import cn.sanli.manage.pojo.vo.Fault.*;
import cn.sanli.manage.pojo.vo.FaultAssess.FaultAssessInfoVO;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.FaultService;
import cn.sanli.manage.service.OrganizationService;
import cn.sanli.manage.utils.DateUtils;
import cn.sanli.manage.utils.EasyExcelUtils;
import cn.sanli.manage.utils.RoleUtils;
import cn.sanli.manage.utils.StringUtils;
import cn.sanli.manage.web.JsonResult;
import cn.sanli.manage.web.ServiceCode;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 过错信息表(Fault)表控制层
 *
 * @author wzy
 * @since 2023-12-07 16:58:13
 */
@RestController
@RequestMapping("/fault")
@Slf4j
@Api(tags = "过错信息管理接口")
public class FaultController {
    /**
     * 服务对象
     */
    @Resource
    private FaultService faultService;

    @Resource
    private FaultAssessMapper faultAssessMapper;

    @Resource
    private CorresMapper corresMapper;

    @Resource
    private OrganizationService organizationService;

    @Resource
    private UserOrganizationMapper userOrganizationMapper;


    /**
     * 通过主键查询单条数据
     *
     * @param faultNum 主键
     * @return 单条数据
     */
    @GetMapping("/queryOne")
    @ApiOperation("通过主键查询单条数据")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public JsonResult<Fault> queryById(String faultNum) {
        log.debug("开始查询id为{}的过错信息表中的数据.................................................", faultNum);
        if(faultNum == null || "".equals(faultNum)) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "请求不能为空");
        }
        Fault fault = faultService.queryById(faultNum);
        return JsonResult.ok(fault);
    }

    /**
     * 新增数据
     *
     * @param faultInsertRequest 实体
     * @return 新增结果
     */
    @PostMapping("/insert")
    @ApiOperation("新增一条数据")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    public JsonResult<?> add(@Valid @RequestBody FaultInsertRequest faultInsertRequest, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        log.debug("开始处理新增过错信息请求, 参数: {}, 当事人: {}.................................................", faultInsertRequest.toString(), loginPrincipal.toString());
        if(StringUtils.isAnyBlank(faultInsertRequest.getResName(), faultInsertRequest.getFaultContent(), faultInsertRequest.getDisposalDept(),
                faultInsertRequest.getDisposalName(), faultInsertRequest.getDisposalNum()))
        {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数不完整");
        }

        RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);
        log.debug("当前用户权限列表: {}, 及所辖大口: {}, 所辖部门:{}", roleAndOrganDTO.getRoleList(),
                roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept());
        // 判断用户是否有权限添加该数据
        boolean b = RoleUtils.passOrNot(String.valueOf(faultInsertRequest.getResDept()),
                String.valueOf(faultInsertRequest.getResCenter()),
                loginPrincipal, roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept(), roleAndOrganDTO.getRoleList());
        if(!b) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "权限不足");
        }

        boolean whether = faultService.insert(faultInsertRequest, loginPrincipal);
        if(!whether) {
            throw new ServiceException(ServiceCode.ERROR_FORBIDDEN, "新增数据失败");
        }
        return JsonResult.ok();
    }


    /**
     * 修改数据
     *
     * @param faultUpdateRequest 实体
     * @return 新增结果
     */
    @PostMapping("/update")
    @ApiOperation("修改一条数据")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    public JsonResult<?> update(@RequestBody FaultUpdateRequest faultUpdateRequest, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        log.debug("开始处理修改过错信息请求, 参数: {}.................................................", faultUpdateRequest.toString());
        if(StringUtils.isAnyBlank(faultUpdateRequest.getResName(), faultUpdateRequest.getFaultContent(), faultUpdateRequest.getDisposalDept(),
                faultUpdateRequest.getDisposalName()))
        {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数不完整");
        }

        RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);
        log.debug("当前用户权限列表: {}, 及所辖大口: {}, 所辖部门:{}", roleAndOrganDTO.getRoleList(),
                roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept());
        // 判断用户是否有权限添加该数据
        boolean b = RoleUtils.passOrNot(String.valueOf(faultUpdateRequest.getResDept()),
                String.valueOf(faultUpdateRequest.getResCenter()),
                loginPrincipal, roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept(), roleAndOrganDTO.getRoleList());
        if(!b) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "权限不足");
        }

        boolean whether = faultService.update(faultUpdateRequest, loginPrincipal);
        if(!whether) {
            throw new ServiceException(ServiceCode.ERROR_FORBIDDEN, "修改数据失败");
        }
        return JsonResult.ok();
    }


    /**
     * 删除数据
     *
     * @param faultNum 主键
     * @return 删除是否成功
     */
    @GetMapping("/deleteOne")
    @ApiOperation("根据id删除一条数据")
    @PreAuthorize("hasAnyAuthority('1')")
    public JsonResult<?> deleteById(@ApiParam(name = "faultNum", value = "考核编号", required = true)
                                        @RequestParam String faultNum) {
        log.debug("开始删除id为{}的过错信息表数据.................................................", faultNum);
        if(faultNum == null || "".equals(faultNum)) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数不完整");
        }
        boolean whether = faultService.deleteById(faultNum);
        if(!whether) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "根据id删除数据失败");
        }
        return JsonResult.ok();
    }


    @GetMapping("/deleteBatch")
    @ApiOperation("批量删除考核信息")
    @PreAuthorize("hasAnyAuthority('1')")
    public JsonResult<?> deleteBatch(@ApiParam(name = "faultNums", value = "考核编号", required = true)
                                         @RequestParam String[] faultNums) {
        if(faultNums.length == 0) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数不能为空");
        }
        log.debug("参数校验完毕, 开始执行批量删除考核信息操作: {}", faultNums);
        log.debug("需进行操作的数据条数:{}", faultNums.length);
        boolean b = faultService.deleteBatch(faultNums);
        if(!b) {
            throw new ServiceException(ServiceCode.ERROR_DELETE, "删除失败");
        }
        return JsonResult.ok();
    }




    /**
     * 根据多条件分页查询过错信息
     *
     * @param queryRequest 查询条件封装类
     * @return 符合条件的过错信息集合
     */
    @PostMapping("/pagingQuery")
    @ApiOperation("根据多条件分页查询过错信息")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public JsonResult<PageInfo<FaultPagingQueryVO>> pagingQuery(@RequestBody PageQueryOfFaultRequest queryRequest,
                                                                @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        // 判断时间是否准确, 如果开始时间大于现在时间, 以上方法结果不小于0
        if (queryRequest.getStartTime() != null &&
                queryRequest.getStartTime().getTime() > DateUtils.getNowDate().getTime()) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "起始时间不能大于当前时间");
        } else if(queryRequest.getPageNum() == null || queryRequest.getPageSize() == null
                || queryRequest.getPageNum() == 0 || queryRequest.getPageSize() == 0) {
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "分页条件不全");
        }

        log.debug("开始执行多条件分页查询, 请求参数: {}", queryRequest.toString());


        // 根据当前用户的权限, 对查询条件进行对应修改赋值
        int roleId = loginPrincipal.getRoleId();
        // 获取当事人所在部门名称
        String name = organizationService.queryById(loginPrincipal.getDeptId()).getName();

        RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);
        log.debug("获取到的用户权限: {}, 用户所辖大口: {}, 用户所辖部门: {}", roleAndOrganDTO.getRoleList(), roleAndOrganDTO.getOrganOfCenter(), roleAndOrganDTO.getOrganOfDept());
        PageQueryOfFaultRequestDTO requestDTO
                = RoleUtils.initByRoleList(queryRequest,
                                            loginPrincipal,
                                            roleAndOrganDTO.getOrganOfCenter(),
                                            roleAndOrganDTO.getOrganOfDept(),
                                            name,
                                            roleAndOrganDTO.getRoleList());


        log.debug("开始执行service层多条件查询方法, 方法参数: {}", requestDTO);
        PageHelper.startPage(requestDTO.getPageNum(), requestDTO.getPageSize());
        List<FaultPagingQueryVO> faultAndFaultAssessInfos = faultService.pagingQuery(requestDTO);

        PageInfo<FaultPagingQueryVO> pageInfo = new PageInfo<>(faultAndFaultAssessInfos);

        // 判断是否有符合条件的Fault数据
        if(!faultAndFaultAssessInfos.isEmpty()) {
            log.debug("查询到的fault数据条数: {}", faultAndFaultAssessInfos.size());

            // 将fault数据中的faultNum去除存入集合用于查询FaultAssess数据
            List<String> faultNums = new ArrayList<>();
            for(FaultPagingQueryVO fault : faultAndFaultAssessInfos) {
                faultNums.add(fault.getFaultNum());
            }
            // 根据faultNum从faultAssess表中获取对应数据
            log.debug("即将根据考核编号查询faultAssess考核信息, 参数: {}", faultNums);
            // 将faultAssess表中的数据以faultId为key存入一个Map中
            Map<String, List<FaultAssessInfoVO>> faultAssessMap = new HashMap<>();
            for (FaultAssessInfoVO faultAssess : faultAssessMapper.queryByIds(faultNums)) {
                String faultId = faultAssess.getFaultId();
                // 判断是否存在相同的key
                if(faultAssessMap.containsKey(faultId)) {
                    // 存在便直接存入
                    faultAssessMap.get(faultId).add(faultAssess);
                }else{
                    // 不存在则创建一个集合并存入数据
                    List<FaultAssessInfoVO> faultAssessList = new ArrayList<>();
                    faultAssessList.add(faultAssess);
                    faultAssessMap.put(faultId, faultAssessList);
                }
            }

            // 创建一个对应类型集合作为方法返回值
//        List<FaultPagingQueryVO> faultAndFaultAssessInfos = new ArrayList<>();
            // 从字段映射表中获取所有信息
            List<Corres> corresList = corresMapper.selectAll();
            Map<Integer, String> map = new HashMap<>();
            // 字段映射集合, 以id为key, name为value存入map
            for (Corres corres : corresList) {
                map.put(corres.getId(), corres.getName());
            }
            // 循环遍历已获取的fault数据, 将以获取的所有数据进行重新组合
            List<FaultPagingQueryVO> list = pageInfo.getList();
            log.debug("即将循环遍历:{}条数据", pageInfo.getList().size());
            for (FaultPagingQueryVO faultInfo : list) {
                FaultPagingQueryVO pagingQueryVO = new FaultPagingQueryVO();
                BeanUtils.copyProperties(faultInfo, pagingQueryVO);
                // 将属性数字到字符串转换
                faultInfo.setFaultType(map.get(faultInfo.getFaultTypeId()));
                faultInfo.setFaultLevel(map.get(faultInfo.getFaultLevelId()));
                faultInfo.setFaultDisposalForm(map.get(faultInfo.getFaultDisposalFormId()));
                faultInfo.setOtherResults(map.get(faultInfo.getOtherResultsId()));
                faultInfo.setOtherDisposal(map.get(faultInfo.getOtherDisposalId()));
                faultInfo.setDisposalLevel(map.get(faultInfo.getDisposalLevelId()));
                faultInfo.setApprovalType(map.get(faultInfo.getApprovalTypeId()));
                faultInfo.setDisposalLevel(map.get(faultInfo.getDisposalLevelId()));
                faultInfo.setFaultAssessInfos(faultAssessMap.get(faultInfo.getFaultNum()));
            }
            log.debug("上行即将返回的数据: {}", pageInfo);
            return JsonResult.ok(pageInfo);
        }
        log.debug("下行即将返回的数据: {}", pageInfo);
        return JsonResult.ok(pageInfo);
    }



    /**
     * 奖惩信息导出到excel表格
     *
     * @param dataRequest 查询条件
     * @param response 响应
     */
    @PostMapping ("/dataEduce")
    @ApiOperation("数据导出excel")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public void dataEduce(@RequestBody EduceFaultDataRequest dataRequest, HttpServletResponse response,
                          @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal)  {
//        if (dataRequest.getStartTime() != null &&
//                dataRequest.getStartTime().getTime() > DateUtils.getNowDate().getTime()) {
//            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "起始时间不能大于当前时间");
//        } else if(dataRequest.getStartTime() != null && dataRequest.getStartTime().getTime() >= dataRequest.getEndTime().getTime()) {
//            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "起始时间不能大于结束时间");
//        }

        log.debug("开始处理将数据导出到excel的请求, 请求参数: {}", dataRequest.toString());
        // 先将需要的数据查询出来
        PageQueryOfFaultRequest request = new PageQueryOfFaultRequest();
        BeanUtils.copyProperties(dataRequest, request);

        int roleId = loginPrincipal.getRoleId();
        log.debug("当前请求用户主显权限wei:{}", roleId);
        String name = organizationService.queryById(loginPrincipal.deptId).getName();

        RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);

        PageQueryOfFaultRequestDTO requestDTO
                = RoleUtils.initByRoleList(request,
                                            loginPrincipal,
                                            roleAndOrganDTO.getOrganOfCenter(),
                                            roleAndOrganDTO.getOrganOfDept(),
                                            name,
                                            roleAndOrganDTO.getRoleList());




        List<FaultPagingQueryVO> faultAndFaultAssessInfos = faultService.pagingQuery(requestDTO);

        List<FaultPagingOfEduceVO> list = new ArrayList<>();
        if(faultAndFaultAssessInfos != null && !faultAndFaultAssessInfos.isEmpty()) {
            log.debug("获取到的过错信息数据: {}条", faultAndFaultAssessInfos.size());
            // 将fault数据中的faultNum去除存入集合用于查询FaultAssess数据
            // 将编号集合转换成字符串格式
//            StringBuilder faultNumsStr = new StringBuilder();
            List<String> faultNums = new ArrayList<>();
            for(FaultPagingQueryVO fault : faultAndFaultAssessInfos) {
                faultNums.add(fault.getFaultNum());
//                if(faultNumsStr.length() == 0) {
//                    faultNumsStr.append(fault.getFaultNum());
//                }else {
//                    faultNumsStr.append(",").append(fault.getFaultNum());
//                }
            }


            log.debug("参数展示: {}", faultAndFaultAssessInfos.size());
            // 根据faultNum从faultAssess表中获取对应数据
            List<FaultAssessInfoVO> faultAssesses = new ArrayList<>();
            List<String> listStr = new ArrayList<>();
            if(faultNums.size() == 1) {
                listStr.add(faultNums.get(0));
                faultAssesses.addAll(faultAssessMapper.queryByIds(listStr));
            } else {
                for (int i = 0; i <= faultNums.size(); i+=2000){
                    listStr = faultNums.subList(i, i + 2001 >= faultNums.size() ? faultNums.size() - 1 : i + 2001);
                    faultAssesses.addAll(faultAssessMapper.queryByIds(listStr));
                }
            }

            log.debug("查询出的FaultAssess数据: {}条", faultAssesses.size());
            // 将faultAssess表中的数据以faultId为key存入一个Map中
            Map<String, List<FaultAssessInfoVO>> faultAssessMap = new HashMap<>();
            for (FaultAssessInfoVO faultAssess : faultAssesses) {
                String faultId = faultAssess.getFaultId();
                // 判断是否存在相同的key
                if(faultAssessMap.containsKey(faultId)) {
                    // 存在便直接存入
                    faultAssessMap.get(faultId).add(faultAssess);
                }else{
                    // 不存在则创建一个集合并存入数据
                    List<FaultAssessInfoVO> faultAssessList = new ArrayList<>();
                    faultAssessList.add(faultAssess);
                    faultAssessMap.put(faultId, faultAssessList);
                }
            }

            // 创建一个对应类型集合作为方法返回值
//        List<FaultPagingQueryVO> faultAndFaultAssessInfos = new ArrayList<>();
            // 从字段映射表中获取所有信息
            List<Corres> corresList = corresMapper.selectAll();
            Map<Integer, String> map = new HashMap<>();
            // 字段映射集合, 以id为key, name为value存入map
            for (Corres corres : corresList) {
//                log.debug("将id为: {}的,名称为: {}的映射数据存入map", corres.getId(), corres.getName());
                map.put(corres.getId(), corres.getName());
            }
            // 循环遍历已获取的fault数据, 将以获取的所有数据进行重新组合
            for (FaultPagingQueryVO faultInfo : faultAndFaultAssessInfos) {
                FaultPagingQueryVO pagingQueryVO = new FaultPagingQueryVO();
                BeanUtils.copyProperties(faultInfo, pagingQueryVO);
                // 将属性数字到字符串转换
                faultInfo.setFaultType(map.get(faultInfo.getFaultTypeId()));
                faultInfo.setFaultLevel(map.get(faultInfo.getFaultLevelId()));
                faultInfo.setFaultDisposalForm(map.get(faultInfo.getFaultDisposalFormId()));
                faultInfo.setOtherResults(map.get(faultInfo.getOtherResultsId()));
                faultInfo.setOtherDisposal(map.get(faultInfo.getOtherDisposalId()));
                faultInfo.setDisposalLevel(map.get(faultInfo.getDisposalLevelId()));
                faultInfo.setApprovalType(map.get(faultInfo.getApprovalTypeId()));
                faultInfo.setDisposalLevel(map.get(faultInfo.getDisposalLevelId()));
                faultInfo.setFaultAssessInfos(faultAssessMap.get(faultInfo.getFaultNum()));
            }

            for (FaultPagingQueryVO pagingQueryVO : faultAndFaultAssessInfos) {
                // 创建一个Excel导出用的对象
                FaultPagingOfEduceVO educeVO = new FaultPagingOfEduceVO();
                // 同名属性赋值
                BeanUtils.copyProperties(pagingQueryVO, educeVO);
//            educeVO.setCashPledge("/");
//            educeVO.setLossExpense("/");
//            educeVO.setScore("/");
//            educeVO.setMonthScore("/");
//            educeVO.setYearScore("/");
                // 考核处理集合是否为空
                if(pagingQueryVO.getFaultAssessInfos() != null && !pagingQueryVO.getFaultAssessInfos().isEmpty()) {
                    // 不为空则为对应属性赋值
                    for(FaultAssessInfoVO info : pagingQueryVO.getFaultAssessInfos()) {
                        Integer faultAssessDisposalFormId = info.getFaultAssessDisposalFormId();
                        if(faultAssessDisposalFormId != null) {
                            if(faultAssessDisposalFormId == 1026) {
                                educeVO.setCashPledge(info.getDisposalDetails() + "元");
                                educeVO.setCashPledgeId(info.getAssessNum());
                            } else if(faultAssessDisposalFormId == 1027) {
                                educeVO.setLossExpense(info.getDisposalDetails() + "元");
                                educeVO.setLossExpenseId(info.getAssessNum());
                            } else if(faultAssessDisposalFormId == 1028) {
                                educeVO.setScore(info.getDisposalDetails() + "分");
                                educeVO.setScoreId(info.getAssessNum());
                            } else if(faultAssessDisposalFormId == 1029) {
                                educeVO.setMonthScore(info.getDisposalDetails() + "分");
                            } else if(faultAssessDisposalFormId == 1030) {
                                educeVO.setYearScore(info.getDisposalDetails() + "分");
                            } else if(faultAssessDisposalFormId == 1058) {
                                educeVO.setKpi(info.getDisposalDetails() + "分");
                            }
                        }
                    }
                }
                list.add(educeVO);
            }
        }




        String fileName = "考核信息";
        EasyExcelUtils.writeExcel(response, list, fileName, "sheet", FaultPagingOfEduceVO.class);

//        try {
//            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//            response.setCharacterEncoding("utf-8");
//            String finalFileName = URLEncoder.encode("考核信息", "UTF-8").replaceAll("\\+", "%20");
//            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + finalFileName + ".xlsx");
//            EasyExcel.write(response.getOutputStream(), FaultPagingQueryVO.class)
//                    .excelType(ExcelTypeEnum.XLSX)
//                    .sheet("")
//                    .doWrite(pageInfo.getList());
//        } catch (IOException e){
//            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "Excel表格导出过程中出现IOException");
//        }
    }


    /**
     * 个人考核记分统计
     *
     * @param loginPrincipal 当事人
     */
    @PostMapping ("/statistics")
    @ApiOperation("考核记分统计")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public JsonResult<List<?>> statistics(@RequestBody FaultInfoStatisticsRequest statisticsRequest, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal,
                                                                         HttpServletResponse response)  {
        if(statisticsRequest == null || statisticsRequest.getType() == null){
            statisticsRequest = new FaultInfoStatisticsRequest();
            statisticsRequest.setType(0);
        }
        log.debug("开始执行考核积分统计, 当事人: {}", loginPrincipal);
        // 调用方法实现根据请求信息、当事人信息等重组请求信息
        FaultInfoStatisticsDTO statisticsDTO = getStatisticsRequestInfo(loginPrincipal, statisticsRequest);
        log.debug("调用方法实现根据请求信息、当事人信息等重组请求信息: {}", statisticsDTO);
        if(statisticsRequest.getType() == 1) {
            log.debug("开始执行个人统计功能service层, 方法参数: {}", statisticsDTO);
            List<FaultStatisticsInfoVO> statisticsInfoVOList = faultService.statisticsFaultOfPersonage(loginPrincipal, statisticsDTO);
            return JsonResult.ok(statisticsInfoVOList);

        } else if(statisticsRequest.getType() == 2) {
            log.debug("开始执行部门统计功能service层, 方法参数: {}", statisticsDTO);
            List<FaultStatisticsInfoOfDeptVO> statisticsInfoVOList = faultService.statisticsFaultOfDept(loginPrincipal, statisticsDTO);
            return JsonResult.ok(statisticsInfoVOList);

        } else if(statisticsRequest.getType() == 3) {
            log.debug("开始执行大口统计功能service层, 方法参数: {}", statisticsDTO);
            List<FaultStatisticsInfoOfCenterVO> statisticsInfoVOList = faultService.statisticsFaultOfCenter(loginPrincipal, statisticsDTO);
            return JsonResult.ok(statisticsInfoVOList);

        } else {
            return JsonResult.ok(new ArrayList<>());
        }

    }


    /**
     * 导出个人考核记分统计数据
     *
     * @param loginPrincipal 当事人
     */
    @PostMapping ("/statistics/educe")
    @ApiOperation("导出考核记分统计数据")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public void statisticsToEduce(@RequestBody FaultInfoStatisticsRequest statisticsRequest, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal,
                                                                         HttpServletResponse response)  {
        if(statisticsRequest == null || statisticsRequest.getType() == null) {
            statisticsRequest = new FaultInfoStatisticsRequest();
            statisticsRequest.setType(0);
        }
        log.debug("开始执行个人考核积分统计导出, 当事人: {}", loginPrincipal);

        FaultInfoStatisticsDTO statisticsDTO = getStatisticsRequestInfo(loginPrincipal, statisticsRequest);
        if(statisticsRequest.getType() == 1) {
            log.debug("开始执行个人统计导出功能service层, 方法参数: {}", statisticsDTO);
            List<FaultStatisticsInfoVO> statisticsInfoVOList = faultService.statisticsFaultOfPersonage(loginPrincipal, statisticsDTO);
            EasyExcelUtils.writeExcel(response, statisticsInfoVOList, "个人考核记分统计", "sheet", FaultStatisticsInfoVO.class);

        } else if(statisticsRequest.getType() == 2) {
            log.debug("开始执行部门统计导出功能service层, 方法参数: {}", statisticsDTO);
            List<FaultStatisticsInfoOfDeptVO> statisticsInfoVOList = faultService.statisticsFaultOfDept(loginPrincipal, statisticsDTO);
            EasyExcelUtils.writeExcel(response, statisticsInfoVOList, "部门考核记分统计", "sheet", FaultStatisticsInfoOfDeptVO.class);

        } else if(statisticsRequest.getType() == 3) {
            log.debug("开始执行大口统计导出功能service层, 方法参数: {}", statisticsDTO);
            List<FaultStatisticsInfoOfCenterVO> statisticsInfoVOList = faultService.statisticsFaultOfCenter(loginPrincipal, statisticsDTO);
            EasyExcelUtils.writeExcel(response, statisticsInfoVOList, "大口考核记分统计", "sheet", FaultStatisticsInfoOfCenterVO.class);

        } else {
            List<?> list = new ArrayList<>();
            EasyExcelUtils.writeExcel(response, list, "大口考核记分统计", "sheet", FaultStatisticsInfoOfCenterVO.class);
        }
    }




    /**
     * 考核信息模版导出
     *
     */
    @GetMapping ("/educe")
    @ApiOperation("考核信息模版导出")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7','8')")
    public void educe(HttpServletResponse response)  {
        List<FaultInsertOfTextpromptRequest> faultInsertRequests = new ArrayList<>();
        FaultInsertOfTextpromptRequest request = new FaultInsertOfTextpromptRequest();
        request.setResCenter("某某中心");
        request.setResDept("智慧水务项目部");
        request.setResNum(10000);
        request.setResName("张三");
        request.setFaultType("素质");
        request.setFaultTime(DateUtils.dateTime("yyyy-MM-dd" ,"2023-12-01"));
        request.setFaultContent("厂区行走未踏乐");
        request.setFaultLevel("轻微");
        request.setDisposalDept("军管部");
        request.setDisposalName("李四");
        request.setDisposalTime(DateUtils.dateTime("yyyy-MM-dd" ,"2023-12-01"));
        request.setFaultDisposalForm("通报");
        request.setOtherResults("记过");
        request.setOtherDisposal("批评");
        request.setDisposalNum("2023-1201");
        request.setEndTime(DateUtils.dateTime("yyyy-MM-dd" ,"2023-12-01"));
        request.setDisposalLevel("公司性考核");
        request.setEarnestMoney(new BigDecimal("500.00"));
        request.setEarnestMoneyNum("0036131");
        request.setLossExpense(new BigDecimal("500.00"));
        request.setLossExpenseNum("0036131");
        request.setScore(new BigDecimal("0.50"));
        request.setScoreNum("0036131");
        request.setScoreOfMonth(new BigDecimal("0.50"));
        request.setScoreOfMonthNum("0036131");
        request.setScoreOfYear(new BigDecimal("0.50"));
        request.setScoreOfYearNum("0036131");
        request.setKpi(new BigDecimal("0.50"));
        request.setKpiNum("0036131");
        faultInsertRequests.add(request);
        EasyExcelUtils.writeExcel(response, faultInsertRequests, "考核信息模版", "sheet", FaultInsertOfTextpromptRequest.class);
    }


    @PostMapping("/upload")
    @ApiOperation("批量导入考核信息")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6')")
    public JsonResult<?> upload(MultipartFile file, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        if (file == null){
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST, "参数不能为空");
        }
        log.debug("开始批量导入考核信息");
        List<FaultInsertOfTextpromptRequest> faultInsertRequests = EasyExcelUtils.importData(file, FaultInsertOfTextpromptRequest.class);
        boolean upload = faultService.upload(faultInsertRequests, loginPrincipal);
        if(!upload) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "新增失败");
        }
        return JsonResult.ok();
    }

    private RoleAndOrganDTO getRoleAndOrganBuLoginPrincipal(LoginPrincipal loginPrincipal) {
        // 获取用户权限列表
        String roles = loginPrincipal.getRoles();
        List<String> authorityRole= JSON.parseArray(roles, String.class);
        List<Integer> roleList = new ArrayList<>();
        for (String auth :authorityRole){
            System.out.println(auth);
            JSONObject json = JSONObject.parseObject(auth);
            System.out.println("json对象为"+json);
            String authority = json.getString("authority");
            System.out.println(authority);
            roleList.add(Integer.valueOf(authority));
        }
        log.debug("获取到的权限列表: {}", roleList);

        // 获取当事人所辖部门列表
        List<Integer> organOfDept = userOrganizationMapper.selectOrganIdByNumOfDept(loginPrincipal.getNum());
        log.debug("当前用户所辖部门: {}", organOfDept);
        // 获取当事人所辖大口列表
        List<Integer> organOfCenter = userOrganizationMapper.selectOrganIdByNumOfCenter(loginPrincipal.getNum());
        log.debug("当前用户所辖大口: {}", organOfCenter);

        RoleAndOrganDTO roleAndOrganDTO = new RoleAndOrganDTO();
        roleAndOrganDTO.setRoleList(roleList);
        roleAndOrganDTO.setOrganOfDept(organOfDept);
        roleAndOrganDTO.setOrganOfCenter(organOfCenter);

        return roleAndOrganDTO;
    }


    private FaultInfoStatisticsDTO getStatisticsRequestInfo(LoginPrincipal loginPrincipal, FaultInfoStatisticsRequest statisticsRequest) {
        // 根据当事人部门id获取部门名称作为处理部门属性
        String disposalDeptName = organizationService.queryById(loginPrincipal.getDeptId()).getName();
        // 根据当事人查询当事人的所辖大口、所辖部门以及角色列表
        RoleAndOrganDTO roleAndOrganDTO = getRoleAndOrganBuLoginPrincipal(loginPrincipal);
        // 调用方法实现根据请求信息、当事人信息等重组请求信息
        FaultInfoStatisticsDTO statisticsDTO = RoleUtils.initByRoleList(statisticsRequest, loginPrincipal,
                roleAndOrganDTO.getOrganOfCenter(),
                roleAndOrganDTO.getOrganOfDept(),
                roleAndOrganDTO.getRoleList(),
                disposalDeptName);
        return statisticsDTO;
    }





}

