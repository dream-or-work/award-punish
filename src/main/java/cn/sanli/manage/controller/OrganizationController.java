package cn.sanli.manage.controller;

import cn.sanli.manage.ex.ServiceException;
import cn.sanli.manage.mapper.data1.OrganizationMapper;
import cn.sanli.manage.pojo.dto.System.*;
import cn.sanli.manage.pojo.entity.Corres;
import cn.sanli.manage.pojo.entity.Organization;
import cn.sanli.manage.pojo.vo.*;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.OrganizationService;
import cn.sanli.manage.web.JsonResult;
import cn.sanli.manage.web.ServiceCode;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.util.Json;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 组织机构信息
 */
@Slf4j
@RestController
@RequestMapping("/organization")
@Api(tags = "组织结构相关接口")
public class OrganizationController {


    @Autowired
    private OrganizationMapper organizationMapper;
    @Resource
    private OrganizationService organizationService;

    @GetMapping("/listRegion")
    @ApiOperation(value = "查询公司所有大区")
    @ApiOperationSupport(order = 1)
    public JsonResult<List<RegionNameVO>> listDept() {
        List<RegionNameVO> regionNameVOS = organizationMapper.regionName();
        if (regionNameVOS.isEmpty()) {
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, "查询数据不存在，请重新操作");
        }
        return JsonResult.ok(regionNameVOS);
    }

    @GetMapping("/listALlDept")
    @ApiOperation(value = "查询公司所有部门")
    @ApiOperationSupport(order = 1)
    public JsonResult<List<RegionNameVO>> listALlDept() {
        List<RegionNameVO> regionNameVOS = organizationMapper.listALlDept();
        if (regionNameVOS.isEmpty()) {
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, "查询数据不存在，请重新操作");
        }
        return JsonResult.ok(regionNameVOS);
    }
    @GetMapping("/listDept")
    @ApiOperation(value = "查询大区所管辖部门")
    @ApiOperationSupport(order = 2)
    public JsonResult<List<DeptNameVO>> listDept(int id) {
        List<DeptNameVO> deptNameVOS = organizationMapper.deptName(id);
        if (deptNameVOS.isEmpty()) {
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, "查询数据不存在，请重新操作");
        }
        return JsonResult.ok(deptNameVOS);
    }

    @PostMapping("/listMoreDept")
    @ApiOperation(value = "查询多个大口所辖部门")
    @ApiOperationSupport(order = 3)
    public JsonResult<List<DeptNameVO>> listMoreDept(
            @ApiParam(name = "centerIdList", value = "大口id集合", required = true)
            @RequestBody CenterDTO centerDTO) {
        List<Integer> centerIdList = centerDTO.getCenterIdList();
        List<DeptNameVO> deptList = organizationService.selectMoreDept(centerIdList);
        if (deptList.isEmpty()) {
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, "查询数据不存在，请重新操作");
        }
        return JsonResult.ok(deptList);
    }

    /**
     * 查询全部大口和部门
     *
     * @return
     */
    //@PreAuthorize("hasAnyAuthority('1','2','7')")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询全部大口和部门")
    @GetMapping("/listOrganization")
    public JsonResult<List<Organization>> listOrganization() {
        List<Organization> list = organizationService.findNodes();
        return JsonResult.ok(list);
    }

    /**
     * 添加组织结构
     *
     * @param organizationDTO 添加信息
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "添加组织结构")
    @PostMapping("/saveOrganization")
    public JsonResult saveOrganization(@Valid @RequestBody OrganizationDTO organizationDTO) {
        organizationService.saveOrganization(organizationDTO);
        return JsonResult.ok();
    }

    /**
     * 修改组织结构
     *
     * @param organizationDTO 修改信息
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "修改组织结构")
    @PostMapping("/updateOrganization")
    public JsonResult updateOrganization(@RequestBody UpdateOrganizationDTO organizationDTO) {
        organizationService.updateOrganization(organizationDTO);
        return JsonResult.ok();
    }

    /**
     * 删除组织结构
     *
     * @param id 组织结构id
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "删除组织结构")
    @GetMapping("/deleteOrganization")
    public JsonResult deleteOrganization(@RequestParam Integer id) {
        organizationService.deleteOrganization(id);
        return JsonResult.ok();
    }
}
