package cn.sanli.manage.controller;

import cn.sanli.manage.pojo.dto.User.*;
import cn.sanli.manage.pojo.vo.UserInfoListVO;
import cn.sanli.manage.pojo.vo.UserInfoVO;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.UserInfoService;
import cn.sanli.manage.utils.StringUtils;
import cn.sanli.manage.web.JsonResult;
import cn.sanli.manage.web.ServiceCode;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 用户信息表(dbo.userInfo)表控制层
 *
 * @author xxxxx
 */
@Api(tags = "用户管理接口")
@RestController
@RequestMapping("system/userInfo")
public class UserInfoController {
    /**
     * 服务对象
     */
    @Resource
    private UserInfoService userInfoService;

    /**
     * 用户分页列表
     * @param userInfoDto
     * @param loginPrincipal
     * @return
     */
    @ApiOperation(value = "获取用户信息分页列表")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @PostMapping("/findByPage")
    public JsonResult<PageInfo<UserInfoVO>> findByPage(
            @ApiParam(name = "userInfoDto", value = "查询对象")
            @RequestBody UserInfoDTO userInfoDto,
            @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal
    ) {
        //获取登录用户信息
        /*String num = loginPrincipal.getNum();
        UserMessage message = userService.message(num);
        PageInfo<UserInfoVO> pageInfo = userInfoService.findByPage(userInfoDto, message);*/
        PageInfo<UserInfoVO> pageInfo = userInfoService.findByPage(userInfoDto, loginPrincipal);
        return JsonResult.ok(pageInfo);
    }

    @ApiOperation(value = "获取用户信息列表分页（用户大口、部门、角色列表）")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @PostMapping("/findListByPage")
    public JsonResult<PageInfo<UserInfoListVO>> findListByPage(
            @ApiParam(name = "userInfoDto", value = "查询对象")
            @RequestBody UserInfoDTO userInfoDto,
            @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal
    ) {
        PageInfo<UserInfoListVO> pageInfo = userInfoService.findListByPage(userInfoDto, loginPrincipal);
        return JsonResult.ok(pageInfo);
    }

    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询工号是否存在")
    @GetMapping("/findNum")
    public JsonResult findNum(@ApiParam(name = "num", value = "用户工号", required = true)
                              @RequestParam String num) {
        userInfoService.findByNum(num);
        return JsonResult.ok();
    }

    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询用户信息")
    @GetMapping("/findUserInfo")
    public JsonResult<UserInfoVO> findUserInfo(@ApiParam(name = "num", value = "用户工号", required = true) @RequestParam String num)
    {
        System.out.println(num);
        UserInfoVO userInfo = userInfoService.findUserInfo(num);
        return JsonResult.ok(userInfo);
    }

    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询用户信息（用户大口、部门、角色列表）")
    @GetMapping("/findListUserInfo")
    public JsonResult<UserInfoListVO> findListUserInfo(@ApiParam(name = "num", value = "用户工号", required = true) @RequestParam String num)
    {
        System.out.println(num);
        UserInfoListVO userInfo = userInfoService.findListUserInfo(num);
        return JsonResult.ok(userInfo);
    }
    /**
     * 添加用户
     *
     * @param info 用户信息
     */
    @PreAuthorize("hasAnyAuthority('1','3','4','5','6')")
    @ApiOperation(value = "添加用户")
    @PostMapping("/saveUser")
    public JsonResult saveUser(@Valid @RequestBody UserDTO info,
                               @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        //获取登录用户信息
        /*String num = loginPrincipal.getNum();
        UserMessage message = userService.message(num);
        userInfoService.saveUser(info, message);*/
        userInfoService.saveUser(info, loginPrincipal);
        return JsonResult.ok();
    }
    /**
     * 修改用户数据
     *
     * @param info 修改信息
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1','3','4','5','6')")
    @ApiOperation(value = "修改用户")
    @PostMapping("/updateUser")
    public JsonResult updateUser(@RequestBody UpdateUserDTO info,
                                 @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        userInfoService.update(info,loginPrincipal);
        return JsonResult.ok();
    }
    /*@PreAuthorize("hasAnyAuthority('1','3','4','5','6')")
    @ApiOperation(value = "修改用户记分提醒标准")
    @PostMapping("/updateUserStandard")
    public JsonResult updateUserStandard(@RequestBody UserStandardDTO info){
        userInfoService.updateUserStandard(info);
        return JsonResult.ok();
    }*/
    /**
     * 重置密码
     *
     * @param id 用户id
     */
    @PreAuthorize("hasAnyAuthority('1','3','4','5','6')")
    @ApiOperation(value = "重置密码")
    @GetMapping("/resetPassWord")
    public JsonResult resetPassWord(@ApiParam(name = "id", value = "用户id", required = true)
                                    @RequestParam Integer id) {
        userInfoService.resetPassWord(id);
        return JsonResult.ok();
    }

    /**
     * 账号锁定
     *
     * @param userIsLockDto 用户账号有无被锁定【1：正常使用，2：账号被锁定】
     */
    @PreAuthorize("hasAnyAuthority('1','3','4','5','6')")
    @ApiOperation(value = "账号锁定")
    @PostMapping("/updateIsLock")
    public JsonResult updateIsLock(
            @ApiParam(name = "userIsLockDto", value = "用户账号有无被锁定", required = true)
            @RequestBody UserIsLockDTO userIsLockDto) {
        userInfoService.updateIsLock(userIsLockDto);
        return JsonResult.ok();
    }

    /**
     * 删除用户
     *
     * @param id 用户id
     */
    @PreAuthorize("hasAnyAuthority('1','3','4','5','6')")
    @ApiOperation(value = "删除用户")
    @GetMapping("/deleteUser")
    public JsonResult deleteUser(@ApiParam(name = "id", value = "用户id", required = true)
                                 @RequestParam Integer id, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        //获取登录用户信息
        /*String num = loginPrincipal.getNum();
        UserMessage message = userService.message(num);
        userInfoService.updateIsDelete(id, message);*/
        userInfoService.updateIsDelete(id, loginPrincipal);
        return JsonResult.ok();
    }

    /**
     * 批量删除
     *
     * @param info 用户id集合
     */
    @PreAuthorize("hasAnyAuthority('1','3','4','5','6')")
    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public JsonResult batchDelete(
            @ApiParam(name = "info", value = "用户id集合", required = true)
            @RequestBody BatchDeleteUserDTO info, @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        //获取登录用户信息
        /*String num = loginPrincipal.getNum();
        UserMessage message = userService.message(num);
        userInfoService.batchDelete(info, message);*/
        userInfoService.batchDelete(info, loginPrincipal);
        return JsonResult.ok();
    }

    /**
     *
     * 修改当前登录用户
     *
     * @param info           修改信息
     * @param loginPrincipal 登录用户信息
     * @return
     */
    @ApiOperation(value = "修改当前登录用户")
    @PostMapping("/updateLoginUser")
    public JsonResult updateLoginUser(@RequestBody UpdateLoginUserDTO info,
                                      @ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal) {
        //获取登录用户信息
        /*String num = loginPrincipal.getNum();
        UserMessage message = userService.message(num);
        userInfoService.updateLoginUser(info, message);*/
        userInfoService.updateLoginUser(info, loginPrincipal);
        if (StringUtils.isNotEmpty(info.getPassword())) {
            return JsonResult.fail(ServiceCode.JWT_EXPIRED,"用户已修改密码，请重新登录");
        }
        return JsonResult.ok();
    }

}
