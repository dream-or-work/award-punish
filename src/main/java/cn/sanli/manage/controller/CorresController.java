package cn.sanli.manage.controller;

import cn.sanli.manage.pojo.dto.System.CategoryDTO;
import cn.sanli.manage.pojo.dto.System.CorresDTO;
import cn.sanli.manage.pojo.dto.System.UpdateCategoryDTO;
import cn.sanli.manage.pojo.dto.System.UpdateCorresDTO;
import cn.sanli.manage.pojo.entity.Corres;
import cn.sanli.manage.pojo.vo.UserMessage;
import cn.sanli.manage.security.LoginPrincipal;
import cn.sanli.manage.service.CorresService;
import cn.sanli.manage.service.UserService;
import cn.sanli.manage.web.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 字段映射表(dbo.corres)表控制层
 *
 * @author xxxxx
 */
@Api(tags = "系统管理接口")
@RestController
@RequestMapping("system/menus")
public class CorresController {
    /**
     * 服务对象
     */
    @Resource
    private CorresService corresService;

    /**
     * 查询菜单
     * @param loginPrincipal 登录用户信息
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询菜单")
    @PostMapping("/findMenus")
    public JsonResult<List<Corres>> findMenus(@ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal){
        //获取登录用户信息
        /*String num = loginPrincipal.getNum();
        UserMessage message = userService.message(num);
        List<Corres> list = corresService.findNodes(message);*/
        List<Corres> list = corresService.findNodes(loginPrincipal);
        return JsonResult.ok(list);
    }

    /**
     * 查询考核分级
     * @param loginPrincipal
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询考核分级")
    @PostMapping("/findAssess")
    public JsonResult<List<Corres>> findAssess(@ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal){
        //获取登录用户信息
        /*String num = loginPrincipal.getNum();
        UserMessage message = userService.message(num);
        List<Corres> list = corresService.findAssess(message);*/
        List<Corres> list = corresService.findAssess(loginPrincipal);
        return JsonResult.ok(list);
    }

    /**
     * 查询奖励分级
     * @param loginPrincipal 登录用户信息
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询奖励分级")
    @PostMapping("/findReward")
    public JsonResult<List<Corres>> findReward(@ApiIgnore @AuthenticationPrincipal LoginPrincipal loginPrincipal){
        //获取登录用户信息
        /*String num = loginPrincipal.getNum();
        UserMessage message = userService.message(num);
        List<Corres> list = corresService.findReward(message);*/
        List<Corres> list = corresService.findReward(loginPrincipal);
        return JsonResult.ok(list);
    }

    /**
     * 查询处理类别
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询处理类别")
    @GetMapping("/findAssessCategory")
    public JsonResult<List<Corres>> findAssessCategory(){
        List<Corres> list = corresService.findAssessCategory();
        return JsonResult.ok(list);
    }

    /**
     * 查询过错级别
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询过错级别")
    @GetMapping("/findFaultLevel")
    public JsonResult<List<Corres>> findFaultLevel(){
        List<Corres> list = corresService.findFaultLevel();
        return JsonResult.ok(list);
    }

    /**
     * 查询处理形式
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询处理形式")
    @GetMapping("/findAssessWay")
    public JsonResult<List<Corres>> findAssessWay(){
        List<Corres> list = corresService.findAssessWay();
        return JsonResult.ok(list);
    }

    /**
     * 查询其他考核方式
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询其他考核方式")
    @GetMapping("/findOtherAssessWay")
    public JsonResult<List<Corres>> findOtherAssessWay(){
        List<Corres> list = corresService.findOtherAssessWay();
        return JsonResult.ok(list);
    }

    /**
     * 查询其他荣誉处理
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询其他荣誉处理")
    @GetMapping("/findOtherRewardWay")
    public JsonResult<List<Corres>> findOtherRewardWay(){
        List<Corres> list = corresService.findOtherRewardWay();
        return JsonResult.ok(list);
    }

    /**
     * 查询奖励形式
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询奖励形式")
    @GetMapping("/findRewardWay")
    public JsonResult<List<Corres>> findRewardWay(){
        List<Corres> list = corresService.findRewardWay();
        return JsonResult.ok(list);
    }
    /**
     * 查询处理方式
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @ApiOperation(value = "查询处理方式")
    @GetMapping("/findPunish")
    public JsonResult<List<Corres>> findPunish(){
        List<Corres> list = corresService.findPunish();
        return JsonResult.ok(list);
    }

    /**
     * 添加菜单
     * @param corresDTO
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "添加菜单")
    @PostMapping("/saveMenu")
    public JsonResult saveMenu(@Valid @RequestBody CorresDTO corresDTO){
        corresService.saveMenu(corresDTO);
        return JsonResult.ok();
    }

    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "添加处理方式")
    @PostMapping("/savePunish")
    public JsonResult savePunish(@Valid @RequestBody CategoryDTO categoryDTO){
        corresService.savePunish(categoryDTO);
        return JsonResult.ok();
    }
    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "添加奖励形式")
    @PostMapping("/saveRewardWay")
    public JsonResult saveRewardWay(@Valid @RequestBody CategoryDTO categoryDTO){
        corresService.saveRewardWay(categoryDTO);
        return JsonResult.ok();
    }
    /**
     * 修改菜单
     * @param corresDTO
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "修改菜单")
    @PostMapping("/updateMenu")
    public JsonResult updateMenu(@RequestBody UpdateCorresDTO corresDTO){
        corresService.updateMenu(corresDTO);
        return JsonResult.ok();
    }

    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "修改处理方式或奖励形式")
    @PostMapping("/updateCategory")
    public JsonResult updateCategory(@RequestBody UpdateCategoryDTO categoryDTO){
        corresService.updateCategory(categoryDTO);
        return JsonResult.ok();
    }
    /**
     * 删除菜单
     * @param id 删除菜单id
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "删除菜单")
    @GetMapping("/deleteMenu")
    public JsonResult deleteMenu(@RequestParam Integer id){
        corresService.deleteMenu(id);
        return JsonResult.ok();
    }
}
