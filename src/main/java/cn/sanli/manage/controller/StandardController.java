package cn.sanli.manage.controller;

import cn.sanli.manage.pojo.dto.User.SaveStandardDTO;
import cn.sanli.manage.pojo.dto.User.StandardEnableDTO;
import cn.sanli.manage.pojo.entity.Standard;
import cn.sanli.manage.service.StandardService;
import cn.sanli.manage.web.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 记分提醒标准表(dbo.[standard])表控制层
 *
 * @author xxxxx
 */
@Api(tags = "记分提醒标准接口")
@RestController
@RequestMapping("/system/standard")
public class StandardController {
    /**
     * 服务对象
     */
    @Resource
    private StandardService standardService;

    /**
     * 获取记分提醒标准列表
     * @return
     */
    @ApiOperation(value = "获取记分提醒标准列表")
    @PreAuthorize("hasAnyAuthority('1','2','3','4','5','6','7')")
    @GetMapping("/findByPage")
    public JsonResult<List<Standard>> findStandardList() {
        List<Standard> standardList = standardService.findStandardList();
        return JsonResult.ok(standardList);
    }

    /**
     * 获取记分提醒标准
     * @param id
     * @return 记分标准id
     */
    @ApiOperation(value = "获取记分提醒标准")
    @PreAuthorize("hasAnyAuthority('1','3','4','5','6')")
    @GetMapping("/findStandard")
    public JsonResult<Standard> findStandard(@ApiParam(name = "id", value = "记分标准id", required = true) @RequestParam Integer id){
        Standard standard = standardService.findStandard(id);
        return JsonResult.ok(standard);
    }
    /**
     * 添加记分提醒标准
     * @param info
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "添加记分提醒标准")
    @PostMapping("/saveStandard")
    public JsonResult saveStandard(@Valid @RequestBody SaveStandardDTO info){
        standardService.saveStandard(info);
        return JsonResult.ok();
    }

    /**
     * 修改记分提醒标准
     * @param standard
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "修改记分提醒标准")
    @PostMapping("/updateStandard")
    public JsonResult updateStandard(@RequestBody Standard standard){
        standardService.updateStandard(standard);
        return JsonResult.ok();
    }

    /**
     * 修改记分提醒标准是否启用
     * @param isEnable
     * @return
     */
    @PreAuthorize("hasAnyAuthority('1')")
    @ApiOperation(value = "修改记分提醒标准是否启用")
    @PostMapping("/updateIsEnable")
    public JsonResult updateIsEnable(@Valid @RequestBody StandardEnableDTO isEnable){
        standardService.updateIsEnable(isEnable);
        return JsonResult.ok();
    }
}
