package cn.sanli.manage.WeChat;

import cn.sanli.manage.aop.QywxForestInterceptor;

import cn.sanli.manage.pojo.Wechat.MessageTextDto;
import com.dtflys.forest.annotation.*;


import java.util.Map;

/**
 * 企业微信
 */
@BaseRequest(interceptor = QywxForestInterceptor.class, headers = {"Accept: */*", "Content-Type: application/json"})
public interface QywxForestClient {

    /**
     * 获取access_token
     *
     * @param corpId
     * @param corpSecret
     * @return
     */
    @Get(url = "/gettoken?corpid={corpId}&corpsecret={corpSecret}")
    //获取token
    Map<String, Object> getToken(@Var("corpId") String corpId, @Var("corpSecret") String corpSecret);

    /**
     * 获取访问用户敏感信息
     *
     * @param messageTextDto
     * @param accessToken
     * @return
     */
    @Post(url = "/message/send?access_token={accessToken}")
    // 发送消息，参数为消息文本对象和访问令牌
    Map<String, Object> messageSend(@Body MessageTextDto messageTextDto, @Var("accessToken") String accessToken);


}