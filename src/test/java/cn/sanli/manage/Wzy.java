package cn.sanli.manage;

import cn.sanli.manage.mapper.data1.*;
import cn.sanli.manage.pojo.dto.Fault.FaultAssessInfoDTO;
import cn.sanli.manage.pojo.dto.Fault.FaultInfoStatisticsDTO;
import cn.sanli.manage.pojo.dto.Fault.FaultInsertDTO;
import cn.sanli.manage.pojo.dto.Fault.PageQueryOfFaultRequest;
import cn.sanli.manage.pojo.entity.IdAndName;
import cn.sanli.manage.pojo.vo.Fault.FaultPagingQueryVO;
import cn.sanli.manage.pojo.vo.Fault.FaultStatisticsVO;
import cn.sanli.manage.service.FaultService;
import cn.sanli.manage.utils.DateUtils;
import cn.sanli.manage.utils.MapUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @Author wzy
 * @Date 2023/12/7 21:13
 * @Description: 王智永的测试
 * @Version 1.0
 */
@SpringBootTest
@Slf4j
public class Wzy {

    @Resource
    private RewardsMapper rewardsMapper;

    @Resource
    private FaultMapper faultMapper;

    @Resource
    private FaultService faultService;

    @Resource
    private FaultAssessMapper faultAssessMapper;

    @Resource
    private CorresMapper corresMapper;

    @Resource
    private OrganizationMapper organizationMapper;


//    @Test
//    void testQueryPage() {
//        PageQueryOfFaultRequest request = new PageQueryOfFaultRequest();
//        List<FaultPagingQueryVO> faultPagingQueryVOS = faultService.pagingQuery(request);
//        log.debug(faultPagingQueryVOS.toString());
//    }

//    @Test
//    void testInsert() {
//        FaultInsertDTO faultInsertDTO = new FaultInsertDTO();
//        FaultAssessInfoDTO faultAssessInfoDTO = new FaultAssessInfoDTO();
//        faultAssessInfoDTO.setDisposalDetails(new BigDecimal("0.3"));
//        List<FaultAssessInfoDTO> list = new ArrayList<>();
//        list.add(faultAssessInfoDTO);
//        faultInsertDTO.setFaultAssessInfos(list);
//
//        int row = faultAssessMapper.insert(faultInsertDTO);
//    }


//    @Test
//    void isBlank() {
//        String str1 = null;
//        String str2 = "第二";
//        String str3 = "第三";
//        String str4 = null;
//
//        if(StringUtils.isAnyBlank(str2, str3)) {
//            log.debug("无null");
//        }
//
//    }

    @Test
    void selectMaxRId() {
        String maxRId = rewardsMapper.getMaxRId();
        log.debug("最新的rid: {}", maxRId);
    }

    @Test
    void date() {
        Date date = new Date();
        log.debug("今天时间:{}", date);
        Date threeDaysAgo = new Date(date.getTime() - (1000 * 60 * 60 * 24 * 3));
        log.debug("三天前日期: {}", threeDaysAgo);
    }

//    @Test
//    void testInsertOrUpdate() {
//        FaultAndFaultAssessAddAddRequest faultAndFaultAssessRequest = new FaultAndFaultAssessAddAddRequest();
//        faultAndFaultAssessRequest.setCreatName("测试新增或修改数据功能");
//        faultAndFaultAssessRequest.setFaultId(55);
////        faultAndFaultAssessRequest.setFaultNum("2335235");
//        faultAndFaultAssessRequest.setAssessNum("llll");
//        int row = faultAssessMapper.update(faultAndFaultAssessRequest);
//        log.debug("操作是否成功: {}", row);
//        log.debug("操作的数据的id :{}", faultAndFaultAssessRequest.getFaultId());
//    }


//    @Test
//    void testQueryByPage() {
//        FaultInfoStatisticsRequest faultInfoStatisticsRequest = new FaultInfoStatisticsRequest();
//        faultInfoStatisticsRequest.setResCenter(23);
//        List<FaultStatisticsOfPersonageVO> faultStatisticsInfos = faultMapper.statisticsFault(faultInfoStatisticsRequest);
//        log.info("获取到的数据:{}", faultStatisticsInfos);
//
//    }

    @Test
    void testStatistics() {
        Calendar calendar = Calendar.getInstance();
        Map<String, Date[]> statisticsTime = DateUtils.getStatisticsTime();
        log.debug("年份开始:{}", statisticsTime.get(DateUtils.THISYEAR)[0]);
        log.debug("年份结束:{}", statisticsTime.get(DateUtils.THISYEAR)[1]);
        log.debug("半年分开始: {}", statisticsTime.get(DateUtils.THISHALF)[0]);
        log.debug("半年分结束: {}", statisticsTime.get(DateUtils.THISHALF)[1]);
        log.debug("季度开始: {}", statisticsTime.get(DateUtils.THISQUARTER)[0]);
        log.debug("季度结束: {}", statisticsTime.get(DateUtils.THISQUARTER)[1]);
        log.debug("月份开始: {}", statisticsTime.get(DateUtils.THISMONTH)[0]);
        log.debug("月份结束: {}", statisticsTime.get(DateUtils.THISMONTH)[1]);
        log.debug("周开始: {}", statisticsTime.get(DateUtils.THISWEEK)[0]);
        log.debug("周结束: {}", statisticsTime.get(DateUtils.THISWEEK)[1]);

    }

    @Test
    void testBreak() {
//        for (int i = 1;i<=5; i++){
//            log.debug("i: {}", i);
//            for(int j = 1;j<=5; j++) {
//                if(j == 2) {
//                    continue;
//                }else {
//                    log.debug("j: {}", j);
//                }
//                log.debug("本次的j: {}", j);
//
//            }
//        }
//        LoginPrincipal loginPrincipal = new LoginPrincipal();
//        loginPrincipal.setNum(String.valueOf(16766));
//        faultService.statisticsFaultOfPersonage(loginPrincipal);

        List<IdAndName> idAndNames = organizationMapper.queryIdAndName();
        log.debug("数据: {}", idAndNames);


    }

    @Test
    void teatDeleteBatch() {
//        String[] faultNums = new String[]{"G123238", "G123239"};
//        log.debug("asdfsdf {}", faultNums);
//        int i = faultMapper.deleteBatch(faultNums);
//        log.debug("受影响行数:{}", i);
        FaultInfoStatisticsDTO faultInfoStatisticsDTO = new FaultInfoStatisticsDTO();
        Map<String, Date[]> statisticsTime = DateUtils.getStatisticsTime();
        faultInfoStatisticsDTO.setStartTime(statisticsTime.get(DateUtils.THISYEAR)[0]);
        faultInfoStatisticsDTO.setEndTime(statisticsTime.get(DateUtils.THISYEAR)[1]);
        List<FaultStatisticsVO> list = faultMapper.statisticsFaultOfPersonage(faultInfoStatisticsDTO);
        log.debug("获取到的数据: {}", list);

    }





}
