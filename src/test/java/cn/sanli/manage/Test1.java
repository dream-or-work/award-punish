package cn.sanli.manage;

import cn.sanli.manage.mapper.data1.UserMapper;
import cn.sanli.manage.pojo.dto.Login.LoginDTO;
import cn.sanli.manage.pojo.vo.LoginUserVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.Resource;
import java.util.List;
//测试5
@SpringBootTest
public class Test1 {

    @Resource
    public UserMapper userMapper;

    @Test
    void test1() {
        LoginUserVO loginUserVO = userMapper.selectRole("0000");
        System.out.println(loginUserVO);
    }

    @Test
    void test2() {
        LoginDTO admin = userMapper.selectUserMapper("1111");
        if (admin==null){
            System.out.println("无账号");
        }
        System.out.println(admin);
    }

    @Test
    void test3() {
        List<Integer> roles = userMapper.selectRoles("0000");
        for (Integer role:roles){
            System.out.println(role);
        }
    }

    @Test
    void test4() {
        /*String pass = "admin";
        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
        String hashPass = bcryptPasswordEncoder.encode(pass);
        System.out.println(hashPass);
        //$2a$10$8piqkSNuCU1aXl9AaDgPd.n8fcqrtA/2mXAe3bOaUmjyok7YExnQC
        boolean f = bcryptPasswordEncoder.matches("admin",hashPass);
        System.out.println(f);*/

        BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("123456");
        System.out.println(encode);
        //$2a$10$UAstUpboWzTn2ozjJpxCM.DGWe.kAEv8r4AL1tY.AlNFOl9nMzoIi

    }

    @Test
    void test5() {
    }
}
