package cn.sanli.manage;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import cn.sanli.manage.service.WechatService;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @program: award-punish
 * @description: 微信推送测试
 * @author: lsk
 * @create: 2023-12-05 15:04
 * @since jdk1.8
 **/
@SpringBootTest
public class WechatTest {

    @Resource
    private WechatService wechatService;

//    @Resource
//    private WechatServiceImpl wechatService1;

    @Test
    void test1() {
//        wechatService1.getAccessToken();
        wechatService.sendMessage("ABenBen", "批量推送测试：尊敬的***（工号：****），您好，您本周因*****扣除荣誉积分**分，请前往积分管理软件进行查看或申诉！");
    }



}

