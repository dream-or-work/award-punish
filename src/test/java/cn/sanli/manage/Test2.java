package cn.sanli.manage;

import cn.sanli.manage.mapper.data1.OrganizationMapper;
import cn.sanli.manage.pojo.vo.DeptNameVO;
import cn.sanli.manage.pojo.vo.RegionNameVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class Test2 {

    @Autowired
    public OrganizationMapper organizationMapper;

    @Test
    void test1() {
        List<RegionNameVO> regionNameVOS = organizationMapper.regionName();
        System.out.println(regionNameVOS.toString());
    }

    @Test
    void test2() {
        List<DeptNameVO> deptNameVOS = organizationMapper.deptName(16);
        System.out.println(deptNameVOS.toString());
    }
}
